<?php namespace App\Models;

use CodeIgniter\Model;


class SkdetailModel extends Model
{

    protected $table = 't_sk_detail';
    protected $primaryKey = 'id_sk_detail'; 

    protected $useSoftDeletes = true;
    protected $useTimestamps  = true;
    protected $deletedField   = 'deleted_at';
    protected $createdField   = 'created_at';
    protected $updatedField   = 'updated_at';
    
    protected $allowedFields = ['id_sk', 'nopeg', 'nim', 'nama', 'nik', 'nama', 'status'];

    public function getId(Array $conditions = [])
    {
        $res = $this->select('id_sk_detail')->where($conditions)->first();

        return (count($res) > 0) ? $res['id_sk_detail'] : '';
    }


    

}