<?php namespace App\Models;

use CodeIgniter\Model;

/**
 *
 */
class PegawaikreditModel extends Model
{

    protected $table = 't_pegawai_kredit';
    protected $primaryKey = 'nip'; 

    protected $useSoftDeletes = true;
    protected $useTimestamps  = true;
    protected $deletedField   = 'deleted_at';
    protected $createdField   = 'created_at';
    protected $updatedField   = 'updated_at';

    protected $allowedFields = ['nip', 'jabatan_usulan', 'ak_syarat', 'ak_perolehan', 'ak_pendidikan', 'ak_penelitian', 'ak_pengabdian', 'ak_pengembangan', 'syarat_ak', 'syarat_keterangan', 'status', 'ak_pendidikan_persyaratan', 'ak_penelitian_persyaratan', 'ak_pengabdian_persyaratan', 'ak_pengembangan_persyaratan', 'syarat_khusus', 'tgl_posisi_usulan'];

    public function getLastUpdate (array $conditions = []) 
    {
        return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal')
                   ->where($conditions)
                   ->orderBy('updated_at', 'DESC')
                   ->first();
    }

    public function getPegawaiByNip($nip){

        return $this->where(['nip' => $nip])->first();
  
    }

}
