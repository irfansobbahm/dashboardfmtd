<?php namespace App\Models;

use CodeIgniter\Model;


class PengadaanModel extends Model
{

  protected $table = 't_pengadaan';
  protected $primaryKey = 'id_pengadaan'; 

  protected $useSoftDeletes = true;
  protected $useTimestamps  = true;
  protected $deletedField   = 'deleted_at';
  protected $createdField   = 'created_at';
  protected $updatedField   = 'updated_at';
  
  protected $allowedFields = ['nama_kegiatan', 'id_const', 'deskripsi', 'tgl_ri', 'rencana_tgl_mulai', 'rencana_tgl_selesai', 'nominal_rab', 'diadakan', 'status', 'status_pembayaran', 'nopeg_pemohon', 'thn_anggaran', 'status_aktual', 'tgl_pembayaran', 'sispran', 'sumber_dana'];


  public function getAll (array $conditions = []) {

   return $this->select('t_pengadaan.*, a.nama alokasi')
               ->join('t_const a', 'a.id_const = t_pengadaan.id_const')
               ->join('t_const b', 'b.id_const = a.parent_id')
               ->where($conditions)
               // ->getCompiledSelect();
               ->findAll();
  
   // return $this->findAll();
  }


  public function getSummaryStatus (array $conditions = []) {

    // return $this->select("
    //                 sum(case when status = 0 then 1 else 0 end) as baru,
    //                 sum(case when status = 1 then 1 else 0 end) as diproses,
    //                 sum(case when status = 2 then 1 else 0 end) as penawaran,
    //                 sum(case when status = 3 then 1 else 0 end) as spk,
    //                 sum(case when status = 4 then 1 else 0 end) as serah_terima,
    //                 sum(case when status = 5 then 1 else 0 end) as invoice,
    //                 sum(case when status = 6 then 1 else 0 end) as selesai,
    //                 ")
    //              ->where($conditions)
    //              ->first();


    return $this->select("
                    sum(case when status = 0 then 1 else 0 end) as pemaketan,
                    sum(case when status = 1 then 1 else 0 end) as kontrak,                    
                    sum(case when status = 2 then 1 else 0 end) as invoice,
                    sum(case when status = 3 then 1 else 0 end) as selesai,
                    ")
                 ->where($conditions)
                 ->first();
  
   // return $this->findAll();
  }

  public function getSummaryDeadline (array $conditions = []) {

    return $this->select("
                    sum(case when datediff(rencana_tgl_selesai, current_date()) < 0 and status <> 3 then 1 else 0 end) as lewat,
                    sum(case when (datediff(rencana_tgl_selesai, current_date()) > 0 and datediff(rencana_tgl_selesai, current_date()) <=2) and status <> 3 then 1 else 0 end) as dua,
                    sum(case when (datediff(rencana_tgl_selesai, current_date()) > 2 and datediff(rencana_tgl_selesai, current_date()) <=7) and status <> 3 then 1 else 0 end) as dua_tujuh,
                    sum(case when datediff(rencana_tgl_selesai, current_date()) > 7 and status <> 3 then 1 else 0 end) as tujuh
                    ")
                 ->where($conditions)
                 ->first();
                 // ->getCompiledSelect();
  }

  public function getSummaryStatusAktual (array $conditions = []) {

    return $this->select("
                    sum(case when status_aktual < 50 then 1 else 0 end) as lima,
                    sum(case when (status_aktual >= 50 and status_aktual <= 75) then 1 else 0 end) as lima_tujuhlima,
                    sum(case when (status_aktual > 75 and status_aktual < 100) then 1 else 0 end) as tujuhlima,
                    sum(case when status_aktual = 100 then 1 else 0 end) as selesai
                    ")
                 ->where($conditions)
                 ->first();
  }

    public function getPengadaanBulanan (array $conditions = []) 
    {

        return $this->select("
                    sum(case when month(rencana_tgl_mulai) = 1 then nominal_rab else 0 end) as jan,
                    sum(case when month(rencana_tgl_mulai) = 2 then nominal_rab else 0 end) as feb,
                    sum(case when month(rencana_tgl_mulai) = 3 then nominal_rab else 0 end) as mar,
                    sum(case when month(rencana_tgl_mulai) = 4 then nominal_rab else 0 end) as apr,
                    sum(case when month(rencana_tgl_mulai) = 5 then nominal_rab else 0 end) as mei,
                    sum(case when month(rencana_tgl_mulai) = 6 then nominal_rab else 0 end) as jun,
                    sum(case when month(rencana_tgl_mulai) = 7 then nominal_rab else 0 end) as jul,
                    sum(case when month(rencana_tgl_mulai) = 8 then nominal_rab else 0 end) as agu,
                    sum(case when month(rencana_tgl_mulai) = 9 then nominal_rab else 0 end) as sep,
                    sum(case when month(rencana_tgl_mulai) = 10 then nominal_rab else 0 end) as okt,
                    sum(case when month(rencana_tgl_mulai) = 11 then nominal_rab else 0 end) as nov,
                    sum(case when month(rencana_tgl_mulai) = 12 then nominal_rab else 0 end) as des
                    ")
                 ->where($conditions)
                 ->first();
                 // ->getCompiledSelect();
    }

    public function getLastUpdate (array $conditions = []) 
    {
        return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal')
                   ->where($conditions)
                   ->orderBy('updated_at', 'DESC')
                   ->first();
    }
}