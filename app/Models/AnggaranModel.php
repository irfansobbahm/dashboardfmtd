<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;

class AnggaranModel extends Model {


	// protected $db;
	protected $table = 't_anggaran';
	protected $primaryKey = 'id_anggaran'; 

	protected $useSoftDeletes = true;
	protected $useTimestamps  = true;
	protected $deletedField   = 'deleted_at';
	protected $createdField   = 'created_at';
	protected $updatedField   = 'updated_at';

	protected $allowedFields = ['id_const', 'nominal', 'tgl_pengajuan', 'keterangan', 'thn_anggaran', 'bulan', 'triwulan', 'is_logistik', 'kode_file'];


	// public function __construct(ConnectionInterface &$db){
		
	// 	$this->db = &$db;

	// 	$this->db->protectIdentifiers('t_anggaran');

	// }

	public function getAll (array $conditions = []) {

       	return $this->select('t_anggaran.*, 
       				 concat(t_anggaran.keterangan, " ", ifnull(t_anggaran.kode_file, "")) as keterangan,
					 a.nama     as nama,
					 b.id_const as parent_id_const,
					 b.nama     as parent_nama, 
					 c.id_const as kakek_id_const,
					 c.nama     as kakek_nama, 
					 d.id_const as moyang_id_const,
					 d.nama     as moyang_nama')
                   ->join('t_const a', 'a.id_const = t_anggaran.id_const')
                   ->join('t_const b', 'b.id_const = a.parent_id')
                   ->join('t_const c', 'c.id_const = b.parent_id')
                   ->join('t_const d', 'd.id_const = c.parent_id')
                   ->where($conditions)
                   ->findAll();
      
       // return $this->findAll();
    }

    public function getLastUpdate (array $conditions = []) {

       	return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal')
                   ->where($conditions)
                   ->orderBy('updated_at', 'DESC')
                   ->first();
      
       // return $this->findAll();
    }


    public function getSumRkaAll (array $conditions = []) {

       	return $this->select("
       						sum(case when substr(id_const, 1, 4) = '0101' then nominal else 0 end) as rka,
							sum(case when substr(id_const, 1, 4) = '0105' then nominal else 0 end) as pengalihan_masuk,
							sum(case when substr(id_const, 1, 4) = '0106' then nominal else 0 end) as pengalihan_keluar,
							sum(case when substr(id_const, 1, 4) = '0205' then nominal else 0 end) as realokasi,
							sum(case when substr(id_const, 1, 4) = '0206' then nominal else 0 end) as prospektif,
							sum(case when substr(id_const, 1, 6) = '020701' then nominal else 0 end) as kerma_alihan,
							sum(case when substr(id_const, 1, 6) = '020702' then nominal else 0 end) as kerma_non_alihan,
							sum(case when substr(id_const, 3, 2) = '02' then nominal else 0 end) as ri,
							sum(case when substr(id_const, 3, 2) = '03' then nominal else 0 end) as fra,
							sum(case when substr(id_const, 3, 2) = '04' then nominal else 0 end) as realisasi
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getSumRkaSispran1 (array $conditions = []) {
    	$conditions = array_merge($conditions, ['substr(id_const, 1, 2)' => '01']);

       	return $this->select("
       						sum(case when substr(id_const, 3, 2) = '01' then nominal else 0 end) as rka,
							sum(case when substr(id_const, 3, 2) = '05' then nominal else 0 end) as pengalihan_masuk,
							sum(case when substr(id_const, 3, 2) = '06' then nominal else 0 end) as pengalihan_keluar,
							sum(case when substr(id_const, 3, 2) = '02' then nominal else 0 end) as ri,
							sum(case when substr(id_const, 3, 2) = '03' then nominal else 0 end) as fra,
							sum(case when substr(id_const, 3, 2) = '04' then nominal else 0 end) as realisasi
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getSumRkaSispran2 (array $conditions = []) {
    	$conditions = array_merge($conditions, ['substr(id_const, 1, 2)' => '02']);

       	return $this->select("
       						sum(case when substr(id_const, 3, 2) = '01' then nominal else 0 end) as rka,
							sum(case when substr(id_const, 3, 2) = '05' then nominal else 0 end) as realokasi,
							sum(case when substr(id_const, 3, 2) = '06' then nominal else 0 end) as prospektif,
							sum(case when substr(id_const, 3, 4) = '0701' then nominal else 0 end) as kerma_alihan,
							sum(case when substr(id_const, 3, 4) = '0702' then nominal else 0 end) as kerma_non_alihan,
							sum(case when substr(id_const, 3, 2) = '02' then nominal else 0 end) as ri,
							sum(case when substr(id_const, 3, 2) = '03' then nominal else 0 end) as fra,
							sum(case when substr(id_const, 3, 2) = '04' then nominal else 0 end) as realisasi
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getPenyerapanKumulatif (array $conditions = []) {

       	return $this->select("
       						sum(case when month(now()) >= 1 and bulan <= 1 then nominal else 0 end) as jan,
							sum(case when month(now()) >= 2 and bulan <= 2 then nominal else 0 end) as feb,
							sum(case when month(now()) >= 3 and bulan <= 3 then nominal else 0 end) as mar,
							sum(case when month(now()) >= 4 and bulan <= 4 then nominal else 0 end) as apr,
							sum(case when month(now()) >= 5 and bulan <= 5 then nominal else 0 end) as mei,
							sum(case when month(now()) >= 6 and bulan <= 6 then nominal else 0 end) as jun,
							sum(case when month(now()) >= 7 and bulan <= 7 then nominal else 0 end) as jul,
							sum(case when month(now()) >= 8 and bulan <= 8 then nominal else 0 end) as agu,
							sum(case when month(now()) >= 9 and bulan <= 9 then nominal else 0 end) as sep,
							sum(case when month(now()) >= 10 and bulan <= 10 then nominal else 0 end) as okt,
							sum(case when month(now()) >= 11 and bulan <= 11 then nominal else 0 end) as nov,
							sum(case when month(now()) >= 12 and bulan <= 12 then nominal else 0 end) as des
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getPenyerapanPerbulan (array $conditions = []) {

       	return $this->select("
       						sum(case when bulan = 1 then nominal else 0 end) as jan,
							sum(case when bulan = 2 then nominal else 0 end) as feb,
							sum(case when bulan = 3 then nominal else 0 end) as mar,
							sum(case when bulan = 4 then nominal else 0 end) as apr,
							sum(case when bulan = 5 then nominal else 0 end) as mei,
							sum(case when bulan = 6 then nominal else 0 end) as jun,
							sum(case when bulan = 7 then nominal else 0 end) as jul,
							sum(case when bulan = 8 then nominal else 0 end) as agu,
							sum(case when bulan = 9 then nominal else 0 end) as sep,
							sum(case when bulan = 10 then nominal else 0 end) as okt,
							sum(case when bulan = 11 then nominal else 0 end) as nov,
							sum(case when bulan = 12 then nominal else 0 end) as des
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getSumAlokasi (array $conditions = []) {


       	return $this->select("
       						sum(case when substr(id_const, 3, 2) = '01' then nominal else 0 end) as rka,
       						sum(case when substr(id_const, 3, 2) = '02' then nominal else 0 end) as ri,
       						sum(case when substr(id_const, 3, 2) = '03' then nominal else 0 end) as fra,
       						sum(case when substr(id_const, 3, 2) = '04' then nominal else 0 end) as realisasi,
							sum(case when substr(id_const, 3, 4) = '0701' then nominal else 0 end) as kerma_alihan,
							sum(case when substr(id_const, 3, 4) = '0702' then nominal else 0 end) as kerma_non_alihan,
							sum(case when substr(id_const, 3, 2) = '05' then nominal else 0 end) as realokasi
       						")
                   ->where('deleted_at', NULL)
                   ->where($conditions)
                   ->first();

		
       // return $this->findAll();

    }

	public function getSumAlokasi22 (array $conditions = []) {


		return $this->select("
				
						 sum(case when substr(id_const, 3, 4) = '0701' then nominal else 0 end) as kerma_alihan,
						 sum(case when substr(id_const, 3, 4) = '0702' then nominal else 0 end) as kerma_non_alihan,
						 sum(case when substr(id_const, 3, 2) = '05' then nominal else 0 end) as realokasi
							")
				->where('deleted_at', NULL)
				->where($conditions)
				->first();

	 
	// return $this->findAll();

 }

	

	function all(array $conditions = [])	{

		return $this->db->table('t_anggaran')->get()->getResult();		
	
	}


	function allAnggaran($anggaran = ''){

		if ($anggaran) {
			$sispran  = substr($anggaran, 0 , 2);		

			#get all anggaran and their parent, grandfather, great grandfather 	
			$sql = " SELECT a.*, 
					 c.nama     as nama,
					 d.id_const as parent_id_const,
					 d.nama     as parent_nama, 
					 e.id_const as kakek_id_const,
					 e.nama     as kakek_nama, 
					 f.id_const as moyang_id_const,
					 f.nama     as moyang_nama 
					 FROM t_anggaran a 
					 JOIN t_const c ON c.id_const = a.id_const
					 JOIN t_const d ON d.id_const = c.parent_id
					 JOIN t_const e ON e.id_const = d.parent_id 
					 JOIN t_const f ON f.id_const = e.parent_id
					 WHERE e.id_const = $anggaran
					 AND f.id_const = $sispran
					 ORDER BY a.id_anggaran DESC";	
		} else {
			$sql = " SELECT a.*, 
					 c.nama     as nama,
					 d.id_const as parent_id_const,
					 d.nama     as parent_nama, 
					 e.id_const as kakek_id_const,
					 e.nama     as kakek_nama, 
					 f.id_const as moyang_id_const,
					 f.nama     as moyang_nama 
					 FROM t_anggaran a 
					 JOIN t_const c ON c.id_const = a.id_const
					 JOIN t_const d ON d.id_const = c.parent_id
					 JOIN t_const e ON e.id_const = d.parent_id 
					 JOIN t_const f ON f.id_const = e.parent_id
					 WHERE c.level = 4
					 ORDER BY a.id_anggaran DESC";	
		}
		
				  
	
		return  $this->db->query($sql)	   
					 ->getResult();

	}


	function allRkaSispran2($realokasi, $prospektif, $penerimaan){

		$sispran  = substr($realokasi, 0 , 2);		

		#get all anggaran and their parent, grandfather, great grandfather 	
		$sql = " SELECT a.*, 
				 c.nama     as nama,
				 d.id_const as parent_id_const,
				 d.nama     as parent_nama, 
				 e.id_const as kakek_id_const,
				 e.nama     as kakek_nama				  
				 FROM t_anggaran a 
				 JOIN t_const c ON c.id_const = a.id_const
				 JOIN t_const d ON d.id_const = c.parent_id
				 JOIN t_const e ON e.id_const = d.parent_id 
				 WHERE 
				 (d.id_const = $realokasi OR d.id_const = $prospektif OR d.id_const = $penerimaan)				
				 AND
				 e.id_const = $sispran
				 ORDER BY a.id_anggaran DESC";
				  
	
		return  $this->db->query($sql)	   
					 ->getResult();

	}



	function create($data){

		$builder = $this->db->table('t_anggaran');
		$builder->insert($data);
	
	}


	// function save($data){

	// 	$builder = $this->db->table('t_anggaran');
	// 	$builder->insert($data);
	
	// }


	// function delete($id){

	// 	$builder = $this->db->table('t_anggaran');
	// 	$builder->where('id_anggaran', $id);
	// 	$builder->delete();

	// }


	// function update($data){

	// 	$builder = $this->db->table('t_anggaran');		
	// 	$builder->where('id_anggaran', $data['id_anggaran']);
	// 	$builder->update($data);

	// }


	// function find($id){

	// 	return $this->db->table('t_anggaran')
	// 	                ->where('id_anggaran' , $id)
	// 	                ->get()
	// 	                ->getRow();
	// }


	// function findArray($id){

	// 	return $this->db->table('t_anggaran')
	// 	                ->where('id_anggaran' , $id)
	// 	                ->get()
	// 	                ->getRowArray();
	
	// }


	function checkRka($id, $thn) {

		$query = $this->db->table('t_anggaran')
		                ->where('id_const' , $id)
		                ->where('thn_anggaran' , $thn)
		                ->get()
		                ->getResult();

		return $query ? true : false;

	}



	/*function allJenisAnggaran($const){

		#get ri, rka, fra, realisasi by constanta (4 digits left of constanta)
		return $this->db->table('t_anggaran')
					->join('t_const', 't_const.id_const = t_anggaran.id_const')
				    ->where('LEFT(t_anggaran.id_const,4) =' , $const)	
				    ->orderBy("t_anggaran.tgl_pengajuan", "ASC")				
					->get()
		            ->getResult();
	}*/


}


