<?php namespace App\Models;

use CodeIgniter\Model;


class SkdibayarModel extends Model
{

    protected $table = 't_sk_dibayar';
    protected $primaryKey = 'id_sk_dibayar'; 

    protected $useSoftDeletes = true;
    protected $useTimestamps  = true;
    protected $deletedField   = 'deleted_at';
    protected $createdField   = 'created_at';
    protected $updatedField   = 'updated_at';
    
    protected $allowedFields = ['id_sk', 'id_jenis_honor', 'status', 'tgl_pengajuan', 'tgl_proses', 'tgl_pembayaran', 'total_nominatif', 'thn_anggaran', 'periode', 'keterangan', 'no_spp'];

    public function getId(Array $conditions = [])
    {
        $res = $this->select('id_sk_dibayar')->where($conditions)->first();

        return (count($res) > 0) ? $res['id_sk_dibayar'] : '';
    }

    public function getAll(array $conditions = [])
    {
        if (empty($conditions)) {
            // return $this->select('t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, t_peg_itb.nama, concat(t_const2.nama, " ", t_const.nama) as jenis')
            return $this->select('t_sk_dibayar.*, concat(t_const2.nama, " ", t_const.nama) as jenis')
                    // ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    // ->join('t_peg_itb', 't_peg_itb.nopeg = t_sk.dekan')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->findAll();
                    // ->getCompiledSelect();
        }

        // return $this->select('t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, t_peg_itb.nama, concat(t_const2.nama, " ", t_const.nama) as jenis')
        return $this->select('t_sk_dibayar.*, concat(t_const2.nama, " ", t_const.nama) as jenis')
                    // ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    // ->join('t_peg_itb', 't_peg_itb.nopeg = t_sk.dekan', 'left')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor', 'left')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->where($conditions)
                    // ->getCompiledSelect();
                    ->findAll();
    }

    public function getSummaryStatus(Array $conditions = [])
    {
        return $this->select("
                            sum(case when date_format(tgl_pengajuan, '%m') = '01' then 1	else 0 end) as jan,
                            sum(case when date_format(tgl_pengajuan, '%m') = '02' then 1	else 0 end) as feb,
                            sum(case when date_format(tgl_pengajuan, '%m') = '03' then 1	else 0 end) as mar,
                            sum(case when date_format(tgl_pengajuan, '%m') = '04' then 1	else 0 end) as apr,
                            sum(case when date_format(tgl_pengajuan, '%m') = '05' then 1	else 0 end) as mei,
                            sum(case when date_format(tgl_pengajuan, '%m') = '06' then 1	else 0 end) as jun,
                            sum(case when date_format(tgl_pengajuan, '%m') = '07' then 1	else 0 end) as jul,
                            sum(case when date_format(tgl_pengajuan, '%m') = '08' then 1	else 0 end) as agu,
                            sum(case when date_format(tgl_pengajuan, '%m') = '09' then 1	else 0 end) as sep,
                            sum(case when date_format(tgl_pengajuan, '%m') = '10' then 1	else 0 end) as okt,
                            sum(case when date_format(tgl_pengajuan, '%m') = '11' then 1	else 0 end) as nov,
                            sum(case when date_format(tgl_pengajuan, '%m') = '12' then 1	else 0 end) as des
                            ") 
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    ->andWhere($conditions)
                    ->first();
    }

    public function getSummaryJenis(Array $conditions = [])
    {
        return $this->select("
                            sum(case when date_format(tgl_pembayaran, '%m') = '01' then total_nominatif else 0 end) as jan,
                            sum(case when date_format(tgl_pembayaran, '%m') = '02' then total_nominatif else 0 end) as feb,
                            sum(case when date_format(tgl_pembayaran, '%m') = '03' then total_nominatif else 0 end) as mar,
                            sum(case when date_format(tgl_pembayaran, '%m') = '04' then total_nominatif else 0 end) as apr,
                            sum(case when date_format(tgl_pembayaran, '%m') = '05' then total_nominatif else 0 end) as mei,
                            sum(case when date_format(tgl_pembayaran, '%m') = '06' then total_nominatif else 0 end) as jun,
                            sum(case when date_format(tgl_pembayaran, '%m') = '07' then total_nominatif else 0 end) as jul,
                            sum(case when date_format(tgl_pembayaran, '%m') = '08' then total_nominatif else 0 end) as agu,
                            sum(case when date_format(tgl_pembayaran, '%m') = '09' then total_nominatif else 0 end) as sep,
                            sum(case when date_format(tgl_pembayaran, '%m') = '10' then total_nominatif else 0 end) as okt,
                            sum(case when date_format(tgl_pembayaran, '%m') = '11' then total_nominatif else 0 end) as nov,
                            sum(case when date_format(tgl_pembayaran, '%m') = '12' then total_nominatif else 0 end) as des
                            ") 
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk', 'left')
                    ->where($conditions)
                    ->first();
    }

    public function getLastUpdate (array $conditions = []) 
    {
        return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal, updated_at')
                   ->where($conditions)
                   ->orderBy('updated_at', 'DESC')
                   ->first();
    }

}