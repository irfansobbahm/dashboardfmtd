<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;

class AnggaranunitModel extends Model {


	// protected $db;
	protected $table = 't_anggaran_unit';
	protected $primaryKey = 'id_anggaran'; 

	protected $useSoftDeletes = true;
	protected $useTimestamps  = true;
	protected $deletedField   = 'deleted_at';
	protected $createdField   = 'created_at';
	protected $updatedField   = 'updated_at';

	protected $allowedFields = ['thn_anggaran', 'triwulan', 'bulan', 'jenis', 'coa_kegiatan', 'coa_prodi_kk', 'coa_bidang', 'jenis_belanja', 'sispran', 'nominal', 'logistik', 'kode_file', 'ado', 'program', 'pelaksana', 'keterangan', 'kontrak', 'mitra', 'judul', 'ppn', 'pph23', 'biaya_lain', 'dana_diterima', 'ops_ftmd', 'dpi_itb', 'dpi_bpp'];

	public function getAll (array $conditions = []) {

       	return $this->select('t_anggaran_unit.sispran, t_anggaran_unit.jenis, t_anggaran_unit.ado, t_anggaran_unit.jenis_belanja,
       							t_anggaran_unit.nominal, t_anggaran_unit.thn_anggaran,    
       							ifnull(bulan, "-") as bulan, 
       							ifnull(t_coa_bidang.nama_akun, "-") as keterangan')
                   ->join('t_coa_kegiatan', 't_coa_kegiatan.kode_akun = t_anggaran_unit.coa_kegiatan', 'left')
                   ->join('t_coa_prodi_kk', 't_coa_prodi_kk.kode_akun = t_anggaran_unit.coa_prodi_kk', 'left')
                   ->join('t_coa_bidang', 't_coa_bidang.kode_akun = t_anggaran_unit.coa_bidang', 'left')
                   ->where($conditions)
                   ->findAll();
      
       // return $this->findAll();
    }

    public function getLastUpdate (array $conditions = []) {

       	return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal')
                   ->where($conditions)
                   ->orderBy('updated_at', 'DESC')
                   ->first();
      
       // return $this->findAll();
    }


    public function getSumRkaAll (array $conditions = []) {
		// print_r($conditions);die;
       	return $this->select("
       						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
							sum(case when jenis = 'RKA ALIH' AND program = 'ALIH_MASUK' then nominal else 0 end) as pengalihan_masuk,
							sum(case when jenis = 'RKA ALIH' AND program = 'ALIH_KELUAR' then nominal else 0 end) as pengalihan_keluar,
							sum(case when jenis = 'RKA KERMA' AND program = 'REALOKASI' then nominal else 0 end) as realokasi,
							sum(case when jenis = 'PROSPEKTIF' then kontrak else 0 end) as prospektif_capaian,
							sum(case when jenis = 'PROSPEKTIF_TARGET' then kontrak else 0 end) as prospektif_target,
							sum(case when jenis = 'KERMA_ALIHAN' then nominal else 0 end) as kerma_alihan,
							sum(case when jenis = 'KERMA_NON_ALIHAN' then nominal else 0 end) as kerma_non_alihan,
							sum(case when jenis = 'RI' then nominal else 0 end) as ri,
							sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
							sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

	// public function getSumRkaSispran1(array $conditions = [])
	// {
	// 	$builder = $this->db->table('t_anggaran_unit');
	
	// 	$builder->select("
	// 	sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
	// 	sum(case when jenis = 'ALIH_MASUK' then nominal else 0 end) as pengalihan_masuk,
	// 	sum(case when jenis = 'ALIH_KELUAR' then nominal else 0 end) as pengalihan_keluar,
	// 	sum(case when jenis = 'RI' then nominal else 0 end) as ri,
	// 	sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
	// 	sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
	// 	");
		
	// 	$builder->where('deleted_at', NULL);
	// 	$builder->where($conditions);
		
	// 	$query = $builder->get();
	// 	// Get the generated SQL query
	// 	$sql = $builder->getCompiledSelect();
	// 	print_r($builder);die;
	
	// 	// You can print the SQL query for debugging purposes
	// 	// echo $sql;
	
	// 	return $query->getRow();
	// }
	
    // public function getSumRkaSispran1old (array $conditions = []) {
    // 	$conditions = array_merge($conditions, ['sispran' => '1']);
	// 	// print_r($conditions);die;
    //    	return $this->select("
    //    						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
	// 						sum(case when jenis = 'ALIH_MASUK' then nominal else 0 end) as pengalihan_masuk,
	// 						sum(case when jenis = 'ALIH_KELUAR' then nominal else 0 end) as pengalihan_keluar,
	// 						sum(case when jenis = 'RI' then nominal else 0 end) as ri,
	// 						sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
	// 						sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
    //    						")
    //    				->where('deleted_at', NULL)
    //                	->where($conditions)
    //                	->first();
      
    //    return $this->findAll();
    // }

	public function getSumRkaSispran1 (array $conditions = []) {
	// $conditions = array_merge($conditions, ['sispran' => '1']);
	
	return $this->select("
						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
						sum(case when jenis = 'RKA ALIH' AND program = 'ALIH_MASUK' then nominal else 0 end) as pengalihan_masuk,
						sum(case when jenis = 'RKA ALIH' AND program = 'ALIH_KELUAR' then nominal else 0 end) as pengalihan_keluar,
						sum(case when jenis = 'RI' then nominal else 0 end) as ri,
						sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
						sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
						")
				->where('deleted_at', NULL)
				->where($conditions)
				->first();
	
	return $this->findAll();
    }

    // public function getSumRkaSispran2old (array $conditions = []) {
    // 	$conditions = array_merge($conditions, ['sispran' => '2']);

    //    	return $this->select("
    //    						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
	// 						sum(case when jenis = 'REALOKASI' then nominal else 0 end) as realokasi,
	// 						sum(case when jenis = 'PROSPEKTIF' then nominal else 0 end) as prospektif,
	// 						sum(case when jenis = 'KERMA_ALIHAN' then nominal else 0 end) as kerma_alihan,
	// 						sum(case when jenis = 'KERMA_NON_ALIHAN' then nominal else 0 end) as kerma_non_alihan,
	// 						sum(case when jenis = 'RI' then nominal else 0 end) as ri,
	// 						sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
	// 						sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
    //    						")
    //    				->where('deleted_at', NULL)
    //                	->where($conditions)
    //                	->first();
      
    //    // return $this->findAll();
    // }

	public function getSumRkaSispran2 (array $conditions = []) {
    	// $conditions = array_merge($conditions, ['sispran' => '2']);

       	return $this->select("
       						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
							sum(case when jenis = 'RKA KERMA' AND program = 'REALOKASI' then nominal else 0 end) as realokasi,
							sum(case when jenis = 'PROSPEKTIF' then nominal else 0 end) as prospektif,
							sum(case when jenis = 'KERMA_ALIHAN' then nominal else 0 end) as kerma_alihan,
							sum(case when jenis = 'KERMA_NON_ALIHAN' then nominal else 0 end) as kerma_non_alihan,
							sum(case when jenis = 'RI' then nominal else 0 end) as ri,
							sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
							sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getPenyerapanKumulatif (array $conditions = []) {

       	return $this->select("
       						sum(case when bulan <= 1 then nominal else 0 end) as jan,
							sum(case when bulan <= 2 then nominal else 0 end) as feb,
							sum(case when bulan <= 3 then nominal else 0 end) as mar,
							sum(case when bulan <= 4 then nominal else 0 end) as apr,
							sum(case when bulan <= 5 then nominal else 0 end) as mei,
							sum(case when bulan <= 6 then nominal else 0 end) as jun,
							sum(case when bulan <= 7 then nominal else 0 end) as jul,
							sum(case when bulan <= 8 then nominal else 0 end) as agu,
							sum(case when bulan <= 9 then nominal else 0 end) as sep,
							sum(case when bulan <= 10 then nominal else 0 end) as okt,
							sum(case when bulan <= 11 then nominal else 0 end) as nov,
							sum(case when bulan <= 12 then nominal else 0 end) as des
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	// ->getCompiledSelect();
                   	->first();
      
       // return $this->findAll();
    }

    public function getPenyerapanPerbulan (array $conditions = []) {

       	return $this->select("
       						sum(case when bulan = 1 then nominal else 0 end) as jan,
							sum(case when bulan = 2 then nominal else 0 end) as feb,
							sum(case when bulan = 3 then nominal else 0 end) as mar,
							sum(case when bulan = 4 then nominal else 0 end) as apr,
							sum(case when bulan = 5 then nominal else 0 end) as mei,
							sum(case when bulan = 6 then nominal else 0 end) as jun,
							sum(case when bulan = 7 then nominal else 0 end) as jul,
							sum(case when bulan = 8 then nominal else 0 end) as agu,
							sum(case when bulan = 9 then nominal else 0 end) as sep,
							sum(case when bulan = 10 then nominal else 0 end) as okt,
							sum(case when bulan = 11 then nominal else 0 end) as nov,
							sum(case when bulan = 12 then nominal else 0 end) as des
       						")
       				->where('deleted_at', NULL)
                   	->where($conditions)
                   	->first();
      
       // return $this->findAll();
    }

    public function getSumAlokasi (array $conditions = []) {


       	return $this->select("
       						sum(case when jenis = 'RKA' then nominal else 0 end) as rka,
       						sum(case when jenis = 'RI' then nominal else 0 end) as ri,
       						sum(case when jenis = 'FRA' then nominal else 0 end) as fra,
       						sum(case when jenis = 'REALISASI' then nominal else 0 end) as realisasi
       						")
                   ->where('deleted_at', NULL)
                   ->where($conditions)
                   // ->getCompiledSelect();
                   ->first();
      
       // return $this->findAll();
    }

	function all(array $conditions = [])	{

		return $this->db->table('t_anggaran_unit')->get()->getResult();		
	
	}


	function allAnggaran($anggaran = ''){

		if ($anggaran) {
			$sispran  = substr($anggaran, 0 , 2);		

			#get all anggaran and their parent, grandfather, great grandfather 	
			$sql = " SELECT a.*, 
					 c.nama     as nama,
					 d.id_const as parent_id_const,
					 d.nama     as parent_nama, 
					 e.id_const as kakek_id_const,
					 e.nama     as kakek_nama, 
					 f.id_const as moyang_id_const,
					 f.nama     as moyang_nama 
					 FROM t_anggaran a 
					 JOIN t_const c ON c.id_const = a.id_const
					 JOIN t_const d ON d.id_const = c.parent_id
					 JOIN t_const e ON e.id_const = d.parent_id 
					 JOIN t_const f ON f.id_const = e.parent_id
					 WHERE e.id_const = $anggaran
					 AND f.id_const = $sispran
					 ORDER BY a.id_anggaran DESC";	
		} else {
			$sql = " SELECT a.*, 
					 c.nama     as nama,
					 d.id_const as parent_id_const,
					 d.nama     as parent_nama, 
					 e.id_const as kakek_id_const,
					 e.nama     as kakek_nama, 
					 f.id_const as moyang_id_const,
					 f.nama     as moyang_nama 
					 FROM t_anggaran a 
					 JOIN t_const c ON c.id_const = a.id_const
					 JOIN t_const d ON d.id_const = c.parent_id
					 JOIN t_const e ON e.id_const = d.parent_id 
					 JOIN t_const f ON f.id_const = e.parent_id
					 WHERE c.level = 4
					 ORDER BY a.id_anggaran DESC";	
		}
		
				  
	
		return  $this->db->query($sql)	   
					 ->getResult();

	}


	function allRkaSispran2($realokasi, $prospektif, $penerimaan){

		$sispran  = substr($realokasi, 0 , 2);		

		#get all anggaran and their parent, grandfather, great grandfather 	
		$sql = " SELECT a.*, 
				 c.nama     as nama,
				 d.id_const as parent_id_const,
				 d.nama     as parent_nama, 
				 e.id_const as kakek_id_const,
				 e.nama     as kakek_nama				  
				 FROM t_anggaran a 
				 JOIN t_const c ON c.id_const = a.id_const
				 JOIN t_const d ON d.id_const = c.parent_id
				 JOIN t_const e ON e.id_const = d.parent_id 
				 WHERE 
				 (d.id_const = $realokasi OR d.id_const = $prospektif OR d.id_const = $penerimaan)				
				 AND
				 e.id_const = $sispran
				 ORDER BY a.id_anggaran DESC";
				  
	
		return  $this->db->query($sql)	   
					 ->getResult();

	}



	function create($data){

		$builder = $this->db->table('t_anggaran');
		$builder->insert($data);
	
	}


	// function save($data){

	// 	$builder = $this->db->table('t_anggaran');
	// 	$builder->insert($data);
	
	// }


	// function delete($id){

	// 	$builder = $this->db->table('t_anggaran');
	// 	$builder->where('id_anggaran', $id);
	// 	$builder->delete();

	// }


	// function update($data){

	// 	$builder = $this->db->table('t_anggaran');		
	// 	$builder->where('id_anggaran', $data['id_anggaran']);
	// 	$builder->update($data);

	// }


	// function find($id){

	// 	return $this->db->table('t_anggaran')
	// 	                ->where('id_anggaran' , $id)
	// 	                ->get()
	// 	                ->getRow();
	// }


	// function findArray($id){

	// 	return $this->db->table('t_anggaran')
	// 	                ->where('id_anggaran' , $id)
	// 	                ->get()
	// 	                ->getRowArray();
	
	// }


	function checkRka($id, $thn) {

		$query = $this->db->table('t_anggaran')
		                ->where('id_const' , $id)
		                ->where('thn_anggaran' , $thn)
		                ->get()
		                ->getResult();

		return $query ? true : false;

	}



	/*function allJenisAnggaran($const){

		#get ri, rka, fra, realisasi by constanta (4 digits left of constanta)
		return $this->db->table('t_anggaran')
					->join('t_const', 't_const.id_const = t_anggaran.id_const')
				    ->where('LEFT(t_anggaran.id_const,4) =' , $const)	
				    ->orderBy("t_anggaran.tgl_pengajuan", "ASC")				
					->get()
		            ->getResult();
	}*/


}


