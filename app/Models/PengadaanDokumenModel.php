<?php namespace App\Models;

use CodeIgniter\Model;


class PengadaanDokumenModel extends Model
{

    protected $table = 't_pengadaan_dokumen';
    protected $primaryKey = 'id_pengadaan_dok'; 

    protected $allowedFields = ['id_pengadaan', 'id_dokumen', 'tgl_dokumen'];


  	public function getDokumen($id)
  	{  	
  		return $this->where('id_pengadaan', $id)->findAll();
  	}


  	public function getDokumenByIds($id, $id2)
  	{
  		$conditions = [ 'id_pengadaan' => $id, 'id_dokumen' => $id2 ];

  		return $this->where($conditions)->first();
  	}

}