<?php namespace App\Models;

use CodeIgniter\Model;

/**
 *
 */
class KepegawaianModel extends Model
{

  protected $table = 't_peg_itb';

  public function getDosen(array $conditions = [])
  {
    if (empty($conditions)) {
      return $this->whereIn('id_sta_peg', ['Dosen PNS', 'Dosen BHMN'])->findAll();
    }

    return $this->where($conditions)->whereIn('id_sta_peg', ['Dosen PNS', 'Dosen BHMN'])->findAll();
  }

  public function getAkDosen(array $conditions = [])
  {
    if (empty($conditions)) {
      return $this->select('
                        t_peg_itb.*, 
                        t_peg_itb_kredit.jabatan_usulan, t_peg_itb_kredit.ak, t_peg_itb_kredit.ak_pendidikan, t_peg_itb_kredit.ak_penelitian,
                        t_peg_itb_kredit.ak_pengabdian, t_peg_itb_kredit.ak_pengembangan, t_peg_itb_kredit.syarat, t_peg_itb_kredit.syarat_keterangan,
                        t_peg_itb_kredit.status as status_angka_kredit,
                        t_jabatan_fungsional.ak_total as ak_dibutuhkan, t_jabatan_fungsional.ak_pp as ak_pendidikan_syarat, t_jabatan_fungsional.ak_penelitian as ak_penelitian_syarat,
                        t_jabatan_fungsional.ak_ppm as ak_pengabdian_syarat, t_jabatan_fungsional.ak_pi as ak_pengembangan_syarat
                        ')
                  ->join('t_peg_itb_kredit', 't_peg_itb_kredit.nopeg = t_peg_itb.nopeg', 'left')
                  ->join('t_jabatan_fungsional', 't_jabatan_fungsional.jabfung = t_peg_itb_kredit.jabatan_usulan', 'left')
                  ->whereIn('id_sta_peg', ['Dosen PNS', 'Dosen BHMN'])->findAll();
    }

    return $this->select('
                        t_peg_itb.*, 
                        t_peg_itb_kredit.jabatan_usulan, t_peg_itb_kredit.ak, t_peg_itb_kredit.ak_pendidikan, t_peg_itb_kredit.ak_penelitian,
                        t_peg_itb_kredit.ak_pengabdian, t_peg_itb_kredit.ak_pengembangan, t_peg_itb_kredit.syarat, t_peg_itb_kredit.syarat_keterangan,
                        t_peg_itb_kredit.status as status_angka_kredit,
                        t_jabatan_fungsional.ak_total as ak_dibutuhkan, t_jabatan_fungsional.ak_pp as ak_pendidikan_syarat, t_jabatan_fungsional.ak_penelitian as ak_penelitian_syarat,
                        t_jabatan_fungsional.ak_ppm as ak_pengabdian_syarat, t_jabatan_fungsional.ak_pi as ak_pengembangan_syarat
                        ')
                ->join('t_peg_itb_kredit', 't_peg_itb_kredit.nopeg = t_peg_itb.nopeg', 'left')
                ->join('t_jabatan_fungsional', 't_jabatan_fungsional.jabfung = t_peg_itb_kredit.jabatan_usulan', 'left')
                ->where($conditions)->whereIn('id_sta_peg', ['Dosen PNS', 'Dosen BHMN'])->findAll();
  }


  public function getPegawai(){

  	return $this->orderBy('nama', 'asc')->findAll();
  
  }

  public function getSummaryJabFungsional(array $conditions = [])
  {
    return $this->select('
                        sum(case when jabatan_fungsional = "GURU BESAR" THEN 1 else 0 end) as gb,
                        sum(case when jabatan_fungsional = "LEKTOR KEPALA" THEN	1	else 0 end) as lk,
                        sum(case when jabatan_fungsional = "LEKTOR" THEN 1 else 0 end) as l,
                        sum(case when jabatan_fungsional = "ASISTEN AHLI" THEN 1 else	0 end) as aa,
                        sum(case when jabatan_fungsional = "" THEN 1 else	0 end) as non_jab
                        ') 
                ->inWhere('status', ['AKTIF', 'TBLN'])
                ->andWhere($conditions)
                ->first();
  }


}
