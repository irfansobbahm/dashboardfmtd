<?php namespace App\Models;

use CodeIgniter\Model;


class RoleModel extends Model
{

  protected $table = 't_users_role';
  protected $primaryKey = 'id_role'; 

  protected $useSoftDeletes = true;
  protected $useTimestamps  = true;
  protected $deletedField   = 'deleted_at';
  protected $createdField   = 'created_at';
  protected $updatedField   = 'updated_at';
  
  protected $allowedFields = ['username', 'role', 'is_active', 'user_added'];

}