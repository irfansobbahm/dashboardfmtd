<?php namespace App\Models;

use CodeIgniter\Model;


class SkdibayardetailModel extends Model
{

    protected $table = 't_sk_dibayar_detail';
    protected $primaryKey = 'id_sk_dibayar_detail'; 

    protected $useSoftDeletes = true;
    protected $useTimestamps  = true;
    protected $deletedField   = 'deleted_at';
    protected $createdField   = 'created_at';
    protected $updatedField   = 'updated_at';
    
    protected $allowedFields = ['id_sk_dibayar', 'id_sk_detail', 'keterangan', 'nilai_maksimal', 'nilai_dibayar', 'volume', 'total', 'nip', 'nama', 'satuan', 'kategori', 'kode_file', 'periode', 'id_sk'];


    public function getAll(array $conditions = [])
    {
        if (empty($conditions)) {
        return $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, 
                                (case 
                                    when t_sk_detail.nip is not null then 
                                        t_pegawai.nip 
                                    when t_sk_detail.nim is not null then 
                                        t_mahasiswa.nim
                                    else
                                        t_sk_detail.nik
                                end) no_identitas,
                                (case 
                                    when t_sk_detail.nip is not null then 
                                        t_pegawai.nama 
                                    when t_sk_detail.nim is not null then 
                                        t_mahasiswa.nama
                                    else
                                        t_sk_detail.nama
                                end) nama, 
                                concat(t_const2.nama, " ", t_const.nama) as jenis')
                    ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    ->join('t_sk_detail', 't_sk_detail.id_sk_detail = t_sk_dibayar_detail.id_sk_detail')
                    ->join('t_pegawai', 't_pegawai.nip = t_sk_detail.nip', 'left')
                    ->join('t_mahasiswa', 't_mahasiswa.nim = t_sk_detail.nim', 'left')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->findAll();
        }

        return $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, 
                                (case 
                                    when t_sk_detail.nip is not null then 
                                        t_pegawai.nip 
                                    when t_sk_detail.nim is not null then 
                                        t_mahasiswa.nim
                                    else
                                        t_sk_detail.nik
                                end) no_identitas,
                                (case 
                                    when t_sk_detail.nip is not null then 
                                        t_pegawai.nama 
                                    when t_sk_detail.nim is not null then 
                                        t_mahasiswa.nama
                                    else
                                        t_sk_detail.nama
                                end) nama, 
                                concat(t_const2.nama, " ", t_const.nama) as jenis')
                    ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    ->join('t_sk_detail', 't_sk_detail.id_sk_detail = t_sk_dibayar_detail.id_sk_detail')
                    ->join('t_pegawai', 't_pegawai.nip = t_sk_detail.nip', 'left')
                    ->join('t_mahasiswa', 't_mahasiswa.nim = t_sk_detail.nim', 'left')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->where($conditions)->findAll();
    }

    public function getAllAlt(array $conditions = [], $order = 't_sk_dibayar.tgl_pembayaran')
    {
        if (empty($conditions)) {
        return $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.periode as periode_detail, t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, nip as no_identitas, t_sk_dibayar.no_spp, t_sk_dibayar.keterangan as kegiatan, 
            DATE_FORMAT(tgl_pembayaran,"%d-%m-%Y") AS tgl_pembayaran_f, 
                                t_const.nama as jenis')
                    ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar AND t_sk_dibayar.deleted_at IS NULL')
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar_detail.id_sk')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->orderBy($order, 'asc')
                    // ;
                    // ->getCompiledSelect();
                    ->findAll();
                    // $sql = $query->getCompiledSelect();
                    // print_r($sql);die;
        }

        return  $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.periode as periode_detail, t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, t_sk.tgl_terbit, t_sk.tgl_berakhir, nip as no_identitas, t_sk_dibayar.no_spp, t_sk_dibayar.keterangan as kegiatan, 
                                DATE_FORMAT(tgl_pembayaran,"%d-%m-%Y") AS tgl_pembayaran_f, 
                                t_const.nama as jenis')
                    ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar AND t_sk_dibayar.deleted_at IS NULL')
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar_detail.id_sk')
                    ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
                    ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
                    ->where($conditions)
                    ->orderBy($order, 'asc')
                    // ;
                    // ->getCompiledSelect();
                    ->findAll();
                    // $sql = $query->getCompiledSelect();
                    // print_r($sql);die;
    }

    // public function getAllAlt(array $conditions = [], $order = 't_sk_dibayar.tgl_pembayaran')
    // {
    //     if (empty($conditions)) {
    //         return $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.periode as periode_detail, 
    //         t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, 
    //         t_sk.tgl_terbit, t_sk.tgl_berakhir, t_sk_dibayar_detail.nip as no_identitas, t_sk_dibayar.no_spp, 
    //         t_sk_dibayar.keterangan as kegiatan, DATE_FORMAT(tgl_pembayaran,"%d-%m-%Y") AS tgl_pembayaran_f, 
    //         t_const.nama as jenis')
    //         ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar AND t_sk_dibayar.deleted_at IS NULL')
    //         ->join('t_sk', 't_sk.id_sk = t_sk_dibayar_detail.id_sk')
    //         ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
    //         ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
    //         ->join('t_pegawai','t_pegawai.nip = t_sk_dibayar_detail.nip')
    //                 ->orderBy($order, 'asc')
    //                 ->findAll();
    //                 // ->first();
    //                 // ->getCompiledSelect();

    //                 // $sql = $query->getCompiledSelect();
    //                 // echo $sql;
    //                 // print_r($sql);die;
    //     }

    //     return $this->select('t_sk_dibayar_detail.*, t_sk_dibayar_detail.periode as periode_detail, 
    //     t_sk_dibayar_detail.keterangan as keterangan_detail, t_sk_dibayar.*, t_sk.no_sk, t_sk.judul_sk, 
    //     t_sk.tgl_terbit, t_sk.tgl_berakhir, t_sk_dibayar_detail.nip as no_identitas, t_sk_dibayar.no_spp, 
    //     t_sk_dibayar.keterangan as kegiatan, DATE_FORMAT(tgl_pembayaran,"%d-%m-%Y") AS tgl_pembayaran_f, 
    //     t_const.nama as jenis')
    //     ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar AND t_sk_dibayar.deleted_at IS NULL')
    //     ->join('t_sk', 't_sk.id_sk = t_sk_dibayar_detail.id_sk')
    //     ->join('t_const', 't_const.id_const = t_sk_dibayar.id_jenis_honor')
    //     ->join('t_const as t_const2', 't_const2.id_const = t_const.parent_id', 'left')
    //     ->join('t_pegawai','t_pegawai.nip = t_sk_dibayar_detail.nip')
    //             ->orderBy($order, 'asc')
    //             ->where($conditions)
    //             ->findAll();
    //             // ->first();
    //             // ->getCompiledSelect();

    //             // $sql = $query->getCompiledSelect();
    //             // echo $sql;
    //             // print_r($sql);die;
    // }

    public function getAllPenerima(array $conditions = [])
    {
        return $this->select('distinct(nip), nama')
                    ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
                    ->join('t_sk', 't_sk.id_sk = t_sk_dibayar.id_sk')
                    ->where($conditions)
                    ->orderBy('nama')
                    // ->getCompiledSelect();
                    ->findAll();
    }


    public function getSummary (array $conditions = []) {

        return $this->select("
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_pegawai,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_pegawai_sipran1,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '02' then total else 0 end) as total_belanja_pegawai_sipran2,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '03' then total else 0 end) as total_belanja_jasa,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '03' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_jasa_sipran1,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '03' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '02' then total else 0 end) as total_belanja_jasa_sipran2,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'DOSEN' then total else 0 end) as total_belanja_dosen,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'DOSEN' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_dosen_sispran1,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'DOSEN' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '02' then total else 0 end) as total_belanja_dosen_sispran2,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'TENDIK' then total else 0 end) as total_belanja_tendik,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'TENDIK' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_tendik_sipran1,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan = 'TENDIK' AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '02' then total else 0 end) as total_belanja_tendik_sipran2,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan IS NULL then total else 0 end) as total_belanja_lainnya,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan IS NULL AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '01' then total else 0 end) as total_belanja_lainnya_sispran1,
                        sum(case when RIGHT(t_sk_dibayar.id_jenis_honor,2) = '01' AND t_pegawai.jenis_penugasan IS NULL AND LEFT(t_sk_dibayar.id_jenis_honor,2) = '02' then total else 0 end) as total_belanja_lainnya_sispran2
                        
                        ")
                        ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
                        ->join('t_pegawai', 't_pegawai.nip = t_sk_dibayar_detail.nip', 'left')
                        ->where('t_sk_dibayar.deleted_at', NULL)
                        ->where($conditions)
                        ->first();
      
       // return $this->findAll();
    }

    public function getSummaryPenerima (array $conditions = []) {

        return $this->select("
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '01' then total  else 0 end) as jan,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '02' then total else 0 end) as feb,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '03' then total else 0 end) as mar,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '04' then total else 0 end) as apr,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '05' then total else 0 end) as mei,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '06' then total else 0 end) as jun,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '07' then total else 0 end) as jul,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '08' then total else 0 end) as agu,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '09' then total else 0 end) as sep,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '10' then total else 0 end) as okt,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '11' then total else 0 end) as nov,
                            sum(case when date_format(t_sk_dibayar.tgl_pembayaran, '%m') = '12' then total else 0 end) as des                        
                        ")
                        ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
                        ->join('t_pegawai', 't_pegawai.nip = t_sk_dibayar_detail.nip', 'left')
                        ->where('t_sk_dibayar.deleted_at', NULL)
                        ->where($conditions)
                        ->first();
      
       // return $this->findAll();
    }

    public function getPembayaran(Array $conditions = []){
        return $this->select("
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '01' THEN t_sk.id_sk END) AS jan,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '02' THEN t_sk.id_sk END) AS feb,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '03' THEN t_sk.id_sk END) AS mar,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '04' THEN t_sk.id_sk END) AS apr,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '05' THEN t_sk.id_sk END) AS mei,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '06' THEN t_sk.id_sk END) AS jun,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '07' THEN t_sk.id_sk END) AS jul,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '08' THEN t_sk.id_sk END) AS agu,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '09' THEN t_sk.id_sk END) AS sep,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '10' THEN t_sk.id_sk END) AS okt,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '11' THEN t_sk.id_sk END) AS nov,
        COUNT(DISTINCT CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '12' THEN t_sk.id_sk END) AS des
        ")
        ->join('t_sk_dibayar','t_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
        ->join('t_sk','t_sk.id_sk = t_sk_dibayar_detail.id_sk')
        ->where('t_sk_dibayar_detail.deleted_at', NULL)
        ->where($conditions)
        ->first();

        // $sql= $query->getCompiledselect();
        // print_r($sql);die;
  
    }
    // public function getSummaryPenerima(array $conditions = [])
    // {
    //     $query = $this->select("
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '01' THEN total ELSE 0 END) AS jan,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '02' THEN total ELSE 0 END) AS feb,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '03' THEN total ELSE 0 END) AS mar,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '04' THEN total ELSE 0 END) AS apr,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '05' THEN total ELSE 0 END) AS mei,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '06' THEN total ELSE 0 END) AS jun,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '07' THEN total ELSE 0 END) AS jul,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '08' THEN total ELSE 0 END) AS agu,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '09' THEN total ELSE 0 END) AS sep,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '10' THEN total ELSE 0 END) AS okt,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '11' THEN total ELSE 0 END) AS nov,
    //             SUM(CASE WHEN DATE_FORMAT(t_sk_dibayar.tgl_pembayaran, '%m') = '12' THEN total ELSE 0 END) AS des
    //         ")
    //         ->join('t_sk_dibayar', 't_sk_dibayar.id_sk_dibayar = t_sk_dibayar_detail.id_sk_dibayar')
    //         ->join('t_pegawai', 't_pegawai.nip = t_sk_dibayar_detail.nip', 'left')
    //         ->where('t_sk_dibayar.deleted_at', NULL)
    //         ->where($conditions);
    
    //     $sql = $query->getCompiledSelect();
    //     // echo $sql;
    
    //     $result = $query->first();
    
    //     if ($result) {
    //         print_r($sql);
    //     } else {
    //         // Handle query execution error
    //         echo "Error executing the query";
    //     }
    
    //     die;
    // }
    
    
}