<?php namespace App\Models;

use CodeIgniter\Model;

/**
 *
 */
class SkModel extends Model
{

  protected $table = 't_sk';
  protected $primaryKey = 'id_sk'; 

  protected $useSoftDeletes = true;
  protected $useTimestamps  = true;
  protected $deletedField   = 'deleted_at';
  protected $createdField   = 'created_at';
  protected $updatedField   = 'updated_at';
  
  protected $allowedFields = ['dekan', 'no_sk', 'judul_sk', 'tgl_terbit', 'tgl_berakhir', 'tgl_tandatangan', 'tahun', 'jenis'];

  public function getAll(array $conditions = [])
  {
    if (empty($conditions)) {
      return $this->select('t_sk.*, t_peg_itb.nama')
    							->join('t_peg_itb', 't_peg_itb.nopeg = t_sk.dekan', 'LEFT')
                  ->findAll();
    }

    return $this->select('t_sk.*, t_peg_itb.nama')
    						->join('t_peg_itb', 't_peg_itb.nopeg = t_sk.dekan', 'LEFT')
    						->where($conditions)->findAll();
  }

  public function getId(Array $conditions = [])
  {

    $res = $this->select('id_sk')->where($conditions)->first();

    if (!is_array($res))
      return '';

    return (count($res) > 0) ? $res['id_sk'] : '';
  }

  public function getLastUpdate (array $conditions = []) 
  {
      return $this->select('YEAR(updated_at) as tahun, MONTH(updated_at) as bulan, DAY(updated_at) as tanggal, updated_at')
                 ->where($conditions)
                 ->orderBy('updated_at', 'DESC')
                 ->first();
  }

  public function getSummary (array $conditions = []) {

        return $this->select("
                        sum(1) as total,
                        sum(case when sispran = '1' then 1 else 0 end) as sispran_1,
                        sum(case when sispran = '2' then 1 else 0 end) as sispran_2,
                        ")
                      ->where('deleted_at', NULL)
                      ->where($conditions)
                      ->first();
      
       // return $this->findAll();
  }



  
}
