<?php namespace App\Models;

use CodeIgniter\Model;

/**
 *
 */
class PegawaiModel extends Model
{

  protected $table = 't_pegawai';

  protected $primaryKey = 'nip'; 

  // protected $useSoftDeletes = true;
  // protected $useTimestamps  = true;
  // protected $deletedField   = 'deleted_at';
  // protected $createdField   = 'created_at';
  // protected $updatedField   = 'updated_at';

  protected $allowedFields = ['nama', 'gelar_depan', 'gelar_belakang', 'tgl_lahir', 'pendidikan', 'jenis_penugasan', 'golongan', 'jab_fungsional', 'kelompok_keahlian', 'tmt_kerja', 'is_aktif', 'status_kepegawaian', 'tmt_jab_fungsional', 'tmt_golongan', 'umur', 'tmt_pensiun', 'masa_jabfung'];

  public function getDosen(array $conditions = [])
  {
    $conditions = array_merge($conditions, ['jenis_penugasan' => 'DOSEN']);

    return $this->where($conditions)->where($conditions)->findAll();
  }

public function getAkDosen(array $conditions = [])
{
    $conditions = array_merge($conditions, ['jenis_penugasan' => 'DOSEN']);

    return $this->select('
        t_pegawai.*, 
        t_pegawai_kredit.jabatan_usulan, t_pegawai_kredit.ak_syarat, t_pegawai_kredit.ak_perolehan, t_pegawai_kredit.ak_pendidikan, t_pegawai_kredit.ak_penelitian,
        t_pegawai_kredit.ak_pengabdian, t_pegawai_kredit.ak_pengembangan, t_pegawai_kredit.syarat_ak, t_pegawai_kredit.syarat_keterangan,
        t_pegawai_kredit.syarat_ak as ak_dibutuhkan, 
        t_pegawai_kredit.ak_pendidikan_persyaratan as ak_pendidikan_syarat, 
        t_pegawai_kredit.ak_penelitian_persyaratan as ak_penelitian_syarat, 
        t_pegawai_kredit.ak_pengabdian_persyaratan as ak_pengabdian_syarat, 
        t_pegawai_kredit.ak_pengembangan_persyaratan as ak_pengembangan_syarat,
        t_pegawai_kredit.status as status_angka_kredit
    ')
    ->join('t_pegawai_kredit', 't_pegawai_kredit.nip = t_pegawai.nip')
    ->join('t_jabatan_fungsional', 't_jabatan_fungsional.jabfung = t_pegawai_kredit.jabatan_usulan', 'left')
    ->where($conditions)
    ->findAll();
}

    // public function getAkDosen(array $conditions = [])
    // {
    //   $query = $this->select(' t_pegawai.*, 
    //   t_pegawai_kredit.jabatan_usulan, t_pegawai_kredit.ak_syarat, t_pegawai_kredit.ak_perolehan, t_pegawai_kredit.ak_pendidikan, t_pegawai_kredit.ak_penelitian,
    //   t_pegawai_kredit.ak_pengabdian, t_pegawai_kredit.ak_pengembangan, t_pegawai_kredit.syarat_ak, t_pegawai_kredit.syarat_keterangan,
    //   t_pegawai_kredit.syarat_ak as ak_dibutuhkan, 
    //   t_pegawai_kredit.ak_pendidikan_persyaratan as ak_pendidikan_syarat, 
    //   t_pegawai_kredit.ak_penelitian_persyaratan as ak_penelitian_syarat, 
    //   t_pegawai_kredit.ak_pengabdian_persyaratan as ak_pengabdian_syarat, 
    //   t_pegawai_kredit.ak_pengembangan_persyaratan as ak_pengembangan_syarat,
    //   t_pegawai_kredit.status as status_angka_kredit')
    //   ->join('t_pegawai_kredit', 't_pegawai_kredit.nip = t_pegawai.nip')
    //   ->join('t_jabatan_fungsional', 't_jabatan_fungsional.jabfung = t_pegawai_kredit.jabatan_usulan', 'left')
    //   // ->where('
    //   //     (t_pegawai.jab_fungsional = "GURU BESAR" AND (70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2) OR
    //   //     (t_pegawai.jab_fungsional <> "GURU BESAR" AND (65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2)
    //   // ');
    //   ->where($conditions);

    //   $sql = $query->getCompiledSelect();
    //   // echo $sql;
    //   print_r($sql);die; // Tampilkan SQL untuk debugging

    // }

  public function getPegawai(){

  	return $this->orderBy('nama', 'asc')->findAll();
  
  }

  public function getPegawaiByNip($nip){

    return $this->where(['nip' => $nip])->first();
  
  }

  public function getSummaryJabFungsional(array $conditions = [])
  {
    return $this->select('
                        sum(case when jab_fungsional = "GURU BESAR" THEN 1 else 0 end) as gb,
                        sum(case when jab_fungsional = "LEKTOR KEPALA" THEN	1	else 0 end) as lk,
                        sum(case when jab_fungsional = "LEKTOR" THEN 1 else 0 end) as l,
                        sum(case when jab_fungsional = "ASISTEN AHLI" THEN 1 else	0 end) as aa,
                        sum(case when jab_fungsional = "NON JABATAN" THEN 1 else	0 end) as non_jab
                        ') 
                ->where('is_aktif', 1)
                ->where($conditions)
                ->first();
  }

  public function getSummaryPemenuhanAk(array $conditions = [])
  {
    return $this->select('
                        sum(case when t_pegawai_kredit.status = 0 THEN 1 else 0 end) as belum,
                        sum(case when (t_pegawai_kredit.status = 1 OR t_pegawai_kredit.status = 2) THEN 1 else 0 end) as memenuhi,
                        sum(case when (t_pegawai_kredit.status = 3 OR t_pegawai_kredit.status = 4) THEN 1 else 0 end) as diproses_ftmd,
                        sum(case when (t_pegawai_kredit.status = 5 OR t_pegawai_kredit.status = 6 OR t_pegawai_kredit.status = 7) THEN 1 else 0 end) as diproses_itb,
                        sum(case when t_pegawai_kredit.status = 8 THEN 1 else 0 end) as diproses_dikti
                        ') 
                ->join('t_pegawai_kredit', 't_pegawai_kredit.nip = t_pegawai.nip')
                // ->inWhere('status', ['AKTIF', 'TBLN'])
                ->where($conditions)
                ->where('is_aktif', 1)
                // ->getCompiledSelect();
                ->first();
  }

  public function getSummaryStatus(array $conditions = [])
  {
    return $this->select('
                        sum(case when status_kepegawaian = "Aktif" THEN 1 else 0 end) as aktif,
                        sum(case when status_kepegawaian = "Tugas Belajar" THEN 1 else 0 end) as tubel,
                        sum(case when status_kepegawaian = "Menjabat di Luar" THEN 1 else 0 end) as menjabat,
                        sum(case when status_kepegawaian = "PENSIUN" THEN 1 else 0 end) as pensiun
                        ') 
                ->where($conditions)
                ->where('is_aktif',1)
                ->first();
  }

  // public function getSummaryPurnabakti(array $conditions = [])
  // {
  //   // return $this->select("
  //   //                     sum(  case when jab_fungsional = 'GURU BESAR' then
  //   //                         (case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 2 then 1 else 0 end)
  //   //                       else
  //   //                         (case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 2 then 1 else 0 end)
  //   //                       end
  //   //                     ) as dua,
  //   //                     sum(
  //   //                       case when jab_fungsional = 'GURU BESAR' then
  //   //                         (
  //   //                         case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 2 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <= 4 
  //   //                         then 1 else 0 end
  //   //                         )
  //   //                       else
  //   //                         (
  //   //                         case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 2 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <= 4 
  //   //                         then 1 else 0 end
  //   //                         )
  //   //                       end
  //   //                     ) as dua_empat,
  //   //                     sum(
  //   //                       case when jab_fungsional = 'GURU BESAR' then
  //   //                         (
  //   //                         case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) > 4 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <= 6 
  //   //                         then 1 else 0 end
  //   //                         )
  //   //                       else
  //   //                         (
  //   //                         case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) > 4 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <= 6 
  //   //                         then 1 else 0 end
  //   //                         )
  //   //                       end
                          
  //   //                     ) as empat_enam,
  //   //                     sum(
  //   //                       case when jab_fungsional = 'GURU BESAR' then
  //   //                         (case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) > 6  then 1 else 0 end)
  //   //                       else
  //   //                         (case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) > 6  then 1 else 0 end)
  //   //                       end
  //   //                     ) as enam
  //   //                     ") 
  //   //             ->where($conditions)
  //   //             ->first();


  //   return $this->select("
  //                       sum(  case when jab_fungsional = 'GURU BESAR' then
  //                           (case when (70 - umur) < 2 then 1 else 0 end)
  //                         else
  //                           (case when (65 - umur) < 2 then 1 else 0 end)
  //                         end
  //                       ) as dua,
  //                       sum(
  //                         case when jab_fungsional = 'GURU BESAR' then
  //                           (
  //                           case when (70 - umur) >= 2 and (65 - umur) <= 4 
  //                           then 1 else 0 end
  //                           )
  //                         else
  //                           (
  //                           case when (65 - umur) >= 2 and (65 - umur) <= 4 
  //                           then 1 else 0 end
  //                           )
  //                         end
  //                       ) as dua_empat,
  //                       sum(
  //                         case when jab_fungsional = 'GURU BESAR' then
  //                           (
  //                           case when (70 - umur) > 4 and (65 - umur) <= 6 
  //                           then 1 else 0 end
  //                           )
  //                         else
  //                           (
  //                           case when (65 - umur) > 4 and (65 - umur) <= 6 
  //                           then 1 else 0 end
  //                           )
  //                         end
                          
  //                       ) as empat_enam,
  //                       sum(
  //                         case when jab_fungsional = 'GURU BESAR' then
  //                           (case when (70 - umur) > 6  then 1 else 0 end)
  //                         else
  //                           (case when (65 - umur) > 6  then 1 else 0 end)
  //                         end
  //                       ) as enam
  //                       ") 
  //               ->where($conditions)
  //               ->where('is_aktif',1)
  //               ->whereIn('status_kepegawaian', ['AKTIF', 'MENJABAT DI LUAR', 'TUGAS BELAJAR'])
  //               ->first();

  // }  

  public function getSummaryPurnabakti(array $conditions = [])
  {
    return $this->select("
                        sum(  case when jab_fungsional = 'GURU BESAR' then
                            (case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 2 then 1 else 0 end)
                          else
                            (case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 2 then 1 else 0 end)
                          end
                        ) as dua,
                        sum(
                          case when jab_fungsional = 'GURU BESAR' then
                            (
                            case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 2 and (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <4 
                            then 1 else 0 end
                            )
                          else
                            (
                            case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 2 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) <4 
                            then 1 else 0 end
                            )
                          end
                        ) as dua_empat,
                        sum(
                          case when jab_fungsional = 'GURU BESAR' then
                            (
                            case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 4 and (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 6 
                            then 1 else 0 end
                            )
                          else
                            (
                            case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 4 and (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) < 6 
                            then 1 else 0 end
                            )
                          end
                          
                        ) as empat_enam,
                        sum(
                          case when jab_fungsional = 'GURU BESAR' then
                            (case when (70 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 6  then 1 else 0 end)
                          else
                            (case when (65 - TIMESTAMPDIFF (YEAR, tgl_lahir, CURDATE())) >= 6  then 1 else 0 end)
                          end
                        ) as enam
                        ") 
                ->where($conditions)
                ->where('is_aktif',1)
                ->first();


    // return $this->select("
    //                     sum(  case when jab_fungsional = 'GURU BESAR' then
    //                         (case when (70 - umur) < 2 then 1 else 0 end)
    //                       else
    //                         (case when (65 - umur) < 2 then 1 else 0 end)
    //                       end
    //                     ) as dua,
    //                     sum(
    //                       case when jab_fungsional = 'GURU BESAR' then
    //                         (
    //                         case when (70 - umur) >= 2 and (65 - umur) <= 4 
    //                         then 1 else 0 end
    //                         )
    //                       else
    //                         (
    //                         case when (65 - umur) >= 2 and (65 - umur) <= 4 
    //                         then 1 else 0 end
    //                         )
    //                       end
    //                     ) as dua_empat,
    //                     sum(
    //                       case when jab_fungsional = 'GURU BESAR' then
    //                         (
    //                         case when (70 - umur) > 4 and (65 - umur) <= 6 
    //                         then 1 else 0 end
    //                         )
    //                       else
    //                         (
    //                         case when (65 - umur) > 4 and (65 - umur) <= 6 
    //                         then 1 else 0 end
    //                         )
    //                       end
                          
    //                     ) as empat_enam,
    //                     sum(
    //                       case when jab_fungsional = 'GURU BESAR' then
    //                         (case when (70 - umur) > 6  then 1 else 0 end)
    //                       else
    //                         (case when (65 - umur) > 6  then 1 else 0 end)
    //                       end
    //                     ) as enam
    //                     ") 
    //             ->where($conditions)
    //             ->where('is_aktif',1)
    //             ->whereIn('status_kepegawaian', ['AKTIF', 'MENJABAT DI LUAR', 'TUGAS BELAJAR'])
    //             ->first();

  }  

  public function getSummaryLamaMenjabat(array $conditions = [])
  {
    return $this->select('
                        sum(case when TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) < 2 THEN 1 else 0 end) as satu,
                        sum(case when (TIMESTAMPDIFF (YEAR,  IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >= 2 and TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) < 6) THEN 1 else 0 end) as satu_tiga,
                        sum(case when (TIMESTAMPDIFF (YEAR,  IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >= 6 and TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <= 10) THEN 1 else 0 end) as tiga_lima,
                        sum(case when TIMESTAMPDIFF (YEAR,  IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) > 10  THEN 1 else 0 end) as lima
                        ') 
                ->where($conditions)
                ->where('is_aktif',1)
                // ->getCompiledSelect();
                ->first();
  }  

  // public function getSummaryLamaMenjabat(array $conditions = [])
  // {
  //   return $this->select('
  //                       sum(case when masa_jabfung < 2 THEN 1 else 0 end) as satu,
  //                       sum(case when masa_jabfung >= 2 and masa_jabfung < 6 THEN 1 else 0 end) as satu_tiga,
  //                       sum(case when masa_jabfung >= 6 and masa_jabfung <= 10 THEN 1 else 0 end) as tiga_lima,
  //                       sum(case when masa_jabfung > 10 THEN 1 else 0 end) as lima
  //                       ') 
  //               ->where($conditions)
  //               ->where('is_aktif',1)
  //               // ->getCompiledSelect();
  //               ->first();
  // }  

  public function getKategori(Array $conditions = [])
  {

    $res = $this->select('jenis_penugasan')->where($conditions)->first();

    if (!is_array($res))
      return '';

    return (count($res) > 0) ? $res['jenis_penugasan'] : '';
  }

}
