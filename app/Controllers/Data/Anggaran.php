<?php namespace App\Controllers\Data;


use App\Controllers\BaseController;
use App\Models\AnggaranModel;
use App\Models\AnggaranunitModel;
use CodeIgniter\API\ResponseTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


#----------------------------------------------------------
#
#	BASIC 
#
#----------------------------------------------------------

#peer 
# 1 escaped string, buat field keterangan
# 2 rka jika sudah ada, tambahkan ke validation


class Anggaran extends BaseController
{
	
	use ResponseTrait;  #for API


	protected $anggaranModel;
	protected $anggaranunitModel;
	protected $thnAktif;

	public function __construct()
	{
    	
		#db connection can open in specific function or in construct
		$db = db_connect();  #set global connection
		$this->db = db_connect(); 
    	$this->anggaranModel = new AnggaranModel();
    	$this->anggaranunitModel = new AnggaranunitModel();

    	#date
    	$this->year       = date("Y"); 
    	$this->today      = date("Y-m-d");
    	$this->now        = date("Y-m-d H:i:s");    	
    	$this->list_years = [ $this->year-1, $this->year, $this->year+1 ];
    	$this->thnAktif   = date("Y");
			
    	#set anggaran type and its constanta
    	$this->rka     = '0101';    	
    	$this->ri      = '0102';
    	$this->fra     = '0103';    	
    	$this->real    = '0104';    	
    	$this->ko      = '01';
    	$this->p3mi    = '02';
    	$this->pegawai = '01';
    	$this->barang  = '02';
    	$this->jasa    = '03';
    	$this->modal   = '04';

    	$this->realokasi      = '0201';
    	$this->prospektif     = '0202';
    	$this->penerimaan     = '0203';
    	$this->ri2            = '0204';
    	$this->fra2           = '0205';
    	$this->real2          = '0206';
    	$this->kermaAlihan    = '01';
    	$this->kermaNonAlihan = '02';
    	$this->kelasInter     = '03';  	

	}


	public function index()
	{
		$conditions = [];
		$sParams = ['sis' => '', 'jns' => '', 'sbr' => '', 'alk' => '', 'aw' => '', 'ak' => '', 'th' => $this->year, 'tw' => '1'];

		switch ($sParams['tw']) {
			case '1': $tview = 'v_anggaran_unit_tw_satu'; break;
			case '2': $tview = 'v_anggaran_unit_tw_dua'; break;
			case '3': $tview = 'v_anggaran_unit_tw_tiga'; break;
			case '4': $tview = 'v_anggaran_unit_tw_empat'; break;
			default:
				$tview = 'v_anggaran_unit_tw_satu';
				break;
		}


		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$twParams =  $uri->getQuery(['only' => ['triwulan']]);
			$sisParams =  $uri->getQuery(['only' => ['sis']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$sbrParams =  $uri->getQuery(['only' => ['sbr']]);
			$alkParams =  $uri->getQuery(['only' => ['alk']]);
			$awParams =  $uri->getQuery(['only' => ['aw']]);
			$akParams =  $uri->getQuery(['only' => ['ak']]);

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);
				if ($thVal){
					$sParams['th'] = $thVal;		
					$conditions = array_merge($conditions, ['thn_anggaran' => $sParams['th']]);
					$this->year = $sParams['th'];
				}
			}

			if ($twParams) {
				list($twKey, $twVal) = explode('=',$twParams);
				if ($twVal){
					$sParams['tw'] = $twVal;		
					// $conditions = array_merge($conditions, ['thn_anggaran' => $sParams['th']]);
				}
			}

			switch ($sParams['tw']) {
				case '1': $tview = 'v_anggaran_unit_tw_satu'; break;
				case '2': $tview = 'v_anggaran_unit_tw_dua'; break;
				case '3': $tview = 'v_anggaran_unit_tw_tiga'; break;
				case '4': $tview = 'v_anggaran_unit_tw_empat'; break;
				default:
					$tview = 'v_anggaran_unit_tw_satu';
					break;
			}

			if ($sisParams) {
				list($sisKey, $sisVal) = explode('=',$sisParams);
				if ($sisVal){
					$sParams['sis'] = $sisVal;		
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);
				if ($jnsVal){
					$sParams['jns'] = $jnsVal;		
					// $conditions = array_merge($conditions, ['jenis_belanja' => $sParams['jns']]);
				}
			}
			
			if ($sbrParams) {
				list($sbrKey, $sbrVal) = explode('=',$sbrParams);
				if ($sbrVal) {
					$sParams['sbr'] = $sbrVal;		
					$conditions = array_merge($conditions, ['ado' => 'ADO-'.$sParams['sbr']]);
				}
			}
			
			if ($alkParams) {
				list($alkKey, $alkVal) = explode('=',$alkParams);
				if ($alkVal) {
					$sParams['alk'] = $alkVal;	
					$conditions = array_merge($conditions, [$tview.'.jenis_belanja' => $sParams['alk']]);	
				}
			}

			if ($awParams) {
				list($awKey, $awVal) = explode('=',$awParams);
				if ($awVal) {
					$sParams['aw'] = $awVal;		
				}
			}

			if ($akParams) {
				list($akKey, $akVal) = explode('=',$akParams);
				if ($akVal) {
					$sParams['ak'] = $akVal;		
				}
			}
		}

		// print_r($sParams);exit;


		$builder = $this->db->table($tview);

		$list = $builder->select($tview.'.*, t_coa_bidang.nama_akun as nama_bidang, t_coa_kegiatan.nama_akun as nama_kegiatan, t_coa_prodi_kk.nama_akun as nama_prodi_kk')
				// ->table('v_anggaran_unit_tw_satu')
				->join('t_coa_bidang', 't_coa_bidang.kode_akun = '.$tview.'.coa_bidang', 'left')
				->join('t_coa_kegiatan', 't_coa_kegiatan.kode_akun = '.$tview.'.coa_kegiatan', 'left')
				->join('t_coa_prodi_kk', 't_coa_prodi_kk.kode_akun = '.$tview.'.coa_prodi_kk', 'left')
                ->where($conditions)
                ->get()
                // ->getCompiledSelect();
                ->getResultArray();

                // echo $list;exit;
                

        $builder_rka = $this->db->table('v_anggaran_unit_rka');
        $list_rka = $builder_rka->where(['thn_anggaran' => $this->year])->get()->getResultArray();

        $builder_realokasi = $this->db->table('v_anggaran_unit_realokasi_kerma');
        $list_realokasi = $builder_realokasi->select('v_anggaran_unit_realokasi_kerma.*, t_coa_prodi_kk.nama_akun as unit')
							        		->join('t_coa_prodi_kk', 't_coa_prodi_kk.kode_akun = v_anggaran_unit_realokasi_kerma.coa_prodi_kk', 'left')
							        		->where(['thn_anggaran' => $this->year])->get()->getResultArray();


		$builder_pros = $this->db->table('t_anggaran_unit');
		$list_pros_target = $builder_pros->select('t_anggaran_unit.*')
							        		->where(['thn_anggaran' => $this->year, 'jenis' => 'PROSPEKTIF_TARGET', 'deleted_at' => NULL])->get()->getResultArray();

		$list_pros_capaian = $builder_pros->select('t_anggaran_unit.*')
							        		->where(['thn_anggaran' => $this->year, 'jenis' => 'PROSPEKTIF', 'deleted_at' => NULL])->get()->getResultArray();


		$data = [
	      		'menu'     	=> 'data',
	      		'submenu'  	=> 'anggaran',
				'submenu2' 	=> $sParams['th'],
				'sParams' 	=> $sParams,
				'uriParams' => $uriParams,
				'list'		=> $list,
				'list_rka'	=> $list_rka,
				'list_realokasi'	=> $list_realokasi,
				'list_pros_target' => $list_pros_target,
				'list_pros_capaian' => $list_pros_capaian,
				'list_kerma_alihan' => [],
				'list_kerma_non_alihan' => [],
	    ];		
		
		return view('data/anggaran/index', $data);
	}


	public function api_data($anggaran)
	{
		$data = $this->anggaranModel->allAnggaran($anggaran);

		$totalData = count($data); 

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData,'recordsFiltered' => $totalData, 'data'=> $data ]);	
	}


	public function api_data_rka_sispran2()
	{
		$data = $this->anggaranModel->allRkaSispran2($this->realokasi, $this->prospektif, $this->penerimaan);

        $totalData = count($data); 

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData,'recordsFiltered' => $totalData, 'data'=> $data ]);	
	
	}


#----------------------------------------------------------
#
#	RKA SISPRAN 1
#
#----------------------------------------------------------

	public function rka_sispran1()
	{
		$data = [
      			'menu'     => 'data',
      			'submenu'  => 'anggaran',
				'submenu2' => 'rka',
				'submenu3' => 'sispran 1',
				'id_const' => $this->rka
    	];    	

		return view('data/anggaran/rka_sispran1', $data);
	}

	
	public function rka_new_sispran1() 
	{

		$data = [
      			'menu'         => 'data',
      			'submenu'      => 'anggaran',
				'submenu2'     => 'rka',
				'submenu3'     => 'sispran 1',
				'ko'           => $this->ko,
				'p3mi'         => $this->p3mi,
				'pegawai'      => $this->pegawai,
				'barang'       => $this->barang,
				'jasa'         => $this->jasa,
				'modal'        => $this->modal,
				'thn_anggaran' => $this->list_years,
				'this_year'    => $this->year
    	];


    	if($this->request->getMethod() == 'post')
		{      	          	    
			#access libraries
      	    helper(['form']); 

            #set rules
            $rules = [
                		'nominal'=> 'required|numeric',                		
            ];
            

            if ($this->validate($rules)) {
                
            	$id_const = $this->rka . $_POST['dana'] . $_POST['quota']; 
            	$thn_anggaran = $_POST['thn_anggaran'];
                
                #cek rka sudah pernah diinsert 
                $rkaExist     = $this->anggaranModel->checkRka($id_const, $thn_anggaran);
	       		     
				if ($rkaExist == false) { 
					
					$posts = [
			      			    'id_const'       => $id_const,
							    'nominal'        => $_POST['nominal'],
							    'tgl_pengajuan'  => $this->today,
							    'keterangan'     => $_POST['keterangan'],
							    'created_at'     => $this->now,
							    'updated_at'     => $this->now,
							    'thn_anggaran'   => $thn_anggaran					
					];

				    #saving process	
				    $this->anggaranModel->save($posts);

				    #go to another controller
				    return redirect()->to('rka_sispran1');
				}
				
				else {

					$data['rka_exist'] = 'RKA sudah ada';
				}

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }      	    
		}	

		return view('data/anggaran/rka_new_sispran1', $data);
	}


	public function rka_edit_sispran1($id)
	{
		$result   = $this->anggaranModel->findArray($id);
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);

			$data = [
      			        'menu'         => 'data',
      			        'submenu'      => 'anggaran',
				        'submenu2'     => 'rka',
				        'submenu3'     => 'sispran 1',				
				        'anggaran'     => $result,
				        'dana'	       => $dana,
				        'quota'        => $quota,
				        'ko'           => $this->ko,
						'p3mi'         => $this->p3mi,
						'pegawai'      => $this->pegawai,
						'barang'       => $this->barang,
						'jasa'         => $this->jasa,
						'modal'        => $this->modal,
						'thn_anggaran' => $this->list_years,				        
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {
				
		   	    $posts = [
							'nominal'        => $_POST['nominal'],							
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],							
							'created_at'     => $this->now,
							'updated_at'     => $this->now							
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/rka_sispran1'); 

            }  else {

                #pass the error
                $data['validation'] = $this->validator;        
            }		
		} 

		return view('data/anggaran/rka_edit_sispran1', $data);		
	}


	public function rka_delete_sispran1()
	{
		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/rka_sispran1');
	    }
	}


#----------------------------------------------------------
#
#	RKA SISPRAN 2
#
#----------------------------------------------------------

	public function rka_sispran2()
	{
		
		$data = [
      			   'menu'     => 'data',
      			   'submenu'  => 'anggaran',
				   'submenu2' => 'rka',
				   'submenu3' => 'sispran 2'
    	];

		return view('data/anggaran/rka_sispran2', $data);
	}


	public function rka_new_sispran2() 
	{
		$data = [
      			   'menu'                => 'data',
      			   'submenu'             => 'anggaran',
				   'submenu2'            => 'rka',
				   'submenu3'            => 'sispran 2',
				   'today'               => $this->today,
				   'realokasi'           => $this->realokasi,
				   'prospektif'          => $this->prospektif,
				   'penerimaan'          => $this->penerimaan,
				   'kermaAlihan'         => $this->kermaAlihan,
				   'kermaNonAlihan'      => $this->kermaNonAlihan,
				   'kelasInter'          => $this->kelasInter,	
				   'id_const'            => $this->realokasi,
				   'thn_anggaran'        => $this->list_years,
				   'this_year'           => $this->year				   
	  	];


    	if($this->request->getMethod() == 'post')
		{
      	   	#access libraries
      	    helper(['form']); 

            #set rules    
            $rules = [
                		'nominal'=> 'required|numeric',                		
            ];
            
            if ($this->validate($rules)) {

            	$id_const  = $_POST['jenis_rka'] . $_POST['dana'];
            	$jenis_rka = $_POST['jenis_rka'];

            	if (($jenis_rka == $this->realokasi)  or ($jenis_rka == $this->prospektif)){

            		$thn_anggaran = $_POST['thn_anggaran'];
            		
            		#cek rka
            		$rkaExist     = $this->anggaranModel->checkRka($id_const, $thn_anggaran);
	       		     
					if ($rkaExist == false) { 

						#insert realokasi / prospektif

						$posts = [
			      			    'id_const'       => $id_const,
							    'nominal'        => $_POST['nominal'],
							    'tgl_pengajuan'  => $this->today,
							    'keterangan'     => $_POST['keterangan'],
							    'created_at'     => $this->now,
							    'updated_at'     => $this->now,
							    'thn_anggaran'   => $thn_anggaran					
						];

				    	#saving process	
				    	$this->anggaranModel->save($posts);

				    	#go to another controller				    	
				    	return redirect()->to('rka_sispran2');   
					}

					else {

					    $data['rka_exist'] = 'RKA sudah ada';
				    
				    } #end rka exist
                 
            	} else if ($jenis_rka == $this->penerimaan){

            		#insert penerimaan
            		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

            		$posts = [
			      			    'id_const'       => $id_const,
							    'nominal'        => $_POST['nominal'],
							    'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							    'keterangan'     => $_POST['keterangan'],
							    'created_at'     => $this->now,
							    'updated_at'     => $this->now,
							    'thn_anggaran'   => $thn_anggaran			
						];

				    	#saving process	
				    	$this->anggaranModel->save($posts);

				    	#go to another controller
				    	return redirect()->to('rka_sispran2'); 

            	} #end jenis rka

            } else {

                #pass the error
                $data['validation'] = $this->validator;

            }
      	    
		}  #end method post	

		return view('data/anggaran/rka_new_sispran2', $data);
	}


	public function rka_edit_sispran2($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const  = $result['id_const'];
		$dana      = substr($id_const,4,2); 
		$jenis_rka = substr($id_const,0,4); 
		
			$data = [
      			        'menu'                => 'data',
      			   		'submenu'             => 'anggaran',
				  		'submenu2'            => 'rka',
				  		'submenu3'            => 'sispran 2',
				  		'anggaran'            => $result,
				 		'today'               => $this->today,
				 		'realokasi'           => $this->realokasi,
				 		'prospektif'          => $this->prospektif,
				 		'penerimaan'          => $this->penerimaan,
				 		'kermaAlihan'         => $this->kermaAlihan,
				 		'kermaNonAlihan'      => $this->kermaNonAlihan,
				 		'kelasInter'          => $this->kelasInter,					 		
				 		'thn_anggaran'        => $this->list_years,
				 		'this_year'           => $this->year,
				 		'dana'                => $dana,
				 		'jenis_rka'           => $jenis_rka				 		        
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) 
			{
			    $posts = [
							'nominal'        => $_POST['nominal'],							
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],							
							'created_at'     => $this->now,
							'updated_at'     => $this->now							
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/rka_sispran2'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator;        

            }
		} 

		return view('data/anggaran/rka_edit_sispran2', $data);			
	}


	public function rka_delete_sispran2()
	{
		if($this->request->getMethod() == 'post')
		{	    
			$id = $_POST['id'];

			$this->anggaranModel->delete($id);

			return redirect()->to('/data/anggaran/rka_sispran2');
	    }
	}


#----------------------------------------------------------
#
#	RI SISPRAN 1
#
#----------------------------------------------------------


	public function ri_sispran1()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'ri',
				    'submenu3' => 'sispran 1',				  
				    'id_const' => $this->ri
		];		

		return view('data/anggaran/ri_sispran1', $data);
	
	}


	public function ri_new_sispran1() 
	{

		$data = [
      			    'menu'     => 'data',
      			    'submenu'  => 'anggaran',
				    'submenu2' => 'ri',
				    'submenu3' => 'sispran 1',				    
				    'today'    => $this->today,
				    'ko'       => $this->ko,
					'p3mi'     => $this->p3mi,
					'pegawai'  => $this->pegawai,
					'barang'   => $this->barang,
					'jasa'     => $this->jasa,
					'modal'    => $this->modal
		];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];
            

            if ($this->validate($rules)) {
                
	       		$id_const     = $this->ri . $_POST['dana'] . $_POST['quota']; 
	       		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));
	      
				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
							
					     ];

				#saving process	
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('ri_sispran1');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/ri_new_sispran1', $data);

	}


	public function ri_edit_sispran1($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);

			$data = [
      			        'menu'      => 'data',
      			        'submenu'   => 'anggaran',
				        'submenu2'  => 'ri',
				        'submenu3'  => 'sispran 1',				
				        'anggaran'  => $result,
				        'dana'	    => $dana,
				        'quota'     => $quota,
				        'ko'        => $this->ko,
						'p3mi'      => $this->p3mi,
						'pegawai'   => $this->pegawai,
						'barang'    => $this->barang,
						'jasa'      => $this->jasa,
						'modal'     => $this->modal
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/ri_sispran1'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator;        

            }		

		} 

		return view('data/anggaran/ri_edit_sispran1', $data);		
	
	}



	public function ri_delete_sispran1()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/ri_sispran1');

	    }

	}	


#----------------------------------------------------------
#
#	RI SISPRAN 2
#
#----------------------------------------------------------

	
	public function ri_sispran2()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'ri',
				    'submenu3' => 'sispran 2',				  
				    'id_const' => $this->ri2
		];		

		return view('data/anggaran/ri_sispran2', $data);
	
	}


	public function ri_new_sispran2() 
	{

		$data = [
      			    'menu'           => 'data',
      			    'submenu'        => 'anggaran',
				    'submenu2'       => 'ri',
				    'submenu3'       => 'sispran 2',				    
				    'today'          => $this->today,
				    'kermaAlihan'    => $this->kermaAlihan,
				    'kermaNonAlihan' => $this->kermaNonAlihan,
				    'kelasInter'     => $this->kelasInter,	
					'pegawai'        => $this->pegawai,
					'barang'         => $this->barang,
					'jasa'           => $this->jasa,
					'modal'          => $this->modal,
					
					
		];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];
            

            if ($this->validate($rules)) {
                
	       		$id_const     = $this->ri2 . $_POST['dana'] . $_POST['quota']; 
	       		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));
	      
				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
							
					     ];

				#saving process	
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('ri_sispran2');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/ri_new_sispran2', $data);

	}


	public function ri_edit_sispran2($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);

			$data = [
      			        'menu'           => 'data',
      			        'submenu'        => 'anggaran',
				        'submenu2'       => 'ri',
				        'submenu3'       => 'sispran 2',				
				        'anggaran'       => $result,
				        'dana'	         => $dana,
				        'quota'          => $quota,
				        'kermaAlihan'    => $this->kermaAlihan,
						'kermaNonAlihan' => $this->kermaNonAlihan,
						'kelasInter'     => $this->kelasInter,
						'pegawai'        => $this->pegawai,
						'barang'         => $this->barang,
						'jasa'           => $this->jasa,
						'modal'          => $this->modal
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/ri_sispran2'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator;        

            }		

		} 

		return view('data/anggaran/ri_edit_sispran2', $data);		
	
	}



	public function ri_delete_sispran2()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/ri_sispran2');

	    }

	}	
	



#----------------------------------------------------------
#
#	FRA SISPRAN 1
#
#----------------------------------------------------------


    public function fra_sispran1()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'fra',
				    'submenu3' => 'sispran 1',		
				    'id_const' => $this->fra,
		];		
		
		return view('data/anggaran/fra_sispran1', $data);

	}



	public function fra_new_sispran1() 
	{

		$data = [
      			    'menu'     => 'data',
      			    'submenu'  => 'anggaran',
				    'submenu2' => 'fra',
				    'submenu3' => 'sispran 1',				    
				    'today'    => $this->today,
				    'ko'       => $this->ko,
					'p3mi'     => $this->p3mi,
					'pegawai'  => $this->pegawai,
					'barang'   => $this->barang,
					'jasa'     => $this->jasa,
					'modal'    => $this->modal
	   	];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];


            if ($this->validate($rules)) {
                
	       		$id_const     = $this->fra . $_POST['dana'] . $_POST['quota']; 
	       	    $thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));	      

				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
					     ];

				#saving process	- 		
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('fra_sispran1');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/fra_new_sispran1', $data);

	}


	public function fra_edit_sispran1($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);


			$data = [
      			        'menu'      => 'data',
      			        'submenu'   => 'anggaran',
				        'submenu2'  => 'fra',
				        'submenu3'  => 'sispran 1',				
				        'anggaran'  => $result,
				        'dana'	    => $dana,
				        'quota'     => $quota,
				        'ko'        => $this->ko,
						'p3mi'      => $this->p3mi,
						'pegawai'   => $this->pegawai,
						'barang'    => $this->barang,
						'jasa'      => $this->jasa,
						'modal'     => $this->modal
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		  'rules' => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran						
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/fra_sispran1'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator; 

            }		

		} 

		return view('data/anggaran/fra_edit_sispran1', $data);		
	
	}



	public function fra_delete_sispran1()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/fra_sispran1');

	    }

	}	



#----------------------------------------------------------
#
#	FRA SISPRAN 2
#
#----------------------------------------------------------


	public function fra_sispran2()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'fra',
				    'submenu3' => 'sispran 2',				  
				    'id_const' => $this->fra2
		];		

		return view('data/anggaran/fra_sispran2', $data);
	
	}


	public function fra_new_sispran2() 
	{

		$data = [
      			    'menu'           => 'data',
      			    'submenu'        => 'anggaran',
				    'submenu2'       => 'fra',
				    'submenu3'       => 'sispran 2',				    
				    'today'          => $this->today,
				    'kermaAlihan'    => $this->kermaAlihan,
				    'kermaNonAlihan' => $this->kermaNonAlihan,
				    'kelasInter'     => $this->kelasInter,	
					'pegawai'        => $this->pegawai,
					'barang'         => $this->barang,
					'jasa'           => $this->jasa,
					'modal'          => $this->modal
		];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];
            

            if ($this->validate($rules)) {
                
	       		$id_const     = $this->fra2 . $_POST['dana'] . $_POST['quota']; 
	       		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));
	      
				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
							
					     ];

				#saving process	
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('fra_sispran2');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/fra_new_sispran2', $data);

	}


	public function fra_edit_sispran2($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);

			$data = [
      			        'menu'           => 'data',
      			        'submenu'        => 'anggaran',
				        'submenu2'       => 'fra',
				        'submenu3'       => 'sispran 2',				
				        'anggaran'       => $result,
				        'dana'	         => $dana,
				        'quota'          => $quota,
				        'kermaAlihan'    => $this->kermaAlihan,
						'kermaNonAlihan' => $this->kermaNonAlihan,
						'kelasInter'     => $this->kelasInter,
						'pegawai'        => $this->pegawai,
						'barang'         => $this->barang,
						'jasa'           => $this->jasa,
						'modal'          => $this->modal
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/fra_sispran2'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator;
                $data['form-disabled'] = 'disabled';

            }		

		} 

		return view('data/anggaran/fra_edit_sispran2', $data);		
	
	}



	public function fra_delete_sispran2()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/fra_sispran2');

	    }

	}	



#----------------------------------------------------------
#
#	REALISASI SISPRAN 1
#
#----------------------------------------------------------

	
	public function real_sispran1()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'realisasi',
				    'submenu3' => 'sispran 1',		
				    'id_const' => $this->real,				    
		];		
		
		return view('data/anggaran/real_sispran1', $data);

	}



	public function real_new_sispran1() 
	{

		$data = [
      			    'menu'     => 'data',
      			    'submenu'  => 'anggaran',
				    'submenu2' => 'realisasi',
				    'submenu3' => 'sispran 1',				    
				    'today'    => $this->today,
				    'ko'       => $this->ko,
					'p3mi'     => $this->p3mi,
					'pegawai'  => $this->pegawai,
					'barang'   => $this->barang,
					'jasa'     => $this->jasa,
					'modal'    => $this->modal
	   	];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];


            if ($this->validate($rules)) {
                
	       		$id_const     = $this->real . $_POST['dana'] . $_POST['quota']; 
	       		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));
	      
				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
					     ];

				#saving process	- 		
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('real_sispran1');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/real_new_sispran1', $data);

	}


	public function real_edit_sispran1($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);


			$data = [
      			        'menu'      => 'data',
      			        'submenu'   => 'anggaran',
				        'submenu2'  => 'real',
				        'submenu3'  => 'sispran 1',				
				        'anggaran'  => $result,
				        'dana'	    => $dana,
				        'quota'     => $quota,
				        'ko'        => $this->ko,
						'p3mi'      => $this->p3mi,
						'pegawai'   => $this->pegawai,
						'barang'    => $this->barang,
						'jasa'      => $this->jasa,
						'modal'     => $this->modal
				        
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		  'rules' => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran

			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/real_sispran1'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator; 
                           	 
            }		

		} 

		return view('data/anggaran/real_edit_sispran1', $data);		
	
	}



	public function real_delete_sispran1()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/real_sispran1');

	    }

	}	



#----------------------------------------------------------
#
#	REALISASI SISPRAN 2
#
#----------------------------------------------------------


	public function real_sispran2()
	{
		
		$data = [
			        'menu'     => 'data',
			        'submenu'  => 'anggaran',
				    'submenu2' => 'realisasi',
				    'submenu3' => 'sispran 2',				  
				    'id_const' => $this->real2
		];		

		return view('data/anggaran/real_sispran2', $data);
	
	}


	public function real_new_sispran2() 
	{

		$data = [
      			    'menu'           => 'data',
      			    'submenu'        => 'anggaran',
				    'submenu2'       => 'realisasi',
				    'submenu3'       => 'sispran 2',				    
				    'today'          => $this->today,
				    'kermaAlihan'    => $this->kermaAlihan,
				    'kermaNonAlihan' => $this->kermaNonAlihan,
				    'kelasInter'     => $this->kelasInter,	
					'pegawai'        => $this->pegawai,
					'barang'         => $this->barang,
					'jasa'           => $this->jasa,
					'modal'          => $this->modal
		];


    	if($this->request->getMethod() == 'post')
		{
      	    
      	    #set rules    
      	    helper(['form']); #access libraries

            $rules = [
                		'nominal'=> 'required|numeric',                
            ];
            

            if ($this->validate($rules)) {
                
	       		$id_const     = $this->real2 . $_POST['dana'] . $_POST['quota']; 
	       		$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));
	      
				$posts = [
			      			'id_const'       => $id_const,
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
							
					     ];

				#saving process	
				$this->anggaranModel->save($posts);

				#go to another controller
				return redirect()->to('real_sispran2');     

            } else {

                #pass the error
                $data['validation'] = $this->validator;
            }
      	    
		}	

		return view('data/anggaran/real_new_sispran2', $data);

	}


	public function real_edit_sispran2($id)
	{

		$result = $this->anggaranModel->findArray($id);
		
		$id_const = $result['id_const'];
		$dana     = substr($id_const,4,-2); 
		$quota    = substr($id_const,6);

			$data = [
      			        'menu'           => 'data',
      			        'submenu'        => 'anggaran',
				        'submenu2'       => 'realisasi',
				        'submenu3'       => 'sispran 2',				
				        'anggaran'       => $result,
				        'dana'	         => $dana,
				        'quota'          => $quota,
				        'kermaAlihan'    => $this->kermaAlihan,
						'kermaNonAlihan' => $this->kermaNonAlihan,
						'kelasInter'     => $this->kelasInter,
						'pegawai'        => $this->pegawai,
						'barang'         => $this->barang,
						'jasa'           => $this->jasa,
						'modal'          => $this->modal
			];		
	
					
		if($this->request->getMethod() == 'post')
		{
			  
		    helper(['form']); #access libraries
		        
		    #set rules  
		    $rules = [
		                 	'nominal' => [ 
				      		'rules'   => 'required|numeric',				      		
				     ]		
		    ];


			if ($this->validate($rules)) {

				$thn_anggaran = date('Y', strtotime($_POST['tgl_pengajuan']));

		   	    $posts = [
							'nominal'        => $_POST['nominal'],
							'tgl_pengajuan'  => $_POST['tgl_pengajuan'],
							'keterangan'     => $_POST['keterangan'],
							'id_anggaran'    => $_POST['id_anggaran'],
							'created_at'     => $this->now,
							'updated_at'     => $this->now,
							'thn_anggaran'   => $thn_anggaran
			    ];

				$this->anggaranModel->update($posts);

				#go to another controller
				return redirect()->to('/data/anggaran/real_sispran2'); 


            }  else {

                #pass the error
                $data['validation'] = $this->validator;        

            }		

		} 

		return view('data/anggaran/real_edit_sispran2', $data);		
	
	}



	public function real_delete_sispran2()
	{

		if($this->request->getMethod() == 'post')
		{	    
		
			$id = $_POST['id'];
			$this->anggaranModel->delete($id);

			#go to another controller
			return redirect()->to('/data/anggaran/real_sispran2');

	    }

	}

	public function rka_sispran1_upform()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2' => 'rka',
			'submenu3' => 'sispran 1',
			'id_const' => $this->rka,

		];    	

  		return view('data/anggaran/rka_sispran1_upform', $data);
	}

	public function rka_sispran1_upload()
	{
		$rules = [
			'anggaran_file' => 'uploaded[anggaran_file]|ext_in[anggaran_file,xls,xlsx]|max_size[anggaran_file,1000]',
		];

		$errors = [
			'anggaran_file' => [
				'ext_in'    => 'File Excel hanya boleh diisi dengan xls atau xlsx.',
				'max_size'  => 'File Excel anggaran maksimal 1mb',
				'uploaded'  => 'File Excel anggaran wajib diisi'
			]
		];
	
		// $data = array(
		// 	'upload_file' => $file,
		// );
		
		if (!$this->validate($rules, $errors))
		{
			$validation =  \Config\Services::validation();

			session()->setFlashdata('errors', $validation->getErrors());
			return redirect()->to(base_url('data/anggaran/rka_sispran1_upform'));
		} 
		else 
		{
			$file = $this->request->getFile('anggaran_file');

			// ambil extension dari file excel
			$extension = $file->getClientExtension();
			
			// format excel 2007 ke bawah
			if('xls' == $extension){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			// format excel 2010 ke atas
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}
			
			$spreadsheet = $reader->load($file);
			$data = $spreadsheet->getActiveSheet()->toArray();
	
			foreach($data as $idx => $row){
				
				// lewati baris ke 0 pada file excel
				// dalam kasus ini, array ke 0 adalahpara title
				if($idx == 0) {
					continue;
				}
				
				// get data
				$nomor = $row[0]; // get nomor from excel
				$id_const = $row[1]; 
				$tgl_pengajuan = $row[2]; 
				$nominal = $row[3]; 
				$keterangan = $row[4];
				
				$data = [
					"id_const"    	=> (string)$id_const,
					"tgl_pengajuan" => date('Y-m-d', strtotime($tgl_pengajuan)),
					"nominal"     	=> $nominal,
					"keterangan" 	=> $keterangan,
					"thn_anggaran" 	=> date('Y', strtotime($tgl_pengajuan)),
					'created_at'     => $this->now,
					'updated_at'     => $this->now,
				];

				// var_dump($data);exit;
	
				$simpan = $this->anggaranModel->save($data);
			}
	
			session()->setFlashdata('success', 'Upload Data berhasil');
			return redirect()->to(base_url('data/anggaran/rka_sispran1')); 
		}
	}

	public function upload_anggaran()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2'  => 'ri'

		];    	

  		return view('data/anggaran/upload_anggaran', $data);
	}

	public function upload_rka()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2'  => 'rka'

		];    	

  		return view('data/anggaran/upload_rka', $data);
	}

	public function upload_realokasi_kerma()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2'  => 'realokasi_kerma'

		];    	

  		return view('data/anggaran/upload_realokasi_kerma', $data);
	}

	public function upload_prospektif()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2'  => 'upload_prospektif'

		];    	

  		return view('data/anggaran/upload_prospektif', $data);
	}

	public function upload_kerma_alihan()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'anggaran',
			'submenu2'  => 'kerma_alihan'

		];    	

  		return view('data/anggaran/upload_kerma_alihan', $data);
	}

	// public function save_upload_anggaran()
	// {
	// 	$tahun = $this->request->getVar('tahun');
	// 	$sispran = $this->request->getVar('sispran');
	// 	$triwulan = $this->request->getVar('triwulan');
	// 	$jenis_belanja = $this->request->getVar('jenis_belanja');

	// 	#transaction
	// 	$this->db = db_connect(); 
	// 	$this->db->transStart();

	// 	$file = $this->request->getFile('file_anggaran');

	// 	// ambil extension dari file excel
	// 	$extension = $file->getClientExtension();
		
	// 	// format excel 2007 ke bawah
	// 	if('xls' == $extension){
	// 		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
	// 	// format excel 2010 ke atas
	// 	} else {
	// 		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	// 	}
		
	// 	$spreadsheet = $reader->load($file);

	// 	$cntSheet = $spreadsheet->getSheetCount();

	// 	for ($i=1; $i <= $cntSheet; $i++) { 			
	// 		switch ($i) {
	// 			case '2':
	// 				$ri_1 = '010202';
	// 				$fra_1 = '010302';
	// 				$real_1 = '010402';
	// 				break;
	// 			case '3':
	// 				$ri_1 = '020202';
	// 				$fra_1 = '020302';
	// 				$real_1 = '020402';
	// 				break;
	// 			default:
	// 				$ri_1 = '010201';
	// 				$fra_1 = '010301';
	// 				$real_1 = '010401';
	// 				break;
	// 		}

	// 		$spreadsheet->setActiveSheetIndex($i-1);

	// 		$data = $spreadsheet->getActiveSheet()->toArray();
	// 		foreach($data as $idx => $row){
			
	// 			// lewati baris ke 0 pada file excel
	// 			// dalam kasus ini, array ke 0 adalahpara title
	// 			if($idx < 3) {
	// 				continue;
	// 			}
				
	// 			if ($sispran == '02') {
	// 				// get data from excel
	// 				$ri_1 = '020201';
	// 				$fra_1 = '020301';
	// 				$real_1 = '020401';

	// 				$data = [
	// 					'nomor' => $row[0],  
	// 					'uraian' => $row[1],  
	// 					'kode_file' => $row[2],  
	// 					'pagu' => $row[3],  
	// 					'ri_bln_1' => str_replace(',', '', $row[4]),  
	// 					'ri_bln_2' => str_replace(',', '', $row[5]), 
	// 					'ri_bln_3' => str_replace(',', '', $row[6]), 
	// 					'ri_jml' => str_replace(',', '', $row[7]),
	// 					'fra_unit_bln_1' => str_replace(',', '', $row[8]),  
	// 					'fra_log_bln_1' => str_replace(',', '', $row[9]), 
	// 					'fra_unit_bln_2' => str_replace(',', '', $row[10]), 
	// 					'fra_log_bln_2' => str_replace(',', '', $row[11]), 
	// 					'fra_unit_bln_3' => str_replace(',', '', $row[12]), 
	// 					'fra_log_bln_3' => str_replace(',', '', $row[13]), 
	// 					'fra_jml' => str_replace(',', '', $row[14]),
	// 					'real_unit_bln_1' => str_replace(',', '', $row[15]), 
	// 					'real_log_bln_1' => str_replace(',', '', $row[16]), 
	// 					'real_unit_bln_2' => str_replace(',', '', $row[17]), 
	// 					'real_log_bln_2' => str_replace(',', '', $row[18]), 
	// 					'real_unit_bln_3' => str_replace(',', '', $row[19]), 
	// 					'real_log_bln_3' => str_replace(',', '', $row[20]), 
	// 					'real_jml' => str_replace(',', '', $row[21])
	// 				];
	// 			} else {
	// 				// get data from excel
	// 				$data = [
	// 					'nomor' => $row[0],  
	// 					'uraian' => $row[1],  
	// 					'ri_bln_1' => str_replace(',', '', $row[2]),  
	// 					'ri_bln_2' => str_replace(',', '', $row[3]), 
	// 					'ri_bln_3' => str_replace(',', '', $row[4]), 
	// 					'ri_jml' => str_replace(',', '', $row[5]),
	// 					'fra_unit_bln_1' => str_replace(',', '', $row[6]),  
	// 					'fra_log_bln_1' => str_replace(',', '', $row[7]), 
	// 					'fra_unit_bln_2' => str_replace(',', '', $row[8]), 
	// 					'fra_log_bln_2' => str_replace(',', '', $row[9]), 
	// 					'fra_unit_bln_3' => str_replace(',', '', $row[10]), 
	// 					'fra_log_bln_3' => str_replace(',', '', $row[11]), 
	// 					'fra_jml' => str_replace(',', '', $row[12]),
	// 					'real_unit_bln_1' => str_replace(',', '', $row[13]), 
	// 					'real_log_bln_1' => str_replace(',', '', $row[14]), 
	// 					'real_unit_bln_2' => str_replace(',', '', $row[15]), 
	// 					'real_log_bln_2' => str_replace(',', '', $row[16]), 
	// 					'real_unit_bln_3' => str_replace(',', '', $row[17]), 
	// 					'real_log_bln_3' => str_replace(',', '', $row[18]), 
	// 					'real_jml' => str_replace(',', '', $row[19]),
	// 					'kode_file' => ''
	// 				];
	// 			}
				
	// 			// print_r($data); exit;

	// 			// RI bln-1
	// 			if ($data['ri_bln_1'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $ri_1.$jenis_belanja,
	// 					'nominal' => $data['ri_bln_1'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+1,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);	
	// 			}				

	// 			// RI bln-2
	// 			if ($data['ri_bln_2'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $ri_1.$jenis_belanja,
	// 					'nominal' => $data['ri_bln_2'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+2,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// RI bln-3
	// 			if ($data['ri_bln_3'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $ri_1.$jenis_belanja,
	// 					'nominal' => $data['ri_bln_3'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+3,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// FRA unit bln-1
	// 			if ($data['fra_unit_bln_1'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_unit_bln_1'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+1,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}
				

	// 			// FRA unit bln-2
	// 			if ($data['fra_unit_bln_2'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_unit_bln_2'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+2,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// FRA unit bln-3
	// 			if ($data['fra_unit_bln_3'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_unit_bln_3'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+3,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}
				

	// 			// FRA log bln-1
	// 			if ($data['fra_log_bln_1'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_log_bln_1'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+1,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}
				

	// 			// FRA log bln-2
	// 			if ($data['fra_log_bln_2'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_unit_bln_2'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+2,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}

	// 			// FRA log bln-3
	// 			if ($data['fra_log_bln_3'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $fra_1.$jenis_belanja,
	// 					'nominal' => $data['fra_log_bln_3'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+3,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// ---
	// 			// Realisasi unit bln-1
	// 			if ($data['real_unit_bln_1'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_unit_bln_1'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+1,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// Realisasi unit bln-2
	// 			if ($data['real_unit_bln_2'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_unit_bln_2'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+2,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}
				

	// 			// Realisasi unit bln-3
	// 			if ($data['real_unit_bln_3'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_unit_bln_3'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+3,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 0,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}

	// 			// Realisasi log bln-1
	// 			if ($data['real_log_bln_1'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_log_bln_1'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+1,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}

	// 			// Realisasi log bln-2
	// 			if ($data['real_log_bln_2'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_unit_bln_2'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+2,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}				

	// 			// Realisasi log bln-3
	// 			if ($data['real_log_bln_3'] > 0) {
	// 				$this->anggaranModel->save([
	// 					'id_const' => $real_1.$jenis_belanja,
	// 					'nominal' => $data['real_log_bln_3'],
	// 					'keterangan' => $data['uraian'],
	// 					'thn_anggaran' => $tahun,
	// 					'bulan' => 3*($triwulan-1)+3,
	// 					'triwulan' => $triwulan,
	// 					'is_logistik' => 1,
	// 					'kode_file' => $data['kode_file']
	// 				]);
	// 			}
	// 		}
	// 	}
		

	// 	#end transaction
	// 	$this->db->transComplete();

	// 	session()->setFlashdata('success', 'Data berhasil ditambahkan');

	// 	return redirect()->to(base_url('/data/anggaran'));
	// }

	public function save_upload_anggaran()
	{
		$tahun = $this->request->getVar('tahun');
		$sispran = $this->request->getVar('sispran');
		$triwulan = $this->request->getVar('triwulan');
		$jenis_belanja = $this->request->getVar('jenis_belanja');

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		//delete old
		$this->anggaranunitModel->where(['thn_anggaran' => $tahun, 'sispran' => $sispran, 'triwulan' => $triwulan, 'jenis_belanja' => $jenis_belanja])->delete();

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		$arrSumberDana = ['PENUGASAN', 'KERMA ALIHAN', 'KERMA NON ALIHAN'];

		for ($i=1; $i <= $cntSheet; $i++) { 			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$ado = $spreadsheet->getSheetNames();


			$data = $spreadsheet->getActiveSheet()->toArray();
			foreach($data as $idx => $row){
			
				if($idx < 2) {
					continue;
				}

				if ($ado[$i-1] == 'PENUGASAN' || $ado[$i-1] == 'KERMA ALIHAN' || $ado[$i-1] == 'KERMA NON ALIHAN') {
					$data = [
						'nomor' => $row[0],  
						'coa_kegiatan' => $row[1],  
						'coa_prodi_kk' => $row[4],  
						'kode_file' => $row[8], 					
						'coa_bidang' => $row[9], 					
						'ri_1' => str_replace(',', '', $row[11]), 
						'ri_2' => str_replace(',', '', $row[12]), 
						'ri_3' => str_replace(',', '', $row[13]), 
						'real_1' => str_replace(',', '', $row[15]), 
						'real_2' => str_replace(',', '', $row[16]), 
						'real_3' => str_replace(',', '', $row[17]), 
					];	
				} else {
					$data = [
						'nomor' => $row[0],  
						'coa_kegiatan' => $row[1],  
						'coa_prodi_kk' => $row[4],  
						'coa_bidang' => $row[8], 					
						'ri_1' => str_replace(',', '', $row[10]), 
						'ri_2' => str_replace(',', '', $row[11]), 
						'ri_3' => str_replace(',', '', $row[12]), 
						'real_1' => str_replace(',', '', $row[14]), 
						'real_2' => str_replace(',', '', $row[15]), 
						'real_3' => str_replace(',', '', $row[16]), 
					];
				}
				
				

				// print_r($data);exit;
				
				//ri
				for ($j=1; $j <= 3; $j++) { 
					if ($data['ri_'.$j] > 0) {
						$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'bulan' => 3*($triwulan-1)+$j,
							'triwulan' => $triwulan,
							'jenis' => 'RI',
							'coa_kegiatan' => $data['coa_kegiatan'],
							'coa_prodi_kk' => $data['coa_prodi_kk'],
							'coa_bidang' => $data['coa_bidang'],
							'jenis_belanja' => $jenis_belanja,
							'sispran' => $sispran,
							'nominal' => $data['ri_'.$j],
							'logistik' => 0,
							'kode_file' => '',
							'ado' => 'ADO-'.$ado[$i-1]
						]);		
					}					
				}

				//realisasi
				for ($k=1; $k <= 3; $k++) { 
					if ($data['real_'.$k] > 0) {
						$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'bulan' => 3*($triwulan-1)+$k,
							'triwulan' => $triwulan,
							'jenis' => 'REALISASI',
							'coa_kegiatan' => $data['coa_kegiatan'],
							'coa_prodi_kk' => $data['coa_prodi_kk'],
							'coa_bidang' => $data['coa_bidang'],
							'jenis_belanja' => $jenis_belanja,
							'sispran' => $sispran,
							'nominal' => $data['real_'.$k],
							'logistik' => 0,
							'kode_file' => '',
							'ado' => 'ADO-'.$ado[$i-1]
						]);		
					}
				}
			}
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/anggaran'));
	}

	public function save_upload_rka()
	{
		$tahun = $this->request->getVar('tahun');
		
		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		$jenis = $this->request->getVar('jenis');

		//delete old
		$this->anggaranunitModel->where(['thn_anggaran' => $tahun, 'jenis' => $jenis])->delete();	

		

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		for ($i=1; $i <= $cntSheet; $i++) { 			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$ado = $spreadsheet->getSheetNames();


			$data = $spreadsheet->getActiveSheet()->toArray();
			foreach($data as $idx => $row){
			
				if($idx < 2 && $row[0] > 0) {
					continue;
				}
				
				$data = [
					'nomor' => $row[0],  
					'program' => $row[1],  
					'ado' => $row[2],  
					'pegawai' => str_replace(',', '', $row[3]), 
					'barang' => str_replace(',', '', $row[4]), 
					'jasa' => str_replace(',', '', $row[5]), 
					'modal' => str_replace(',', '', $row[6]),
					'keterangan' => $row[8],
				];

				// print_r($data);exit;

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jenis,
							'program' => $data['program'],
							'ado' => $data['ado'],
							'jenis_belanja' => '01',
							'nominal' => $data['pegawai'],
							'keterangan' => $data['keterangan']
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jenis,
							'program' => $data['program'],
							'ado' => $data['ado'],
							'jenis_belanja' => '02',
							'nominal' => $data['barang'],
							'keterangan' => $data['keterangan']
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jenis,
							'program' => $data['program'],
							'ado' => $data['ado'],
							'jenis_belanja' => '03',
							'nominal' => $data['jasa'],
							'keterangan' => $data['keterangan']
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jenis,
							'program' => $data['program'],
							'ado' => $data['ado'],
							'jenis_belanja' => '04',
							'nominal' => $data['modal'],
							'keterangan' => $data['keterangan']
						]);	

			}
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/anggaran'));
	}

	public function save_upload_realokasi_kerma()
	{
		$tahun = $this->request->getVar('tahun');
		
		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		//delete old
		$this->anggaranunitModel->where(['thn_anggaran' => $tahun, 'jenis' => 'REALOKASI_KERMA'])->delete();

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		for ($i=1; $i <= $cntSheet; $i++) { 			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$ado = $spreadsheet->getSheetNames();


			$data = $spreadsheet->getActiveSheet()->toArray();
			foreach($data as $idx => $row){
			
				if($idx < 2) {
					continue;
				}
				
				$data = [
					'nomor' => $row[0],  
					'kode_file' => $row[1],  
					'unit' => $row[2],  
					'pelaksana' => $row[3],  
					'judul' => $row[4],  
					'mitra' => $row[5],  
					'kontrak' => str_replace(',', '', $row[6]), 
					'pegawai' => str_replace(',', '', $row[7]), 
					'barang' => str_replace(',', '', $row[8]), 
					'jasa' => str_replace(',', '', $row[9]), 
					'modal' => str_replace(',', '', $row[10])
				];

				// print_r($data);exit;

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => 'REALOKASI_KERMA',
							'kode_file' => $data['kode_file'],  
							'coa_prodi_kk' => $data['unit'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'kontrak' =>$data['kontrak'], 
							'jenis_belanja' => '01',
							'nominal' => $data['pegawai'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => 'REALOKASI_KERMA',
							'kode_file' => $data['kode_file'],  
							'coa_prodi_kk' => $data['unit'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'kontrak' =>$data['kontrak'], 
							'jenis_belanja' => '02',
							'nominal' => $data['barang'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => 'REALOKASI_KERMA',
							'kode_file' => $data['kode_file'],  
							'coa_prodi_kk' => $data['unit'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'kontrak' =>$data['kontrak'], 
							'jenis_belanja' => '03',
							'nominal' => $data['jasa'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => 'REALOKASI_KERMA',
							'kode_file' => $data['kode_file'],  
							'coa_prodi_kk' => $data['unit'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'kontrak' =>$data['kontrak'], 
							'jenis_belanja' => '04',
							'nominal' => $data['modal'] 
						]);	

			}
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/anggaran'));
	}

	public function save_upload_kerma_alihan()
	{
		$tahun = $this->request->getVar('tahun');
		$jns_kerma = $this->request->getVar('jenis_kerma');

		// echo $jns_kerma;exit;
		
		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		//delete old
		$this->anggaranunitModel->where(['thn_anggaran' => $tahun, 'jenis' => $jns_kerma])->delete();

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		for ($i=1; $i <= $cntSheet; $i++) { 			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$ado = $spreadsheet->getSheetNames();


			$data = $spreadsheet->getActiveSheet()->toArray();
			foreach($data as $idx => $row){
			
				if($idx < 2) {
					continue;
				}

				$data = [
					'nomor' => $row[0],  
					'kode_file' => $row[1],  
					'judul' => $row[2],
					'keterangan' => $row[3],  
					'pelaksana' => $row[4],  
					'mitra' => $row[5],  
					'pegawai' => str_replace(',', '', $row[6]), 
					'barang' => str_replace(',', '', $row[7]), 
					'jasa' => str_replace(',', '', $row[8]), 
					'modal' => str_replace(',', '', $row[9])
				];

				// print_r($data);exit;

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jns_kerma,
							'kode_file' => $data['kode_file'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'jenis_belanja' => '01',
							'nominal' => $data['pegawai'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jns_kerma,
							'kode_file' => $data['kode_file'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'jenis_belanja' => '02',
							'nominal' => $data['barang'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jns_kerma,
							'kode_file' => $data['kode_file'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'jenis_belanja' => '03',
							'nominal' => $data['jasa'] 
						]);	

				$this->anggaranunitModel->save([
							'thn_anggaran' => $tahun,
							'jenis' => $jns_kerma,
							'kode_file' => $data['kode_file'],  
							'pelaksana' => $data['pelaksana'],  
							'keterangan' => $data['judul'],  
							'mitra' => $data['mitra'],  
							'jenis_belanja' => '04',
							'nominal' => $data['modal'] 
						]);	

			}
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/anggaran'));
	}

	public function save_upload_prospektif()
	{
		$tahun = $this->request->getVar('tahun');
		$jns_prospektif = $this->request->getVar('jenis_prospektif');
		
		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		//delete old
		$this->anggaranunitModel->where(['thn_anggaran' => $tahun, 'jenis' => $jns_prospektif])->delete();

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		for ($i=1; $i <= $cntSheet; $i++) { 			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$ado = $spreadsheet->getSheetNames();


			$data = $spreadsheet->getActiveSheet()->toArray();
			foreach($data as $idx => $row){
					

				if ($jns_prospektif == 'PROSPEKTIF') {
					if($idx < 2) {
						continue;
					}

					$data = [
						'nomor' => $row[0],  
						'kode_file' => $row[1],  
						'judul' => $row[2],
						'keterangan' => $row[3],  
						'pelaksana' => $row[4],  
						'mitra' => $row[5],  
						'kontrak' => str_replace(',', '', $row[6]), 
						'ppn' => str_replace(',', '', $row[7]), 
						'pph23' => str_replace(',', '', $row[8]), 
						'biaya_lain' => str_replace(',', '', $row[9]), 
						'dana_diterima' => str_replace(',', '', $row[10]),
						'ops_ftmd' => str_replace(',', '', $row[11]),
						'dpi_itb' => str_replace(',', '', $row[12]),
						'dpi_bpp' => str_replace(',', '', $row[13])

					];

					// print_r($data);exit;

					$this->anggaranunitModel->save([
								'thn_anggaran' => $tahun,
								'jenis' => $jns_prospektif,
								'kode_file' => $data['kode_file'],  
								'judul' => $data['judul'],
								'keterangan' => $data['keterangan'],  
								'pelaksana' => $data['pelaksana'],  
								'mitra' => $data['mitra'],  
								'kontrak' => $data['kontrak'], 
								'ppn' => $data['ppn'], 
								'pph23' => $data['pph23'], 
								'biaya_lain' => $data['biaya_lain'], 
								'dana_diterima' => $data['dana_diterima'], 
								'ops_ftmd' => $data['ops_ftmd'], 
								'dpi_itb' => $data['dpi_itb'], 
								'dpi_bpp' => $data['dpi_bpp'] 
							]);	
				} else {

					if($idx < 1) {
						continue;
					}

					$data = [
						'nomor' => $row[0],  
						'judul' => $row[1],
						'mitra' => $row[2],  
						'kontrak' => str_replace(',', '', $row[3])
					];

					// print_r($data);exit;

					$this->anggaranunitModel->save([
								'thn_anggaran' => $tahun,
								'jenis' => $jns_prospektif,
								'judul' => $data['judul'],
								'mitra' => $data['mitra'],  
								'kontrak' => $data['kontrak']
							]);	
				}
				
				

			}
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/anggaran'));
	}

	public function api_data_all()
	{
		$conditions = [];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$sisParams =  $uri->getQuery(['only' => ['sis']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$sbrParams =  $uri->getQuery(['only' => ['sbr']]);
			$alkParams =  $uri->getQuery(['only' => ['alk']]);
			$awParams =  $uri->getQuery(['only' => ['aw']]);
			$akParams =  $uri->getQuery(['only' => ['ak']]);
			$thParams =  $uri->getQuery(['only' => ['th']]);

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['t_anggaran_unit.thn_anggaran' => $thVal]);				
				}
			}

			if ($sisParams) {
				list($sisKey, $sisVal) = explode('=',$sisParams);	
				if ($sisVal){
					$conditions = array_merge($conditions, ['t_anggaran_unit.sispran)' => $sisVal]);				
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);
				if ($jnsVal){
					$conditions = array_merge($conditions, ['t_anggaran_unit.jenis' => $jnsVal]);
				}
			}			
			
			if ($sbrParams) {
				list($sbrKey, $sbrVal) = explode('=',$sbrParams);
				if ($sbrVal) {
					$conditions = array_merge($conditions, ['t_anggaran_unit.ado' => $sbrVal]);
				}
			}	
			
			if ($alkParams) {
				list($alkKey, $alkVal) = explode('=',$alkParams);
				if ($alkVal) {
					$conditions = array_merge($conditions, ['t_anggaran_unit.jenis_belanja' => $alkVal]);
				}
			}

			if ($akParams) {
				list($akKey, $akVal) = explode('=',$akParams);
				if ($akVal) {
					$conditions = array_merge($conditions, ['bulan >=' => 1, 'bulan <=' => $akVal]);
				}
			}		
			
		}

		$data = $this->anggaranunitModel->getAll($conditions);

		$totalData = count($data); 

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData,'recordsFiltered' => $totalData, 'data'=> $data ]);	
	}

	// public function api_data_all()
	// {
	// 	$conditions = [];

	// 	$uri = service('uri');
	// 	$uriParams = $uri->getQuery();
	// 	if ($uriParams) {
	// 		$sisParams =  $uri->getQuery(['only' => ['sis']]);
	// 		$jnsParams =  $uri->getQuery(['only' => ['jns']]);
	// 		$sbrParams =  $uri->getQuery(['only' => ['sbr']]);
	// 		$alkParams =  $uri->getQuery(['only' => ['alk']]);
	// 		$awParams =  $uri->getQuery(['only' => ['aw']]);
	// 		$akParams =  $uri->getQuery(['only' => ['ak']]);
	// 		$thParams =  $uri->getQuery(['only' => ['th']]);

	// 		if ($thParams) {
	// 			list($thKey, $thVal) = explode('=',$thParams);	
	// 			if ($thVal){
	// 				$conditions = array_merge($conditions, ['t_anggaran.thn_anggaran' => $thVal]);				
	// 			}
	// 		}

	// 		if ($sisParams) {
	// 			list($sisKey, $sisVal) = explode('=',$sisParams);	
	// 			if ($sisVal){
	// 				$conditions = array_merge($conditions, ['substr(t_anggaran.id_const,1,2)' => $sisVal]);				
	// 			}
	// 		}

	// 		if ($jnsParams) {
	// 			list($jnsKey, $jnsVal) = explode('=',$jnsParams);
	// 			if ($jnsVal){
	// 				$conditions = array_merge($conditions, ['substr(t_anggaran.id_const,3,2)' => $jnsVal]);
	// 			}
	// 		}			
			
	// 		if ($sbrParams) {
	// 			list($sbrKey, $sbrVal) = explode('=',$sbrParams);
	// 			if ($sbrVal) {
	// 				$conditions = array_merge($conditions, ['substr(t_anggaran.id_const,5,2)' => $sbrVal]);
	// 			}
	// 		}	
			
	// 		if ($alkParams) {
	// 			list($alkKey, $alkVal) = explode('=',$alkParams);
	// 			if ($alkVal) {
	// 				$conditions = array_merge($conditions, ['substr(t_anggaran.id_const,7,2)' => $alkVal]);
	// 			}
	// 		}

	// 		if ($awParams && $akParams) {
	// 			list($awKey, $awVal) = explode('=',$awParams);
	// 			list($akKey, $akVal) = explode('=',$akParams);

	// 			if ($awVal && $akVal) {
	// 				$conditions = array_merge($conditions, ['bulan >=' => $awVal, 'bulan <=' => $akVal]);
	// 			} elseif ($awVal) {
	// 				$conditions = array_merge($conditions, ['bulan >=' => $awVal, 'bulan <=' => 12]);
	// 			} elseif ($akVal) {
	// 				$conditions = array_merge($conditions, ['bulan >=' => 1, 'bulan <=' => $akVal]);
	// 			}

	// 		} elseif ($awParams) {
	// 			list($awKey, $awVal) = explode('=',$awParams);
	// 			if ($awVal) {
	// 				$conditions = array_merge($conditions, ['bulan >=' => $awVal, 'bulan <=' => 12]);
	// 			}
	// 		} elseif ($akParams) {
	// 			list($akKey, $akVal) = explode('=',$akParams);
	// 			if ($akVal) {
	// 				$conditions = array_merge($conditions, ['bulan >=' => 1, 'bulan <=' => $akVal]);
	// 			}
	// 		}		
			
	// 	}

	// 	$data = $this->anggaranModel->getAll($conditions);

	// 	$totalData = count($data); 

	// 	return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData,'recordsFiltered' => $totalData, 'data'=> $data ]);	
	// }


}  #end anggaran