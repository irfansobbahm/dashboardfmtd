<?php namespace App\Controllers\Data;

use App\Controllers\BaseController;
use App\Models\PengadaanModel; 
use App\Models\PengadaanDokumenModel;
use App\Models\KepegawaianModel;
use CodeIgniter\API\ResponseTrait;




class Pengadaan extends BaseController
{
	use ResponseTrait;

	protected $db; #for transaction
	
	protected $pengadaanModel;
	protected $pengadaanDokModel;
	protected $pegawaiModel;


	public function __construct()
	{
    	
		$this->pengadaanModel = new PengadaanModel($db);
    	$this->pengadaanDokModel = new PengadaanDokumenModel($db);    	
    	$this->pegawaiModel = new KepegawaianModel();

    	$this->year  = date("Y"); 
    	$this->today = date("Y-m-d");
    	$this->now   = date("Y-m-d H:i:s");
	}
	

	public function index()
	{
		$sParams = ['th' => $this->year, 'st' => '', 'ol' => '', 'st_ak' => '', 'st_de' => '', 'bl' => '', 'jns' => '', 'sis' => ''];
		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$stParams =  $uri->getQuery(['only' => ['st']]);
			$olParams =  $uri->getQuery(['only' => ['ol']]);
			$akParams =  $uri->getQuery(['only' => ['ak']]);
			$deParams =  $uri->getQuery(['only' => ['de']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$blParams =  $uri->getQuery(['only' => ['bl']]);
			$sisParams =  $uri->getQuery(['only' => ['sis']]);

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);
				if ($jnsVal){
					$sParams['jns'] = $jnsVal;		
				}
			}

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);
				if ($thVal){
					$sParams['th'] = $thVal;		
				}
			}

			if ($sisParams) {
				list($sisKey, $sisVal) = explode('=',$sisParams);
				if ($sisVal){
					$sParams['sis'] = $sisVal;		
				}
			}

			if ($blParams) {
				list($blKey, $blVal) = explode('=',$blParams);
				if ($blVal){
					$sParams['bl'] = $blVal;		
				}
			}

			if ($stParams) {
				list($stKey, $stVal) = explode('=',$stParams);
				if ($stVal!=''){
					$sParams['st'] = $stVal;		
				}
			}
			
			if ($olParams) {
				list($olKey, $olVal) = explode('=',$olParams);
				if ($olVal) {
					$sParams['ol'] = $olVal;		
				}
			}
			
			if ($akParams) {
				list($akKey, $akVal) = explode('=',$akParams);
				if ($akVal) {
					$sParams['st_ak'] = $akVal;		
				}
			}

			if ($deParams) {
				list($deKey, $deVal) = explode('=',$deParams);
				if ($deVal) {
					$sParams['st_de'] = $deVal;		
				}
			}
		}

		$data = [
		      		'menu' => 'data',
      				'submenu' => 'pengadaan',
      				'submenu2' => $sParams['th'],
      				'sParams' 	=> $sParams,
					'uriParams' => $uriParams
	            ];

		return view('data/pengadaan/index', $data);
	}


	public function tambah()
	{
		$employees = $this->pegawaiModel->getPegawai();

		$data = [
                    'menu'   => 'data',
                    'submenu' => 'pengadaan',
                    'submenu2' => 'tambah',
                    'employees' => $employees,
	    ];


	    if($this->request->getMethod() == 'post')
		{	    
			helper(['form']); #access libraries

            #set rules    
            $rules = [
                		'kegiatan'           => 'required',
                		'jenis'              => 'required',
                		'rab'                => 'required|numeric',
                		'diadakan'           => 'required',                		
                		'tgl_ri' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal RI',
					  	],
                		'rencana_tgl_mulai' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal rencana mulai',					  		
					  	],
                		'rencana_tgl_selesai' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal rencana selesai',					  		
					  	],
            ];


            if ($this->validate($rules)) {
                
	       		#transaction
	       		$this->db = db_connect(); 
			    $this->db->transStart();
	    
	       		#
	       		# save pengadaan
	       		#

	       		$posts = [
			      			'nama_kegiatan'       => $_POST['kegiatan'],
							'id_const'            => $_POST['jenis'],
							'nominal_rab'         => $_POST['rab'],
							'diadakan'            => $_POST['diadakan'],
							'tgl_ri'              => date('Y-m-d', strtotime($_POST['tgl_ri'])),
							'rencana_tgl_mulai'   => date('Y-m-d', strtotime($_POST['rencana_tgl_mulai'])),
							'rencana_tgl_selesai' => date('Y-m-d', strtotime($_POST['rencana_tgl_selesai'])),
							'deskripsi'           => esc($_POST['deskripsi']),
							'status_pembayaran'   => $_POST['status_pembayaran'],
							'nopeg_pemohon'       => $_POST['pemohon'],
					     ];

				$this->pengadaanModel->save($posts);


				#
				#save pengadaan dokumen
				#				
				
				#find last id https://forum.codeigniter.com/thread-76392.html
				$id_pengadaan =  $this->pengadaanModel->getInsertID();

				$docs = $_POST['dokumen'];
  			
				if($docs){

					foreach ($docs as $dok){

						$posts2 = [
			      					'id_pengadaan' => $id_pengadaan,
			      					'id_dokumen'   => $dok,								
									'tgl_dokumen'  => date('Y-m-d', strtotime($_POST['tgl_'.$dok])),
			    		];

		        		$this->pengadaanDokModel->save($posts2);
					}	
				}

				#end transaction
				$this->db->transComplete();

				return redirect()->to('/data/pengadaan/index'); 						

            } else {

                $data['validation'] = $this->validator; #pass the error

            } #end validation
	    }

		return view('data/pengadaan/tambah', $data);
	}


	public function edit($id){

		$employees  = $this->pegawaiModel->getPegawai();
		$pengadaan  = $this->pengadaanModel->find($id);
		$docs       = $this->pengadaanDokModel->getDokumen($id);

	
		$data = [
                    'menu'      => 'data',
                    'submenu'   => 'pengadaan',
                    'submenu2'  => 'edit',
                    'employees' => $employees,
                    'pengadaan' => $pengadaan,
                    'docs'      => $docs,                   
	    ];

	    if($this->request->getMethod() == 'post')
		{	    
			helper(['form']); #access libraries

            #set rules    
            $rules = [
                		'kegiatan'           => 'required',
                		'jenis'              => 'required',
                		'rab'                => 'required|numeric',
                		'diadakan'           => 'required',                		
                		'tgl_ri' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal RI',
					  	],
                		'rencana_tgl_mulai' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal rencana mulai',					  		
					  	],
                		'rencana_tgl_selesai' => [
					  		'rules' => 'required',
					  		'label' => 'tanggal rencana selesai',					  		
					  	],
            ];

            if ($this->validate($rules)) {
                
	       		#transaction
	       		$this->db = db_connect(); 
			    $this->db->transStart();
	    
	       		$id_pengadaan = $_POST['id_pengadaan'];
	       		
	       		$posts = [
			      			'id_pengadaan'		  => $id_pengadaan,
			      			'nama_kegiatan'       => $_POST['kegiatan'],
							'id_const'            => $_POST['jenis'],
							'nominal_rab'         => $_POST['rab'],
							'diadakan'            => $_POST['diadakan'],
							'tgl_ri'              => date('Y-m-d', strtotime($_POST['tgl_ri'])),
							'rencana_tgl_mulai'   => date('Y-m-d', strtotime($_POST['rencana_tgl_mulai'])),
							'rencana_tgl_selesai' => date('Y-m-d', strtotime($_POST['rencana_tgl_selesai'])),
							'deskripsi'           => esc($_POST['deskripsi']),
							'status_pembayaran'   => $_POST['status_pembayaran'],
							'nopeg_pemohon'       => $_POST['pemohon'],
					     ];

				$this->pengadaanModel->save($posts);

				#1 form ada, tbl ada => update
				#2 form ada, tbl tidak ada => insert
				#3 form tidak ada, tbl ada => delete d table	

				$dokumen = $_POST['dokumen'];				

				if($dokumen){

					foreach ($dokumen as $dok){
						
						$new[] = $dok;
						

						$exist = $this->pengadaanDokModel->getDokumenByIds($id_pengadaan, $dok);
					
						#1 ada => update
						if ($exist) {
							
							$posts2 = [
								'id_pengadaan_dok'	  => $exist['id_pengadaan_dok'],
			      				'id_pengadaan'		  => $id_pengadaan,
			      				'id_dokumen'          => $dok,
								'tgl_dokumen'         => date('Y-m-d', strtotime($_POST['tgl_'.$dok])),
					        ];

					        $this->pengadaanDokModel->save($posts2);

						} 

						#2 tidak ada => insert
						else {						
							
								$posts3 = [								
			      					'id_pengadaan'		  => $id_pengadaan,
			      					'id_dokumen'          => $dok,
									'tgl_dokumen'         => date('Y-m-d', strtotime($_POST['tgl_'.$dok])),
					        	];

					        	$this->pengadaanDokModel->save($posts3);
						}						
					}

					$olds = $this->pengadaanDokModel->getDokumen($id_pengadaan);
					
					#3 delete
					foreach ($olds as $o) {
						
						$old              = $o['id_dokumen'];
						$id_pengadaan_dok = $o['id_pengadaan_dok'];

						if( !in_array($old, $new)){
						
							$this->pengadaanDokModel->delete($id_pengadaan_dok);
						}
					}
					
				}				
				
				#end transaction
				$this->db->transComplete();

				return redirect()->to('/data/pengadaan/index'); 						

            } else {

                $data['validation'] = $this->validator; #pass the error

            } #end validation
	    }
	
	    return view('data/pengadaan/edit', $data);
	}


	public function delete(){

		if($this->request->getMethod() == 'post')
		{	
		
			$id    = $_POST['id'];
			$exist = $this->pengadaanModel->find($id);

			if($exist){
				
				$this->pengadaanModel->delete($id);				
				
			}

			return redirect()->to('/data/pengadaan/index');
		}		
	}

	public function upload_pengadaan()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'pengadaan'

		];    	

  		return view('data/pengadaan/upload_pengadaan', $data);
	}

	public function save_upload_pengadaan()
	{
		$tahun = $this->request->getPost('tahun');
		$sispran = $this->request->getPost('sispran');

		// die($tahun);exit;

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		// echo (int)$sispran;exit;

		$this->pengadaanModel->where(['thn_anggaran' => $tahun, 'sispran' => (int)$sispran])->delete();

		$file = $this->request->getFile('file_anggaran');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);
		$data = $spreadsheet->getActiveSheet()->toArray();

		foreach($data as $idx => $row){
			
			// lewati baris ke 0 pada file excel
			// dalam kasus ini, array ke 0 adalahpara title
			if($idx < 2) {
				continue;
			}

			// $tgl_ri = ($row[11] != '') ? explode("/", $row[11]) : '';
			// $tgl_mulai = ($row[12] != '') ? explode("/", $row[12]) : '';
			// $tgl_selesai = ($row[13] != '') ? explode("/", $row[13]) : '';
				
			// get data
			$data = [
				'nomor' => $row[0],
				'kegiatan' => $row[1], 
				'deskripsi' => $row[1],
				'jenis_anggaran' => $row[2],
				'jenis_belanja' => $row[3],
				'tgl_ri' => trim($row[10]),
				'tgl_mulai' => trim($row[11]),
				'tgl_selesai' => trim($row[12]),
				'rab' => str_replace(',', '', $row[13]),
				'diadakan' => $row[14],
				'status' => $row[15],
				// 'status_pembayaran' => $row[11],
				// 'tgl_status_pembayaran' => $row[12],
				'status_aktual' => $row[16], 
				'tgl_pembayaran' => trim($row[17])
			];

			if($data['kegiatan'] == '') {
				continue;
			}

			// if ($idx == 19) {
			// 	print_r($data); exit;
			// }

			$tgl_ri = null;
			$tgl_mulai = null;
			$tgl_selesai = null;
			$tgl_status_pembayaran = null;

			if ($data['tgl_ri'] && $data['tgl_ri'] != '') {
				list($m, $d, $y) = explode('/', $data['tgl_ri']);
				$tgl_ri = $y.'-'.$m.'-'.$d;
			} else {
				$tgl_ri = NULL;
			}
			if ($data['tgl_mulai'] && $data['tgl_mulai'] != '') {
				list($m, $d, $y) = explode('/', $data['tgl_mulai']);
				$tgl_mulai = $y.'-'.$m.'-'.$d;
			} else {
				$tgl_mulai = NULL;
			}
			if ($data['tgl_selesai'] && $data['tgl_selesai'] != '') {
				// echo $idx.'--'.$data['tgl_selesai'].'<br/>';
				list($m, $d, $y) = explode('/', $data['tgl_selesai']);
				$tgl_selesai = $y.'-'.$m.'-'.$d;
			} else {
				$tgl_selesai = NULL;
			}
			if ($data['tgl_pembayaran'] && $data['tgl_pembayaran'] != '') {
				list($m, $d, $y) = explode('/', $data['tgl_pembayaran']);
				$tgl_pembayaran = $y.'-'.$m.'-'.$d;
			} else {
				$tgl_pembayaran = NULL;
			}
			// if ($data['tgl_status_pembayaran'] != '') {
			// 	list($m, $d, $y) = explode('/', $data['tgl_status_pembayaran']);
			// 	$tgl_status_pembayaran = $y.'-'.$m.'-'.$d;
			// }

			switch (trim(strtolower($data['jenis_anggaran']))) {
				case 'pendidikan':
					$jns_anggaran = '01';
					break;		
				case 'ppmi':
					$jns_anggaran = '02';
					break;
				// case 'kerma alihan':
				// 	$jns_anggaran = ($sispran == '01') ? '10' : '01';
				// 	break;				
				case 'kerma non alihan':
					$jns_anggaran = '02';
					break;				
				default:
					$jns_anggaran = '01';
					break;
			}


			switch (trim(strtolower($data['jenis_belanja']))) {
				case 'barang':
					$jns_belanja = '02';
					break;		
				case 'jasa':
					$jns_belanja = '03';
					break;
				case 'modal':
					$jns_belanja = '04';
					break;				
				default:
					$jns_belanja = '02';
					break;
			}

			$diadakan = $data['diadakan'];

			// switch (trim(strtolower($data['diadakan']))) {
			// 	case 'Logistik':
			// 		$diadakan = 'ITB';
			// 		break;		
			// 	default:
			// 		$diadakan = 'FTMD';
			// 		break;
			// }

			// switch (trim(strtolower($data['status']))) {
			// 	case 'pemaketan baru':
			// 		$status = 0;
			// 		break;		
			// 	case 'diproses itb':
			// 		$status = 1;
			// 		break;		
			// 	case 'penawaran':
			// 		$status = 2;
			// 		break;		
			// 	case 'spk':
			// 		$status = 3;
			// 		break;
			// 	case 'bast':
			// 		$status = 4;
			// 		break;
			// 	case 'invoice':
			// 		$status = 5;
			// 		break;	
			// 	case 'selesai':
			// 		$status = 6;
			// 		break;							
			// 	default:
			// 		$status = 0;
			// 		break;
			// }

			switch (trim(strtolower($data['status']))) {
				case 'pemaketan':
					$status = 0;
					break;		
				case 'kontrak':
					$status = 1;
					break;		
				case 'invoice':
					$status = 2;
					break;	
				case 'selesai':
					$status = 3;
					break;							
				default:
					$status = 0;
					break;
			}

			// switch (trim(strtolower($data['status']))) {
			// 	case 'ajuan unit':
			// 		$status_pembayaran = 1;
			// 		break;		
			// 	case 'verifikator':
			// 		$status_pembayaran = 1;
			// 		break;		
			// 	case 'validator':
			// 		$status_pembayaran = 2;
			// 		break;								
			// 	case 'bendahara':
			// 		$status_pembayaran = 2;
			// 		break;								
			// 	case 'selesai':
			// 		$status_pembayaran = 3;
			// 		break;								
			// 	default:
			// 		$status_pembayaran = 0;
			// 		break;
			// }

			$id_const = $sispran.'04'.$jns_anggaran.$jns_belanja;
			
			$data_insert = [
				"nama_kegiatan"    		=> $data['kegiatan'],
				"id_const"				=> $id_const,
				"deskripsi"				=> $data['deskripsi'],
				"tgl_ri"     			=> $tgl_ri,
				"rencana_tgl_mulai" 	=> $tgl_mulai,
				"rencana_tgl_selesai"	=> $tgl_selesai,
				"nominal_rab" 			=> $data['rab'],
				"diadakan" 				=> $diadakan,
				"status" 				=> $status,
				"status_aktual"   		=> $data['status_aktual'],
				// "status_pembayaran" 	=> $status_pembayaran,
				"thn_anggaran"		    => $tahun,
				"tgl_pembayaran"		=> $tgl_pembayaran,
				"sispran"				=> (int)$sispran,
				"sumber_dana"			=> $data['jenis_anggaran']
			];

			// print_r($data_insert);exit;

			$simpan = $this->pengadaanModel->save($data_insert);
		}
		// exit;

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Upload Data berhasil');
		return redirect()->to(base_url('data/pengadaan')); 
	}

	public function api_data()
	{
		$conditions = [];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$blParams =  $uri->getQuery(['only' => ['bl']]);
			$stParams =  $uri->getQuery(['only' => ['st']]);
			$olParams =  $uri->getQuery(['only' => ['ol']]);
			$akParams =  $uri->getQuery(['only' => ['ak']]);
			$deParams =  $uri->getQuery(['only' => ['de']]);			
			$sisParams =  $uri->getQuery(['only' => ['sis']]);			

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);
				if ($jnsVal){
					$conditions = array_merge($conditions, ['substr(t_pengadaan.id_const,7,2)' => $jnsVal]);
				}
			}

			if ($sisParams) {
				list($sisKey, $sisVal) = explode('=',$sisParams);
				if ($sisVal){
					$conditions = array_merge($conditions, ['t_pengadaan.sispran' => $sisVal]);
				}
			}	

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['t_pengadaan.thn_anggaran' => $thVal]);				
				}
			}

			if ($blParams) {
				list($blKey, $blVal) = explode('=',$blParams);	
				if ($blVal){
					$conditions = array_merge($conditions, ['month(t_pengadaan.rencana_tgl_mulai)' => $blVal]);				
				}
			}					
			
			if ($stParams) {
				list($stKey, $stVal) = explode('=',$stParams);
				if ($stVal!='') {
					$conditions = array_merge($conditions, ['t_pengadaan.status' => $stVal]);
				}
			}	
			
			if ($olParams) {
				list($olKey, $olVal) = explode('=',$olParams);
				if ($olVal) {
					$conditions = array_merge($conditions, ['t_pengadaan.diadakan' => $olVal]);
				}
			}

			if ($akParams) {
				list($akKey, $akVal) = explode('=',$akParams);
				if ($akVal) {
					switch ($akVal) {
						case '1':
							$conditions = array_merge($conditions, ['t_pengadaan.status_aktual <' => 50]);
							break;
						case '2':
							$conditions = array_merge($conditions, ['t_pengadaan.status_aktual >=' => 50, 't_pengadaan.status_aktual <' => 75]);
							break;
						case '3':
							$conditions = array_merge($conditions, ['t_pengadaan.status_aktual >=' => 75, 't_pengadaan.status_aktual <' => 100]);
							break;
						case '4':
							$conditions = array_merge($conditions, ['t_pengadaan.status_aktual ' => 100]);
							break;
					}
				}
			}

			if ($deParams) {
				list($deKey, $deVal) = explode('=',$deParams);
				if ($deVal) {
					switch ($deVal) {
						case '1':
							$conditions = array_merge($conditions, ['datediff(rencana_tgl_selesai, current_date()) >' => 7]);
							break;
						case '2':
							$conditions = array_merge($conditions, ['datediff(rencana_tgl_selesai, current_date()) >=' => 3, 'datediff(rencana_tgl_selesai, current_date()) <' => 7]);
							break;
						case '3':
							$conditions = array_merge($conditions, ['datediff(rencana_tgl_selesai, current_date()) <' => 3, 'datediff(rencana_tgl_selesai, current_date()) >=' => 0]);
							break;
						case '4':
							$conditions = array_merge($conditions, ['datediff(rencana_tgl_selesai, current_date()) < ' => 0]);
							break;
					}
					$conditions = array_merge($conditions, ['status <>' => 3]);
				}
			}		
			
		}


		$data = $this->pengadaanModel->getAll($conditions);
		// print_r($data);exit;
	
		$totalData = count($data);

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);
	}
} 