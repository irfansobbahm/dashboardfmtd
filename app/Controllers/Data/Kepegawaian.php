<?php namespace App\Controllers\Data;

use App\Controllers\BaseController;
use App\Models\PegawaiModel;
use App\Models\PegawaikreditModel;
use CodeIgniter\API\ResponseTrait;

class Kepegawaian extends BaseController
{
	use ResponseTrait;

	protected $dosenModel;
	protected $pegawaikreditModel;
	protected $pegawaiModel;

	public function __construct()
	{
    	$this->thn = date('Y');
		$this->dosenModel = new PegawaiModel();
		$this->pegawaikreditModel = new PegawaikreditModel();
		$this->pegawaiModel = new PegawaiModel();
	}

	public function index()
	{
		$sParams = ['jab' => '', 'st' => '', 'jab_usul' => '', 'kk' => '', 'pb' => '', 'lm' => '', 'stp' => ''];
		$uri = service('uri');
		$uriParams = $uri->getQuery();
		// print_r($uriParams);die;
		if ($uriParams) {
			$jabParams =  $uri->getQuery(['only' => ['jab']]);
			$jabUsulParams =  $uri->getQuery(['only' => ['jab_usul']]);
			$stParams = $uri->getQuery(['only' => ['st']]);
			$kkParams = $uri->getQuery(['only' => ['kk']]);
			$pbParams =  $uri->getQuery(['only' => ['pb']]);
			$lmParams =  $uri->getQuery(['only' => ['lm']]);
			$stpParams =  $uri->getQuery(['only' => ['stp']]);
			
			// print_r($pbParams);die;
			if ($jabParams) {
				list($jabKey, $jabVal) = explode('=',$jabParams);
				if ($jabVal){
					$sParams['jab'] = $jabVal;		
				}
			}
			
			if ($stParams) {
				list($stKey, $stVal) = explode('=',$stParams);
				if ($stVal){
					$sParams['st'] = $stVal;	
				}
			}
			if ($jabUsulParams) {
				list($jabUsulKey, $jabUsulVal) = explode('=',$jabUsulParams);
				if ($jabUsulVal){
					$sParams['jab_usul'] = $jabUsulVal;		
				}
			}
			if ($kkParams) {
				list($kkKey, $kkVal) = explode('=',$kkParams);
				if ($kkVal){
					$sParams['kk'] = $kkVal;		
				}
			}
			if ($pbParams) {
				list($pbKey, $pbVal) = explode('=',$pbParams);
				if ($pbVal){
					$sParams['pb'] = $pbVal;		
				}
			}
			if ($lmParams) {
				list($lmKey, $lmVal) = explode('=',$lmParams);
				if ($lmVal){
					$sParams['lm'] = $lmVal;		
				}
			}
			
			if ($stpParams) {
				list($stpKey, $stpVal) = explode('=',$stpParams);
				if ($stpVal){
					$sParams['stp'] = $stpVal;		
				}
			}
		}
		
		// $list = $this->dosenModel->getDosen();
		
		$data = [
			'menu' => 'data',
			'submenu' => 'kepegawaian',
			'submenu2' => date('Y'),
			// 'list' => $list,
			'sParams' 	=> $sParams,
			'uriParams' => $uriParams
			
		];
		return view('data/kepegawaian/index', $data);
	}

	// public function index()
	// {
	// 	$arKk = [
	// 		'irt' 		=> 'Ilmu dan Rekayasa Termal',
	// 		'dfp' 		=> 'Dinamika Fluida dan Propulsi',
	// 		'mpsr' 		=> 'Mekanika Padatan dan Struktur Ringan',
	// 		'itm' 		=> 'Ilmu dan Teknik Material',
	// 		'dk' 		=> 'Dinamika dan Kendali',
	// 		'mot'		=> 'Mekanika dan Operasi Terbang', 
	// 		'ptp' 		=> 'Perancangan Teknik dan Produksi'
	// 	];

	// 	$conditions = ['t_pegawai.is_aktif !=' => '0'];

	// 	$jabArr = ['gb' => 'GURU BESAR', 'lk' => 'LEKTOR KEPALA', 'l' => 'LEKTOR', 'aa' => 'ASISTEN AHLI', 'non' => 'NON JABATAN'];
	// 	$statusPeg = ['aktif' => 'AKTIF', 'tubel' => 'TUGAS BELAJAR', 'mdl' => 'MENJABAT DI LUAR'];

	// 	$uri = service('uri');
	// 	$uriParams = $uri->getQuery();
	// 	// print_r($uriParams);die;
	// 	if ($uriParams) {
	// 		$jabParams =  $uri->getQuery(['only' => ['jab']]);
	// 		$stParams =  $uri->getQuery(['only' => ['st']]);
	// 		$jabUsulParams =  $uri->getQuery(['only' => ['jab_usul']]);
	// 		$kkParams =  $uri->getQuery(['only' => ['kk']]);
	// 		$pbParams =  $uri->getQuery(['only' => ['pb']]);
	// 		$lmParams =  $uri->getQuery(['only' => ['lm']]);
	// 		$stpParams =  $uri->getQuery(['only' => ['stp']]);
			
	// 		if ($jabParams) {
	// 			list($jabKey, $jabVal) = explode('=',$jabParams);

	// 			if ($jabVal){
	// 				$conditions = array_merge($conditions, ['t_pegawai.jab_fungsional' => $jabArr[$jabVal]]);
	// 			}
	// 		}	

	// 		if ($stParams) {
	// 			list($stKey, $stVal) = explode('=',$stParams);	
	// 			if ($stVal=='0'){
	// 				$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);				
	// 			} elseif($stVal=='8'){
	// 				$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);
	// 			} elseif($stVal=='5'){
	// 				$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);
	// 			}
	// 		}

	// 		if ($jabUsulParams) {
	// 			list($jabUsulKey, $jabUsulVal) = explode('=',$jabUsulParams);	
	// 			if ($jabUsulVal){
	// 				$conditions = array_merge($conditions, ['t_pegawai_kredit.jabatan_usulan' => $jabArr[$jabUsulVal]]);				
	// 			}
	// 		}

	// 		if ($kkParams) {
	// 			list($kkKey, $kkVal) = explode('=',$kkParams);	
	// 			if ($kkVal && $kkVal != 'all'){
	// 				$conditions = array_merge($conditions, (['t_pegawai.kelompok_keahlian' => $arKk[$kkVal]]));				
	// 			}
	// 		}

	// 		if ($pbParams) {
	// 			list($pbKey, $pbVal) = explode('=',$pbParams);	
	// 			if ($pbVal){
	// 				switch ($pbVal) {
	// 					case '2':
	// 						if($jabVal=='gb'){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <2)' => NULL

	// 							]);										
	// 						}elseif($jabVal==''){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2 OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2) ' => NULL

	// 							]);
	// 						}else{
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <2)' => NULL

	// 							]);
	// 						}				
	// 						break;
																		
	// 					case '24':
	// 						if($jabVal=='gb'){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=4)' => NULL
	// 							]);										
	// 						}elseif($jabVal==''){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <= 4
	// 								OR t_pegawai.jab_fungsional <>' => 'GURU BESAR', 
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=4)' => NULL,
									
	// 							]);
	// 						}else{
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=4)' => NULL,
	// 							]);
	// 						}
							
	// 						break;		
	// 					case '46':
	// 						if($jabVal=='gb'){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >' => 4, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=6)' => NULL
	// 							]);										
	// 						}elseif($jabVal==''){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >' => 4, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <= 6
	// 								OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >' => 4, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=6)' => NULL
									
	// 							]);
	// 						}else{
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >' => 4, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <=6)' => NULL
	// 							]);
	// 						}
	// 						break;		
	// 					case '6':
	// 						if($jabVal=='gb'){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >6)' => NULL
									

	// 							]);										
	// 						}elseif($jabVal==''){
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
	// 								'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) > 6
	// 								OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >6)' => NULL
									
	// 							]);
	// 						}else{
	// 							$conditions = array_merge($conditions, [
	// 								'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
	// 								'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >6)' => NULL

	// 							]);
	// 						}
	// 						break;		
	// 				}
	// 			}
	// 		}

	// 		if ($stpParams) {
	// 			list($stpKey, $stpVal) = explode('=',$stpParams);	
	// 			if ($stpVal){
	// 				$conditions = array_merge($conditions, ['t_pegawai.status_kepegawaian' => $statusPeg[$stpVal]]);				
	// 			}
	// 		}
	// 	}
	// 	// print_r($conditions);die;
	// 	$data = $this->dosenModel->getAkDosen($conditions);
	// 	// $list = $this->dosenModel->getDosen();
		
	// 	// $data = [
	// 	// 	'menu' => 'data',
	// 	// 	'submenu' => 'kepegawaian',
	// 	// 	'submenu2' => date('Y'),
	// 	// 	// 'list' => $list,
	// 	// 	'sParams' 	=> $sParams,
	// 	// 	'uriParams' => $uriParams
			
	// 	// ];
	// 	return view('data/kepegawaian/index', $data);
	// }

	public function detail($id)
	{
		//$list = $this->dosenModel->getDosen();

		$data = [
			'menu' => 'data',
			'submenu' => 'kepegawaian',
			'detail' => []
		];
		
		return view('data/kepegawaian/detail', $data);
	}

	public function updateform()
	{
		$data = [
			'menu' => 'dashboard',
			'submenu' => 'kepegawaian',
			'submenu2' => 'upload'
	    ];

		return view('data/kepegawaian/upload_form', $data);
	}

	public function upload()
	{
		$rules = [
			'ak_file' => 'uploaded[ak_file]|ext_in[ak_file,xls,xlsx]|max_size[ak_file,1000]',
		];

		$errors = [
			'ak_file' => [
				'ext_in'    => 'File Excel hanya boleh diisi dengan xls atau xlsx.',
				'max_size'  => 'File Excel anggaran maksimal 1mb',
				'uploaded'  => 'File Excel anggaran wajib diisi'
			]
		];
		
		if (!$this->validate($rules, $errors))
		{
			$validation =  \Config\Services::validation();

			session()->setFlashdata('errors', $validation->getErrors());
			return redirect()->to(base_url('data/kepegawaian/upform'));
		} 
		else 
		{
			$jenis = $this->request->getVar('jenis');

			#transaction
			$this->db = db_connect(); 
			$this->db->transStart();
			
			$file = $this->request->getFile('ak_file');

			// ambil extension dari file excel
			$extension = $file->getClientExtension();
			
			// format excel 2007 ke bawah
			if('xls' == $extension){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			// format excel 2010 ke atas
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}
			
			$spreadsheet = $reader->load($file);
			$data = $spreadsheet->getActiveSheet()->toArray();

			$arrMonth = ['Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06',
							'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'];

			$arrPendidikan = ['Sarjana' => 'S1', 'Magister' => 'S2', 'Doktor' => 'S3'];
	
			foreach($data as $idx => $row){

				if ($jenis == 'DUK') {

					if($idx < 7 or trim($row['4']) == '') {
						continue;
					}

					// echo $row[1]; exit;

					list($d, $m, $y) = explode('-', $row['26']);
					list($d2, $m2, $y2) = explode('-', $row['9']);
					list($d3, $m3, $y3) = explode('-', $row['19']);
					list($d4, $m4, $y4) = explode('-', $row['14']);
					list($d5, $m5, $y5) = explode('-', $row['29']);
					$umur = $row['27'];
					$umur_kerja = $row['11'];
					$umur_jabfung = $row['20'];
					$umur_gol = $row['16'];

					$tgl_lahir = ($umur > (date("Y")-2000) ? '19' : '20' ).$y.'-'.$arrMonth[$m].'-'.str_pad($d, 2, '0', STR_PAD_LEFT);
					$tmt_kerja = ($umur_kerja > (date("Y")-2000) ? '19' : '20' ).$y2.'-'.$arrMonth[$m2].'-'.str_pad($d2, 2, '0', STR_PAD_LEFT);
					$tmt_jab_fungsional = ($umur_jabfung > (date("Y")-2000) ? '19' : '20' ).$y3.'-'.$arrMonth[$m3].'-'.str_pad($d3, 2, '0', STR_PAD_LEFT);
					$tmt_golongan = ($umur_gol > (date("Y")-2000) ? '19' : '20' ).$y4.'-'.$arrMonth[$m4].'-'.str_pad($d4, 2, '0', STR_PAD_LEFT);					
					$tmt_pensiun = '20'.$y4.'-'.$arrMonth[$m4].'-'.str_pad($d4, 2, '0', STR_PAD_LEFT);					
					
					// get data
					$data = [
						'nomor' => $row[0],
						'nip' => trim(str_replace(' ', '', trim($row[4]))), 
						'nama' => $row[1],
						'gelar_blkg' => $row[2],
						'gelar_depan' => $row[3],
						'kelompok_keahlian' => $row[6],
						'nidn' => $row[7],
						'karpeg' => $row[8],
						'tgl_lahir' => $tgl_lahir, 
						'pendidikan' => $arrPendidikan[trim($row['23'])],
						'jenis_penugasan' => 'DOSEN',
						'golongan' => $row['13'],
						'jab_fungsional' => $row['18'],
						'tmt_kerja' => $tmt_kerja,
						'is_aktif' => (strtoupper($row['32']) == 'AKTIF' ? 1: 0),
						'status_kepegawaian' => strtoupper($row['32']),
						'tmt_jab_fungsional' => $tmt_jab_fungsional,
						'tmt_golongan' => $tmt_golongan,
						'umur' => $umur,
						'masa_jabfung' => $umur_jabfung,
						'tmt_pensiun' => $tmt_pensiun
					];
					
					// print_r($data);

					//cek data terlebih dahulu, jika tidak ada insert klo sudah ada update
					// $cek = $this->pegawaikreditModel->getPegawaiByNip($data['nip']);
					// if (!is_array($cek)) {
					// 	$simpan = $this->pegawaikreditModel->save(array_merge($data_update, ['nip', $data['nip']]));
					// }
		

					$data_update_peg = [
						'nama' => $data['nama'], 
						'gelar_depan' => $data['gelar_depan'], 
						'gelar_belakang' => $data['gelar_blkg'], 
						'tgl_lahir' => $data['tgl_lahir'], 
						'pendidikan' => $data['pendidikan'], 
						'jenis_penugasan' => $data['jenis_penugasan'], 
						'golongan' => $data['golongan'], 
						'jab_fungsional' => $data['jab_fungsional'], 
						'kelompok_keahlian' => $data['kelompok_keahlian'], 
						'tmt_kerja' => $data['tmt_kerja'], 
						'is_aktif' => $data['is_aktif'], 
						'status_kepegawaian' => $data['status_kepegawaian'], 
						'tmt_jab_fungsional' => $data['tmt_jab_fungsional'], 
						'tmt_golongan' => $data['tmt_golongan'], 
						'umur' => $data['umur'], 
						'tmt_pensiun' => $data['tmt_pensiun'], 
						'masa_jabfung' => $data['masa_jabfung']
					];

					$simpanpeg = $this->pegawaiModel->update($data['nip'], $data_update_peg);

				} else {
					// lewati baris ke 0 pada file excel
					// dalam kasus ini, array ke 0 adalahpara title
					if($idx < 3) {
						continue;
					}
					
					// get data
					$data = [
						'nomor' => $row[0],
						'nip' => str_replace(' ', '', $row[1]), 
						'nama' => $row[2],
						'tmt_pensiun' => $row[3],
						'umur' => $row[4],
						'jab_saat_ini' => ($row[5] == 'NA') ? 'NON JABATAN' : strtoupper($row[5]),
						'jab_usulan' => $row[6],
						'masa_jab' => $row[7],
						'gol' => $row[8],
						'ak_perolehan' => $row[9],
						'ak_syarat' => $row[10],
						'ak_pendidikan_perolehan' => $row[11],
						'ak_pendidikan_syarat' => $row[12],
						'ak_penelitian_perolehan' => $row[13],
						'ak_penelitian_syarat' => $row[14],
						'ak_pengabdian_perolehan' => $row[15],
						'ak_pengabdian_syarat' => $row[16],
						'ak_pengembangan_perolehan' => $row[17],
						'ak_pengembangan_syarat' => $row[18],
						'syarat_ak' => $row[19],
						'syarat_khusus' => $row[20],
						'posisi_usulan' => $row[21],
						// 'tgl_posisi' => $row[22],
						// 'keterangan' => $row[23],
						// 'status_peg' => $row[24],
						// 'kelompok_keahlian' => $row[25]
					];

					switch (trim(strtolower($data['posisi_usulan']))) {
						case 'admin kk': $status = '1';break;
						case 'kepegawaian ftmd': $status = '2';break;
						case 'tpakk f/s': $status = '3';break;
						case 'senat f/s': $status = '4';break;
						case 'tpak itb': $status = '5';break;
						case 'senat itb': $status = '6';break;
						case 'kepegawaian pusat': $status = '7';break;
						case 'dikti': $status = '8';break;
						default: $status = 0; break;
					}
					
					$data_update = [
						"jabatan_usulan"    		=> $data['jab_usulan'],
						"ak_syarat"					=> $data['ak_syarat'],
						"ak_perolehan"				=> $data['ak_perolehan'],
						"ak_pendidikan"     		=> $data['ak_pendidikan_perolehan'],
						"ak_pendidikan_persyaratan" => $data['ak_pendidikan_syarat'],
						"ak_penelitian" 			=> $data['ak_penelitian_perolehan'],
						"ak_penelitian_persyaratan" => $data['ak_penelitian_syarat'],
						"ak_pengabdian" 			=> $data['ak_pengabdian_perolehan'],
						"ak_pengabdian_persyaratan" => $data['ak_pengabdian_syarat'],
						"ak_pengembangan"   		=> $data['ak_pengembangan_perolehan'],
						"ak_pengembangan_persyaratan" => $data['ak_pengembangan_syarat'],
						"syarat_ak"		     		=> $data['syarat_ak'],
						"syarat_khusus"		     	=> $data['syarat_khusus'],
						// "syarat_keterangan" 		=> $data['keterangan'],
						// "tgl_posisi_usulan"			=> $data['tgl_posisi'], 
						"status" 					=> $status
					];

					// print_r($data);

					//cek data terlebih dahulu, jika tidak ada insert klo sudah ada update
					// $cek = $this->pegawaikreditModel->getPegawaiByNip($data['nip']);
					// if (!is_array($cek)) {
					// 	$simpan = $this->pegawaikreditModel->save(array_merge($data_update, ['nip', $data['nip']]));
					// }
		
					$simpan = $this->pegawaikreditModel->update($data['nip'], $data_update);

					$data_update_peg = [
						"tmt_pensiun" 	=> $data['tmt_pensiun'],
						"umur" 			=> $data['umur'],
						"masa_jabfung" 	=> $data['masa_jab'],
						"jab_fungsional" => $data['jab_saat_ini'],
						// "status_kepegawaian" => $data['status_peg'],
						// "kelompok_keahlian" => $data['kelompok_keahlian']
					];

					// $simpanpeg = $this->pegawaiModel->update($data['nip'], $data_update_peg);

				}

			}
			// exit;

			#end transaction
			$this->db->transComplete();
	
			session()->setFlashdata('success', 'Upload Data berhasil');
			return redirect()->to(base_url('data/kepegawaian')); 
		}
	}

	public function api_data()
	{
		$arKk = [
			'irt' 		=> 'Ilmu dan Rekayasa Termal',
			'dfp' 		=> 'Dinamika Fluida dan Propulsi',
			'mpsr' 		=> 'Mekanika Padatan dan Struktur Ringan',
			'itm' 		=> 'Ilmu dan Teknik Material',
			'dk' 		=> 'Dinamika dan Kendali',
			'mot'		=> 'Mekanika dan Operasi Terbang', 
			'ptp' 		=> 'Perancangan Teknik dan Produksi'
		];

		$conditions = ['t_pegawai.is_aktif !=' => '0'];

		$jabArr = ['gb' => 'GURU BESAR', 'lk' => 'LEKTOR KEPALA', 'l' => 'LEKTOR', 'aa' => 'ASISTEN AHLI', 'non' => 'NON JABATAN'];
		$statusPeg = ['aktif' => 'AKTIF', 'tubel' => 'TUGAS BELAJAR', 'mdl' => 'MENJABAT DI LUAR'];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		// print_r($uriParams);die;
		if ($uriParams) {
			$jabParams =  $uri->getQuery(['only' => ['jab']]);
			$stParams =  $uri->getQuery(['only' => ['st']]);
			$jabUsulParams =  $uri->getQuery(['only' => ['jab_usul']]);
			$kkParams =  $uri->getQuery(['only' => ['kk']]);
			$pbParams =  $uri->getQuery(['only' => ['pb']]);
			$lmParams =  $uri->getQuery(['only' => ['lm']]);
			$stpParams =  $uri->getQuery(['only' => ['stp']]);
			
			if ($jabParams) {
				list($jabKey, $jabVal) = explode('=',$jabParams);

				if ($jabVal){
					$conditions = array_merge($conditions, ['t_pegawai.jab_fungsional' => $jabArr[$jabVal]]);
				}
			}	

			if ($stParams) {
				list($stKey, $stVal) = explode('=',$stParams);	
				if ($stVal=='0'){
					$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);				
				} elseif($stVal=='8'){
					$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);
				} elseif($stVal=='5'){
					$conditions = array_merge($conditions, ['t_pegawai_kredit.status' => $stVal,'t_pegawai.jab_fungsional !=' => 'GURU BESAR']);
				}
			}

			if ($jabUsulParams) {
				list($jabUsulKey, $jabUsulVal) = explode('=',$jabUsulParams);	
				if ($jabUsulVal){
					$conditions = array_merge($conditions, ['t_pegawai_kredit.jabatan_usulan' => $jabArr[$jabUsulVal]]);				
				}
			}

			if ($kkParams) {
				list($kkKey, $kkVal) = explode('=',$kkParams);	
				if ($kkVal && $kkVal != 'all'){
					$conditions = array_merge($conditions, ['t_pegawai.kelompok_keahlian' => $arKk[$kkVal]]);				
				}
			}

			if ($pbParams) {
				list($pbKey, $pbVal) = explode('=',$pbParams);	
				if ($pbVal){
					switch ($pbVal) {
						case '2':
							if($jabVal=='gb'){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <2)' => NULL

								]);										
							}elseif($jabVal==''){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2 OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) < 2) ' => NULL

								]);
							}else{
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <2)' => NULL

								]);
							}				
							break;
																		
						case '24':
							if($jabVal=='gb'){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <4)' => NULL
								]);										
							}elseif($jabVal==''){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <4
									OR t_pegawai.jab_fungsional <>' => 'GURU BESAR', 
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <4)' => NULL,
									
								]);
							}else{
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 2, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <4)' => NULL,
								]);
							}
							
							break;		
						case '46':
							if($jabVal=='gb'){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 4, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <6)' => NULL
								]);										
							}elseif($jabVal==''){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 4, '(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <6
									OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 4, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <6)' => NULL
									
								]);
							}else{
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=' => 4, '(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) <6)' => NULL
								]);
							}
							break;		
						case '6':
							if($jabVal=='gb'){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=6)' => NULL
									

								]);										
							}elseif($jabVal==''){
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional =' => 'GURU BESAR',
									'(70 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >= 6
									OR t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=6)' => NULL
									
								]);
							}else{
								$conditions = array_merge($conditions, [
									'(t_pegawai.jab_fungsional <>' => 'GURU BESAR',
									'(65 - TIMESTAMPDIFF(YEAR, t_pegawai.tgl_lahir, CURDATE())) >=6)' => NULL

								]);
							}
							break;		
					}
				}
			}

			if ($lmParams) {
				list($lmKey, $lmVal) = explode('=',$lmParams);	
				if ($lmVal){
					switch ($lmVal) {
						case '1':
							// $conditions = array_merge($conditions, [
							// 	// 'masa_jabfung <' => 2
							// 	'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 2,
							// 	// 't_pegawai.jab_fungsional !=' => 'GURU BESAR'
							// ]);
							
							if($jabVal==''){
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 2,
								]);										
							}else{
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 2,
								]);
							}

							break;
						case '13':
							// $conditions = array_merge($conditions, [
							// 	// 'masa_jabfung >=' => 2,
							// 	// 'masa_jabfung <' => 6
							// 	'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 2,
							// 	'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 6,
							// 	// 't_pegawai.jab_fungsional !=' => 'GURU BESAR'
							// ]);

							if($jabVal==''){
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 2,
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 6,
								]);										
							}else{
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 2,
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <' => 6,
								]);
							}
							
							break;		
						case '35':
							// $conditions = array_merge($conditions, [
							// 	// 'masa_jabfung >=' => 6,
							// 	// 'masa_jabfung <=' => 10
								// 'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 6,
								// 'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <=' => 10,
							// 	// 't_pegawai.jab_fungsional !=' => 'GURU BESAR'
							// ]);

							if($jabVal==''){
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 6,
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <=' => 10,
								]);										
							}else{
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >=' => 6,
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) <=' => 10,
								]);
							}

							break;		
						case '5':
							// $conditions = array_merge($conditions, [
							// 	// 'masa_jabfung >' => 10
								// 'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >' => 10,
							// 	// 't_pegawai.jab_fungsional !=' => 'GURU BESAR'
							// ]);

							if($jabVal==''){
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >' => 10,
								]);										
							}else{
								$conditions = array_merge($conditions, [
									'TIMESTAMPDIFF (YEAR, IFNULL(tmt_jab_fungsional, tmt_kerja), CURDATE()) >' => 10,
								]);
							}
							break;		
					}
				}
			}

			if ($stpParams) {
				list($stpKey, $stpVal) = explode('=',$stpParams);	
				if ($stpVal){
					$conditions = array_merge($conditions, ['t_pegawai.status_kepegawaian' => $statusPeg[$stpVal]]);				
				}
			}
		}


		$data = $this->dosenModel->getAkDosen($conditions);
		// echo $data; exit;
		$totalData = count($data);
		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);
	}

	public function edit($id){

		$pegawai 	 = $this->pegawaiModel->find($id);
		$detail      = $this->pegawaikreditModel->find($id);

	
		$data = [
                    'menu'      => 'data',
                    'submenu'   => 'pegawai',
                    'submenu2'  => 'edit',
                    'pegawai' 	=> $pegawai,
                    'detail'    => $detail,                   
	    ];

	    if($this->request->getMethod() == 'post')
		{	    
			helper(['form']); #access libraries

            #set rules    
            $rules = [
                		'nama'	           	=> 'required',
                		'ak_pendidikan'     => 'required|numeric',
                		'ak_pengabdian'     => 'required|numeric',
                		'ak_penelitian'     => 'required|numeric',
						'ak_pengembangan'   => 'required|numeric',                		
                
            ];

            if ($this->validate($rules)) {
                
	    //    		#transaction
	       		$this->db = db_connect(); 
			    $this->db->transStart();
	    
	       		$nip = $_POST['nip'];
	       		
	       		$posts = [
			      			'nip'		  		=> $nip,
			      			'nama'       		=> $_POST['nama'],
					     ];
				// print_r($posts);die;
				$this->pegawaiModel->save($posts);

				#1 form ada, tbl ada => update
				#2 form ada, tbl tidak ada => insert
				#3 form tidak ada, tbl ada => delete d table	

				// $dokumen = $_POST['dokumen'];				

				// if($dokumen){

					// foreach ($dokumen as $dok){
						
						// $new[] = $dok;
						

						// $exist = $this->pegawaiModel->getDokumenByIds($nip);
					
						#1 ada => update
						if ($nip) {
							
							$syarat_ak = ''; // Default value
							
							if (
								($_POST['ak_pendidikan'] >= $detail['ak_pendidikan']) &&
								($_POST['ak_pengabdian'] >= $detail['ak_pengabdian']) &&
								($_POST['ak_penelitian'] >= $detail['ak_penelitian']) &&
								($_POST['ak_pengembangan'] >= $detail['ak_pengembangan'])
							) {
								$syarat_ak = 'Terpenuhi';
							}
							$posts2 = [
								'nip'              	=> $nip,
								'ak_pendidikan'    	=> $_POST['ak_pendidikan'],
								'ak_pengabdian'    	=> $_POST['ak_pengabdian'],
								'ak_penelitian'    	=> $_POST['ak_penelitian'],
								'ak_pengembangan'  	=> $_POST['ak_pengembangan'],
								'syarat_ak'			=> $syarat_ak,
								'jabatan_usulan'	=> $_POST['jabatan'],
							];
							
							
							// print_r($posts2);die;
					        $this->pegawaikreditModel->save($posts2);

						} 

						#2 tidak ada => insert
						// else {						
							
						// 		$posts3 = [								
			      		// 			'id_pengadaan'		  => $id_pengadaan,
			      		// 			'id_dokumen'          => $dok,
						// 			'tgl_dokumen'         => date('Y-m-d', strtotime($_POST['tgl_'.$dok])),
					    //     	];

					    //     	$this->pengadaanDokModel->save($posts3);
						// }						
					// }

					// $olds = $this->pengadaanDokModel->getDokumen($id_pengadaan);
					
					#3 delete
					// foreach ($olds as $o) {
						
					// 	$old              = $o['id_dokumen'];
					// 	$id_pengadaan_dok = $o['id_pengadaan_dok'];

					// 	if( !in_array($old, $new)){
						
					// 		$this->pengadaanDokModel->delete($id_pengadaan_dok);
					// 	}
					// }
					
				// }				
				
				#end transaction
				$this->db->transComplete();

				return redirect()->to('/data/kepegawaian/index'); 						

            } else {

                $data['validation'] = $this->validator; #pass the error

            } #end validation
	    }
	
	    return view('data/kepegawaian/edit', $data);
	}

	public function delete(){

		if($this->request->getMethod() == 'post')
		{	
		
			$id    = $_POST['id'];
			$exist = $this->pegawaikreditModel->find($id);
			
			if($exist){
				
				$this->pegawaikreditModel->delete($id);				
				
			}
			return redirect()->to('/data/kepegawaian/index');
		}		
	}
}
