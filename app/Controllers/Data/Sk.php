<?php namespace App\Controllers\Data;

use App\Controllers\BaseController;
use App\Models\SkModel;
use App\Models\SkdetailModel;
use App\Models\SkdibayarModel;
use App\Models\SkdibayardetailModel;
use App\Models\PegawaiModel;
use CodeIgniter\API\ResponseTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sk extends BaseController
{
	use ResponseTrait;

	protected $skModel;
	protected $skdetailModel;
	protected $pegawaiModel;
	protected $arBulan;

	public function __construct()
	{
		$this->thn = date('Y');
		$this->skModel 			= new SkModel();
		$this->skdetailModel 	= new SkdetailModel();
		$this->skdibayarModel 	= new SkdibayarModel();
		$this->skdibayardetailModel = new SkdibayardetailModel();
		$this->pegawaiModel 	= new PegawaiModel();

		$this->arBulan = [
			'01' => 'Januari', 
			'02' => 'Februari', 
			'03' => 'Maret', 
			'04' => 'April', 
			'05' => 'Mei', 
			'06' => 'Juni', 
			'07' => 'Juli', 
			'08' => 'Agustus', 
			'09' => 'September', 
			'10' => 'Oktober', 
			'11' => 'November', 
			'12' => 'Desember'
		];
	}

	public function index()
	{
		$conditions = [];
		$sParams = ['th' => $this->thn];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thParams =  $uri->getQuery(['only' => ['th']]);

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);
				if ($thVal){
					$sParams['th'] = $thVal;		
					$conditions = array_merge($conditions, ['thn_anggaran' => $sParams['th']]);
					$this->thn = $sParams['th'];
				}
			}
		}

		$data = [
			'menu' => 'data',
			'submenu' => 'sk',
			'submenu2' => $this->thn,
			'sParams' 	=> $sParams,
			'uriParams' => $uriParams,
	    ];

		return view('data/sk/index', $data);
	}

	public function add()
	{
		$dosen = $this->pegawaiModel->getDosen();

		$data = [
			'menu' => 'data',
			'submenu' => 'sk',
			'submnenu2' => 'tambah data',
			'dosen' => $dosen,
			// 'validation' => \Config\Services::validation(),
		];
		
		return view('data/sk/tambah', $data);
	}

	public function save()
	{
		$rules = [
			'nomor_sk' => 'required',
			'judul_sk' => 'required',
			'dekan' => 'required',
			'tgl_terbit' => 'required',
			'tgl_akhir' => 'required',
			'tgl_ttd' => 'required',
			'detail_sk_file' => 'uploaded[detail_sk_file]|ext_in[detail_sk_file,xls,xlsx]|max_size[detail_sk_file,1000]',
		];

		$errors = [
			'nomor_sk' => [
				'required' => 'Nomor SK belum diisi'
			],
			'judul_sk' => [
				'required' => 'Judul SK belum diisi'
			],
			'dekan' => [
				'required' => 'Dekan belum dipilih'
			],
			'tgl_terbit' => [
				'required' => 'Tanggal terbit belum dipilih'
			],
			'tgl_akhir' => [
				'required' => 'Tanggal akhir belum dipilih'
			],
			'tgl_ttd' => [
				'required' => 'Tanggal tandatangan belum dipilih'
			],
			'detail_sk_file' => [
				'ext_in'    => 'File Excel hanya boleh diisi dengan xls atau xlsx.',
				'max_size'  => 'File Excel detaik SK maksimal 1mb',
				'uploaded'  => 'File Excel detaik SK wajib diisi'
			]
		];

		if (!$this->validate($rules, $errors))
		{
			// var_dump($this->validator->listErrors()); exit;
			// $isError = true;
			// $data_validation = $this->validator;
			$validation = \Config\Services::validation();
			session()->setFlashdata('errors', $validation->getErrors());

			return redirect()->to('/data/sk/add');
		}

		list($mt, $dt, $yt) = explode('/', $this->request->getVar('tgl_terbit'));
		list($ma, $da, $ya) = explode('/', $this->request->getVar('tgl_akhir'));
		list($md, $dd, $yd) = explode('/', $this->request->getVar('tgl_ttd'));

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		$this->skModel->save([
			'dekan' 			=> $this->request->getVar('dekan'),
			'no_sk' 			=> $this->request->getVar('nomor_sk'),
			'judul_sk' 			=> $this->request->getVar('judul_sk'),
			'tgl_terbit' 		=> $yt.'-'.$mt.'-'.$dt,
			'tgl_berakhir' 		=> $ya.'-'.$ma.'-'.$da,
			'tgl_tandatangan' 	=> $yd.'-'.$md.'-'.$dd,
			'tahun' 			=> $yt
		]);

		// $id_sk = $this->skModel->getIdByNomor($this->request->getVar('nomor_sk'));
		#find last id
		$id_sk =  $this->skModel->getInsertID();

		$file = $this->request->getFile('detail_sk_file');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);
		$data = $spreadsheet->getActiveSheet()->toArray();

		foreach($data as $idx => $row){
			
			// lewati baris ke 0 pada file excel
			// dalam kasus ini, array ke 0 adalahpara title
			if($idx == 0) {
				continue;
			}
			
			// get data from excel
			$nomor = $row[0];  
			$nomor_sk = $row[1];  
			$id_pegawai = (string)$row[2]; //nip, nopeg, nim, nik 
			$nama = $row[3]; 
			$status = $row[4]; 
			$keterangan = $row[5];
			
			$data = [
				"id_sk"    		=> $id_sk,
				"nopeg" 		=> ($status == 'DSN' || $status == 'TDK') ? $id_pegawai : NULL,
				"nim"     		=> ($status == 'MHS') ? $id_pegawai : NULL,
				"nik" 			=> ($status == 'PL') ? $id_pegawai : NULL,
				"nama" 			=> ($status == 'PL') ? $nama : NULL,
				"status"		=> $status,
				"keterangan"	=> $keterangan
			];

			// var_dump($data);exit;

			$this->skdetailModel->save($data);
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/sk'));
	}

	public function upload_sk()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'sk'

		];    	

  		return view('data/sk/upload_sk', $data);
	}

	public function save_upload_sk()
	{
		$tahun = $this->request->getVar('tahun');

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		$file = $this->request->getFile('file_sk');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}


		$spreadsheet = $reader->load($file);


		$id_sk = [];

		//delete old
		$this->skModel->where(['tahun' => $tahun])->delete();


		$spreadsheet->setActiveSheetIndex(0);
		$data = $spreadsheet->getActiveSheet()->toArray();

		foreach($data as $idx => $row){
			
			// lewati baris ke 0 pada file excel
			// dalam kasus ini, array ke 0 adalahpara title
			if($idx == 0) {
				continue;
			}
			
			// get data
			$data = [
				'nomor' => $row[0],
				'sk' => $row[1], 
				'deskripsi' => $row[2],
				'tgl_terbit' => $row[3],
				'tgl_berakhir' => $row[4],
				'thn_sk' => $row[5],
				'jenis' => $row[6],
				'nip_dekan' => str_replace(' ', '', $row[7]),
				'sispran' => $row[8]
			];

			// $tgl_terbit = null;
			// $tgl_berakhir = null;

			// if ($data['tgl_terbit'] != '') {
			// 	list($m, $d, $y) = explode(' ', $data['tgl_terbit']);
			// 	$tgl_terbit = $y.'-'.array_search($m, $this->arBulan).'-'.$d;
			// }
			// if ($data['tgl_berakhir'] != '') {
			// 	list($m, $d, $y) = explode(' ', $data['tgl_berakhir']);
			// 	$tgl_berakhir = $y.'-'.array_search($m, $this->arBulan).'-'.$d;
			// }

			// $this->skModel->delete(['thn_anggaran' => $tahun]);
			
			$data_insert = [
				"dekan"    			=> $data['nip_dekan'],
				"no_sk"				=> trim($data['sk']),
				"judul_sk"			=> trim($data['deskripsi']),
				"tgl_terbit"    	=> $data['tgl_terbit'],
				"tgl_berakhir" 		=> $data['tgl_berakhir'],
				"tgl_tandatangan"	=> $data['tgl_terbit'],
				"tahun" 			=> $data['thn_sk'],
				"jenis" 			=> $data['jenis'],
				"sispran"			=> $data['sispran']
			];

			$simpan = $this->skModel->save($data_insert);

			$id_sk[$data['sk']] = $this->skModel->getInsertID();
		}

		// print_r($id_sk);exit;
		// exit;

		// update detail
		$spreadsheet->setActiveSheetIndex(1);
		$data = $spreadsheet->getActiveSheet()->toArray();

		foreach($data as $idx => $row){
			
			// lewati baris ke 0 pada file excel
			// dalam kasus ini, array ke 0 adalahpara title
			if($idx == 0) {
				continue;
			}

			if (array_key_exists(trim($row[1]), $id_sk)) {
				// get data
				$data_detail = [
					'nomor' => $row[0],
					'sk' => trim($row[1]), 
					'nip' => str_replace(' ', '', $row[2]),
					'nama' => $row[3],
					'keterangan' => $row[4],
					// 'jenis' => $row[5]
				];

				$data_insert_detail = [
					"id_sk"    		=> $id_sk[$data_detail['sk']],
					"nopeg"			=> $data_detail['nip'],
					"nim"			=> null,
					"nik"			=> null,
					"nama"			=> $data_detail['nama'],
					"status"    	=> null,
					"keterangan" 	=> $data_detail['keterangan']
				];

				// print_r($data_insert);exit;

				$simpan = $this->skdetailModel->save($data_insert_detail);

				// $id_sk[$data['sk']] = $this->skModel->getInsertID();
			}
			
			
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Upload Data berhasil');
		return redirect()->to(base_url('data/sk')); 
	}

	public function pembayaran()
	{
		$data = [
			'menu' => 'data',
			'submenu' => 'sk',
			'submenu2' => 'pembayaran'
	    ];

		return view('data/sk/pembayaran', $data);
	}

	public function add_nominatif()
	{
		$sk = $this->skModel->getAll(['tahun' => $this->thn]);

		$data = [
			'menu' => 'data',
			'submenu' => 'sk',
			'submnenu2' => 'tambah data',
			'list_sk' => $sk,
			// 'validation' => \Config\Services::validation(),
		];
		
		return view('data/sk/tambah_nominatif', $data);
	}

	public function save_nominatif()
	{
		$rules = [
			'nomor_sk' => 'required',
			'tgl_pengajuan' => 'required',
			'jenis' => 'required',
			'total' => 'required',
			'detail_sk_file' => 'uploaded[detail_sk_file]|ext_in[detail_sk_file,xls,xlsx]|max_size[detail_sk_file,1000]',
		];

		$errors = [
			'nomor_sk' => [
				'required' => 'Nomor SK belum diisi'
			],
			'tgl_pengajuan' => [
				'required' => 'Tanggal pengajuan pembayaran belum dipilih'
			],
			'jenis' => [
				'required' => 'Jenis belanja belum dipilih'
			],
			'total' => [
				'required' => 'Tanggal akhir belum dipilih'
			],
			'detail_sk_file' => [
				'ext_in'    => 'File Excel hanya boleh diisi dengan xls atau xlsx.',
				'max_size'  => 'File Excel detaik SK maksimal 1mb',
				'uploaded'  => 'File Excel detaik SK wajib diisi'
			]
		];

		if (!$this->validate($rules, $errors))
		{
			// var_dump($this->validator->listErrors()); exit;
			// $isError = true;
			// $data_validation = $this->validator;
			$validation = \Config\Services::validation();
			session()->setFlashdata('errors', $validation->getErrors());

			return redirect()->to('/data/sk/add_nominatif');
		}

		list($m, $d, $y) = explode('/', $this->request->getVar('tgl_pengajuan'));

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		$id_sk = $this->request->getVar('nomor_sk');

		$this->skdibayarModel->save([
			'id_sk' 			=> $id_sk,
			'id_jenis_honor' 	=> $this->request->getVar('jenis'),
			'status' 			=> 1,
			'tgl_pengajuan' 	=> $y.'-'.$m.'-'.$d,
			'total_nominatif' 	=> $this->request->getVar('total')
		]);

		// $id_sk = $this->skModel->getIdByNomor($this->request->getVar('nomor_sk'));
		#find last id
		$id_sk_dibayar =  $this->skdibayarModel->getInsertID();

		$file = $this->request->getFile('detail_sk_file');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		
		$spreadsheet = $reader->load($file);
		$data = $spreadsheet->getActiveSheet()->toArray();

		foreach($data as $idx => $row){
			
			// lewati baris ke 0 pada file excel
			// dalam kasus ini, array ke 0 adalahpara title
			if($idx == 0) {
				continue;
			}
			
			// get data from excel
			$nomor 			= $row[0];  
			$nomor_sk 		= $row[1];  
			$id_pegawai 	= (string)$row[2]; //nip, nopeg, nim, nik 
			$nama 			= $row[3]; 
			$keterangan 	= $row[4];
			$nilai_maks 	= $row[5];
			$nilai_dibayar 	= $row[6];
			$volume 		= $row[7];
			$total 			= $row[8];
			$status_pegawai	= ucwords($row[9]);

			switch ($status_pegawai) {
				case 'PL':
					$id_sk_detail = $this->skdetailModel->getId(['id_sk' => $id_sk, 'nik' => $id_pegawai]);		
					break;
				case 'MHS':
					$id_sk_detail = $this->skdetailModel->getId(['id_sk' => $id_sk, 'nim' => $id_pegawai]);		
					break;
				default:
					$id_sk_detail = $this->skdetailModel->getId(['id_sk' => $id_sk, 'nopeg' => $id_pegawai]);		
					break;
			}
	
			$data = [
				"id_sk_dibayar" => $id_sk_dibayar,
				"id_sk_detail" 	=> $id_sk_detail,
				"keterangan"    => $keterangan,
				"nilai_maksimal"=> $nilai_maks,
				"nilai_dibayar" => $nilai_dibayar,
				"volume"		=> $volume,
				"total"			=> $total
			];

			// var_dump($data);exit;

			$this->skdibayardetailModel->save($data);
		}

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Data berhasil ditambahkan');

		return redirect()->to(base_url('/data/sk/pembayaran'));
	}

	public function upload_nominatif()
	{
		$data = [
			'menu'     => 'data',
			'submenu'  => 'sk'

		];    	

  		return view('data/sk/upload_nominatif', $data);
	}

	public function save_upload_nominatif()
	{
		$tahun = $this->request->getVar('tahun');

		#transaction
		$this->db = db_connect(); 
		$this->db->transStart();

		$kode_sispran = $this->request->getVar('sispran');
		$jenis_anggaran = $this->request->getVar('jenis_anggaran');
		$jenis_belanja = $this->request->getVar('jenis_belanja');
		$id_jenis_honor = $kode_sispran.'04'.$jenis_anggaran.$jenis_belanja;

		$file = $this->request->getFile('file_sk');

		// ambil extension dari file excel
		$extension = $file->getClientExtension();
		
		// format excel 2007 ke bawah
		if('xls' == $extension){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		// format excel 2010 ke atas
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}


		$spreadsheet = $reader->load($file);

		$cntSheet = $spreadsheet->getSheetCount();

		for ($i=1; $i <= $cntSheet; $i++) { 	

			
			
			$spreadsheet->setActiveSheetIndex($i-1);
			$data = $spreadsheet->getActiveSheet()->toArray();

			// $id_sk = '';
			$tgl_pengajuan = '';
			$periode = '';
			$nospp = '';
			$keterangan = '';
			$id_dibayar = '';
			$total = 0;
			$no_spp = '';

			//$a = $data[6];

			//delete old
			$this->skdibayarModel->where(['no_spp' => $data[6][2]])->delete();
			$ddibayar = $this->skdibayarModel->where(['no_spp' => $data[6][2]])->first();

			// print_r($ddibayar);exit;

			if (is_array($ddibayar)) {
				$this->skdibayardetailModel->where(['id_sk_dibayar' => $ddibayar['id_sk_dibayar']])->delete();
			}

			// print_r($data);exit;

			foreach($data as $idx => $row){

				if($idx == 3) {
					$keterangan = trim($row[2]); 
				}

				if($idx == 6) {
					$no_spp = trim($row[2]); 
				}

				if($idx == 7) {
					$tgl = explode("/", $row[2]); 
					$tgl_pengajuan = $tgl[2].'-'.str_pad($tgl[1], 2, "0", STR_PAD_LEFT).'-'.str_pad($tgl[0], 2, "0", STR_PAD_LEFT);
					// echo $tgl_pengajuan;exit;
				}

				if($idx == 8) {
					$tgl = explode("/", $row[2]); 
					$tgl_pembayaran = $tgl[2].'-'.str_pad($tgl[1], 2, "0", STR_PAD_LEFT).'-'.str_pad($tgl[0], 2, "0", STR_PAD_LEFT);
					// echo $tgl_pengajuan;exit;
				}

				// if ($idx == 10) {
				// 	print_r($row);exit;
				// }

				if($idx == 10) {
					
					if ($no_spp != '') {
						$data_dibayar = [
							'id_jenis_honor' => $id_jenis_honor,
							'status' => 0,
							'tgl_pengajuan' => $tgl_pengajuan,
							'tgl_pembayaran' => $tgl_pembayaran,
							'total_nominatif' => 1,
							'keterangan' => $keterangan,
							'thn_anggaran' => $tahun,
							'periode' => $periode,
							'no_spp' => $no_spp
						];

						// print_r($data_dibayar);exit;

						$this->skdibayarModel->save($data_dibayar);

						$id_dibayar = $this->skdibayarModel->getInsertID(); 	
					}
					// exit;
				}

				
				
				
				if ($id_dibayar > 0 && $idx > 10) {
					$nilai_maksimal = strtr($row[9], array('.'=>'', ','=> ''));
					$nilai_dibayar = strtr($row[10], array('.'=>'', ','=> ''));
					$subtotal = strtr($row[12], array('.'=>'', ','=> ''));

					$kategori = $this->pegawaiModel->getKategori(['nip'=> $row[1]] );

					$id_sk = $this->skModel->getId(['no_sk' => trim($row[3])]); 
					$data = [
						'id_sk_dibayar' => $id_dibayar,
						'keterangan' => $row[5], 
						'nilai_maksimal' => $nilai_maksimal,
						'nilai_dibayar' => $nilai_dibayar,
						'volume' => $row[11],
						'total' => $subtotal,
						'nip' => $row[1],
						'nama' => $row[2],
						'satuan' => $row[8],
						'periode' => $row[7],
						'kode_file' => $row[6],
						'id_sk' => $id_sk,
						'kategori' => $kategori,
					];

					// print_r($data);die;
					$total = $total + (int)$subtotal;

					$simpan = $this->skdibayardetailModel->save($data);
				}
				
			}

			if ($id_dibayar > 0) {
				$this->skdibayarModel->update($id_dibayar, ['total_nominatif' => $total]);
			}

		}

		// exit;

		#end transaction
		$this->db->transComplete();

		session()->setFlashdata('success', 'Upload Data berhasil');
		return redirect()->to(base_url('data/sk/pembayaran')); 
	}

	public function nominatif()
	{
		$sParams = ['nip' => '', 'th' => $this->thn, 'bln_pengajuan' => '', 'bln_pembayaran' => '', 'kat' => '', 'jns' => ''];
		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$nipParams =  $uri->getQuery(['only' => ['nip']]);
			$katParams =  $uri->getQuery(['only' => ['kat']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$blnPengajuanParams =  $uri->getQuery(['only' => ['bln_pengajuan']]);
			$blnPembayaranParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);

			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);
				if ($thVal){
					$sParams['th'] = $thVal;		
				}
			}

			if ($nipParams) {
				list($nipKey, $nipVal) = explode('=',$nipParams);
				if ($nipVal){
					$sParams['nip'] = $nipVal;		
				}
			}

			if ($katParams) {
				list($katKey, $katVal) = explode('=',$katParams);
				if ($katVal){
					$sParams['kat'] = $katVal;		
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);
				if ($jnsVal){
					$sParams['jns'] = $jnsVal;		
				}
			}

			if ($blnPengajuanParams) {
				list($blnPengajuanKey, $blnPengajuanVal) = explode('=',$blnPengajuanParams);
				if ($blnPengajuanVal){
					$sParams['bln_pengajuan'] = $blnPengajuanVal;		
				}
			}

			if ($blnPembayaranParams) {
				list($blnPembayaranKey, $blnPembayaranVal) = explode('=',$blnPembayaranParams);
				if ($blnPembayaranVal){
					$sParams['bln_pembayaran'] = $blnPembayaranVal;		
				}
			}

		}

		$nipList = $this->pegawaiModel->getPegawai(['status_kepegawaian' => 'AKTIF']);

		// print_r($nipList);die;

		$data = [
	    	'menu' => 'data',
			'submenu' => 'sk', 
			'submenu2' => 'nominatif',
			'sParams' 	=> $sParams,
			'uriParams' => $uriParams,
			'nipList' => $nipList
	    ];
		// print_r($data);die;
		return view('data/sk/nominatif', $data);
	}

	// public function nominatif()
	// {
	// 	// $sParams = ['nip' => '', 'th' => $this->thn, 'bln_pengajuan' => '', 'bln_pembayaran' => '', 'kat' => '', 'jns' => ''];
	// 	// $uri = service('uri');
	// 	// $uriParams = $uri->getQuery();
	// 	// if ($uriParams) {
	// 	// 	$thParams =  $uri->getQuery(['only' => ['th']]);
	// 	// 	$nipParams =  $uri->getQuery(['only' => ['nip']]);
	// 	// 	$katParams =  $uri->getQuery(['only' => ['kat']]);
	// 	// 	$jnsParams =  $uri->getQuery(['only' => ['jns']]);
	// 	// 	$blnPengajuanParams =  $uri->getQuery(['only' => ['bln_pengajuan']]);
	// 	// 	$blnPembayaranParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);

	// 	// 	if ($thParams) {
	// 	// 		list($thKey, $thVal) = explode('=',$thParams);
	// 	// 		if ($thVal){
	// 	// 			$sParams['th'] = $thVal;		
	// 	// 		}
	// 	// 	}

	// 	// 	if ($nipParams) {
	// 	// 		list($nipKey, $nipVal) = explode('=',$nipParams);
	// 	// 		if ($nipVal){
	// 	// 			$sParams['nip'] = $nipVal;		
	// 	// 		}
	// 	// 	}

	// 	// 	if ($katParams) {
	// 	// 		list($katKey, $katVal) = explode('=',$katParams);
	// 	// 		if ($katVal){
	// 	// 			$sParams['kat'] = $katVal;		
	// 	// 		}
	// 	// 	}
	// 	// 	// print_r($katParams);die;
	// 	// 	if ($jnsParams) {
	// 	// 		list($jnsKey, $jnsVal) = explode('=',$jnsParams);
	// 	// 		if ($jnsVal){
	// 	// 			$sParams['jns'] = $jnsVal;		
	// 	// 		}
	// 	// 	}

	// 	// 	if ($blnPengajuanParams) {
	// 	// 		list($blnPengajuanKey, $blnPengajuanVal) = explode('=',$blnPengajuanParams);
	// 	// 		if ($blnPengajuanVal){
	// 	// 			$sParams['bln_pengajuan'] = $blnPengajuanVal;		
	// 	// 		}
	// 	// 	}

	// 	// 	if ($blnPembayaranParams) {
	// 	// 		list($blnPembayaranKey, $blnPembayaranVal) = explode('=',$blnPembayaranParams);
	// 	// 		if ($blnPembayaranVal){
	// 	// 			$sParams['bln_pembayaran'] = $blnPembayaranVal;		
	// 	// 		}
	// 	// 	}

	// 	// }

	// 	// $nipList = $this->pegawaiModel->getPegawai(['status_kepegawaian' => 'AKTIF']);

	// 	// // print_r($nipList);die;

	// 	// $data = [
	//     // 	'menu' => 'data',
	// 	// 	'submenu' => 'sk', 
	// 	// 	'submenu2' => 'nominatif',
	// 	// 	'sParams' 	=> $sParams,
	// 	// 	'uriParams' => $uriParams,
	// 	// 	'nipList' => $nipList
	//     // ];

	// 	$conditions = [];
		

	// 	$uri = service('uri');
	// 	$uriParams = $uri->getQuery();
	// 	if ($uriParams) {
	// 		$nipParams =  $uri->getQuery(['only' => ['nip']]);
	// 		$thParams =  $uri->getQuery(['only' => ['th']]);
	// 		$katParams =  $uri->getQuery(['only' => ['kat']]);
	// 		$jnsParams =  $uri->getQuery(['only' => ['jns']]);
	// 		$blnPengajuanParams =  $uri->getQuery(['only' => ['bln_pengajuan']]);
	// 		$blnPembayaranParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);
			
	// 		if ($thParams) {
	// 			list($thKey, $thVal) = explode('=',$thParams);	
	// 			if ($thVal){
	// 				$conditions = array_merge($conditions, ['t_sk_dibayar.thn_anggaran' => $thVal]);				
	// 			}
	// 		}

	// 		if ($nipParams) {
	// 			list($nipKey, $nipVal) = explode('=',$nipParams);	
	// 			if ($nipVal && $nipVal != "all"){
	// 				$conditions = array_merge($conditions, ['t_sk_dibayar_detail.nip' => $nipVal]);				
	// 			}
	// 		}

	// 		if ($katParams) {
	// 			list($katKey, $katVal) = explode('=',$katParams);	
	// 			// if ($katVal){
	// 				if($katVal == 'null'){
	// 					$conditions = array_merge($conditions, ['t_sk_dibayar_detail.kategori' => null]);
	// 				}elseif($katVal){
	// 					$conditions = array_merge($conditions, ['lower(t_sk_dibayar_detail.kategori)' => strtolower($katVal)]);
	// 				}

	// 				// $conditions = array_merge($conditions, ['t_pegawai.jenis_penugasan' => $katVal]);				
	// 			// }
	// 		}

	// 		if ($jnsParams) {
	// 			list($jnsKey, $jnsVal) = explode('=',$jnsParams);	
	// 			if ($jnsVal){
	// 				$conditions = array_merge($conditions, ['substr(t_sk_dibayar.id_jenis_honor,7,2)' => $jnsVal]);				
	// 			}
	// 		}

	// 		if ($blnPengajuanParams) {
	// 			list($blnPengajuanKey, $blnPengajuanVal) = explode('=',$blnPengajuanParams);
	// 			if ($blnPengajuanVal){
	// 				$sParams['bln_pengajuan'] = $blnPengajuanVal;	
	// 				$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pengajuan)' => $blnPengajuanVal]);					
	// 			}
	// 		}

	// 		if ($blnPembayaranParams) {
	// 			list($blnPembayaranKey, $blnPembayaranVal) = explode('=',$blnPembayaranParams);
	// 			if ($blnPembayaranVal){
	// 				$sParams['bln_pembayaran'] = $blnPembayaranVal;		
	// 				$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pembayaran)' => $blnPembayaranVal]);					
	// 			}
	// 		}
			
	// 	}
		
	// 	$data = $this->skdibayardetailModel->getAllAlt($conditions);
	// 	// print_r("masuk");exit;

	// 	// print_r($katParams);die;
	// 	return view('data/sk/nominatif', $data);
	// }

	public function api_data()
	{
		$conditions = ['tahun' => $this->thn];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thParams =  $uri->getQuery(['only' => ['th']]);
			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['tahun' => $thVal]);				
				}
			}
		}

		$data = $this->skModel->getAll($conditions);
		$totalData = count($data);

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);
	}

	public function api_data_pembayaran()
	{
		// $data = $this->skdibayarModel->getAll(['t_sk_dibayar.thn_anggaran' => $this->thn]);
		$data = $this->skdibayarModel->getAll(['t_sk_dibayar.thn_anggaran' => '2022']);
		// print_r($data);exit;
		$totalData = count($data);

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);
	}

	// public function api_data_pengajuan()
	// {
	// 	$data = $this->skModel->getAll();
	// 	$totalData = count($data);

	// 	return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);
	// }

	public function api_data_nominatif()
	{
		$conditions = [];
		

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$nipParams =  $uri->getQuery(['only' => ['nip']]);
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$katParams =  $uri->getQuery(['only' => ['kat']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$blnPengajuanParams =  $uri->getQuery(['only' => ['bln_pengajuan']]);
			$blnPembayaranParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);
			
			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['t_sk_dibayar.thn_anggaran' => $thVal]);				
				}
			}

			if ($nipParams) {
				list($nipKey, $nipVal) = explode('=',$nipParams);	
				if ($nipVal && $nipVal != "all"){
					$conditions = array_merge($conditions, ['t_sk_dibayar_detail.nip' => $nipVal]);				
				}
			}

			if ($katParams) {
				list($katKey, $katVal) = explode('=',$katParams);	
				// if ($katVal){
				// $conditions = array_merge($conditions, ['t_sk_dibayar_detail.kategori' => $katVal]);
				// // $conditions = array_merge($conditions, ['t_pegawai.jenis_penugasan' => $katVal]);				
				// }
				if($katVal == 'null'){
					$conditions = array_merge($conditions, ['t_sk_dibayar_detail.kategori' => null]);
				}elseif($katVal){
					$conditions = array_merge($conditions, ['lower(t_sk_dibayar_detail.kategori)' => strtolower($katVal)]);
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);	
				if ($jnsVal){
					$conditions = array_merge($conditions, ['substr(t_sk_dibayar.id_jenis_honor,7,2)' => $jnsVal]);				
				}
			}

			if ($blnPengajuanParams) {
				list($blnPengajuanKey, $blnPengajuanVal) = explode('=',$blnPengajuanParams);
				if ($blnPengajuanVal){
					$sParams['bln_pengajuan'] = $blnPengajuanVal;	
					$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pengajuan)' => $blnPengajuanVal]);					
				}
			}

			if ($blnPembayaranParams) {
				list($blnPembayaranKey, $blnPembayaranVal) = explode('=',$blnPembayaranParams);
				if ($blnPembayaranVal){
					$sParams['bln_pembayaran'] = $blnPembayaranVal;		
					$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pembayaran)' => $blnPembayaranVal]);					
				}
			}
			
		}

		$data = $this->skdibayardetailModel->getAllAlt($conditions);

		// print_r($data);exit;
		
		$totalData = count($data);

		return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);

	}

	public function gen_pdf_nominatif() 
	{
		$conditions = [];
		$nipVal = '1';
		$nama = 'Nama';

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$nipParams =  $uri->getQuery(['only' => ['nip']]);
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$katParams =  $uri->getQuery(['only' => ['kat']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$blnParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);
			
			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['t_sk_dibayar.thn_anggaran' => $thVal]);				
				} 
			}

			if ($nipParams) {
				list($nipKey, $nipVal) = explode('=',$nipParams);	
				if ($nipVal){
					$conditions = array_merge($conditions, ['t_sk_dibayar_detail.nip' => $nipVal]);	
					$peg = $this->pegawaiModel->getPegawaiByNip($nipVal);
					if (count($peg) > 0) {
						$nama = $peg['nama'];
					}			
				}
			}

			if ($katParams) {
				list($katKey, $katVal) = explode('=',$katParams);	
				if ($katVal){
					$conditions = array_merge($conditions, ['lower(t_sk_dibayar_detail.kategori)' => strtolower($katVal)]);				
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);	
				if ($jnsVal){
					$conditions = array_merge($conditions, ['substr(t_sk_dibayar.id_jenis_honor,7,2)' => $jnsVal]);				
				}
			}

			if ($blnParams) {
				list($blnKey, $blnVal) = explode('=',$blnParams);	
				if ($blnVal){
					$blnVal = str_pad($blnVal,2,"0", STR_PAD_LEFT);
					$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pembayaran)' => $blnVal]);				
				}
			}
			
		}

		// print_r($conditions);exit;

		// print_r($conditions);exit;

		$dataDibayar = $this->skdibayardetailModel->getAllAlt($conditions, 't_sk_dibayar.no_spp');

		// print_r($dataDibayar);exit;

		$data = [
	    	// 'menu' => 'data',
			// 'submenu' => 'sk', 
			// 'submenu2' => 'nominatif',
			// 'sParams' 	=> $sParams,
			// 'uriParams' => $uriParams,
			'dataDibayar' => $dataDibayar
	    ];

		// return view('data/sk/nominatif_2', $data);


		$html = '<!DOCTYPE html>
					<html lang="en">
					<head>
					<meta charset="utf-8">
					<title>Example 1</title>
					
					<style>
						
body {
  margin: 0;
  font-family: var(--bs-font-sans-serif);
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: #212529;
  background-color: #fff;
  -webkit-text-size-adjust: 100%;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

h1, .h1 {
  font-size: calc(1.375rem + 1.5vw);
  padding: 0.1rem;
}
@media (min-width: 1200px) {
  h1, .h1 {
    font-size: 2.5rem;
    padding: 0.1rem;
  }
}

h2, .h2 {
  font-size: calc(1.325rem + 0.9vw);
  padding: 0.1rem;
}
@media (min-width: 1200px) {
  h2, .h2 {
    font-size: 2rem;
    padding: 0.1rem;
  }
}

h3, .h3 {
  font-size: calc(1.3rem + 0.6vw);
  padding: 0.1rem;
}
@media (min-width: 1200px) {
  h3, .h3 {
    font-size: 1.75rem;
    padding: 0.1rem;
  }
}

h4, .h4 {
  font-size: calc(1.275rem + 0.3vw);
}
@media (min-width: 1200px) {
  h4, .h4 {
    font-size: 1.5rem;
  }
}

h5, .h5 {
  font-size: 1.25rem;
}

h6, .h6 {
  font-size: 1rem;
}

hr {
  margin: 1rem 0;
  color: inherit;
  background-color: currentColor;
  border: 0;
  opacity: 0.25;
}

hr .tebel {
  margin: 2rem 0;
  color: inherit;
  background-color: currentColor;
  border: 1;
  opacity: 0.25;
}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  margin-bottom: 1rem;
  padding: 0.5rem 0.5rem;
  font-size: 0.75rem;
}

.row {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -12.5px;
  margin-left: -12.5px; 
}

.col-left, .col-right {
  position: relative;
  width: 100%;
  padding-right: 12.5px;
  padding-left: 12.5px; 
}

.col-left {
	-webkit-box-flex: 0;
    -ms-flex: 0 0 25%;
    flex: 0 0 25%;
    max-width: 25%; 
}

.col-right {
	-webkit-box-flex: 0;
    -ms-flex: 0 0 75%;
    flex: 0 0 75%;
    max-width: 75%; 
}



					</style>
					</head>
					<body>
						<header class="clearfix">
						<div id="logo">
							<!--img src="themes/logo/companylogo.jpeg" width="200"-->
						</div>
						<div class="container">
					    <div class="row">
					        <div class="panel panel-default">
					        	<div class="panel-heading" style="text-align: center">
					        		<img src="https://dashboard.ftmd.itb.ac.id/assets/gajah_itb_trans.png" width="150" style="float: left">
				    				<!--h1 class="panel-title"><strong>INSTITUT TEKNOLOGI BANDUNG</strong></h1-->
				    				<!--h3 class="panel-title"><strong>FAKULTAS TEKNIK MESIN DAN DIRGANTARA</strong></h3-->
				    				<span style="font-size: 2.5rem;"><strong>INSTITUT TEKNOLOGI BANDUNG</strong></span><br />
				    				<span style="font-size: 1.75rem;"><strong>FAKULTAS TEKNIK MESIN DAN DIRGANTARA</strong></span><br />
				    				<span>Gedung Labtek II lantai 2 Jalan Ganesa 10 Bandung 40132, Telp:+6222 2504243, Fax:+6222 2505425</span><br />
				    				<span>Email:info@ftmd.itb.ac.id, Website: www.ftmd.itb.ac.id</span>
				    			</div>
				    			<div class="panel-body" >
				    				<div class="table-responsive">
				    					<div style="border-bottom: 3px solid black; margin-bottom:25px;" width="100%">&nbsp;</div>
				    					<!--div style="border-bottom: 1px solid black" width="100%">&nbsp;</div-->

				    					<table style="border:0px;font-size: 1rem;">
				    						<tr style="border:0px;"><td style="border:0px;font-size: 1rem;">NIP</td><td style="border:0px;font-size: 1rem;">: '.$nipVal.'</td></tr>
				    						<tr style="border:0px;"><td style="border:0px;font-size: 1rem;">Nama</td><td style="border:0px;font-size: 1rem;">: '.$nama.'</td></tr>
				    					</table>

    									<table>
    									<thead>
											<tr>
												<th>No.</th>
												<!--th>NIP</th>
												<th>Nama</th-->
												<th>No. SK</th>
												<th>Judul SK</th>												
												<th>Keterangan</th>
												<th>Periode</th>
												<th>Tgl. Transfer</th>
												<th>Total</th>
											</tr>
										</thead>
				
			    						<tbody>
			    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
			    							';
			    							$i = 0; $nospp = ''; $subtotal = 0; $total  = 0; $kegiatan = '';
			    							// echo count($dataDibayar);exit;

											foreach ($dataDibayar as $dd) {
												if ($nospp == $dd['no_spp']) {
													$i++;	
													$html.= '<tr>';
														$html.= '<td>'.$i.'</td>';
														// $html.= '<td>'.$dd["nip"].'</td>';
														// $html.= '<td>'.$dd["nama"].'</td>';
														$html.= '<td>'.$dd["no_sk"].'</td>';
														$html.= '<td>'.$dd["judul_sk"].'</td>';														
														$html.= '<td>'.$dd["keterangan_detail"].'</td>';
														$html.= '<td>'.$dd["periode_detail"].'</td>';
														$html.= '<td>'.$dd["tgl_pembayaran_f"].'</td>';
														$html.= '<td class="text-lg-right">Rp. '.number_format($dd["total"]).'</td>';
													$html.= '</tr>';
													$subtotal = $subtotal + $dd['total'];
													$total = $total + $dd['total'];
													
												} else {												
													
													if ($i > 0) {
														$html.= '<tr>';
															$html.= '<td></td>';
															$html.= '<td colspan="5"><strong>Subtotal '.$kegiatan.'</strong></td>';
															$html.= '<td class="text-lg-right"><strong>Rp. '.number_format($subtotal).'</strong></td>';
														$html.= '</tr>';

													}

													$nospp = $dd['no_spp'];
													$kegiatan = $dd['kegiatan'];

													$i++;
													$subtotal = 0;

													$html.= '<tr>';
														$html.= '<td>'.$i.'</td>';
														// $html.= '<td>'.$dd["nip"].'</td>';
														// $html.= '<td>'.$dd["nama"].'</td>';
														$html.= '<td>'.$dd["no_sk"].'</td>';
														$html.= '<td>'.$dd["judul_sk"].'</td>';														
														$html.= '<td>'.$dd["keterangan_detail"].'</td>';
														$html.= '<td>'.$dd["periode_detail"].'</td>';
														$html.= '<td>'.$dd["tgl_pembayaran_f"].'</td>';
														$html.= '<td class="text-lg-right">Rp. '.number_format($dd["total"]).'</td>';
													$html.= '</tr>';


													$subtotal = $subtotal + $dd['total'];
													$total = $total + $dd['total'];
																									}
												
											}
											$html.= '<tr>';
												$html.= '<td></td>';
												$html.= '<td colspan="5"><strong>Subtotal '.$kegiatan.'</strong></td>';
												$html.= '<td class="text-lg-right"><strong>Rp. '.number_format($subtotal).'</strong></td>';
											$html.= '</tr>';
											$html.= '<tr>';
												$html.= '<td></td>';
												$html.= '<td colspan="5"><strong>Total</strong></td>';
												$html.= '<td class="text-lg-right"><strong>Rp. '.number_format($total).'</strong></td>';
											$html.= '</tr>';
			    							
			    						$html.=	'
			    								
			    						</tbody>
    									</table>
    								</div>
    								<div style="margin-top:25px;" width="100%">
    								Keterangan : <br />
    								<table style="border:0px;font-size: 1rem;">
			    						<tr style="border:0px;"><td style="border:0px;font-size: 1rem;vertical-align:top;">1. </td><td style="border:0px;font-size: 1rem;">Nominal honor di atas merupakan nominal yang belum dikurangi pajak.</td></tr>
			    						<tr style="border:0px;"><td style="border:0px;font-size: 1rem;vertical-align:top;">2. </td><td style="border:0px;font-size: 1rem;">Nama insentif atau honor yang tertulis dalam website https://remunerasi.itb.ac.id/ belum tentu sesuai dengan program studi terkait, dikarenakan FTMD menganut sistem sharing anggaran untuk kegiatan yang berlangsung di lingkungan FTMD</td></tr>
			    					</table>
    								</div>
				    			</div>
					        </div>
					    </div>
					</body>
					</html>
			';

// echo $html;exit;

		$dompdf = new \Dompdf\Dompdf(['isRemoteEnabled' => true]); 
		// $dompdf = new \Dompdf\Dompdf(); 
        // $dompdf->loadHtml(view('data/sk/nominatif_2', $data));
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($nipVal.' - '.$nama.'.pdf', [ 'Attachment' => true ]);



		// $file = WRITEPATH . 'uploads/'.$conditions['t_sk_dibayar_detail.nip'].'.pdf';

        // $filename = 'Nominatif_'.$conditions['t_sk_dibayar_detail.nip'].'.pdf'; /* Note: Always use .pdf at the end. */

        // header('Content-type: application/pdf');
        // header('Content-Disposition: inline; filename="' . $filename . '"');
        // header('Content-Transfer-Encoding: binary');
        // header('Content-Length: ' . filesize($file));
        // header('Accept-Ranges: bytes');

        // @readfile($file); exit;
	}

	public function gen_excel_nominatif()
	{

		$conditions = [];

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$nipParams =  $uri->getQuery(['only' => ['nip']]);
			$thParams =  $uri->getQuery(['only' => ['th']]);
			$katParams =  $uri->getQuery(['only' => ['kat']]);
			$jnsParams =  $uri->getQuery(['only' => ['jns']]);
			$blnParams =  $uri->getQuery(['only' => ['bln_pembayaran']]);
			
			if ($thParams) {
				list($thKey, $thVal) = explode('=',$thParams);	
				if ($thVal){
					$conditions = array_merge($conditions, ['t_sk_dibayar.thn_anggaran' => $thVal]);				
				} 
			}

			if ($nipParams) {
				list($nipKey, $nipVal) = explode('=',$nipParams);	
				if ($nipVal){
					$conditions = array_merge($conditions, ['t_sk_dibayar_detail.nip' => $nipVal]);				
				}
			}

			if ($katParams) {
				list($katKey, $katVal) = explode('=',$katParams);	
				if ($katVal){
					$conditions = array_merge($conditions, ['lower(t_sk_dibayar_detail.kategori)' => strtolower($katVal)]);				
				}
			}

			if ($jnsParams) {
				list($jnsKey, $jnsVal) = explode('=',$jnsParams);	
				if ($jnsVal){
					$conditions = array_merge($conditions, ['substr(t_sk_dibayar.id_jenis_honor,7,2)' => $jnsVal]);				
				}
			}

			if ($blnParams) {
				list($blnKey, $blnVal) = explode('=',$blnParams);	
				if ($blnVal){
					$blnVal = str_pad($blnVal,2,"0", STR_PAD_LEFT);
					$conditions = array_merge($conditions, ['month(t_sk_dibayar.tgl_pembayaran)' => $blnVal]);				
				}
			}
			
		}

		// print_r($conditions);exit;

		$data = $this->skdibayardetailModel->getAllAlt($conditions);

		// print_r($data);exit;

		// $totalData = count($data);

		// return $this->setResponseFormat('json')->respond(['recordsTotal' => $totalData, 'recordsFiltered' => $totalData, 'data' => $data]);

		$spreadsheet = new Spreadsheet();
	    $spreadsheet->setActiveSheetIndex(0)
	    			->setCellValue('A2', 'NIP')
	                ->setCellValue('B2', 'Nama')
	                ->setCellValue('C2', 'No. SK')
	                ->setCellValue('D2', 'Judul SK')
	                ->setCellValue('E2', 'Total')
	                ->setCellValue('F2', 'keterangan')
	                ->setCellValue('G2', 'Tgl Transfer');
	    
	    $column = 3;
	    foreach($data as $dt) {
	        $spreadsheet->setActiveSheetIndex(0)
	                    ->setCellValue('A' . $column, '="'.$dt['nip'].'"')
	                    ->setCellValue('B' . $column, $dt['nama'])
	                    ->setCellValue('C' . $column, $dt['no_sk'])
	                    ->setCellValue('D' . $column, $dt['judul_sk'])
	                    ->setCellValue('E' . $column, $dt['total'])
	                    ->setCellValue('F' . $column, $dt['keterangan_detail'])
	                    ->setCellValue('G' . $column, $dt['tgl_pembayaran']);
	        $column++;
	    }

	    // $spreadsheet->setActiveSheetIndex(0)->setCellValue('A'. $column+1, 'Catatan: Nilai yang tertera belum dikurangi dengan Pph21');
	    // tulis dalam format .xlsx
	    $writer = new Xlsx($spreadsheet, 'Excel2007');
	    $fileName = 'Data Nominatif';

	    // Redirect hasil generate xlsx ke web client
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    // header('Content-Type: Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
	    header('Cache-Control: max-age=0');

	    $writer->save('php://output');

	}

}
