<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		
		switch (session()->get('role')) {
			case 'anggaran':
				return redirect()->to(base_url('dashboard/anggaran'));
				break;
			case 'kepegawaian':
				return redirect()->to(base_url('dashboard/kepegawaian'));
				break;
			case 'akademik':
				return redirect()->to(base_url('dashboard/sk'));
				break;
			case 'sarpras':
				return redirect()->to(base_url('dashboard/pengadaan'));
				break;
			
			default:
				return redirect()->to(base_url('dashboard/anggaran'));
				break;
		}
	}

	//--------------------------------------------------------------------

}
