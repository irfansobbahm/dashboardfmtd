<?php namespace App\Controllers\Dashboard;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\AnggaranunitModel;

class Anggaran extends BaseController
{
	use ResponseTrait;

	protected $anggaranModel;
	protected $pembagi; 
	protected $arrBulan;

	public function __construct()
	{
    	$this->thn = session()->get('tahun');
    	$this->pembagi = 1000000000;

    	$this->anggaranModel = new AnggaranunitModel();
    	$this->arrBulan = [
    		'1' => 'Januari', 
    		'2' => 'Februari',
    		'3' => 'Maret', 
    		'4' => 'April',
    		'5' => 'Mei', 
    		'6' => 'Juni',
    		'7' => 'Juli', 
    		'8' => 'Agustus',
    		'9' => 'September', 
    		'10' => 'Oktober',
    		'11' => 'November', 
    		'12' => 'Desember'
    	];
	}

	public function index($thn = null)
	{

		$uri = service('uri');
		$uriParams = $uri->getQuery();

		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);

			if ($thnParams) {
				list($thnKey, $thnVal) = explode('=',$thnParams);
				if ($thnVal){
					$thn = $thnVal;		
				}
			}
		}

		$thn = $thn ? $thn : $this->thn;

		$summary = $this->anggaranModel->getSumRkaAll(['thn_anggaran' => $thn]);
		// print_r($summary);die;	
		$last_update = $this->anggaranModel->getLastUpdate();

		$data = [
			'menu' 		  => 'dashboard',
			'submenu' 	  => 'anggaran',
			'submenu2' 	  => $thn,
			'submenu3' 	  => 'semua',
			'summary' 	  => $summary,
			'total'		  => bcdiv(($summary['rka']+$summary['realokasi']+$summary['kerma_alihan']+$summary['kerma_non_alihan']+$summary['pengalihan_masuk']-$summary['pengalihan_keluar']),$this->pembagi,2),
			'last_update' => (is_array($last_update) ? $last_update['tanggal'].' '.$this->arrBulan[$last_update['bulan']].' '.$last_update['tahun'] : '-')
		];

		// print_r($data); exit;

		return view('dashboard/anggaran/index', $data);
	}

	public function sispran1($thn = null)
	{
		$thn = $thn ? $thn : $this->thn;
		$summary = $this->anggaranModel->getSumRkaSispran1([ 'thn_anggaran' => $thn ]);
		$last_update = $this->anggaranModel->getLastUpdate([ 'thn_anggaran' => $thn ]);
		// print_r($summary);die;
		$data = [
			'menu' => 'dashboard',
			'submenu' => 'anggaran',
			'submenu2' => $thn,
			'submenu3' => 'sispran 1',
			'summary' 	  => $summary,
			'total'		  => bcdiv(($summary['rka']+$summary['pengalihan_masuk']-$summary['pengalihan_keluar']),$this->pembagi,2),
			'last_update' => (is_array($last_update) ? $last_update['tanggal'].' '.$this->arrBulan[$last_update['bulan']].' '.$last_update['tahun'] : '-')
		];
		return view('dashboard/anggaran/tahunan_sispran1', $data);
	}

	public function sispran2($thn = null)
	{
		$thn = $thn ? $thn : $this->thn;
		$summary = $this->anggaranModel->getSumRkaSispran2([ 'thn_anggaran' => $thn ]);
		$last_update = $this->anggaranModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

		$data = [
			'menu' => 'dashboard',
			'submenu' => 'anggaran',
			'submenu2' => $thn,
			'submenu3' => 'sispran 2',
			'summary' 	  => $summary,
			'total'		  => bcdiv(($summary['realokasi']+$summary['kerma_alihan']+$summary['kerma_non_alihan']),$this->pembagi,2),
			'last_update' => (is_array($last_update) ? $last_update['tanggal'].' '.$this->arrBulan[$last_update['bulan']].' '.$last_update['tahun'] : '-')
		];
		// print_r($data);die;
		return view('dashboard/anggaran/tahunan_sispran2', $data);
	}

	//--------------------------------------------------------------------

	public function api_penyerapan_persen($sispran, $thn = null)
	{
		// $data = $this->pegawaiModel->getSummaryLamaMenjabat();

		// print_r($sispran);die;
		if ($sispran > '0')
			$conditions = ['sispran' => $sispran];
			// $conditions = [];
					
		else
			$conditions = [];
		$thn = $thn ? $thn : $this->thn;

		$summary = $this->anggaranModel->getSumRkaAll(array_merge($conditions, [ 'thn_anggaran' => $thn ]));

		// print_r($summary);exit;

		$total = $summary['rka']+$summary['realokasi']+$summary['kerma_alihan']+$summary['kerma_non_alihan']+$summary['pengalihan_masuk']-$summary['pengalihan_keluar'];

		$ri = round(($summary['ri']/$total)*100,2);
		// $fra = round(($summary['fra']/$total)*100,2);
		$realisasi = round(($summary['realisasi']/$total)*100,2);

		// $data = [$ri, $fra, $realisasi];
		$data = [$ri, $realisasi];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_penyerapan_persen_s1($sispran, $thn = null)
	{
		// $data = $this->pegawaiModel->getSummaryLamaMenjabat();

		// print_r($sispran);die;
		if ($sispran > '0')
			$conditions = ['sispran' => $sispran];
					
		else
			$conditions = [];
		$thn = $thn ? $thn : $this->thn;

		$summary = $this->anggaranModel->getSumRkaAll(array_merge($conditions, [ 'thn_anggaran' => $thn ]));

		// print_r($summary);exit;

		// $total = $summary['rka']+$summary['realokasi']+$summary['kerma_alihan']+$summary['kerma_non_alihan']+$summary['pengalihan_masuk']-$summary['pengalihan_keluar'];
		$total = $summary['rka']+$summary['pengalihan_masuk']-$summary['pengalihan_keluar'];

		$ri = round(($summary['ri']/$total)*100,2);
		// $fra = round(($summary['fra']/$total)*100,2);
		$realisasi = round(($summary['realisasi']/$total)*100,2);

		$data = [$ri, $realisasi];
		
		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_penyerapan_persen_s2($sispran, $thn = null)
	{
		// $data = $this->pegawaiModel->getSummaryLamaMenjabat();

		// print_r($sispran);die;
		if ($sispran > '0')
			$conditions = ['sispran' => $sispran];
		else
			$conditions = [];
		$thn = $thn ? $thn : $this->thn;

		$summary = $this->anggaranModel->getSumRkaAll(array_merge($conditions, [ 'thn_anggaran' => $thn ]));

		// print_r($summary);exit;

		$total = $summary['realokasi']+$summary['kerma_alihan']+$summary['kerma_non_alihan'];

		$kerma_non 		= round(($summary['kerma_non_alihan']/$total)*100,2);
		$kerma_alihan 	= round(($summary['kerma_alihan']/$total)*100,2);
		$kerma_re 		= round(($summary['realokasi']/$total)*100,2);

		$data = [$kerma_alihan, $kerma_non, $kerma_re];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_penyerapan($sispran, $thn = null)
	{
		if ($sispran > '0')
			$conditions = ['sispran' => $sispran];		
		else
			$conditions = [];

		$thn = $thn ? $thn : $this->thn;

		// $fra = $this->anggaranModel->getPenyerapanKumulatif(array_merge($conditions, ['thn_anggaran' => $thn, 'jenis' => 'FRA']));
		$realisasi = $this->anggaranModel->getPenyerapanKumulatif(array_merge($conditions, ['thn_anggaran' => $thn, 'jenis' => 'REALISASI']));
		
		$realokasi = $this->anggaranModel->getPenyerapanKumulatif(array_merge($conditions, ['thn_anggaran' => $thn, 'jenis' => 'REALOKASI']));

		// print_r($realisasi);exit;

		$data = [
			// 'fra' => [
			// 	bcdiv($fra['jan'],$this->pembagi,2),
			// 	bcdiv($fra['feb'],$this->pembagi,2),
			// 	bcdiv($fra['mar'],$this->pembagi,2),
			// 	bcdiv($fra['apr'],$this->pembagi,2),
			// 	bcdiv($fra['mei'],$this->pembagi,2),
			// 	bcdiv($fra['jun'],$this->pembagi,2),
			// 	bcdiv($fra['jul'],$this->pembagi,2),
			// 	bcdiv($fra['agu'],$this->pembagi,2),
			// 	bcdiv($fra['sep'],$this->pembagi,2),
			// 	bcdiv($fra['okt'],$this->pembagi,2),
			// 	bcdiv($fra['nov'],$this->pembagi,2),
			// 	bcdiv($fra['des'],$this->pembagi,2)
			// ],
			'realisasi' => [
				bcdiv($realisasi['jan'],$this->pembagi,2),
				bcdiv($realisasi['feb'],$this->pembagi,2),
				bcdiv($realisasi['mar'],$this->pembagi,2),
				bcdiv($realisasi['apr'],$this->pembagi,2),
				bcdiv($realisasi['mei'],$this->pembagi,2),
				bcdiv($realisasi['jun'],$this->pembagi,2),
				bcdiv($realisasi['jul'],$this->pembagi,2),
				bcdiv($realisasi['agu'],$this->pembagi,2),
				bcdiv($realisasi['sep'],$this->pembagi,2),
				bcdiv($realisasi['okt'],$this->pembagi,2),
				bcdiv($realisasi['nov'],$this->pembagi,2),
				bcdiv($realisasi['des'],$this->pembagi,2)
			],
			'realokasi' => [
				bcdiv($realokasi['jan'],$this->pembagi,2),
				bcdiv($realokasi['feb'],$this->pembagi,2),
				bcdiv($realokasi['mar'],$this->pembagi,2),
				bcdiv($realokasi['apr'],$this->pembagi,2),
				bcdiv($realokasi['mei'],$this->pembagi,2),
				bcdiv($realokasi['jun'],$this->pembagi,2),
				bcdiv($realokasi['jul'],$this->pembagi,2),
				bcdiv($realokasi['agu'],$this->pembagi,2),
				bcdiv($realokasi['sep'],$this->pembagi,2),
				bcdiv($realokasi['okt'],$this->pembagi,2),
				bcdiv($realokasi['nov'],$this->pembagi,2),
				bcdiv($realokasi['des'],$this->pembagi,2)
			]
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_penyerapan_perbulan($sispran, $thn = null)
	{
		if ($sispran > '0')
			$conditions = ['sispran' => $sispran];		
		else
			$conditions = [];

		$thn = $thn ? $thn : $this->thn;

		// $fra = $this->anggaranModel->getPenyerapanPerbulan(array_merge($conditions, ['thn_anggaran' => $thn, 'jenis' => 'FRA']));
		$realisasi = $this->anggaranModel->getPenyerapanPerbulan(array_merge($conditions, ['thn_anggaran' => $thn, 'jenis' => 'REALISASI']));

		$data = [
			// 'fra' => [
			// 	bcdiv($fra['jan'],$this->pembagi,2),
			// 	bcdiv($fra['feb'],$this->pembagi,2),
			// 	bcdiv($fra['mar'],$this->pembagi,2),
			// 	bcdiv($fra['apr'],$this->pembagi,2),
			// 	bcdiv($fra['mei'],$this->pembagi,2),
			// 	bcdiv($fra['jun'],$this->pembagi,2),
			// 	bcdiv($fra['jul'],$this->pembagi,2),
			// 	bcdiv($fra['agu'],$this->pembagi,2),
			// 	bcdiv($fra['sep'],$this->pembagi,2),
			// 	bcdiv($fra['okt'],$this->pembagi,2),
			// 	bcdiv($fra['nov'],$this->pembagi,2),
			// 	bcdiv($fra['des'],$this->pembagi,2)
			// ],
			'realisasi' => [
				bcdiv($realisasi['jan'],$this->pembagi,2),
				bcdiv($realisasi['feb'],$this->pembagi,2),
				bcdiv($realisasi['mar'],$this->pembagi,2),
				bcdiv($realisasi['apr'],$this->pembagi,2),
				bcdiv($realisasi['mei'],$this->pembagi,2),
				bcdiv($realisasi['jun'],$this->pembagi,2),
				bcdiv($realisasi['jul'],$this->pembagi,2),
				bcdiv($realisasi['agu'],$this->pembagi,2),
				bcdiv($realisasi['sep'],$this->pembagi,2),
				bcdiv($realisasi['okt'],$this->pembagi,2),
				bcdiv($realisasi['nov'],$this->pembagi,2),
				bcdiv($realisasi['des'],$this->pembagi,2)
			]
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_alokasi_pegawai($sispran, $thn = null)
	{
		$thn = $thn ? $thn : $this->thn;
		$conditions = ['thn_anggaran' => $thn, 'jenis_belanja' => '01'];
		if ($sispran > '0')
		$conditions = array_merge($conditions, ['sispran' => $sispran]);				
		
		$summaryUnit = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 0]));
		
		$summaryLog = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 1]));
		// $data2 = $sispran;
		if ($sispran == '2') {
			// $data2 = $sispran;
			$data = [
				'unit' => [
					bcdiv($summaryUnit['rka'],$this->pembagi,3),
					bcdiv($summaryUnit['ri'],$this->pembagi,3),
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['rka'],$this->pembagi,3), 
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
					]	
				];
			} else {
				$data = [
					'unit' => [
						bcdiv($summaryUnit['rka'],$this->pembagi,3),
						bcdiv($summaryUnit['ri'],$this->pembagi,3),
						bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
					],
					'logistik' => [
						bcdiv($summaryLog['rka'],$this->pembagi,3), 
						bcdiv($summaryLog['ri'],$this->pembagi,3), 
						bcdiv($summaryLog['realisasi'],$this->pembagi,3)
						]				
					];

				}
				
			// print_r($data);die;
			return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_alokasi_barang($sispran, $thn = null)
	{
		$thn = $thn ? $thn : $this->thn;

		$conditions = ['thn_anggaran' => $thn, 'jenis_belanja' => '02'];
		if ($sispran > '0')
			$conditions = array_merge($conditions, ['sispran' => $sispran]);			
			// $data2 = $conditions;
		$summaryUnit = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 0]));
		$summaryLog = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 1]));

		if ($sispran == '2') {
		
			$data = [
				'unit' => [
					bcdiv($summaryUnit['rka'],$this->pembagi,3),
					bcdiv($summaryUnit['ri'],$this->pembagi,3),
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['rka'],$this->pembagi,3), 
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]	
			];
		} else {
			// $data2 = $sispran;
			$data = [
				'unit' => [
					bcdiv($summaryUnit['rka'],$this->pembagi,3), 
					bcdiv($summaryUnit['ri'],$this->pembagi,3), 
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['rka'],$this->pembagi,3), 
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]				
			];
		}

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_alokasi_jasa($sispran, $thn = null)
	{
		$thn = $thn ? $thn : $this->thn;

		$conditions = ['thn_anggaran' => $thn, 'jenis_belanja' => '03'];
		if ($sispran > '0')
			$conditions = array_merge($conditions, ['sispran' => $sispran]);			

		$summaryUnit = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 0]));
		$summaryLog = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 1]));


		if ($sispran == '2') {
			$data = [
				'unit' => [
					bcdiv($summaryUnit['ri'],$this->pembagi,3), 
					bcdiv($summaryUnit['fra'],$this->pembagi,3), 
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['fra'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]	
			];
		} else {
			$data = [
				'unit' => [
					bcdiv($summaryUnit['rka'],$this->pembagi,3), 
					bcdiv($summaryUnit['ri'],$this->pembagi,3), 
					bcdiv($summaryUnit['fra'],$this->pembagi,3), 
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['rka'],$this->pembagi,3), 
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['fra'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]				
			];
		}

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_alokasi_modal($sispran, $thn = null)
	{
		$thn = $thn ? $thn : $this->thn;

		$conditions = ['thn_anggaran' => $thn, 'jenis_belanja' => '04'];
		if ($sispran > '0')
			$conditions = array_merge($conditions, ['sispran' => $sispran]);	
			

		$summaryUnit = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 0]));
		$summaryLog = $this->anggaranModel->getSumAlokasi(array_merge($conditions, ['logistik' => 1]));


		if ($sispran == '2') {
			$data = [
				'unit' => [
					bcdiv($summaryUnit['ri'],$this->pembagi,3), 
					bcdiv($summaryUnit['fra'],$this->pembagi,3), 
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['fra'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]	
			];
		} else {
			$data = [
				'unit' => [
					bcdiv($summaryUnit['rka'],$this->pembagi,3), 
					bcdiv($summaryUnit['ri'],$this->pembagi,3), 
					bcdiv($summaryUnit['fra'],$this->pembagi,3), 
					bcdiv($summaryUnit['realisasi'],$this->pembagi,3)
				],
				'logistik' => [
					bcdiv($summaryLog['rka'],$this->pembagi,3), 
					bcdiv($summaryLog['ri'],$this->pembagi,3), 
					bcdiv($summaryLog['fra'],$this->pembagi,3), 
					bcdiv($summaryLog['realisasi'],$this->pembagi,3)
				]				
			];
		}

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

}
