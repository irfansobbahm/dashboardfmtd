<?php namespace App\Controllers\Dashboard;

use App\Controllers\BaseController;
use App\Models\SkModel;
use App\Models\SkdetailModel;
use App\Models\SkdibayarModel;
use App\Models\SkdibayardetailModel;
use CodeIgniter\API\ResponseTrait;

class Sk extends BaseController
{
	use ResponseTrait;
	protected $skModel;
	protected $skdetailModel;
	protected $skdibayarModel;
	protected $skdibayardetailModel;
	protected $arrBulan;

	public function __construct()
	{
		$this->thn = session()->get('tahun');
		$this->skModel = new SkModel();
		$this->skdetailModel = new SkdetailModel();
		$this->skdibayarModel = new SkdibayarModel();
		$this->skdibayardetailModel = new SkdibayardetailModel();
		$this->arrBulan = [
    		'1' => 'Januari', 
    		'2' => 'Februari',
    		'3' => 'Maret', 
    		'4' => 'April',
    		'5' => 'Mei', 
    		'6' => 'Juni',
    		'7' => 'Juli', 
    		'8' => 'Agustus',
    		'9' => 'September', 
    		'10' => 'Oktober',
    		'11' => 'November', 
    		'12' => 'Desember'
    	];
	}

	public function index($thn = null)
	{
		
		$thn = $thn ? $thn : $this->thn;

		$uri = service('uri');
		$uriParams = $uri->getQuery();

		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);

			if ($thnParams) {
				list($thnKey, $thnVal) = explode('=',$thnParams);
				if ($thnVal){
					$thn = $thnVal;		
				}
			}
		}

		
		// $sk_last_update = $this->skModel->getLastUpdate([ 'tahun' => $thn ]);
		// $skd_last_update = $this->skdibayarModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

		// echo $thn;exit;

		$sk_last_update = $this->skModel->getLastUpdate();
		$skd_last_update = $this->skdibayarModel->getLastUpdate();

		if (is_array($skd_last_update) && is_array($sk_last_update)) {
			if ($sk_last_update['updated_at'] > $skd_last_update['updated_at']) {
				$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
			} else {
				$last_update = $skd_last_update['tanggal'].' '.$this->arrBulan[$skd_last_update['bulan']].' '.$skd_last_update['tahun'];
			}
		} elseif (is_array($skd_last_update)) {
			$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
		} else {
			$last_update = '-';
		}

		$summary = $this->skModel->getSummary(['tahun' => $thn]);

		$summaryBelanja = $this->skdibayardetailModel->getSummary(['t_sk_dibayar.thn_anggaran' => $thn]);

		// print_r($summaryBelanja);exit;

		$data = [
			'menu' => 'dashboard',
			'submenu' => 'sk',
			'submenu2' => $thn,
			'submenu3' => 'semua',
			'last_update' => $last_update,
			'summary' => $summary,
			'summaryBelanja' => $summaryBelanja,
		];
		// print_r($summary);die;
		return view('dashboard/sk/index', $data);
	}

	// public function index($thn = null)
	// {
		
	// 	// $thn = $thn ? $thn : $this->thn;

	// 	// $uri = service('uri');
	// 	// $uriParams = $uri->getQuery();

	// 	// if ($uriParams) {
	// 	// 	$thnParams =  $uri->getQuery(['only' => ['thn']]);

	// 	// 	if ($thnParams) {
	// 	// 		list($thnKey, $thnVal) = explode('=',$thnParams);
	// 	// 		if ($thnVal){
	// 	// 			$thn = $thnVal;		
	// 	// 		}
	// 	// 	}
	// 	// }

		
	// 	// // $sk_last_update = $this->skModel->getLastUpdate([ 'tahun' => $thn ]);
	// 	// // $skd_last_update = $this->skdibayarModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

	// 	// // echo $thn;exit;

	// 	// $sk_last_update = $this->skModel->getLastUpdate();
	// 	// $skd_last_update = $this->skdibayarModel->getLastUpdate();

	// 	// if (is_array($skd_last_update) && is_array($sk_last_update)) {
	// 	// 	if ($sk_last_update['updated_at'] > $skd_last_update['updated_at']) {
	// 	// 		$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
	// 	// 	} else {
	// 	// 		$last_update = $skd_last_update['tanggal'].' '.$this->arrBulan[$skd_last_update['bulan']].' '.$skd_last_update['tahun'];
	// 	// 	}
	// 	// } elseif (is_array($skd_last_update)) {
	// 	// 	$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
	// 	// } else {
	// 	// 	$last_update = '-';
	// 	// }

	// 	// $summary = $this->skModel->getSummary(['tahun' => $thn]);

	// 	// $summaryBelanja = $this->skdibayardetailModel->getSummary(['t_sk_dibayar.thn_anggaran' => $thn]);

	// 	// // print_r($summaryBelanja);exit;

	// 	// $data = [
	// 	// 	'menu' => 'dashboard',
	// 	// 	'submenu' => 'sk',
	// 	// 	'submenu2' => $thn,
	// 	// 	'submenu3' => 'semua',
	// 	// 	'last_update' => $last_update,
	// 	// 	'summary' => $summary,
	// 	// 	'summaryBelanja' => $summaryBelanja,
	// 	// ];

	// 	// $data = [];
	// 	// $list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'DOSEN']);
	// 	// print_r($list);die;
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['dosen'][] = bcdiv($val,1000000,2);
	// 	// }
	// 	// // print_r($list);die;

	// 	// $list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'TENDIK']);
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['tendik'][] = bcdiv($val,1000000,2);
	// 	// }

	// 	// $list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.nip' => NULL]);
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['luar'][] = bcdiv($val,1000000,2);
	// 	// }

	// 	// $data['mahasiswa'] = [0,0,0,0,0,0,0,0,0,0,0,0];

		

	// 	$data = [];
	// 	$list = $this->skdibayardetailModel->getPembayaran(['t_sk.tahun' => $this->thn]);
	// 	foreach ($list as $key => $val) {
	// 		$data['sk'][] = $val;
	// 	}
		
		
	// 	print_r($data);die;
	// 	return view('dashboard/sk/index', $data);
	// }

	public function sispran1($thn = null)
	{
		$thn = $thn ? $thn : $this->thn;

		$uri = service('uri');
		$uriParams = $uri->getQuery();

		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);

			if ($thnParams) {
				list($thnKey, $thnVal) = explode('=',$thnParams);
				if ($thnVal){
					$thn = $thnVal;		
				}
			}
		}

		
		// $sk_last_update = $this->skModel->getLastUpdate([ 'tahun' => $thn ]);
		// $skd_last_update = $this->skdibayarModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

		// echo $thn;exit;

		$sk_last_update = $this->skModel->getLastUpdate();
		$skd_last_update = $this->skdibayarModel->getLastUpdate();

		if (is_array($skd_last_update) && is_array($sk_last_update)) {
			if ($sk_last_update['updated_at'] > $skd_last_update['updated_at']) {
				$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
			} else {
				$last_update = $skd_last_update['tanggal'].' '.$this->arrBulan[$skd_last_update['bulan']].' '.$skd_last_update['tahun'];
			}
		} elseif (is_array($skd_last_update)) {
			$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
		} else {
			$last_update = '-';
		}

		$summary = $this->skModel->getSummary(['tahun' => $thn]);
		$summaryBelanja = $this->skdibayardetailModel->getSummary(['t_sk_dibayar.thn_anggaran' => $thn]);
		
		// print_r($summaryBelanja);exit;
		
		$data = [
			'menu' => 'dashboard',
			'submenu' => 'sk',
			'submenu2' => $thn,
			'submenu3' => 'sispran 1',
			'last_update' => $last_update,
			'summary' => $summary,
			'summaryBelanja' => $summaryBelanja,
		];
		
		// print_r($data);die;
		return view('dashboard/sk/sispran1', $data);
	}

	// public function sispran1($thn = null)
	// {
	// 	// $thn = $thn ? $thn : $this->thn;

	// 	// $uri = service('uri');
	// 	// $uriParams = $uri->getQuery();

	// 	// if ($uriParams) {
	// 	// 	$thnParams =  $uri->getQuery(['only' => ['thn']]);

	// 	// 	if ($thnParams) {
	// 	// 		list($thnKey, $thnVal) = explode('=',$thnParams);
	// 	// 		if ($thnVal){
	// 	// 			$thn = $thnVal;		
	// 	// 		}
	// 	// 	}
	// 	// }

		
	// 	// // $sk_last_update = $this->skModel->getLastUpdate([ 'tahun' => $thn ]);
	// 	// // $skd_last_update = $this->skdibayarModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

	// 	// // echo $thn;exit;

	// 	// $sk_last_update = $this->skModel->getLastUpdate();
	// 	// $skd_last_update = $this->skdibayarModel->getLastUpdate();

	// 	// if (is_array($skd_last_update) && is_array($sk_last_update)) {
	// 	// 	if ($sk_last_update['updated_at'] > $skd_last_update['updated_at']) {
	// 	// 		$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
	// 	// 	} else {
	// 	// 		$last_update = $skd_last_update['tanggal'].' '.$this->arrBulan[$skd_last_update['bulan']].' '.$skd_last_update['tahun'];
	// 	// 	}
	// 	// } elseif (is_array($skd_last_update)) {
	// 	// 	$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
	// 	// } else {
	// 	// 	$last_update = '-';
	// 	// }

	// 	// $summary = $this->skModel->getSummary(['tahun' => $thn]);
	// 	// $summaryBelanja = $this->skdibayardetailModel->getSummary(['t_sk_dibayar.thn_anggaran' => $thn]);
		
	// 	// // print_r($summaryBelanja);exit;
		
	// 	// $data = [
	// 	// 	'menu' => 'dashboard',
	// 	// 	'submenu' => 'sk',
	// 	// 	'submenu2' => $thn,
	// 	// 	'submenu3' => 'sispran 1',
	// 	// 	'last_update' => $last_update,
	// 	// 	'summary' => $summary,
	// 	// 	'summaryBelanja' => $summaryBelanja,
	// 	// ];

	// 			// $data = [];
	// 			$list = $this->skdibayardetailModel->getPembayaran(['t_sk.tahun' => $this->thn]);
	// 			foreach ($list as $key => $val) {
	// 				$data['sk'][] = $val;
	// 			}
	// 			print_r($data);die;
	// 			return $this->setResponseFormat('json')->respond(['data' => $data]);
		
	// 	// print_r($data);die;
	// 	return view('dashboard/sk/sispran1', $data);
	// }

	// public function sispran2($thn = null)
	// {
	// 	$thn = $thn ? $thn : $this->thn;
	// 	$data = [
	// 		'menu' => 'dashboard',
	// 		'submenu' => 'sk',
	// 		'submenu2' => $thn,
	// 		'submenu3' => 'sispran 2',
	// 		'last_update' => '05 Mei 2021'
	// 	];

	// 	return view('dashboard/sk/sispran2', $data);
	// }

		public function sispran2($thn = null)
	{
		$thn = $thn ? $thn : $this->thn;

		$uri = service('uri');
		$uriParams = $uri->getQuery();

		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);

			if ($thnParams) {
				list($thnKey, $thnVal) = explode('=',$thnParams);
				if ($thnVal){
					$thn = $thnVal;		
				}
			}
		}

		
		// $sk_last_update = $this->skModel->getLastUpdate([ 'tahun' => $thn ]);
		// $skd_last_update = $this->skdibayarModel->getLastUpdate([ 'thn_anggaran' => $thn ]);

		// echo $thn;exit;

		$sk_last_update = $this->skModel->getLastUpdate();
		$skd_last_update = $this->skdibayarModel->getLastUpdate();

		if (is_array($skd_last_update) && is_array($sk_last_update)) {
			if ($sk_last_update['updated_at'] > $skd_last_update['updated_at']) {
				$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
			} else {
				$last_update = $skd_last_update['tanggal'].' '.$this->arrBulan[$skd_last_update['bulan']].' '.$skd_last_update['tahun'];
			}
		} elseif (is_array($skd_last_update)) {
			$last_update = $sk_last_update['tanggal'].' '.$this->arrBulan[$sk_last_update['bulan']].' '.$sk_last_update['tahun'];
		} else {
			$last_update = '-';
		}

		$summary = $this->skModel->getSummary(['tahun' => $thn]);
		$summaryBelanja = $this->skdibayardetailModel->getSummary(['t_sk_dibayar.thn_anggaran' => $thn]);
		
		// print_r($summaryBelanja);exit;
		
		$data = [
			'menu' => 'dashboard',
			'submenu' => 'sk',
			'submenu2' => $thn,
			'submenu3' => 'sispran 2',
			'last_update' => $last_update,
			'summary' => $summary,
			'summaryBelanja' => $summaryBelanja,
		];
		
		// print_r($data);die;
		return view('dashboard/sk/sispran2', $data);
	}

	/** Api Data */
	// public function api_status_pembayaran()
	// {
	// 	$this->thn = 2023;

	// 	$data = [];
	// 	$list = $this->skdibayarModel->getSummaryStatus(['t_sk.tahun' => $this->thn, 't_status' => '1']);
		
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['SPP'][] = $val;
	// 	// }

	// 	// $list = $this->skdibayarModel->getSummaryStatus(['t_sk.tahun' => $this->thn, 'status' => '2']);
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['disetujui'][] = $val;
	// 	// }

	// 	// $list = $this->skdibayarModel->getSummaryStatus(['t_sk.tahun' => $this->thn, 'status' => '3']);
	// 	// foreach ($list as $key => $val) {
	// 	// 	$data['ditransfer'][] = $val;
	// 	// }
	// 	// print_r($data);die;
	// 	return $this->setResponseFormat('json')->respond(['data' => $data]);
	// }

	public function api_status_pembayaran()
	{


		$data = [];
		$list = $this->skdibayardetailModel->getPembayaran(['t_sk.tahun' => $this->thn]);
		foreach ($list as $key => $val) {
			$data['sk'][] = $val;
		}
		$list = $this->skdibayardetailModel->getPembayaran(['t_sk.tahun' => $this->thn,'t_sk.sispran' => 1]);
		foreach ($list as $key => $val) {
			$data['sk_sispran1'][] = $val;
		}
		$list = $this->skdibayardetailModel->getPembayaran(['t_sk.tahun' => $this->thn,'t_sk.sispran' => 2]);
		foreach ($list as $key => $val) {
			$data['sk_sispran2'][] = $val;
		}
		// print_r($data);die;
		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_jenis_belanja()
	{
		$this->thn = 2023;

		$data = [];
		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '01']);
		foreach ($list as $key => $val) {
			$data['pegawai'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '03']);
		foreach ($list as $key => $val) {
			$data['jasa'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '01','LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 01]);
		foreach ($list as $key => $val) {
			$data['pegawai_sispran1'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '03','LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 01]);
		foreach ($list as $key => $val) {
			$data['jasa_sispran1'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '01','LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 02]);
		foreach ($list as $key => $val) {
			$data['pegawai_sispran2'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayarModel->getSummaryJenis(['t_sk_dibayar.thn_anggaran' => $this->thn, 'substr(t_sk_dibayar.id_jenis_honor, -2)' => '03','LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 02]);
		foreach ($list as $key => $val) {
			$data['jasa_sispran2'][] = bcdiv($val,1000000,2);
		}

		// print_r($data);exit;


		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_penerima()
	{
		$data = [];
		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'DOSEN']);
		foreach ($list as $key => $val) {
			$data['dosen'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'TENDIK']);
		foreach ($list as $key => $val) {
			$data['tendik'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.nip' => NULL]);
		foreach ($list as $key => $val) {
			$data['luar'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'DOSEN', 'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 01]);
		foreach ($list as $key => $val) {
			$data['dosen_sispran1'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'TENDIK', 'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 01]);
		foreach ($list as $key => $val) {
			$data['tendik_sispran1'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.nip' => NULL,'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 01]);
		foreach ($list as $key => $val) {
			$data['luar_sispran1'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'DOSEN', 'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 02]);
		foreach ($list as $key => $val) {
			$data['dosen_sispran2'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.jenis_penugasan' => 'TENDIK', 'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 02]);
		foreach ($list as $key => $val) {
			$data['tendik_sispran2'][] = bcdiv($val,1000000,2);
		}

		$list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk_dibayar.thn_anggaran' => $this->thn, 't_pegawai.nip' => NULL,'LEFT(t_sk_dibayar.id_jenis_honor,2) =' => 02]);
		foreach ($list as $key => $val) {
			$data['luar_sispran2'][] = bcdiv($val,1000000,2);
		}

		$data['mahasiswa'] = [0,0,0,0,0,0,0,0,0,0,0,0];

		// $list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk.tahun' => $this->thn, 't_sk_dibayar_detail.status' => 'MHS']);
		// foreach ($list as $key => $val) {
		// 	$data['mahasiswa'][] = $val;
		// }

		// $list = $this->skdibayardetailModel->getSummaryPenerima(['t_sk.tahun' => $this->thn, 't_sk_dibayar_detail.status' => 'PL']);
		// foreach ($list as $key => $val) {
		// 	$data['luar'][] = $val;
		// }

		// print_r($data);

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}
}
