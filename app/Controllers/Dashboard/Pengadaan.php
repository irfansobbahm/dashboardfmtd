<?php namespace App\Controllers\Dashboard;

use App\Controllers\BaseController;
use App\Models\PengadaanModel;
use CodeIgniter\API\ResponseTrait;

class Pengadaan extends BaseController
{
	use ResponseTrait;
	protected $pengadaanModel;
	protected $pembagi;
	protected $arrBulan;

	public function __construct()
	{
		$this->thn = session()->get('tahun');
		$this->pengadaanModel = new PengadaanModel();
		$this->pembagi = 1000000;
		$this->arrBulan = [
    		'1' => 'Januari', 
    		'2' => 'Februari',
    		'3' => 'Maret', 
    		'4' => 'April',
    		'5' => 'Mei', 
    		'6' => 'Juni',
    		'7' => 'Juli', 
    		'8' => 'Agustus',
    		'9' => 'September', 
    		'10' => 'Oktober',
    		'11' => 'November', 
    		'12' => 'Desember'
    	];
	}

	public function index()
	{
		$thn_aktif = $this->thn;

		$uri = service('uri');
		$uriParams = $uri->getQuery();

		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);

			if ($thnParams) {
				list($thnKey, $thnVal) = explode('=',$thnParams);
				if ($thnVal){
					$thn_aktif = $thnVal;		
				}
			}
		}

		// $last_update = $this->pengadaanModel->getLastUpdate([ 'thn_anggaran' => $thn_aktif ]);
		$last_update = $this->pengadaanModel->getLastUpdate();
		// echo $thn_aktif;exit;

		$uri = service('uri');
		$uriParams = $uri->getQuery();
		if ($uriParams) {
			$thnParams =  $uri->getQuery(['only' => ['thn']]);
			
			list($thnKey, $thnVal) = explode('=',$thnParams);

			if ($thnVal){
				$thn_aktif = $thnVal;
			}
		}

		$data = [
			'menu' => 'dashboard',
			'submenu' => 'pengadaan',
			'submenu2' => $thn_aktif, 
			'thn_aktif' => $thn_aktif,
			'last_update' => (is_array($last_update) ? $last_update['tanggal'].' '.$this->arrBulan[$last_update['bulan']].' '.$last_update['tahun'] : '-')
		];

		return view('dashboard/pengadaan/index', $data);
	}

	/** Api Data */
	public function api_status($tahun)
	{
		$tahun_anggaran = ($tahun) ? $tahun : $this->thn;

		$conditions = ['diadakan' => 'FTMD', 'thn_anggaran' => $tahun_anggaran];
		$res = $this->pengadaanModel->getSummaryStatus($conditions);

		// $data = [
		// 	(int)$res['baru'], 
		// 	(int)$res['penawaran'], 
		// 	(int)$res['spk'],
		// 	(int)$res['serah_terima'],  
		// 	(int)$res['invoice'], 
		// 	(int)$res['selesai'] 
		// ];

		$data = [
			(int)$res['pemaketan'], 
			(int)$res['kontrak'], 
			(int)$res['invoice'], 
			(int)$res['selesai'] 
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_status_itb($tahun)
	{
		$tahun_anggaran = ($tahun) ? $tahun : $this->thn;
		$conditions = ['diadakan' => 'Logistik', 'thn_anggaran' => $tahun_anggaran];

		$res = $this->pengadaanModel->getSummaryStatus($conditions);

		$data = [
			(int)$res['pemaketan'], 
			// (int)$res['diproses'],
			(int)$res['selesai'] 
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_deadline($tahun)
	{
		$tahun_anggaran = ($tahun) ? $tahun : $this->thn;
		$conditions = ['diadakan' => 'FTMD', 'thn_anggaran' => $tahun_anggaran, 'status <' => 6];
			
		$res = $this->pengadaanModel->getSummaryDeadline($conditions);
		$data = [
			(int)$res['tujuh'], 
			(int)$res['dua_tujuh'],
			(int)$res['dua'],
			(int)$res['lewat']
			
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_aktual($tahun)
	{
		$tahun_anggaran = ($tahun) ? $tahun : $this->thn;
		$conditions = ['diadakan' => 'FTMD', 'thn_anggaran' => $tahun_anggaran];

		$res = $this->pengadaanModel->getSummaryStatusAktual($conditions);
		$data = [
			(int)$res['lima'], 
			(int)$res['lima_tujuhlima'],
			(int)$res['tujuhlima'],
			(int)$res['selesai'] 
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_bulanan($tahun, $diadakan)
	{
		$tahun_anggaran = ($tahun) ? $tahun : $this->thn;

		if ($diadakan == 'all') {
			$barang = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'substr(id_const, 7, 2)' => '02']);
			$jasa = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'substr(id_const, 7, 2)' => '03']);
			$modal = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'substr(id_const, 7, 2)' => '04']);
		} else {			
			$barang = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'diadakan' => ucwords($diadakan), 'substr(id_const, 7, 2)' => '02']);
			$jasa = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'diadakan' => ucwords($diadakan), 'substr(id_const, 7, 2)' => '03']);
			$modal = $this->pengadaanModel->getPengadaanBulanan(['thn_anggaran' => $tahun_anggaran, 'diadakan' => ucwords($diadakan), 'substr(id_const, 7, 2)' => '04']);
		}

		// print_r($modal);exit;

		$data = [
			'barang' => [
				bcdiv($barang['jan'],$this->pembagi,2),
				bcdiv($barang['feb'],$this->pembagi,2),
				bcdiv($barang['mar'],$this->pembagi,2),
				bcdiv($barang['apr'],$this->pembagi,2),
				bcdiv($barang['mei'],$this->pembagi,2),
				bcdiv($barang['jun'],$this->pembagi,2),
				bcdiv($barang['jul'],$this->pembagi,2),
				bcdiv($barang['agu'],$this->pembagi,2),
				bcdiv($barang['sep'],$this->pembagi,2),
				bcdiv($barang['okt'],$this->pembagi,2),
				bcdiv($barang['nov'],$this->pembagi,2),
				bcdiv($barang['des'],$this->pembagi,2)
			],
			'jasa' => [
				bcdiv($jasa['jan'],$this->pembagi,2),
				bcdiv($jasa['feb'],$this->pembagi,2),
				bcdiv($jasa['mar'],$this->pembagi,2),
				bcdiv($jasa['apr'],$this->pembagi,2),
				bcdiv($jasa['mei'],$this->pembagi,2),
				bcdiv($jasa['jun'],$this->pembagi,2),
				bcdiv($jasa['jul'],$this->pembagi,2),
				bcdiv($jasa['agu'],$this->pembagi,2),
				bcdiv($jasa['sep'],$this->pembagi,2),
				bcdiv($jasa['okt'],$this->pembagi,2),
				bcdiv($jasa['nov'],$this->pembagi,2),
				bcdiv($jasa['des'],$this->pembagi,2)
			],
			'modal' => [
				bcdiv($modal['jan'],$this->pembagi,2),
				bcdiv($modal['feb'],$this->pembagi,2),
				bcdiv($modal['mar'],$this->pembagi,2),
				bcdiv($modal['apr'],$this->pembagi,2),
				bcdiv($modal['mei'],$this->pembagi,2),
				bcdiv($modal['jun'],$this->pembagi,2),
				bcdiv($modal['jul'],$this->pembagi,2),
				bcdiv($modal['agu'],$this->pembagi,2),
				bcdiv($modal['sep'],$this->pembagi,2),
				bcdiv($modal['okt'],$this->pembagi,2),
				bcdiv($modal['nov'],$this->pembagi,2),
				bcdiv($modal['des'],$this->pembagi,2)
			]
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

}