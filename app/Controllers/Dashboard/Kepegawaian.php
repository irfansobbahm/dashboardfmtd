<?php namespace App\Controllers\Dashboard;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\PegawaiModel;
use App\Models\PegawaikreditModel;

class Kepegawaian extends BaseController
{
	use ResponseTrait;

	protected $pegawaiModel;
	protected $pegawaikreditModel;
	protected $arKk;
	protected $arJab;
	protected $arrBulan;
	
	public function __construct()
	{
		$this->thn = session()->get('tahun');
		$this->pegawaiModel = new PegawaiModel();
		$this->pegawaikreditModel = new PegawaikreditModel();
		// $this->arKkold = [
		// 	'doppt' 	=> 'Desain, Operasi dan Perawatan Pesawat Terbang',
		// 	'ft' 		=> 'Fisika Terbang',
		// 	'itm' 		=> 'Ilmu dan Teknik Material',
		// 	'ke' 		=> 'Konversi Energi',
		// 	'pm' 		=> 'Perancangan Mesin',
		// 	'sr'		=> 'Struktur Ringan', 
		// 	'tekprod' 	=> 'Teknik dan Produksi Mesin'
		// ];
		$this->arKk = [
			'irt' 		=> 'Ilmu dan Rekayasa Termal ',
			'dfp' 		=> 'Dinamika Fluida dan Propulsi',
			'mpsr' 		=> 'Mekanika Padatan dan Struktur Ringan',
			'itm' 		=> 'Ilmu dan Teknik Material',
			'dk' 		=> 'Dinamika dan Kendali',
			'mot'		=> 'Mekanika dan Operasi Terbang', 
			'ptp' 		=> 'Perancangan Teknik dan Produksi'
		];

		$this->arJab = [
			'non' => 'NON JABATAN',
			'aa'  => 'ASISTEN AHLI',
			'l'   => 'LEKTOR',
			'lk'  => 'LEKTOR KEPALA',
			'gb'  => 'GURU BESAR'
		];

		$this->arrBulan = [
    		'1' => 'Januari', 
    		'2' => 'Februari',
    		'3' => 'Maret', 
    		'4' => 'April',
    		'5' => 'Mei', 
    		'6' => 'Juni',
    		'7' => 'Juli', 
    		'8' => 'Agustus',
    		'9' => 'September', 
    		'10' => 'Oktober',
    		'11' => 'November', 
    		'12' => 'Desember'
    	];
	}

	public function index()
	{
		$last_update = $this->pegawaikreditModel->getLastUpdate();

		$data = [
			'menu' => 'dashboard',
			'submenu' => 'kepegawaian',
			'submenu2' => date('Y'),
			'last_update' => (is_array($last_update) ? $last_update['tanggal'].' '.$this->arrBulan[$last_update['bulan']].' '.$last_update['tahun'] : '-')
	    ];

		return view('dashboard/kepegawaian/index', $data);
	}

	/** API Data */

	public function api_jab_fungsional($kk = null)
	{
		$arKk = [
			'irt' 		=> 'Ilmu dan Rekayasa Termal ',
			'dfp' 		=> 'Dinamika Fluida dan Propulsi',
			'mpsr' 		=> 'Mekanika Padatan dan Struktur Ringan',
			'itm' 		=> 'Ilmu dan Teknik Material',
			'dk' 		=> 'Dinamika dan Kendali',
			'mot'		=> 'Mekanika dan Operasi Terbang', 
			'ptp' 		=> 'Perancangan Teknik dan Produksi'
		];

		$conditions = ($kk && $kk != 'all') ? ['kelompok_keahlian' => $arKk[$kk]] : [];


		$res = $this->pegawaiModel->getSummaryJabFungsional(array_merge($conditions, ['jenis_penugasan' => 'DOSEN']));
		$dataStatus = $this->pegawaiModel->getSummaryStatus(array_merge($conditions, ['jenis_penugasan' => 'DOSEN']));

		$data = [
			(int)$res['gb'], 
			(int)$res['lk'], 
			(int)$res['l'], 
			(int)$res['aa'], 
			(int)$res['non_jab']
		];

		return $this->setResponseFormat('json')->respond([
			'data' => $data,
			'data_status' => $dataStatus
		]);
	}

	public function api_jab_fungsional_persen($kk = null)
	{
		$conditions = ($kk && $kk != 'all') ? ['kelompok_keahlian' => $this->arKk[$kk]] : [];

		$res = $this->pegawaiModel->getSummaryJabFungsional(array_merge($conditions, ['jenis_penugasan' => 'DOSEN']));
		$data = [
			(int)$res['gb'], 
			(int)$res['lk'], 
			(int)$res['l'], 
			(int)$res['aa'], 
			(int)$res['non_jab']
		];

		return $this->setResponseFormat('json')->respond(['data' => $data]);
	}

	public function api_pemenuhan_ak($kk = null)
	{
		$conditions = ($kk && $kk != 'all') ? ['kelompok_keahlian' => $this->arKk[$kk]] : [];

		$data = $this->pegawaiModel->getSummaryPemenuhanAk(array_merge($conditions, ['jenis_penugasan' => 'DOSEN', 'jab_fungsional !=' => 'GURU BESAR']));
		// print_r($data);exit;

		return $this->setResponseFormat('json')->respond(['data' => [$data['belum'], $data['memenuhi'], $data['diproses_ftmd'], $data['diproses_itb'], $data['diproses_dikti']]]);
	}

	public function api_purnabakti($kk = null, $jab = null)
	{
		$conditions = [];
		$conditions = ($kk && $kk != 'all') ? array_merge($conditions, ['kelompok_keahlian' => $this->arKk[$kk]]) : $conditions;
		$conditions = ($jab && $jab != 'all') ? array_merge($conditions, ['jab_fungsional' => $this->arJab[$jab]]) : $conditions;

		$data = $this->pegawaiModel->getSummaryPurnabakti(array_merge($conditions, ['jenis_penugasan' => 'DOSEN', 'nip <>' => '']));

		return $this->setResponseFormat('json')->respond(['data' => [$data['dua'], $data['dua_empat'], $data['empat_enam'], $data['enam']]]);
	}

	public function api_lama_menjabat($kk = null, $jab = null)
	{
		$conditions = [];
		$conditions = ($kk && $kk != 'all') ? array_merge($conditions, ['kelompok_keahlian' => $this->arKk[$kk]]) : $conditions;
		$conditions = ($jab && $jab != 'all') ? array_merge($conditions, ['jab_fungsional' => $this->arJab[$jab]]) : $conditions;

		$data = $this->pegawaiModel->getSummaryLamaMenjabat(array_merge($conditions, ['jenis_penugasan' => 'DOSEN']));

		return $this->setResponseFormat('json')->respond(['data' => [$data['satu'], $data['satu_tiga'], $data['tiga_lima'], $data['lima']]]);
	}

}
