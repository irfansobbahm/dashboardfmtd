<?php namespace App\Controllers;
// ini_set('MAX_EXECUTION_TIME', '-1'); 

use App\Models\RoleModel;
use App\Models\PegawaiModel;
// use App\Libraries\ITBCas;

// use App\Libraries\Nusoap\nusoap;
// use App\Libraries\Nusoap\NusoapClient;

class Auth extends BaseController
{
	//protected $db      = \Config\Database::connect();
	//protected $builder = $this->db->table('users');

	protected $userModel;
	protected $roleModel;
	protected $pegawaiModel;

	public function __construct()
	{
		$this->roleModel = new RoleModel();
		$this->pegawaiModel = new PegawaiModel();
		
        // $this->Nusoap_library = new NusoapClient('https://nic.itb.ac.id/riset/sson.php?wsdl', true);
       
	}

	public function index()
	{
		$data = [];
		helper(['form']);

		// $cas = new ITBCas();
        // $cas->loginITB();

        // $sess = session()->get('phpCAS');
        // $attr = $sess['attributes'];

        // $nip = '';
        // if (is_array($attr['itbNIP'])) {
		// 	$n = $attr['itbNIP'];
		// 	if (strlen($n[0]) > strlen($n[1])) {
		// 		$nip = $n[0];
		// 	} else {
		// 		$nip = $n[1];
		// 	}
    	// } else {
    	// 	$nip = $attr['itbNIP'];
    	// }
        
        // $user_role = $this->roleModel->where(['nip' => $nip])->first();

        // if (is_array($user_role)) {
		// 	if (is_array($attr['itbNIP'])) {
		// 		$n = $attr['itbNIP'];
		// 		if (strlen($n[0]) > strlen($n[1])) {
		// 			$nip = $n[0];
		// 		} else {
		// 			$nip = $n[1];
		// 		}
	    // 	} else {
	    // 		$nip = $attr['itbNIP'];
	    // 	}

	    // 	$this->setUserMethod([
		// 			'uid' => $sess['user'],
		// 			'nip' => $nip,
		// 			'cn' => $attr['cn'],
		// 			'role' => is_array($user_role) ? $user_role['role'] : 'user'
		// 		]);

		// 	return redirect()->to(base_url('home'));
		// }


		// echo password_hash('123456', PASSWORD_DEFAULT);exit;

		if ($this->request->getMethod() == 'post')
		{
			//validation here
			$rules = [
				'username' => 'required|min_length[3]|validateUser[username]',
				'password' => 'required|min_length[6]',
			];

			$errors = [
				'username' => [
					'required' => 'Username tidak boleh kosong', 
					'validateUser' => 'Username tidak terdaftar'
				],
				'password' => [
					'required' => 'Password tidak boleh kosong'
					// 'validateUser' => 'Username dan Password tidak sama'
				]
			];

			if (!$this->validate($rules, $errors))
			{
				$data['validation'] = $this->validator;
			}
			else
			{
				// return redirect()->to(base_url('index'));
				$username = $this->request->getVar('username');
				$password = $this->request->getVar('password');

				$param = array('username' => $username, 'password' => $password);

				$result = $this->loginoffline($param);
				// $result = $this->loginonline($param);
				if ($result['status']) {			
					session()->setFlashdata('error', $result['message']);		
					return redirect()->to(base_url('home'));
				} else {
					session()->setFlashdata('error', $result['message']);
        			return redirect()->to(base_url());
				}
				
			}
		}
		
		
		return view('signin', $data);
	}

	private function loginoffline($param)
	{
		$user_role = $this->roleModel->where(['username' => $param['username']])->first();
		if (is_array($user_role)) {
			$user = $this->pegawaiModel->where(['nip' => $user_role['nip']])->first();
			$this->setUserMethod(['uid' => $param['username'], 'nip' => $user['nip'], 'cn' => $user['nama'], 'role' => $user_role['role']]);
			return ['status' => 1, 'message' => ''];
		} else {
			return ['status' => 0, 'message' => 'Maaf, Anda tidak memiliki akses'];
		}

		
	}

	private function loginonline($param)
	{
		$result = $this->Nusoap_library->call('login', $param);

        if ($result) 
        {
        	// $username = 'bayutresnar';
        	$username_param = array('SID'=>$username);
			$res_sso = $this->Nusoap_library->call('getAllAtribute', $username_param);

			// var_dump($this->Nusoap_library->call('getITBNamaLengkap', $username_param));exit;

			$arr = explode(';', $res_sso);

			$params = array();

			foreach ($arr as $key => $value) 
			{
				if ($value != '') 
				{
				  list($a, $b) = explode('=>', $value);
				  $params[$a] = $b;
				}
			}

			if (strtolower($params['itbStatus']) == 'mahasiswa') {
        		return ['status' => 0, 'message' => 'Maaf, Anda tidak memiliki akses'];
			}

			$arr_nip = array_key_exists('itbNIP', $params) ? explode(',', $params['itbNIP']) : null;
			$nip = array_key_exists('itbNIP', $params) ? end($arr_nip) : null; 

			$user_role = $this->roleModel->where(['username' => $username])->first();
			$params['role'] = is_array($user_role) ? $user_role['role'] : 'user';
			$params['nip'] = $nip;					

			// var_dump($params);exit;
			$this->setUserMethod($params);
			
			return ['status' => 1, 'message' => ''];
        }
        else
        {
        	return ['status' => 0, 'message' => 'Username atau password tidak valid'];
        }
	}

	public function signout()
	{
		// return view('signin');
		$cas = new ITBCas();
        $cas->logoutSSO();
	}

	//--------------------------------------------------------------------

	/*
	 *
	 */
	private function setUserMethod($user)
	{
		$data = [
			'id' => $user['uid'],
			'nip' => $user['nip'], 
			'nama' => $user['cn'],
			'role' => $user['role'],
			'tahun' => date('Y'),
			'isLoggedIn' => true,
		];

		session()->set($data);
		return true;
	}

}
