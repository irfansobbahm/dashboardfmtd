<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class Auth implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // Do something here
        if (!session()->get('isLoggedIn'))
          return redirect()->to(base_url());
        else
        {
          if ($arguments != null) {
            if (session()->get('role') != $arguments[0])
              throw new \CodeIgniter\Exceptions\PageNotFoundException("Error Processing Request");
          }
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}
