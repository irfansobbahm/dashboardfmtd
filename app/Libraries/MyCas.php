<?php 
namespace App\Libraries; 

class MyCas 
{ 
    public function __construct() 
    { 
        $this->cas = new \phpCAS(); 
        $this->cas_server_url = "https://login.itb.ac.id/cas"; 
        // init CAS client 
        $defaults = array('path' => '', 'port' => 443); 
        $cas_url = array_merge($defaults, parse_url($this->cas_server_url)); 
        $this->cas::client(CAS_VERSION_2_0, $cas_url['host'], $cas_url['port'], $cas_url['path']); 
    } 
 
    /** 
     * Trigger CAS authentication if user is not yet authenticated. 
     */ 
    public function force_auth() 
    { 
        $this->cas->forceAuthentication(); 
        if ($this->cas->isAuthenticated()) { 
            $userlogin = $this->cas->getUser(); 
            $attributes = $this->cas->getAttributes(); 
            $usercas = array('userlogin' => $userlogin, 'attributes' => $attributes); 
            return (object)$usercas; 
        } else { 
            show_error("Gagal login!"); 
        } 
    } 
 
    /** 
     * Return an object with userlogin and attributes. 
     * Shows an error if called before authentication. 
     */ 
    public function user() 
    { 
        return $this->cas->getUser(); 
    } 
 
    /** 
     * Logout and redirect to the main site URL, 
     * or to the URL passed as argument 
     */ 
    public function logout($url = '') 
    { 
        $session = session(); 
        return $session->destroy(); 
    } 
} 