<?php
/*
 * CAS ITB
 * Create by Wahyuna
 * 2021.08.19
 **/
namespace App\Libraries;

class ITBCas
{
    public function __construct()
    {
        $this->cas = new \phpCAS();

        $this->cas_server_url = "https://login.itb.ac.id/cas";

        $defaults = array('path' => '', 'port' => 443);
        $cas_url = array_merge($defaults, parse_url($this->cas_server_url));

        $this->cas::client(CAS_VERSION_2_0, $cas_url['host'],
            $cas_url['port'], $cas_url['path'],false);

        $this->cas::setNoCasServerValidation();
    }

    public function loginITB()
    {
        $this->cas->forceAuthentication();

        if ($this->cas->isAuthenticated()) {
            $userlogin = $this->cas->getUser();
            $attributes = $this->cas->getAttributes();
            $usercas = array('userlogin' => $userlogin,
                'attributes' => $attributes);
            return (object)$usercas;
        } else {

            throw new \Exception("Anda belum login login");
        }

    }



    public function logout($url = '/')
    {
        $session = session();
        return $session->destroy();
    }

    public function logoutSSO()
    {
        $params = array('service'=>site_url('/'),'url'=>$this->cas_server_url.'/logout');
        $this->cas->logout($params);
    }
}
