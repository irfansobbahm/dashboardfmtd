<?php
namespace App\Libraries;
class Utility {

    public function __construct()
    {

    }

    function call_service($service_url,$curl_data) {
        die("<pre>".print_r($curl_data,1)."</pre>");
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);

        $fields_string = "";
        if (is_array($curl_data)) foreach($curl_data as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string,'&');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);

        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $curl_response;
    }

    public function akunIna($term) {
        $data = file_get_contents("http://webservice.itb.ac.id/service/rest/ai3/getAi3DataByUsername?passwd=ITB2020_ina!&username={$term}");
        return json_decode($data);
    }

}

