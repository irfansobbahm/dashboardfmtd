<?php $this->extend('layout/template') ?>

<?php $this->section('content') ?>

<div class="container">
  	
  	<!-- start content here -->

  	<div class="card card-custom">
	 <div class="card-header">
	  <div class="card-title">
      <span class="card-icon">
          <i class="flaticon2-add text-primary"></i>
      </span>
	   <h3 class="card-label">RKA</h3>
	   <small></small>
	  </div>
	 <!-- <div class="card-toolbar">
	   <ul class="nav nav-bold nav-pills">
	    <li class="nav-item">
	     <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">Realokasi Anggaran</a>
	    </li>
	    <li class="nav-item">
	     <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2">Prospektif</a>
	    </li>
	     <li class="nav-item">
	     <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Penerimaan</a>
	    </li>
	    </li>
	   </ul>
	  </div>-->
	 </div>
	 <div class="card-body">
    <!-- validation message -->
            <?php if(isset($validation)) : ?>
              <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  <div class="alert-text">  <?= $validation->listErrors(); ?></div>
                  <div class="alert-close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      </button>
                  </div>
              </div>
            <?php endif; ?> 


            <!-- validation message -->
            <?php if(isset($rka_exist)) : ?>
              <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  <div class="alert-text">  <?= $rka_exist ?></div>
                  <div class="alert-close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      </button>
                  </div>
              </div>
            <?php endif; ?> 
    <!-- end validation message -->
	  <div class="tab-content">
	   <!--<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">-->

	   		<!--begin::Form RKA -->            
            <form class="form" method="post" id="kt_form" action="/data/anggaran/rka_new_sispran2">
             <div class="row">
              <div class="col-xl-2"></div>
              <div class="col-xl-8">
               <div class="my-5">
                <!--<h3 class=" text-dark font-weight-bold mb-10">Customer Info:</h3>-->     
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Jenis RKA *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" checked="checked" value="<?= $realokasi ?>" onclick="showTahun()"/>
                    <span></span>
                    Realokasi Anggaran
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" value="<?= $prospektif ?>" onclick="showTahun()" />
                    <span></span>
                    Prospektif
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" value="<?= $penerimaan ?>" onclick="showTanggal()" />
                    <span></span>
                    Penerimaan
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Sumber Dana *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" checked="checked" value="<?= $kermaAlihan ?>"/>
                    <span></span>
                    Kerma Alihan
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" value="<?= $kermaNonAlihan ?>"/>
                    <span></span>
                    Kerma Non Alihan
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" value="<?= $kelasInter ?>"/>
                    <span></span>
                    Kelas Internasional
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->                                   
                <div class=" form-group row" id="thn_anggaran">
                 <label class="col-lg-3 text-lg-right">Tahun Anggaran *</label>
                 <div class="col-lg-9">                  
                  <select class="form-control" name="thn_anggaran">
                     <?php foreach($thn_anggaran as $thn) : ?>
                      <option <?= ( $thn == $this_year ) ? 'selected' : ''; ?> 
                              value="<?= $thn ?>">
                              <?= $thn ?>
                      </option>
                    <?php endforeach; ?>                 
                  </select>
                  <span class="form-text text-muted">Masukkan tahun anggaran</span>
                 </div>
                </div> <!--end form group row-->
                 <div class="form-group row" id="tgl_pengajuan" style="display: none">
                 <label class="col-lg-3 text-lg-right">Tanggal Penerimaan *</label>
                 <div class="col-lg-9">
                  <input class="form-control" type="date" value="<?= $today ?>" name="tgl_pengajuan" />
                  <span class="form-text text-muted">Masukkan tanggal penerimaan</span>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right">Nominal *</label>
      			     <div class="input-group col-lg-9">
      			      <div class="input-group-prepend"><span class="input-group-text" >Rp</span></div>
      			      <input type="text" class="form-control" name="nominal" placeholder=""/>			      
      			     </div>			   
      			    </div>
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right" for="exampleTextarea">Keterangan</label>
      			     <div class="col-lg-9">
      			      <textarea class="form-control form-control-solid" name="keterangan" rows="3"></textarea>
      			      <span class="form-text text-muted">Masukkan keterangan</span>                
      			     </div>
      			    </div> <!--end form group row-->
               </div>  <!-- end my-5 -->
      			   <div class="separator separator-dashed my-5"></div>
      			   <div class="row text-right">
      			    <div class="col-3">
      			    </div>
      			    <div class="col-9">
                  <!--<input type="hidden" name="id_const" value="<?= $id_const ?>">-->
      			     <button class="btn btn-success mr-2">Simpan</button>
      			     <button class="btn btn-secondary">Batal</button>                 
      			    </div>
      			   </div> 
              </div> <!-- end col-xl8 -->
              <div class="col-xl-2"></div>
             </div> <!-- end row -->
            </form>
            <!--end::Form-->

	   <!-- </div>  end tab  -->



</div> <!-- end container -->


<script type="text/javascript">

    function showTahun() {

        document.getElementById("tgl_pengajuan").style.display = "none";
        document.getElementById("thn_anggaran").style.display = "";

    }


    function showTanggal() {

        document.getElementById("tgl_pengajuan").style.display = "";
        document.getElementById("thn_anggaran").style.display = "none";

    }

</script>



<?php $this->endSection() ?>

<!--
https://stackoverflow.com/questions/54394965/why-is-my-javascript-code-ruining-my-styling
https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp
https://stackoverflow.com/questions/50460645/getelementbyid-style-display-does-not-work
-->