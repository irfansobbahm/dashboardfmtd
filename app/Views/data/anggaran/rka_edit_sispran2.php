<?php $this->extend('layout/template') ?>

<?php $this->section('content') ?>

<div class="container">
  	
  	<!-- start content here -->

  	<div class="card card-custom">
	 <div class="card-header">
	  <div class="card-title">
      <span class="card-icon">
          <i class="flaticon2-edit text-primary"></i>
      </span>
	   <h3 class="card-label">RKA</h3>
	   <small></small>
	  </div>
	 <!-- <div class="card-toolbar">
	   <ul class="nav nav-bold nav-pills">
	    <li class="nav-item">
	     <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">Realokasi Anggaran</a>
	    </li>
	    <li class="nav-item">
	     <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2">Prospektif</a>
	    </li>
	     <li class="nav-item">
	     <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Penerimaan</a>
	    </li>
	    </li>
	   </ul>
	  </div>-->
	 </div>
	 <div class="card-body">
    <!-- validation message -->
            <?php if(isset($validation)) : ?>
              <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  <div class="alert-text">  <?= $validation->listErrors(); ?></div>
                  <div class="alert-close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      </button>
                  </div>
              </div>
            <?php endif; ?> 


            <!-- validation message -->
            <?php if(isset($rka_exist)) : ?>
              <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  <div class="alert-text">  <?= $rka_exist ?></div>
                  <div class="alert-close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      </button>
                  </div>
              </div>
            <?php endif; ?> 
    <!-- end validation message -->
	  <div class="tab-content">
	   <!--<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">-->

	   		<!--begin::Form RKA -->            
            <form class="form" method="post" id="kt_form" action="/data/anggaran/rka_edit_sispran2/<?= $anggaran['id_anggaran'] ?>">
             <div class="row">
              <div class="col-xl-2"></div>
              <div class="col-xl-8">
               <div class="my-5">
                <!--<h3 class=" text-dark font-weight-bold mb-10">Customer Info:</h3>-->     
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Jenis RKA *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" checked="checked" value="<?= $realokasi ?>"
                    <?= (isset($jenis_rka) && $jenis_rka == $realokasi ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Realokasi Anggaran
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" value="<?= $prospektif ?>"
                    <?= (isset($jenis_rka) && $jenis_rka == $prospektif ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Prospektif
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="jenis_rka" value="<?= $penerimaan ?>" 
                    <?= (isset($jenis_rka) && $jenis_rka == $penerimaan ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Penerimaan
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Sumber Dana *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" checked="checked" value="<?= $kermaAlihan ?>"
                    <?= (isset($dana) && $dana == $kermaAlihan ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Kerma Alihan
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" value="<?= $kermaNonAlihan ?>"
                    <?= (isset($dana) && $dana == $kermaNonAlihan ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Kerma Non Alihan
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" value="<?= $kelasInter ?>"
                    <?= (isset($dana) && $dana == $kelasInter ) ? 'checked' : 'disabled'; ?> />
                    <span></span>
                    Kelas Internasional
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->                                   
                <div class=" form-group row" id="thn_anggaran">
                 <label class="col-lg-3 text-lg-right">Tahun Anggaran *</label>
                 <div class="col-lg-9">                  
                  <select class="form-control" id="thn_anggaran" name="thn_anggaran" disabled>
                      <?php foreach($thn_anggaran as $thn) : ?>
                        <option <?= ( $thn == $anggaran['thn_anggaran'] ) ? 'selected ' : ''; ?> 
                              value="<?= $thn ?>" > 
                              <?= $thn ?>
                        </option>
                      <?php endforeach; ?>
                    </select>
                  <span class="form-text text-muted">Masukkan tahun anggaran</span>
                 </div>
                </div> <!--end form group row-->
                 <div class="form-group row" id="tgl_pengajuan">
                 <label class="col-lg-3 text-lg-right">Tanggal Penerimaan *</label>
                 <div class="col-lg-9">
                  <input class="form-control" type="date" value="<?= $anggaran['tgl_pengajuan'] ?>" name="tgl_pengajuan" disabled />
                  <span class="form-text text-muted">Masukkan tanggal penerimaan</span>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right">Nominal *</label>
      			     <div class="input-group col-lg-9">
      			      <div class="input-group-prepend"><span class="input-group-text" >Rp</span></div>
      			      <input type="text" class="form-control" name="nominal" placeholder="" value="<?= $anggaran['nominal'] ?>"/>			      
      			     </div>			   
      			    </div>
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right" for="exampleTextarea">Keterangan</label>
      			     <div class="col-lg-9">
      			      <textarea class="form-control form-control-solid" name="keterangan" 
                   rows="3"><?= $anggaran['keterangan'] ?></textarea>
      			      <span class="form-text text-muted">Masukkan keterangan</span>                
      			     </div>
      			    </div> <!--end form group row-->
               </div>  <!-- end my-5 -->
      			   <div class="separator separator-dashed my-5"></div>
      			   <div class="row text-right">
      			    <div class="col-3">
      			    </div>
      			    <div class="col-9">               
                 <input type="hidden" name="id_const" value="<?= $anggaran['id_const'] ?>">
                 <input type="hidden" name="id_anggaran" value="<?= $anggaran['id_anggaran'] ?>">
      			     <button class="btn btn-success mr-2">Simpan</button>
      			     <button class="btn btn-secondary">Batal</button>                 
      			    </div>
      			   </div> 
              </div> <!-- end col-xl8 -->
              <div class="col-xl-2"></div>
             </div> <!-- end row -->
            </form>
            <!--end::Form-->

	   <!-- </div>  end tab  -->



</div> <!-- end container -->


<?php $this->endSection() ?>