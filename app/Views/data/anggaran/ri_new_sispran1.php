<?php $this->extend('layout/template') ?>

<?php $this->section('content') ?>

<div class="container">
  	
  	<!-- start content here -->

    

  <div class="card card-custom">
	 <div class="card-header">
	  <div class="card-title">
      <span class="card-icon">
          <i class="flaticon2-add text-primary"></i>
      </span>
	   <h3 class="card-label"><?= strtoupper($submenu2) ?></h3>
	   <small>Baru</small>
	  </div>
	  <!--<div class="card-toolbar">
	   <ul class="nav nav-bold nav-pills">
	    <li class="nav-item">
	     <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">Anggaran</a>
	    </li>
	   </ul>
	  </div>--> <!-- end card toolbar-->
	 </div>
	 <div class="card-body">
	  <!--<div class="tab-content">-->
	   <!--<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">-->

 
            <!-- validation message -->
            <?php if(isset($validation)) : ?>
              <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  <div class="alert-text">  <?= $validation->listErrors(); ?></div>
                  <div class="alert-close">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      </button>
                  </div>
              </div>
            <?php endif; ?> 

	   		<!--begin::Form -->

             <!--<form method="post "class="form" id="kt_form" action="/data/anggaran/new">-->
            <form method="post" id="kt_form" action="/data/anggaran/ri_new_sispran1">
             <div class="row">
              <div class="col-xl-2"></div>
              <div class="col-xl-8">
               <div class="my-5">
                <!--<h3 class=" text-dark font-weight-bold mb-10">Customer Info:</h3>-->                
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Sumber Dana *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" checked="checked" value="<?= $ko ?>"/>
                    <span></span>
                    KO
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="dana" value="<?= $p3mi ?>"/>
                    <span></span>
                    P3MI
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Kuota *</label>
                 <div class="col-lg-9">
                  <div class="radio-inline">
                   <label class="radio radio-solid">
                    <input type="radio" name="quota" checked="checked" value="<?= $pegawai ?>"/>
                    <span></span>
                    Pegawai
                   </label>
                   <label class="radio radio-solid">
                    <input type="radio" name="quota" value="<?= $barang ?>"/>
                    <span></span>
                    Barang
                   </label>
                    <label class="radio radio-solid">
                    <input type="radio" name="quota" value="<?= $jasa ?>"/>
                    <span></span>
                    Jasa
                   </label>
                       </label>
                    <label class="radio radio-solid">
                    <input type="radio" name="quota" value="<?= $modal ?>"/>
                    <span></span>
                    Modal
                   </label>
                  </div>
                 </div>
                </div> <!--end form group row-->               
                <div class="form-group row">
                 <label class="col-lg-3 text-lg-right">Tanggal Pengajuan *</label>
                 <div class="col-lg-9">
                  <input class="form-control" type="date" value="<?= $today ?>" name="tgl_pengajuan"/>
                  <span class="form-text text-muted">Masukkan tanggal pengajuan</span>
                 </div>
                </div> <!--end form group row-->
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right">Nominal *</label>
      			     <div class="input-group col-lg-9">
      			      <div class="input-group-prepend"><span class="input-group-text" >Rp</span></div>
      			      <input type="text" class="form-control" name="nominal" placeholder=""/>			      
      			     </div>			   
      			    </div>
                <div class="form-group row">
      			     <label class="col-lg-3 text-lg-right" for="exampleTextarea">Keterangan</label>
      			     <div class="col-lg-9">
      			      <textarea class="form-control form-control-solid" name="keterangan" rows="3"></textarea>
      			      <span class="form-text text-muted">Masukkan keterangan</span>             
      			     </div>
      			    </div> <!--end form group row-->             
               </div>  <!-- end my-5 -->
      			   <div class="separator separator-dashed my-5"></div>
      			   <div class="row text-right">
      			    <div class="col-3">
      			    </div>
      			    <div class="col-9">
      			     <button class="btn btn-success mr-2">Simpan</button>
      			     <button class="btn btn-secondary">Batal</button>
      			    </div>
      			   </div> 
              </div> <!-- end col-xl8 -->
              <div class="col-xl-2"></div>
             </div> <!-- end row -->
            </form>
            <!--end::Form-->

	   </div> <!--end tab pane--> 
	   
	   
	  <!--</div> --> <!--end card content -->
	 <!-- </div> --> <!--end card body -->
	</div> <!-- end card -->


</div> <!-- end container -->

<?php $this->endSection() ?>