<?php $this->extend('layout/template') ?>

<?php $this->section('content') ?>

<div class="container">
    <!-- start content here -->
    <div class="card card-custom">
	    <div class="card-header">
	        <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-add text-primary"></i>
                </span>
	            <h3 class="card-label"><?= strtoupper($submenu2) ?></h3>
	            <small></small>
	        </div>
	    </div>
	    <div class="card-body"> 
            <!-- validation message -->
            <?php if(session()->getFlashdata('errors')) : ?>
                <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                    <div class="alert-text">
                        <?php 
                        $errors = session()->getFlashdata('errors');
                        if(!empty($errors)) : ?>
                            Maaf! Ada kesalahan saat input data, yaitu:
                            <ul>
                                <?php foreach ($errors as $error) : ?>
                                    <li><?= esc($error) ?></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="ki ki-close"></i></span>
                        </button>
                    </div>
                </div>
            <?php endif; ?> 
	   		<!--begin::Form -->
            <form method="post" action="/data/anggaran/rka_sispran1_upload" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                    <div class="my-5">
                    	<div class="form-group row">
                            <label class="col-lg-3 text-lg-right">File *</label>
                            <div class="col-lg-9 custom-file">
                                <input type="file" class="custom-file-input" id="anggaran_file" name="anggaran_file">
                                <label for="anggaran_file" class="custom-file-label">Pilih File</label>
                            </div>
                        </div> 
                    </div>  
      			    <div class="separator separator-dashed my-5"></div>
                    <div class="row text-right">
                        <div class="col-3"></div>
                        <div class="col-9">
                            <button type="submit" class="btn btn-success mr-2">Simpan</button>
                            <button class="btn btn-secondary">Batal</button>
                        </div>
                    </div> 
                    <div class="col-xl-2"></div>
                </div> 
            </form>
            <!--end::Form-->
	    </div> 
	</div> 
</div> <!-- end container -->

<?php $this->endSection() ?>