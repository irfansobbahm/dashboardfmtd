<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="<?=base_url('assets/plugins/custom/datatables/datatables.bundle.css')?>" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>var HOST_URL = "<?php echo base_url('data/anggaran/api_data_all?'.$uriParams);?>" </script>
<script src="<?=base_url('assets/plugins/custom/datatables/datatables.bundle.js')?>"></script>
<script src="<?=base_url('assets/js/datapages/anggaran-scrollable.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
  <!--begin::Card-->
	<div class="card">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Anggaran</h3>
			</div>
			<div class="card-toolbar">
				<!--begin::Button-->
				<a href="/data/anggaran/upload_anggaran" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data RI</a>
				<a href="/data/anggaran/upload_rka" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data RKA</a>
				<a href="/data/anggaran/upload_realokasi_kerma" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data Realokasi Kerma</a>

				<a href="/data/anggaran/upload_kerma_alihan" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data Kerma</a>

				<a href="/data/anggaran/upload_prospektif" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data Prospektif</a>

				<!--end::Button-->
			</div>
		</div>
		<div class="card-body">

			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="ri-tab" data-toggle="tab" href="#ri">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Rencana Implementasi</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="rka-tab" data-toggle="tab" href="#rka" aria-controls="rka">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Rencana Kerja Anggaran</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="realokasi-kerma-tab" data-toggle="tab" href="#realokasi-kerma" aria-controls="realokasi-kerma">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Realokasi Kerma</span>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="realokasi-kerma-tab" data-toggle="tab" href="#prospektif-target" aria-controls="prospektif-target">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Prospektif Target</span>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="prospektif-capaian-tab" data-toggle="tab" href="#prospektif-capaian" aria-controls="prospektif-capaian">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Prospektif Capaian</span>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="kerma-non-alih-tab" data-toggle="tab" href="#kerma-non-alih" aria-controls="kerma-non-alih">
						<span class="nav-icon">
							<i class="flaticon2-layers-1"></i>
						</span>
						<span class="nav-text">Kerma Non Alihan</span>
					</a>
				</li>

			</ul>
			<div class="tab-content mt-5" id="myTabContent">
				<div class="tab-pane fade show active" id="ri" role="tabpanel" aria-labelledby="ri-tab">
					<!--begin: Search-->
					<div class="row">
						<div class="col-sm-12">
							<form method="get" action="">
				       	<div class="row">
				       		<div class="col-md-12 col-sm-12">
					       		<div class="form-group row">
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Tahun</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="th">
													<option value="">-Pilih Tahun-</option>
													<?php for ($i=date('Y'); $i > date('Y')-2; $i--) : ?>
														<option <?=($sParams['th'] == $i) ? 'selected="selected"' : ''?> value="<?=$i?>"><?=$i?></option>	
													<?php endfor; ?>
												</select>
											</div>
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Triwulan</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="triwulan"> 
													<option label="- Pilih Triwulan -"></option>	
													<option value="1" <?=($sParams['tw'] == '1') ? 'selected="selected"' : ''?>>Triwulan 1</option>				
													<option value="2" <?=($sParams['tw'] == '2') ? 'selected="selected"' : ''?>>Triwulan 2</option>	
													<option value="3" <?=($sParams['tw'] == '3') ? 'selected="selected"' : ''?>>Triwulan 3</option>				
													<option value="4" <?=($sParams['tw'] == '4') ? 'selected="selected"' : ''?>>Triwulan 4</option>				
												</select>
											</div>
											<!-- <label class="col-form-label text-lg-right col-lg-2 col-sm-4">Periode</label>
											<div class="col-lg-2 col-md-2 col-sm-4">
												<select class="form-control" name="aw">
													<option value="">-Awal-</option>
													<option <?=($sParams['aw'] == '1') ? 'selected="selected"' : ''?> value="1">Januari</option>
													<option <?=($sParams['aw'] == '2') ? 'selected="selected"' : ''?> value="2">Februari</option>
													<option <?=($sParams['aw'] == '3') ? 'selected="selected"' : ''?> value="3">Maret</option>
													<option <?=($sParams['aw'] == '4') ? 'selected="selected"' : ''?> value="4">April</option>
												</select>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-4">
												<select class="form-control" name="ak">
													<option value="">-Akhir-</option>
													<option <?=($sParams['ak'] == '1') ? 'selected="selected"' : ''?> value="1">Januari</option>
													<option <?=($sParams['ak'] == '2') ? 'selected="selected"' : ''?> value="2">Februari</option>
													<option <?=($sParams['ak'] == '3') ? 'selected="selected"' : ''?> value="3">Maret</option>
													<option <?=($sParams['ak'] == '4') ? 'selected="selected"' : ''?> value="4">April</option>
												</select>
											</div> -->
										</div>
									</div>
				       		<div class="col-md-12 col-sm-12">
					       		<div class="form-group row">
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Sispran</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="sis">
													<option value="">-Pilih Sispran-</option>
													<!-- <option <?=($sParams['sis'] == '01') ? 'selected="selected"' : ''?> value="01">Sispran 1</option>
													<option <?=($sParams['sis'] == '02') ? 'selected="selected"' : ''?> value="02">Sispran 2</option> -->
													<option <?=($sParams['sis'] == '1') ? 'selected="selected"' : ''?> value="1">Sispran 1</option>
													<option <?=($sParams['sis'] == '2') ? 'selected="selected"' : ''?> value="2">Sispran 2</option>
												</select>
											</div>
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Alokasi</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="alk">
													<option value="">-Pilih Alokasi-</option>
													<option <?=($sParams['alk'] == '01') ? 'selected="selected"' : ''?> value="01">PEGAWAI</option>
													<option <?=($sParams['alk'] == '02') ? 'selected="selected"' : ''?> value="02">BARANG</option>
													<option <?=($sParams['alk'] == '03') ? 'selected="selected"' : ''?> value="03">JASA</option>
													<option <?=($sParams['alk'] == '04') ? 'selected="selected"' : ''?> value="04">MODAL</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-12 col-sm-12">
					       		<div class="form-group row">
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Sumber Dana</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="sbr">
													<option value="">-Pilih Sumber Dana-</option>
													<option <?=($sParams['sbr'] == 'PDDK') ? 'selected="selected"' : ''?> value="PDDK">Pendidikan</option>
													<option <?=($sParams['sbr'] == 'PPMI') ? 'selected="selected"' : ''?> value="PPMI">PPMI</option>
													<option <?=($sParams['sbr'] == 'PENUGASAN') ? 'selected="selected"' : ''?> value="PENUGASAN">Penugasan</option>
													<option <?=($sParams['sbr'] == 'PB') ? 'selected="selected"' : ''?> value="PB">Pengembangan Pendidikan</option>
												</select>
											</div>									
										</div>
									</div>
									<div class="col-lg-12 text-center">
										<button type="submit" class="btn btn-primary mr-2">Cari</button>
										<a href="<?=base_url('data/anggaran')?>" class="btn btn-secondary">Reset</a>
									</div>
				       	</div>
				      </form>		
						</div>
					</div>
					<!--end: Search-->

					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable2">
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Kode Kegiatan</th>
								<th rowspan="2">Kegiatan</th>
								<th rowspan="2">Kode Prodi</th>
								<th rowspan="2">Prodi / KK</th>
								<th rowspan="2">Kode Bidang</th>
								<th rowspan="2">Nama Bidang</th>
								<th colspan="3">RI</th>
								<th rowspan="2">Jumlah</th>
								<th colspan="3">Realisasi</th>
								<th rowspan="2">Jumlah</th>
								<th rowspan="2">Saldo</th>
							</tr>
							<?php switch ($sParams['tw']) {
								case '1':
									$b1 = 'Januari';
									$b2 = 'Februari';
									$b3 = 'Maret';
									break;
								case '2':
									$b1 = 'April';
									$b2 = 'Mei';
									$b3 = 'Juni';
									break;
								case '3':
									$b1 = 'Juli';
									$b2 = 'Agustus';
									$b3 = 'September';
									break;
								case '4':
									$b1 = 'Oktober';
									$b2 = 'November';
									$b3 = 'Desember';
									break;
								
								default:
									$b1 = 'Januari';
									$b2 = 'Februari';
									$b3 = 'Maret';
									break;
							}?>
							<tr>
								<th><?=$b1?></th>
								<th><?=$b2?></th>
								<th><?=$b3?></th>
								<th><?=$b1?></th>
								<th><?=$b2?></th>
								<th><?=$b3?></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list as $l) :
								$total_ri = $l['jan_ri'] + $l['feb_ri'] + $l['mar_ri'];
								$total_real = $l['jan_real'] + $l['feb_real'] + $l['mar_real'];
								$saldo = $total_ri - $total_real;
								
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$l['coa_kegiatan']?></td>
									<td><?=$l['nama_kegiatan']?></td>
									<td><?=$l['coa_prodi_kk']?></td>
									<td><?=$l['nama_prodi_kk']?></td>
									<td><?=$l['coa_bidang']?></td>
									<td><?=$l['nama_bidang']?></td>
									<td><?=number_format($l['jan_ri'])?></td>
									<td><?=number_format($l['feb_ri'])?></td>
									<td><?=number_format($l['mar_ri'])?></td>
									<td><?=number_format($total_ri)?></td>
									<td><?=number_format($l['jan_real'])?></td>
									<td><?=number_format($l['feb_real'])?></td>
									<td><?=number_format($l['mar_real'])?></td>
									<td><?=number_format($total_real)?></td>
									<td><?=number_format($saldo)?></td> 
								</tr>
								<?php
								$i++;
							endforeach;
							?>

							
							
						</tbody>
					</table>
					<!--end: Datatable-->

				</div>

				<div class="tab-pane fade" id="rka" role="tabpanel" aria-labelledby="rka-tab">
					<!--begin: Datatable-->
					<form method="get" action="">
				       	<div class="row">
				       		<div class="col-md-12 col-sm-12">
					       		<div class="form-group row">
											<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Tahun</label>
											<div class="col-lg-4 col-md-4 col-sm-8">
												<select class="form-control" name="th">
													<option value="">-Pilih Tahun-</option>
													<?php for ($i=date('Y'); $i > date('Y')-2; $i--) : ?>
														<option <?=($sParams['th'] == $i) ? 'selected="selected"' : ''?> value="<?=$i?>"><?=$i?></option>	
													<?php endfor; ?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-12 text-center">
										<button type="submit" class="btn btn-primary mr-2">Cari</button>
										<a href="<?=base_url('data/anggaran')?>" class="btn btn-secondary">Reset</a>
									</div>
				       	</div>
				      </form>
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable3">
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Program</th>
								<th rowspan="2">ADO</th>
								<th colspan="4">Jenis Belanja</th>
								<th rowspan="2">Total</th>
							</tr>
							<tr>
								<th>Pegawai</th>
								<th>Barang</th>
								<th>Jasa</th>
								<th>Modal</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_rka as $lr) :
								$total_rka = $lr['pegawai'] + $lr['barang'] + $lr['jasa'] + $lr['modal'];
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lr['program']?></td>
									<td><?=$lr['ado']?></td>
									<td class="text-lg-right"><?=number_format($lr['pegawai'])?></td>
									<td class="text-lg-right"><?=number_format($lr['barang'])?></td>
									<td class="text-lg-right"><?=number_format($lr['jasa'])?></td>
									<td class="text-lg-right"><?=number_format($lr['modal'])?></td>
									<td class="text-lg-right"><?=number_format($total_rka)?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->					
				</div>

				<div class="tab-pane fade" id="realokasi-kerma" role="tabpanel" aria-labelledby="realokasi-kerma-tab">
					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable4">
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Kode File</th>
								<th rowspan="2">Unit</th>
								<th rowspan="2">Nama Peneliti</th>
								<th rowspan="2">Judul</th>
								<th rowspan="2">Mitra</th>
								<th rowspan="2">Kontrak (Rp)</th>
								<th colspan="4">Jenis Belanja</th>
								<th rowspan="2">Total</th>
							</tr>
							<tr>
								<th>Pegawai</th>
								<th>Barang</th>
								<th>Jasa</th>
								<th>Modal</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_realokasi as $lre) :
								$total_realokasi = $lre['pegawai'] + $lre['barang'] + $lre['jasa'] + $lre['modal'];
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lre['kode_file']?></td>
									<td><?=$lre['unit']?></td>
									<td><?=$lre['pelaksana']?></td>
									<td><?=$lre['keterangan']?></td>
									<td><?=$lre['mitra']?></td>
									<td class="text-lg-right"><?=number_format($lre['kontrak'])?></td>
									<td class="text-lg-right"><?=number_format($lre['pegawai'])?></td>
									<td class="text-lg-right"><?=number_format($lre['barang'])?></td>
									<td class="text-lg-right"><?=number_format($lre['jasa'])?></td>
									<td class="text-lg-right"><?=number_format($lre['modal'])?></td>
									<td class="text-lg-right"><?=number_format($total_realokasi)?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->		
				</div>

				<div class="tab-pane fade" id="prospektif-target" role="tabpanel" aria-labelledby="prospektif-target-tab">
					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable5">
						<thead>
							<tr>
								<th>No.</th>
								<th>Judul</th>
								<th>Mitra</th>
								<th>Kontrak (Rp)</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_pros_target as $lpt) :
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lpt['judul']?></td>
									<td><?=$lpt['mitra']?></td>
									<td class="text-lg-right"><?=number_format($lpt['kontrak'])?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->		
				</div>

				<div class="tab-pane fade" id="prospektif-capaian" role="tabpanel" aria-labelledby="prospektif-capaian-tab">
					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable5">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode File</th>
								<th>Judul</th>
								<th>Keterangan</th>
								<th>Peneliti</th>
								<th>Mitra</th>
								<th>Kontrak (Rp)</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_pros_capaian as $lpc) :
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lpc['kode_file']?></td>
									<td><?=$lpc['judul']?></td>
									<td><?=$lpc['keterangan']?></td>
									<td><?=$lpc['pelaksana']?></td>
									<td><?=$lpc['mitra']?></td>
									<td class="text-lg-right"><?=number_format($lpc['kontrak'])?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->		
				</div>

				<div class="tab-pane fade" id="kerma-alih" role="tabpanel" aria-labelledby="kerma-alih-tab">
					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable4">
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Kode File</th>
								<th rowspan="2">Unit</th>
								<th rowspan="2">Nama Peneliti</th>
								<th rowspan="2">Judul</th>
								<th rowspan="2">Mitra</th>
								<th rowspan="2">Kontrak (Rp)</th>
								<th colspan="4">Jenis Belanja</th>
								<th rowspan="2">Total</th>
							</tr>
							<tr>
								<th>Pegawai</th>
								<th>Barang</th>
								<th>Jasa</th>
								<th>Modal</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_kerma_alihan as $lka) :
								$total_kerma_alihan = $lka['pegawai'] + $lka['barang'] + $lka['jasa'] + $lka['modal'];
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lka['kode_file']?></td>
									<td><?=$lka['unit']?></td>
									<td><?=$lka['pelaksana']?></td>
									<td><?=$lka['keterangan']?></td>
									<td><?=$lka['mitra']?></td>
									<td class="text-lg-right"><?=number_format($lka['kontrak'])?></td>
									<td class="text-lg-right"><?=number_format($lka['pegawai'])?></td>
									<td class="text-lg-right"><?=number_format($lka['barang'])?></td>
									<td class="text-lg-right"><?=number_format($lka['jasa'])?></td>
									<td class="text-lg-right"><?=number_format($lka['modal'])?></td>
									<td class="text-lg-right"><?=number_format($total_kerma_alihan)?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->		
				</div>

				<div class="tab-pane fade" id="kerma-non-alih" role="tabpanel" aria-labelledby="kerma-non-alih-tab">
					<!--begin: Datatable-->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable4">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode File</th>
								<th>Judul</th>
								<th>Keterangan</th>
								<th>Peneliti</th>
								<th>Mitra</th>
								<th>Nominal (Rp)</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($list_kerma_non_alihan as $lkna) :
								$total_kerma_non_alihan = $lkna['pegawai'] + $lkna['barang'] + $lkna['jasa'] + $lkna['modal'];
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$lkna['kode_file']?></td>
									<td><?=$lkna['judul']?></td>
									<td><?=$lkna['keterangan']?></td>
									<td><?=$lkna['pelaksana']?></td>									
									<td><?=$lkna['mitra']?></td>
									<td class="text-lg-right"><?=number_format($lkna['kontrak'])?></td>
									<td class="text-lg-right"><?=number_format($lkna['pegawai'])?></td>
									<td class="text-lg-right"><?=number_format($lkna['barang'])?></td>
									<td class="text-lg-right"><?=number_format($lkna['jasa'])?></td>
									<td class="text-lg-right"><?=number_format($lkna['modal'])?></td>
									<td class="text-lg-right"><?=number_format($total_kerma_non_alihan)?></td>
								</tr>
								<?php
								$i++;
							endforeach;
							?>						
							
						</tbody>
					</table>
					<!--end: Datatable-->		
				</div>
			    
			</div>
		</div>
	</div>
	<!--end::Card-->
</div>
<?php $this->endSection() ?>
