<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>var HOST_URL = "<?php echo site_url('data/anggaran/api_data/'.$id_const);?>" </script>
<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="/assets/js/datapages/rka_sispran1.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
  <!--begin::Card-->
	<div class="card card-custom">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Anggaran <?= strtoupper($submenu2) ?></h3>
			</div>
			<div class="card-toolbar">
				<!--begin::Button-->
				<a href="/data/anggaran/rka_sispran1_upform" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
					<!--end::Svg Icon-->
				</span>Upload Data</a>&nbsp;
				<a href="/data/anggaran/rka_new_sispran1" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
					<!--end::Svg Icon-->
				</span>Tambah Data</a>
				<!--end::Button-->
			</div>
		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
				<thead>
					<!--<tr>
						<th colspan="3">Kegiatan</th>
						<th colspan="3">Rencana</th>
						<th colspan="3">RAB</th>
						<th colspan="4">Status</th>
					</tr>-->
					<tr>	
						<th>#ID Anggaran</th>
						<th>#ID Constanta</th>
						<th>Jenis Anggaran</th>
						<th>Sumber Dana</th>
						<th>Alokasi</th>
						<th>Tanggal Pengajuan</th>
						<th>Tahun Anggaran</th>
						<th>Nominal</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
			<!--end: Datatable-->
		</div>
	</div>
	<!--end::Card-->
</div>
<?php $this->endSection() ?>
