<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script src="/assets/js/datapages/kepegawaian_form.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--begin::Card-->
	    <div class="card card-custom gutter-b example example-compact">
				<div class="card-header">
					<h3 class="card-title">Edit Kepegawaian</h3>
				</div>
				<!--begin::Form-->
				<form class="form" method="post" id="kt_form" action="/data/kepegawaian/edit/<?= $pegawai['nip'] ?>">				
					<div class="card-body">

						<!-- validation message -->
            			<?php if(isset($validation)) : ?>
              				<div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  				<div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  				<div class="alert-text">  <?= $validation->listErrors(); ?></div>
                  				<div class="alert-close">
                      				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          				<span aria-hidden="true"><i class="ki ki-close"></i></span>
                      				</button>
                  				</div>
              				</div>
            			<?php endif; ?> 

            			<!-- form -->
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Nama <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="text" name="nama" class="form-control" value="<?= $pegawai['nama'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">AK Pendidikan <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="number" name="ak_pendidikan" class="form-control"  value="<?= $detail['ak_pendidikan'] ?>" >
									</div>
								</div> 
								
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">AK PengMas <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="number" name="ak_pengabdian" class="form-control"  value="<?= $detail['ak_pengabdian'] ?>" >
									</div>
								</div>
						
<!--end form group row-->
							</div>
							<div class="col-md-6 col-sm-12">
							<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Jabatan Usulan <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<select class="form-control" id="kt_select2_jabatan" name="jabatan">
											<option label=""

											></option>
											<option value="Guru Besar"
											<?= (isset($pegawai['jab_fungsional']) && $pegawai['jab_fungsional'] == 'GURU BESAR' ) ? 'selected' : ''; ?>>GURU BESAR</option>
											<option value="Lektor Kepala"
											<?= (isset($pegawai['jab_fungsional']) && $pegawai['jab_fungsional'] == 'LEKTOR KEPALA' ) ? 'selected' : ''; ?>>LEKTOR KEPALA</option>
											<option value="Lektor"
											<?= (isset($pegawai['jab_fungsional']) && $pegawai['jab_fungsional'] == 'LEKTOR' ) ? 'selected' : ''; ?>>LEKTOR</option>
											<option value="Asisten Ahli"
											<?= (isset($pegawai['jab_fungsional']) && $pegawai['jab_fungsional'] == 'ASISTEN AHLI' ) ? 'selected' : ''; ?>>ASISTEN AHLI</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">AK Penelitian <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="number" name="ak_penelitian" class="form-control"  value="<?= $detail['ak_penelitian'] ?>" >
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">AK PI <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="number" name="ak_pengembangan" class="form-control"  value="<?= $detail['ak_pengembangan'] ?>" >
									</div>
								</div>
										
							</div>							
						   
						</div>					

						
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<input type="hidden" name="nip" value="<?= $pegawai['nip'] ?>">
								<button class="btn btn-primary mr-2">Simpan</button>
								<button type="reset" class="btn btn-secondary">Batal</button>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
