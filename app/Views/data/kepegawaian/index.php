<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="<?=base_url('assets/plugins/custom/datatables/datatables.bundle.css')?>" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>var HOST_URL = "<?=base_url('data/kepegawaian/api_data?'.$uriParams);?>" </script>
<script src="<?=base_url('assets/plugins/custom/datatables/datatables.bundle.js')?>"></script>
<script src="<?=base_url('assets/js/datapages/kepegawaian_table.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<!--begin::Card-->
	<div class="card card-custom">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Data Pegawai</h3>
			</div>
			<div class="card-toolbar">
				<!--begin::Dropdown-->
				<div class="dropdown dropdown-inline mr-2">
					<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="svg-icon svg-icon-md">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
								<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>Export</button>
					<!--begin::Dropdown Menu-->
					<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
						<!--begin::Navigation-->
						<ul class="navi flex-column navi-hover py-2">
							<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
							<li class="navi-item">
								<a href="#" class="navi-link">
									<span class="navi-icon">
										<i class="la la-file-excel-o"></i>
									</span>
									<span class="navi-text">Excel</span>
								</a>
							</li>
						</ul>
						<!--end::Navigation-->
					</div>
					<!--end::Dropdown Menu-->
				</div>
				<!--end::Dropdown-->
				<!--begin::Button-->
				<a href="/data/kepegawaian/updateform" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
					<!--end::Svg Icon-->
				</span>Update Data</a>
				<!--end::Button-->
			</div>
		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<?php if (session()->getFlashdata('success')) : ?>
				<div class="alert alert-custom alert-light-success fade show mb-5" role="alert">
					<div class="alert-icon"><i class="flaticon-success"></i></div>                
					<div class="alert-text">
						<?= session()->getFlashdata('success') ?>
					</div>
					<div class="alert-close">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="ki ki-close"></i></span>
						</button>
					</div>
				</div>
			<?php endif; ?>

			<!--begin: Search-->
			<div class="row">
				<div class="col-sm-12">
					<form method="get" action="">
		       	<div class="row">
		       		<div class="col-md-12 col-sm-12">
			       		<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Jabatan</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="jab">
											<option value="">-Pilih Jabatan-</option>
											<option <?=($sParams['jab'] == 'gb') ? 'selected="selected"' : ''?> value="gb">GURU BESAR</option>
											<option <?=($sParams['jab'] == 'lk') ? 'selected="selected"' : ''?> value="lk">LEKTOR KEPALA</option>
											<option <?=($sParams['jab'] == 'l') ? 'selected="selected"' : ''?> value="l">LEKTOR</option>
											<option <?=($sParams['jab'] == 'aa') ? 'selected="selected"' : ''?> value="aa">ASISTEN AHLI</option>
											<option <?=($sParams['jab'] == 'non') ? 'selected="selected"' : ''?> value="non">NON JABATAN</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Status DUPAK</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="st">
											<option value="">-Status Dupak-</option>
											<option <?=($sParams['st'] == '0') ? 'selected="selected"' : ''?> value="0">Belum Memenuhi</option>
											<option <?=($sParams['st'] == '1') ? 'selected="selected"' : ''?> value="1">Admin KK</option>
											<option <?=($sParams['st'] == '2') ? 'selected="selected"' : ''?> value="2">Kepegawaian FTMD</option>
											<option <?=($sParams['st'] == '3') ? 'selected="selected"' : ''?> value="3">TPAKK F/S</option>
											<option <?=($sParams['st'] == '4') ? 'selected="selected"' : ''?> value="4">Senat F/S</option>
											<option <?=($sParams['st'] == '5') ? 'selected="selected"' : ''?> value="5">TPAK ITB</option>
											<option <?=($sParams['st'] == '6') ? 'selected="selected"' : ''?> value="6">Senat ITB</option>
											<option <?=($sParams['st'] == '7') ? 'selected="selected"' : ''?> value="7">Kepegawaian Pusat</option>
											<option <?=($sParams['st'] == '8') ? 'selected="selected"' : ''?> value="8">DIKTI</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Jabatan Usulan</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="jab_usul">
											<option value="">-Pilih Jabatan-</option>
											<option <?=($sParams['jab_usul'] == 'gb') ? 'selected="selected"' : ''?> value="gb">GURU BESAR</option>
											<option <?=($sParams['jab_usul'] == 'lk') ? 'selected="selected"' : ''?> value="lk">LEKTOR KEPALA</option>
											<option <?=($sParams['jab_usul'] == 'l') ? 'selected="selected"' : ''?> value="l">LEKTOR</option>
											<option <?=($sParams['jab_usul'] == 'aa') ? 'selected="selected"' : ''?> value="aa">ASISTEN AHLI</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Kelompok Keahlian</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="kk">
											<option value="">-Pilih Kelompok Keahlian-</option>
											<option <?=($sParams['kk'] == 'irt') ? 'selected="selected"' : ''?> value="irt">Ilmu dan Rekayasa Termal</option>
											<option <?=($sParams['kk'] == 'dfp') ? 'selected="selected"' : ''?> value="dfp">Dinamika Fluida dan Propulsi</option>
											<option <?=($sParams['kk'] == 'mpsr') ? 'selected="selected"' : ''?> value="mpsr">Mekanika Padatan dan Struktur Ringan</option>
											<option <?=($sParams['kk'] == 'itm') ? 'selected="selected"' : ''?> value="itm">Ilmu dan Teknik Material</option>								
											<option <?=($sParams['kk'] == 'dk') ? 'selected="selected"' : ''?> value="dk">Dinamika dan Kendali</option>
											<option <?=($sParams['kk'] == 'mot') ? 'selected="selected"' : ''?> value="mot">Mekanika dan Operasi Terbang</option>
											<option <?=($sParams['kk'] == 'ptp') ? 'selected="selected"' : ''?> value="ptp">Perancangan Teknik dan Produksi</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Masa Purnabakti</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="pb">
											<option value="">-Pilih Masa Purnabakti-</option>
											<option <?=($sParams['pb'] == '2') ? 'selected="selected"' : ''?> value="2">< 2 thn</option>
											<option <?=($sParams['pb'] == '24') ? 'selected="selected"' : ''?> value="24">2 -4 thn</option>
											<option <?=($sParams['pb'] == '46') ? 'selected="selected"' : ''?> value="46">4 - 6 thn</option>
											<option <?=($sParams['pb'] == '6') ? 'selected="selected"' : ''?> value="6">> 6 thn</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Lama Menjabat</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="lm">
											<option value="">-Pilih Lama Menjabat-</option>
											<option <?=($sParams['lm'] == '1') ? 'selected="selected"' : ''?> value="1">< 2 thn</option>
											<option <?=($sParams['lm'] == '13') ? 'selected="selected"' : ''?> value="13">2 - 6 thn</option>
											<option <?=($sParams['lm'] == '35') ? 'selected="selected"' : ''?> value="35">6 - 10 thn</option>
											<option <?=($sParams['lm'] == '5') ? 'selected="selected"' : ''?> value="5">> 10 thn</option>								
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Status Pegawai</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="stp">
											<option value="">-Pilih Status-</option>
											<option <?=($sParams['stp'] == 'aktif') ? 'selected="selected"' : ''?> value="aktif">Aktif</option>
											<option <?=($sParams['stp'] == 'tubel') ? 'selected="selected"' : ''?> value="tubel">Tugas Belajar</option>
											<option <?=($sParams['stp'] == 'mdl') ? 'selected="selected"' : ''?> value="mdl">Menjabat Diluar</option>
										</select>
									</div>									
								</div>
							</div>
							<div class="col-lg-12 text-center">
								<button type="submit" class="btn btn-primary mr-2">Cari</button>
								<a href="<?=base_url('data/kepegawaian')?>" class="btn btn-secondary">Reset</a>
							</div>
		       	</div>
		      </form>		
				</div>
			</div>
			
			<!--end: Search-->

			<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
				<thead>
					<tr>
						<th colspan="3">Data</th>
						<th colspan="2">Pendidikan (Min)</th>
						<th colspan="2">Penelitian (Min)</th>
						<th colspan="2">Pengabdian Masyarakat (Maks)</th>
						<th colspan="2">PI (Maks)</th>
						<th colspan="1">Syarat</th>
						<th colspan="1">Status</th>
						<!-- <th colspan="1">Usia</th>
						<th colspan="1">Masa Purnabakti</th> -->
						<th colspan="1">Aksi</th>
					</tr>
					<tr>
						<th>Nama</th>
						<th>Jabatan Usulan</th>
						<!-- <th>TMT Jabatan</th>
						<th>Tgl Lahir</th>
						<th>Usia</th>
						<th>TMT Pensiun</th> -->
						<th>Kebutuhan AK</th>
						<th>AK</th>
						<th>Syarat</th>
						<th>AK</th>
						<th>Syarat</th>
						<th>AK</th>
						<th>Syarat</th>
						<th>AK</th>
						<th>Syarat</th>
						<th>Khusus</th>
						<!-- <th>Umum</th> -->
						<th>Status</th>
						<!-- <th>Usia</th>
						<th>Masa Purnabakti</th> -->
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
			<!--end: Datatable-->
		</div>
	</div>
	<!--end::Card-->
</div>
<?php $this->endSection() ?>
