<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script src="/assets/js/datapages/sk_form.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--begin::Card-->
	    <div class="card card-custom gutter-b example example-compact">
				<div class="card-header">
					<h3 class="card-title">Pengadaan Baru</h3>
				</div>
				<!--begin::Form-->
				<form class="form" method="post" id="kt_form" action="/data/sk/save" enctype="multipart/form-data">	
                    <?= csrf_field(); ?>			
					<div class="card-body">
						<!-- validation message -->
                        <?php if(session()->getFlashdata('errors')) : ?>
              			    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  			    <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  				<div class="alert-text">
                                    <?php 
                                    $errors = session()->getFlashdata('errors');
                                    if(!empty($errors)) : ?>
                                        Maaf! Ada kesalahan saat input data, yaitu:
                                        <ul>
                                            <?php foreach ($errors as $error) : ?>
                                                <li><?= esc($error) ?></li>
                                            <?php endforeach ?>
                                        </ul>
                                    <?php endif; ?>  
                                </div>
                  				<div class="alert-close">
                      				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          			    <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      				</button>
                  				</div>
              				</div>
            			<?php endif; ?> 

						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Nomor SK <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<input type="text" class="form-control" name="nomor_sk">
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Judul SK <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="text" class="form-control" name="judul_sk">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Dekan <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<select class="form-control" id="kt_select2_dekan" name="dekan">
											<option label=""></option>	
                                            <?php foreach($dosen as $d) : ?>
                                                <option value="<?= $d['nopeg'] ?>">  
                                                    <?= $d['nama'] ?> 
                                                </option>
                                            <?php endforeach; ?>										
										</select>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal Terbit <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_terbit" name="tgl_terbit" readonly="readonly" placeholder="Pilih tanggal" />
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal Terbit <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_akhir" name="tgl_akhir" readonly="readonly" placeholder="Pilih tanggal" />
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal Tandatangan <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_ttd" name="tgl_ttd" readonly="readonly" placeholder="Pilih tanggal" />
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
                                <div class="form-group row">
                                    <label class="col-form-label text-lg-right col-lg-4 col-sm-12">File Detail SK *</label>
                                    <div class="col-lg-4 col-md-4 col-sm-12 custom-file">
                                        <input type="file" class="custom-file-input" id="detail_sk_file" name="detail_sk_file">
                                        <label for="detail_sk_file" class="custom-file-label">Pilih File</label>
                                    </div>
                                </div> 
							</div>
						</div>
					</div> <!-- end card body -->
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<button class="btn btn-primary mr-2">Simpan</button>
								<button type="reset" class="btn btn-secondary">Batal</button>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
