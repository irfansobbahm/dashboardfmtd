<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title>DASHBOARD FTMD | ITB</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" type="text/css">
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body>
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">No.</th>
						<th scope="col">NIP</th>
						<th scope="col">Nama</th>
						<th scope="col">No. SK</th>
						<th scope="col">Judul SK</th>
						<th scope="col">Total</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Tgl. Transfer</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$i = 1;
						foreach ($dataDibayar as $dd) :
							?>
							<tr>
								<td><?=$i?></td>
								<td><?=$dd['nip']?></td>
								<td><?=$dd['nama']?></td>
								<td><?=$dd['no_sk']?></td>
								<td><?=$dd['judul_sk']?></td>
								<td class="text-lg-right"><?=number_format($dd['nilai_dibayar'])?></td>
								<td><?=$dd['keterangan_detail']?></td>
								<td><?=$dd['tgl_pembayaran']?></td>
							</tr>
							<?php
							$i++;
						endforeach;
						?>				
				</tbody>
			</table>
		</div>
		<!--end::Main-->
		
		
		<!--end::Global Theme Bundle-->
	</body>
	<!--end::Body-->
</html>