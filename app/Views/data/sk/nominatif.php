<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>var HOST_URL = "<?php echo site_url('data/sk/api_data_nominatif?'.$uriParams);?>" </script>
<script>var HOST_URL_UTAMA = "<?php echo site_url('data/sk');?>" </script>
<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="/assets/js/datapages/sk_table_nominatif.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
  <!--begin::Card-->
	<div class="card card-custom">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Data Nominatif SK</h3>
			</div>
			<div class="card-toolbar">
				<!--begin::Dropdown-->
				<div class="dropdown dropdown-inline mr-2">
					<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="svg-icon svg-icon-md">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
								<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>Export</button>
					<!--begin::Dropdown Menu-->
					<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
						<!--begin::Navigation-->
						<ul class="navi flex-column navi-hover py-2">
							<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
							<li class="navi-item">
								<a href="#" class="navi-link" id="gen-excel">
									<span class="navi-icon">
										<i class="la la-file-excel-o"></i>
									</span>
									<span class="navi-text">Excel</span>
								</a>
							</li>
						</ul>
						<!--end::Navigation-->
					</div>
					<!--end::Dropdown Menu-->
				</div>
				<!--end::Dropdown-->
			</div>
		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<form class="mb-15">
				<div class="row mb-6">
					<!-- <div class="col-lg-3 mb-lg-0 mb-6">
						<label>No. SK:</label>
						<input type="text" class="form-control datatable-input" placeholder="No. SK" data-col-index="0" />
					</div>
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Judul:</label>
						<input type="text" class="form-control datatable-input" placeholder="Judul SK" data-col-index="1" />
					</div> -->
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Nama: </label>
						<select class="form-control" name="nip" id="kt_select2_penerima">
							<option value="all">- Semua Pegawai -</option>

							<?php foreach ($nipList as $nip) :?>								
								<option <?=($sParams['nip'] == $nip['nip']) ? 'selected="selected"' : ''?> value="<?=$nip['nip']?>"><?=$nip['nama']?></option>
							<?php endforeach;?>
						</select>
						<!-- <input type="text" class="form-control datatable-input" placeholder="Nama" data-col-index="2" /> -->
					</div>
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Kategori:</label>
						<select class="form-control datatable-input" data-col-index="6" name="kat" id="kat">
							<option value="">-Kategori Penerima-</option>
							<option <?=($sParams['kat'] == 'dosen') ? 'selected="selected"' : ''?> value="dosen">Dosen</option>
							<option <?=($sParams['kat'] == 'tendik') ? 'selected="selected"' : ''?> value="tendik">Tenaga Kependidikan</option>
							<option <?=($sParams['kat'] == 'mhs') ? 'selected="selected"' : ''?> value="mhs">Mahasiswa</option>
							<option <?=($sParams['kat'] == 'null') ? 'selected="selected"' : ''?> value="null">Lainnya</option>
						</select>
					</div>
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Jenis Belanja:</label>
						<select class="form-control datatable-input" data-col-index="6" name="jns" id="jns">
							<option value="">-Jenis belanja-</option>
							<option <?=($sParams['jns'] == '01') ? 'selected="selected"' : ''?> value="01">Pegawai</option>
							<option <?=($sParams['jns'] == '03') ? 'selected="selected"' : ''?> value="03">Jasa</option>
						</select>
					</div>
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Tahun:</label>
						<select class="form-control datatable-input" data-col-index="6" name="th" id="th">

							<option label="-Pilih Tahun-"></option>	
              <?php for($i=date("Y"); $i > date("Y")-3; $i--) :?>
                  <option value="<?= $i ?>" <?=($sParams['th'] == $i) ? 'selected="selected"' : ''?>>  
                      <?= $i ?> 
                  </option>
              <?php endfor; ?>	
						</select>
					</div>
				</div>
				<div class="row mb-8">
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Bulan Pengajuan:</label>
						<select class="form-control datatable-input" name="bln_pengajuan" data-col-index="6" id="bln_pengajuan">
							<option value="">-Bulan-</option>
							<option <?=($sParams['bln_pengajuan'] == '01') ? 'selected="selected"' : ''?> value="01">Januari</option>
							<option <?=($sParams['bln_pengajuan'] == '02') ? 'selected="selected"' : ''?> value="02">Februari</option>
							<option <?=($sParams['bln_pengajuan'] == '03') ? 'selected="selected"' : ''?> value="03">Maret</option>
							<option <?=($sParams['bln_pengajuan'] == '04') ? 'selected="selected"' : ''?> value="04">April</option>
							<option <?=($sParams['bln_pengajuan'] == '05') ? 'selected="selected"' : ''?> value="05">Mei</option>
							<option <?=($sParams['bln_pengajuan'] == '06') ? 'selected="selected"' : ''?> value="06">Juni</option>
							<option <?=($sParams['bln_pengajuan'] == '07') ? 'selected="selected"' : ''?> value="07">Juli</option>
							<option <?=($sParams['bln_pengajuan'] == '08') ? 'selected="selected"' : ''?> value="08">Agustus</option>
							<option <?=($sParams['bln_pengajuan'] == '09') ? 'selected="selected"' : ''?> value="09">September</option>
							<option <?=($sParams['bln_pengajuan'] == '10') ? 'selected="selected"' : ''?> value="10">Oktober</option>
							<option <?=($sParams['bln_pengajuan'] == '11') ? 'selected="selected"' : ''?> value="11">November</option>
							<option <?=($sParams['bln_pengajuan'] == '12') ? 'selected="selected"' : ''?> value="12">Desember</option>
						</select>
					</div>
					<div class="col-lg-3 mb-lg-0 mb-6">
						<label>Bulan Pembayaran:</label>
						<select class="form-control datatable-input" name="bln_pembayaran" data-col-index="6" id="bln_pembayaran">
							<option value="">-Bulan-</option>
							<option <?=($sParams['bln_pembayaran'] == '01') ? 'selected="selected"' : ''?> value="01">Januari</option>
							<option <?=($sParams['bln_pembayaran'] == '02') ? 'selected="selected"' : ''?> value="02">Februari</option>
							<option <?=($sParams['bln_pembayaran'] == '03') ? 'selected="selected"' : ''?> value="03">Maret</option>
							<option <?=($sParams['bln_pembayaran'] == '04') ? 'selected="selected"' : ''?> value="04">April</option>
							<option <?=($sParams['bln_pembayaran'] == '05') ? 'selected="selected"' : ''?> value="05">Mei</option>
							<option <?=($sParams['bln_pembayaran'] == '06') ? 'selected="selected"' : ''?> value="06">Juni</option>
							<option <?=($sParams['bln_pembayaran'] == '07') ? 'selected="selected"' : ''?> value="07">Juli</option>
							<option <?=($sParams['bln_pembayaran'] == '08') ? 'selected="selected"' : ''?> value="08">Agustus</option>
							<option <?=($sParams['bln_pembayaran'] == '09') ? 'selected="selected"' : ''?> value="09">September</option>
							<option <?=($sParams['bln_pembayaran'] == '10') ? 'selected="selected"' : ''?> value="10">Oktober</option>
							<option <?=($sParams['bln_pembayaran'] == '11') ? 'selected="selected"' : ''?> value="11">November</option>
							<option <?=($sParams['bln_pembayaran'] == '12') ? 'selected="selected"' : ''?> value="12">Desember</option>
						</select>
					</div>
					<!-- <div class="col-lg-3 mb-lg-0 mb-6">
						<label>Status:</label>
						<select class="form-control datatable-input" data-col-index="7">
							<option value="">Pilih</option>
						</select>
					</div> -->
				</div>
				<div class="row mt-8">
					<div class="col-lg-12">
						<button class="btn btn-primary btn-primary--icon" id="kt_search">
							<span>
								<i class="la la-search"></i>
								<span>Search</span>
							</span>
						</button>&#160;&#160;
						<button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
							<span>
								<i class="la la-close"></i>
								<span>Reset</span>
							</span>
						</button> 
						<button type="button" class="btn btn-success btn-secondary--icon" style="margin-left: 7px;" id="gen-excel">
							<i class="la la-file-excel-o"></i>
							<span>Excel</span> 
						</button>
						<button type="button" class="btn btn-success btn-secondary--icon" style="margin-left: 7px;" id="gen-pdf">
							<i class="la la-file-pdf-o"></i>
							<span>Pdf</span> 
						</a>
					</div>
				</div>
			</form>			
			<table class="table table-bordered table-hover table-checkable" id="kt_datatable_nominatif" style="margin-top: 13px !important">
				<thead>
					<tr>
						<th>No. SK</th>
						<th>Judul</th>
						<th>Nama</th>
						<th>NIP/NIM</th>
						<th>Jenis Honor</th>
						<th>Total</th>
						<th>Keterangan</th>
						<th>Tgl Pengajuan</th>
						<th>Tgl Pembayaran</th>
					</tr>
				</thead>
			</table>
			<!--end: Datatable-->
		</div>
	</div>
	<!--end::Card-->
</div>
<?php $this->endSection() ?>
