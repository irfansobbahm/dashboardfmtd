<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<!-- <script src="/assets/js/datapages/upload_sk_form.js"></script> -->
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--begin::Card-->
	    <div class="card card-custom gutter-b example example-compact">
				<div class="card-header">
					<h3 class="card-title">Upload Data SK</h3>
				</div>
				<!--begin::Form-->
				<form class="form" method="post" id="kt_form" action="/data/sk/save_upload_sk" enctype="multipart/form-data">	
                    <?= csrf_field(); ?>			
					<div class="card-body">
						<!-- validation message -->
                        <?php if(session()->getFlashdata('errors')) : ?>
              			    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  			    <div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  				<div class="alert-text">
                                    <?php 
                                    $errors = session()->getFlashdata('errors');
                                    if(!empty($errors)) : ?>
                                        Maaf! Ada kesalahan saat input data, yaitu:
                                        <ul>
                                            <?php foreach ($errors as $error) : ?>
                                                <li><?= esc($error) ?></li>
                                            <?php endforeach ?>
                                        </ul>
                                    <?php endif; ?>  
                                </div>
                  				<div class="alert-close">
                      				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          			    <span aria-hidden="true"><i class="ki ki-close"></i></span>
                      				</button>
                  				</div>
              				</div>
            			<?php endif; ?> 

						<div class="row">
							<div class="col-md-12 col-sm-12">
								<!-- <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Nomor SK <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<input type="text" class="form-control" name="nomor_sk">
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Judul SK <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="text" class="form-control" name="judul_sk">
									</div>
								</div> -->
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tahun <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<select class="form-control kt_select2" name="tahun" required="required">
											<option label=""></option>	
                                            <?php for($i=date("Y"); $i > date("Y")-3; $i--) :?>
                                                <option value="<?= $i ?>">  
                                                    <?= $i ?> 
                                                </option>
                                            <?php endfor; ?>										
										</select>
									</div>
								</div>
								<!-- <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Sispran <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<select class="form-control kt_select2" name="sispran" required="required">
											<option label=""></option>	
                                            <option value="01">Sispran 1</option>				
											<option value="02">Sispran 2</option>				
										</select>
									</div>
								</div> -->
								<!-- <div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Sumberdana <span class="text-danger">*</span></label>
									<div class="col-lg-4 col-md-4 col-sm-12">
										<select class="form-control kt_select2" name="jenis_belanja" required="required">
											<option label=""></option>	
                                            <option value="01">Pendidikan</option>				
											<option value="02">P2MI</option>	
											<option value="01">Kerma Alihan</option>				
											<option value="02">Kerma Non Alihan</option>				
										</select>
									</div>
								</div> -->
                                <div class="form-group row">
                                    <label class="col-form-label text-lg-right col-lg-4 col-sm-12">File <span class="text-danger">*</span></label>
                                    <div class="col-lg-4 col-md-4 col-sm-12 custom-file">
                                        <input type="file" class="custom-file-input" id="file_sk" name="file_sk" required="required">
                                        <label for="file_sk" class="custom-file-label">Pilih File</label>
                                    </div>
                                </div> 
							</div>
						</div>
					</div> <!-- end card body -->
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<button class="btn btn-primary mr-2">Simpan</button>
								<button type="reset" class="btn btn-secondary">Batal</button>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
