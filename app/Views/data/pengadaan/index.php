<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>var HOST_URL = "<?php echo base_url('data/pengadaan/api_data?'.$uriParams);?>" </script>
<script src="<?=base_url('assets/plugins/custom/datatables/datatables.bundle.js')?>"></script>
<script src="<?=base_url('assets/js/datapages/pengadaan_table.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
  <!--begin::Card-->
	<div class="card card-custom">
		<div class="card-header">
			<div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-supermarket text-primary"></i>
				</span>
				<h3 class="card-label">Data Pengadaan Barang/Jasa</h3>
			</div>
			<div class="card-toolbar">
				<!--begin::Dropdown-->
				<div class="dropdown dropdown-inline mr-2">
					<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="svg-icon svg-icon-md">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
								<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>Export</button>
					<!--begin::Dropdown Menu-->
					<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
						<!--begin::Navigation-->
						<ul class="navi flex-column navi-hover py-2">
							<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
							<li class="navi-item">
								<a href="#" class="navi-link">
									<span class="navi-icon">
										<i class="la la-print"></i>
									</span>
									<span class="navi-text">Print</span>
								</a>
							</li>
							<!-- <li class="navi-item">
								<a href="#" class="navi-link">
									<span class="navi-icon">
										<i class="la la-copy"></i>
									</span>
									<span class="navi-text">Copy</span>
								</a>
							</li> -->
							<li class="navi-item">
								<a href="#" class="navi-link" id="gen-excel">
									<span class="navi-icon">
										<i class="la la-file-excel-o"></i>
									</span>
									<span class="navi-text">Excel</span>
								</a>
							</li>
							<!-- <li class="navi-item">
								<a href="#" class="navi-link">
									<span class="navi-icon">
										<i class="la la-file-text-o"></i>
									</span>
									<span class="navi-text">CSV</span>
								</a>
							</li> -->
							<li class="navi-item">
								<a href="#" class="navi-link" id="gen-pdf">
									<span class="navi-icon">
										<i class="la la-file-pdf-o"></i>
									</span>
									<span class="navi-text">PDF</span>
								</a>
							</li>
						</ul>
						<!--end::Navigation-->
					</div>
					<!--end::Dropdown Menu-->
				</div>
				<!--end::Dropdown-->
				<!--begin::Button-->
				<a href="/data/pengadaan/upload_pengadaan" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Upload Data</a>
				<!-- <a href="/data/pengadaan/tambah" class="btn btn-primary font-weight-bolder">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="9" cy="15" r="6" />
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
						</g>
					</svg>
				</span>Tambah Data</a>-->
				<!--end::Button-->
			</div>
		</div>
		<div class="card-body">
			<!--begin: Search-->
			<div class="row">
				<div class="col-sm-12">
					<form method="get" action="">
		       	<div class="row">
		       		<div class="col-md-12 col-sm-12">
			       		<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Tahun</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="th">
											<option value="">-Pilih Tahun-</option>
													<?php for ($i=date('Y'); $i > date('Y')-4; $i--) : ?>
														<option <?=($sParams['th'] == $i) ? 'selected="selected"' : ''?> value="<?=$i?>"><?=$i?></option>	
													<?php endfor; ?>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Sispran</label>
									<div class="col-lg-2 col-md-2 col-sm-4">
										<select class="form-control" name="sis">
											<option value="">-Jenis Sispran-</option>
											<option <?=($sParams['sis'] == '1') ? 'selected="selected"' : ''?> value="1">Sispran 1</option>
											<option <?=($sParams['sis'] == '2') ? 'selected="selected"' : ''?> value="2">Sispran 2</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
			       		<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Alokasi</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="jns">
											<option value="">-Pilih Alokasi-</option>
											<option <?=($sParams['jns'] == '02') ? 'selected="selected"' : ''?> value="02">Barang</option>
											<option <?=($sParams['jns'] == '03') ? 'selected="selected"' : ''?> value="03">Jasa</option>
											<option <?=($sParams['jns'] == '04') ? 'selected="selected"' : ''?> value="04">Modal</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Bulan</label>
									<div class="col-lg-2 col-md-2 col-sm-4">
										<select class="form-control" name="bl">
											<option value="">-Awal-</option>
											<option <?=($sParams['bl'] == '1') ? 'selected="selected"' : ''?> value="1">Januari</option>
											<option <?=($sParams['bl'] == '2') ? 'selected="selected"' : ''?> value="2">Februari</option>
											<option <?=($sParams['bl'] == '3') ? 'selected="selected"' : ''?> value="3">Maret</option>
											<option <?=($sParams['bl'] == '4') ? 'selected="selected"' : ''?> value="4">April</option>
											<option <?=($sParams['bl'] == '5') ? 'selected="selected"' : ''?> value="5">Mei</option>
											<option <?=($sParams['bl'] == '6') ? 'selected="selected"' : ''?> value="6">Juni</option>
											<option <?=($sParams['bl'] == '7') ? 'selected="selected"' : ''?> value="7">Juli</option>
											<option <?=($sParams['bl'] == '8') ? 'selected="selected"' : ''?> value="8">Agustus</option>
											<option <?=($sParams['bl'] == '9') ? 'selected="selected"' : ''?> value="9">September</option>
											<option <?=($sParams['bl'] == '10') ? 'selected="selected"' : ''?> value="10">Oktober</option>
											<option <?=($sParams['bl'] == '11') ? 'selected="selected"' : ''?> value="11">November</option>
											<option <?=($sParams['bl'] == '12') ? 'selected="selected"' : ''?> value="12">Desember</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
			       		<div class="form-group row">
			       			<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Diadakan</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="ol">
											<option value="">-Diadakan oleh-</option>
											<option <?=($sParams['ol'] == 'FTMD') ? 'selected="selected"' : ''?> value="FTMD">FTMD</option>
											<option <?=($sParams['ol'] == 'Logistik') ? 'selected="selected"' : ''?> value="Logistik">ITB/Logistik</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Status</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="st">
											<option value="">-Pilih Status-</option>
											<!-- <option <?=($sParams['st'] == '0') ? 'selected="selected"' : ''?> value="0">Baru</option>
											<option <?=($sParams['st'] == '1') ? 'selected="selected"' : ''?> value="1">Diproses</option>
											<option <?=($sParams['st'] == '2') ? 'selected="selected"' : ''?> value="2">Penawaran</option>
											<option <?=($sParams['st'] == '3') ? 'selected="selected"' : ''?> value="3">SPK</option>
											<option <?=($sParams['st'] == '4') ? 'selected="selected"' : ''?> value="4">Serah Terima</option>
											<option <?=($sParams['st'] == '5') ? 'selected="selected"' : ''?> value="5">Invoice</option>
											<option <?=($sParams['st'] == '6') ? 'selected="selected"' : ''?> value="6">Selesai</option> -->
											<option <?=($sParams['st'] == '0') ? 'selected="selected"' : ''?> value="0">Pemaketan</option>
											<option <?=($sParams['st'] == '1') ? 'selected="selected"' : ''?> value="1">Kontrak</option>
											<option <?=($sParams['st'] == '2') ? 'selected="selected"' : ''?> value="2">Invoice</option>
											<option <?=($sParams['st'] == '3') ? 'selected="selected"' : ''?> value="3">Selesai</option>
										</select>
									</div>									
			       		</div>
			       	</div>
							<div class="col-md-12 col-sm-12">
			       		<div class="form-group row">									
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Status Deadline</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="de">
											<option value="">-Status Deadline-</option>
											<option <?=($sParams['st_de'] == '1') ? 'selected="selected"' : ''?> value="1">> 7 hari</option>
											<option <?=($sParams['st_de'] == '2') ? 'selected="selected"' : ''?> value="2">3 - 7 hari</option>
											<option <?=($sParams['st_de'] == '3') ? 'selected="selected"' : ''?> value="3">< 3 hari</option>
											<option <?=($sParams['st_de'] == '4') ? 'selected="selected"' : ''?> value="4">Lewat Batas</option>
										</select>
									</div>
									<label class="col-form-label text-lg-right col-lg-2 col-sm-4">Status Aktual</label>
									<div class="col-lg-4 col-md-4 col-sm-8">
										<select class="form-control" name="ak">
											<option value="">-Status Aktual-</option>
											<option <?=($sParams['st_ak'] == '1') ? 'selected="selected"' : ''?> value="1">< 50%</option>
											<option <?=($sParams['st_ak'] == '2') ? 'selected="selected"' : ''?> value="2">50 - 75%</option>
											<option <?=($sParams['st_ak'] == '3') ? 'selected="selected"' : ''?> value="3">> 75%</option>
											<option <?=($sParams['st_ak'] == '4') ? 'selected="selected"' : ''?> value="4">Selesai</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-12 text-center">
								<button type="submit" class="btn btn-primary mr-2">Cari</button>
								<a href="<?=base_url('data/pengadaan')?>" class="btn btn-secondary">Reset</a>
							</div>
		       	</div>
		      </form>		
				</div>
			</div>
			
			<!--end: Search-->

			<!--begin: Datatable-->
			<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
				<thead>
					<tr>
						<th colspan="3">Kegiatan</th>
						<th colspan="3">Rencana</th>
						<th colspan="3">RAB</th>
						<th colspan="4">Status</th>
					</tr>
					<tr>
						<!-- <th>ID Pengadaan</th> -->
						<!-- <th>Kegiatan</th> -->
						<th>Jenis</th>
						<th>Deskripsi</th>
						<th>Sispran</th>
						<th>RI</th>
						<th>Mulai</th>
						<th>Berakhir</th>
						<th>RAB</th>
						<th>Diadakan</th>
						<th>Sumber</th>
						<th>Proses</th>
						<th>Aktual (%)</th>
						<th>Pembayaran</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
			<!--end: Datatable-->
		</div>
	</div>
	<!--end::Card-->
</div>
<?php $this->endSection() ?>
