<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script src="/assets/js/datapages/pengadaan_form.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>

<?php
	
	if ($docs){
		
		foreach($docs as $d){

			$doc[]                 = $d['id_dokumen'];		  
		  	$tgl[$d['id_dokumen']] = date('m/d/Y', strtotime($d['tgl_dokumen']));
		}	
	}

?>



<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--begin::Card-->
	    <div class="card card-custom gutter-b example example-compact">
				<div class="card-header">
					<h3 class="card-title">Pengadaan Baru</h3>
				</div>
				<!--begin::Form-->
				<form class="form" method="post" id="kt_form" action="/data/pengadaan/edit/<?= $pengadaan['id_pengadaan'] ?>">				
					<div class="card-body">

						<!-- validation message -->
            			<?php if(isset($validation)) : ?>
              				<div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                  				<div class="alert-icon"><i class="flaticon-warning"></i></div>                
                  				<div class="alert-text">  <?= $validation->listErrors(); ?></div>
                  				<div class="alert-close">
                      				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          				<span aria-hidden="true"><i class="ki ki-close"></i></span>
                      				</button>
                  				</div>
              				</div>
            			<?php endif; ?> 

            			<!-- form -->
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Kegiatan <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="text" name="kegiatan" class="form-control" name="kegiatan" value="<?= $pengadaan['nama_kegiatan'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Jenis <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<select class="form-control" id="kt_select2_jenis" name="jenis">
											<option label=""

											></option>
											<option value="01020102"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020102' ) ? 'selected' : ''; ?>>KO - Barang</option>
											<option value="01020103"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020103' ) ? 'selected' : ''; ?>>KO - Jasa</option>
											<option value="01020104"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020104' ) ? 'selected' : ''; ?>>KO - Modal</option>
											<option value="01020202"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020202' ) ? 'selected' : ''; ?>>P3MI - Barang</option>
											<option value="01020203"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020203' ) ? 'selected' : ''; ?>>P3MI - Jasa</option>
											<option value="01020204"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '01020204' ) ? 'selected' : ''; ?>>P3MI - Modal</option>
											<option value="02040102"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040102' ) ? 'selected' : ''; ?>>Kerma Alihan - Barang</option>
											<option value="02040103"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040103' ) ? 'selected' : ''; ?>>Kerma Alihan - Jasa</option>
											<option value="02040104"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040104' ) ? 'selected' : ''; ?>>Kerma Alihan - Modal</option>
											<option value="02040202"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040202' ) ? 'selected' : ''; ?>>Kerma Non Alihan - Barang</option>
											<option value="02040203"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040203' ) ? 'selected' : ''; ?>>Kerma Non Alihan - Jasa</option>
											<option value="02040204"
											<?= (isset($pengadaan['id_const']) && $pengadaan['id_const'] == '02040204' ) ? 'selected' : ''; ?>>Kerma Non Alihan - Modal</option>
										</select>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">RAB <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<input type="text" name="rab" class="form-control"  value="<?= $pengadaan['nominal_rab'] ?>" >
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Diadakan Oleh <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<select class="form-control" name="diadakan" id="kt_select2_diadakan">
											<option label=""></option>
											<option value="FTMD" <?= (isset($pengadaan['diadakan']) && $pengadaan['diadakan'] == 'FTMD' ) ? 'selected' : ''; ?>>FTMD</option>
											<option value="ITB" <?= (isset($pengadaan['diadakan']) && $pengadaan['diadakan'] == 'ITB' ) ? 'selected' : ''; ?>>DitLog ITB</option>
										</select>
									</div>
								</div>								
								<div class="form-group row">
                 			        <label class="col-form-label text-lg-right col-lg-4 col-sm-12">Nama Pemohon <span class="text-danger">*</span></label>
                 			        <div class="col-lg-8 col-md-8 col-sm-12">
                  						<select class="form-control" id="kt_select2_pemohon" name="pemohon">
                  						<option label=""></option>
                    					<?php foreach($employees as $employee) : ?>
                      						<option value="<?= $employee['nopeg'] ?>"
                      							<?= (isset($pengadaan['nopeg_pemohon']) && $pengadaan['nopeg_pemohon'] == $employee['nopeg'] ) ? 'selected' : ''; ?>>
                              					<?= $employee['nama'] ?> 
                     			 			</option>
                   						<?php endforeach; ?>
                  						</select>                  						
                 					</div>
                				</div> <!--end form group row-->
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal RI <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_ri" name="tgl_ri" readonly="readonly" placeholder="Select date"  value="<?= date('m/d/Y', strtotime($pengadaan['tgl_ri'])) ?>"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal Rencana Mulai <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_mulai" name="rencana_tgl_mulai" readonly="readonly" placeholder="Select date" value="<?= date('m/d/Y', strtotime($pengadaan['rencana_tgl_mulai'])) ?>"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Tanggal Rencana Selesai <span class="text-danger">*</span></label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<div class="input-group date">
											<input type="text" class="form-control" id="kt_datepicker_akhir" name="rencana_tgl_selesai" readonly="readonly" placeholder="Select date"  value="<?= date('m/d/Y', strtotime($pengadaan['rencana_tgl_mulai'])) ?>"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar-check-o"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-4 col-sm-12">Status Pembayaran</label>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<select class="form-control" name="status_pembayaran" id="kt_select2_bayar">
											<option label=""></option>
											<option value="0"
											<?= (isset($pengadaan['status_pembayaran']) && $pengadaan['status_pembayaran'] == '0' ) ? 'selected' : ''; ?>>Belum Diproses</option>
											<option value="1"
											<?= (isset($pengadaan['status_pembayaran']) && $pengadaan['status_pembayaran'] == '1' ) ? 'selected' : ''; ?>>Diproses FTMD</option>
											<option value="2"
											<?= (isset($pengadaan['status_pembayaran']) && $pengadaan['status_pembayaran'] == '2' ) ? 'selected' : ''; ?>>Diproses Ditkeu</option>
											<option value="3"
											<?= (isset($pengadaan['status_pembayaran']) && $pengadaan['status_pembayaran'] == '3' ) ? 'selected' : ''; ?>>Sudah Dibayar</option>
										</select>
									</div>
								</div>												
							</div>							
							<div class="col-md-12 col-sm-12">
								<div class="form-group row">
									<label class="col-form-label text-lg-right col-lg-2 col-sm-12">Deskripsi</label>
									<div class="col-lg-10 col-md-10 col-sm-12">
										<textarea class="form-control" rows="3" name="deskripsi"><?= $pengadaan['deskripsi'] ?></textarea>
									</div>
								</div>								
							</div>						   
						</div>					

						
						<div class="separator separator-dashed my-5"></div>
						
						<h5 class=" text-dark font-weight-bold mb-10">Status & Tanggal Dokumen</h5>
						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>BA Pembayaran</label>   							 	
  								<div class="input-group date">
  									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox"  name="dokumen[]" value="1"
        									<?= (isset($doc) && in_array("1", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>									
									<input type="text" class="form-control" id="kt_datepicker_ba" name="tgl_1" readonly="readonly" placeholder="Select date" 
									 value="<?= (isset($tgl[1])) ? $tgl[1] : ''; ?>"/>
									<div class="input-group-prepend">										
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>										
									</div>									
								</div>
								<span class="form-text text-muted"></span>   							 	
   							</div>

   							<div class="col-lg-4">
   							 	<label>Surat Permohonan Pembayaran</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="2"
        									<?= (isset($doc) && in_array("2", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_spp" name="tgl_2" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[2])) ? $tgl[2] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Invoice</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="3"
        									<?= (isset($doc) && in_array("3", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_invoice" name="tgl_3" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[3])) ? $tgl[3] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						</div><!--end group row-->

 						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>Kuitansi</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="4"
        									<?= (isset($doc) && in_array("4", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_kuitansi" name="tgl_4" readonly="readonly" placeholder="Select date"
									value="<?= (isset($tgl[4])) ? $tgl[4] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>									
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Faktur Pajak</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="5"
        									<?= (isset($doc) && in_array("5", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_faktur" name="tgl_5" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[5])) ? $tgl[5] : ''; ?>"/> 
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Copy NPWP Penyedia</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="6"
        									<?= (isset($doc) && in_array("6", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_npwp" name="tgl_6" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[6])) ? $tgl[6] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						</div><!--end group row-->

 						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>Surat Pengukuhan PKP</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="7"
        									<?= (isset($doc) && in_array("7", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_pkp" name="tgl_7" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[7])) ? $tgl[7] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Dokumen Referensi Bank Penyedia</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="8"
        									<?= (isset($doc) && in_array("8", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_bank" name="tgl_8" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[8])) ? $tgl[8] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>BA Serah Terima Pekerjaan</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="9"
        									<?= (isset($doc) && in_array("9", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_bast" name="tgl_9" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[9])) ? $tgl[9] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						</div><!--end group row-->

 						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>Surat Jalan</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="10"
        									<?= (isset($doc) && in_array("10", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_jalan" name="tgl_10" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[10])) ? $tgl[10] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Surat Pesanan</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="11"
        									<?= (isset($doc) && in_array("11", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_pesanan" name="tgl_11" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[11])) ? $tgl[11] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Purchase Order</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="12"
        									<?= (isset($doc) && in_array("12", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_po" name="tgl_12" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[12])) ? $tgl[12] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						</div><!--end group row-->

 						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>Penawaran Harga</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="13"
        									<?= (isset($doc) && in_array("13", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_harga" name="tgl_13" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[13])) ? $tgl[13] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>KAK</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="14"
        									<?= (isset($doc) && in_array("14", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_kak" name="tgl_14" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[14])) ? $tgl[14] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Bukti Requisition Oracle</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="15"
        									<?= (isset($doc) && in_array("15", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_ro" name="tgl_15" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[15])) ? $tgl[15] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						</div><!--end group row-->

 						<div class="form-group row">   						
   							<div class="col-lg-4">
   							 	<label>Permintaan Penawaran</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="16"
        									<?= (isset($doc) && in_array("16", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_permintaan" name="tgl_16" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[16])) ? $tgl[16] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Bukti Purchase Order Oracle</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="17"
        									<?= (isset($doc) && in_array("17", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_bukti" name="tgl_17" readonly="readonly" placeholder="Select date" 
									value="<?= (isset($tgl[17])) ? $tgl[17] : ''; ?>"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
   							<div class="col-lg-4">
   							 	<label>Bukti Serah Terima Jasa</label>
  								<div class="input-group date">
									<div class="input-group-prepend">
										<span class="input-group-text">
       									<label class="checkbox checkbox-inline checkbox-success">
        									<input type="checkbox" name="dokumen[]" value="18"
        									<?= (isset($doc) && in_array("18", $doc))  ? 'checked': '' ?>/>
        									<span></span>
       									</label>
       									</span>
									</div>
									<input type="text" class="form-control" id="kt_datepicker_jasa" name="tgl_18" readonly="readonly" placeholder="Select date"
									value="<?= (isset($tgl[18])) ? $tgl[18] : ''; ?>"/> 
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>   							 	
   							</div>
 						 </div><!--end group row-->
 				
					</div> <!-- end card body -->
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<input type="hidden" name="id_pengadaan" value="<?= $pengadaan['id_pengadaan'] ?>">
								<button class="btn btn-primary mr-2">Simpan</button>
								<button type="reset" class="btn btn-secondary">Batal</button>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
