<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title>DASHBOARD FTMD | ITB</title>
		<meta name="description" content="Updates and statistics" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!-- <link rel="canonical" href="https://keenthemes.com/metronic" /> -->
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
    <?php $this->renderSection('stylesheet') ?>
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="<?=base_url('assets/plugins/global/plugins.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/plugins/custom/prismjs/prismjs.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/css/style.bundle.css')?>" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="<?=base_url('assets/media/itb/logo-itb.png')?>" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed header-bottom-enabled page-loading">
		<!--begin::Main-->
		<!--begin::Header Mobile-->
		<div id="kt_header_mobile" class="header-mobile bg-primary header-mobile-fixed">
			<!--begin::Logo-->
			<a href="<?=base_url()?>">
				<img alt="Logo" src="<?=base_url('assets/media/itb/logo-itb.png')?>" class="max-h-30px" />
			</a>
			<!--end::Logo-->
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center">
				<button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
					<span></span>
				</button>
				<button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
								<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>
				</button>
			</div>
			<!--end::Toolbar-->
		</div>
		<!--end::Header Mobile-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page--4-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header flex-column header-fixed">
						<!--begin::Top-->
						<div class="header-top">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Left-->
								<div class="d-none d-lg-flex align-items-center mr-3">
									<!--begin::Logo-->
									<a href="index.html" class="mr-20">
										<img alt="Logo" src="<?=base_url('assets/media/itb/logo-itb.png')?>" class="max-h-45px" />
									</a>
									<!--end::Logo-->
									<!--begin::Tab Navs(for desktop mode)-->
									<ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
										<!--begin::Item-->
										<li class="nav-item">
											<a href="<?=base_url('dashboard/'.$submenu)?>" class="nav-link py-4 px-6 <?= ($menu == 'dashboard') ? 'active' : ''; ?>">Dashboard</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-3">
											<a href="<?=base_url('data/'.$submenu)?>" class="nav-link py-4 px-6 <?= ($menu == 'data') ? 'active' : ''; ?>">Data</a>
										</li>
										<!--end::Item-->
									</ul>
									<!--begin::Tab Navs-->
								</div>
								<div class="d-none d-lg-flex align-items-center mr-3">
									<h3 class="text-white font-weight-bolder">
										DASHBOARD FTMD
									</h3>
								</div>
								<!--end::Left-->
								<!--begin::Topbar-->
								<div class="topbar bg-primary">
									<!--begin::User-->
									<div class="topbar-item">
										<div class="btn btn-icon btn-hover-transparent-white w-sm-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
											<div class="d-flex flex-column text-right pr-sm-3">
												<span class="text-white opacity-50 font-weight-bold font-size-sm d-none d-sm-inline"><?=session()->get('nama')?></span>
												<span class="text-white font-weight-bolder font-size-sm d-none d-sm-inline"><?=ucwords(session()->get('role'))?></span>
											</div>
											<span class="symbol symbol-35">
												<span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-30"><?=strtoupper(substr(session()->get('nama'), 0, 1))?></span>
											</span>
										</div>
									</div>
									<!--end::User-->
								</div>
								<!--end::Topbar-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Top-->
						<!--begin::Bottom-->
						<div class="header-bottom">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Header Menu Wrapper-->
								<div class="header-navs header-navs-left" id="kt_header_navs">
									<!--begin::Tab Navs(for tablet and mobile modes)-->
									<ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean <?= ($menu == 'dashboard') ? 'show active' : ''; ?>" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">Dashboard</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean <?= ($menu == 'data') ? 'show active' : ''; ?>" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">Data</a>
										</li>
										<!--end::Item-->
									</ul>
									<!--begin::Tab Navs-->
									<!--begin::Tab Content-->
									<div class="tab-content">
										<!--begin::Tab Pane-->
										<div class="tab-pane py-5 p-lg-0 justify-content-between <?= ($menu == 'dashboard') ? 'show active' : ''; ?>" id="kt_header_tab_1">
											<!--begin::Menu-->
											<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
												<!--begin::Nav-->
												<ul class="menu-nav">
													<li class="menu-item <?= $submenu == 'anggaran' ? 'menu-item-here' :  ''; ?>" data-menu-toggle="click" aria-haspopup="true">
														<a href="<?=base_url('dashboard/anggaran')?>" class="menu-link">
															<span class="menu-text">Anggaran</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
													</li>
													<li class="menu-item <?= $submenu == 'sk' ? 'menu-item-active' :  ''; ?>" aria-haspopup="true">
														<a href="<?=base_url('dashboard/sk')?>" class="menu-link">
															<span class="menu-text">Remunerasi</span>
														</a>
													</li>
													<li class="menu-item <?= $submenu == 'pengadaan' ? 'menu-item-active' :  ''; ?>" aria-haspopup="true">
														<a href="<?=base_url('dashboard/pengadaan')?>" class="menu-link">
															<span class="menu-text">Pengadaan</span>
														</a>
													</li>
													<li class="menu-item <?= $submenu == 'kepegawaian' ? 'menu-item-active' :  ''; ?>" aria-haspopup="true">
														<a href="<?=base_url('dashboard/kepegawaian')?>" class="menu-link">
															<span class="menu-text">Kepegawaian</span>
														</a>
													</li>
													<li class="menu-item" aria-haspopup="true">
														<a href="#" class="menu-link" data-toggle="modal" data-target="#f-modal-message">
															<span class="menu-text">Kirim Pesan</span>
														</a>
													</li>
												</ul>
												<!--end::Nav-->
											</div>
											<div class="d-flex flex-column text-right">
												<h1><?=date('d M Y')?></h1>
												<span class="text-muted font-size-sm">Terakhir diupdate: <?=(isset($last_update)) ? $last_update : ''?></span>
											</div>
											<!--end::Menu-->
										</div>
										<!--begin::Tab Pane-->
										<!--begin::Tab Pane-->
										<div class="tab-pane p-5 p-lg-0 justify-content-between <?= ($menu == 'data') ? 'show active' : ''; ?>" id="kt_header_tab_2">
											<!--begin::Menu-->
											<div id="kt_header_menu_2" class="header-menu header-menu-mobile header-menu-layout-default">
												<!--begin::Nav-->
												<ul class="menu-nav">
													<li class="menu-item menu-item-submenu menu-item-rel <?= $submenu == 'anggaran' ? 'menu-item-here' :  ''; ?>" data-menu-toggle="click" aria-haspopup="true">
														<a href="<?=base_url('data/anggaran')?>" class="menu-link">
															<span class="menu-text">Anggaran</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<!-- <a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Anggaran</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item <?= (isset($submenu2) && $submenu2 == 'rka') ? 'menu-item-open menu-item-here' : ''; ?>" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/anggaran/rka_sispran1')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<polygon points="0 0 24 0 24 24 0 24" />
																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">RKA</span>
																	</a>
																</li>
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/anggaran/ri_sispran1')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3" />
																					<polygon fill="#000000" opacity="0.3" points="4 19 10 11 16 19" />
																					<polygon fill="#000000" points="11 19 15 14 19 19" />
																					<path d="M18,12 C18.8284271,12 19.5,11.3284271 19.5,10.5 C19.5,9.67157288 18.8284271,9 18,9 C17.1715729,9 16.5,9.67157288 16.5,10.5 C16.5,11.3284271 17.1715729,12 18,12 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">RI</span>
																	</a>
																</li>
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/anggaran/fra_sispran1')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M16.3740377,19.9389434 L22.2226499,11.1660251 C22.4524142,10.8213786 22.3592838,10.3557266 22.0146373,10.1259623 C21.8914367,10.0438285 21.7466809,10 21.5986122,10 L17,10 L17,4.47708173 C17,4.06286817 16.6642136,3.72708173 16.25,3.72708173 C15.9992351,3.72708173 15.7650616,3.85240758 15.6259623,4.06105658 L9.7773501,12.8339749 C9.54758575,13.1786214 9.64071616,13.6442734 9.98536267,13.8740377 C10.1085633,13.9561715 10.2533191,14 10.4013878,14 L15,14 L15,19.5229183 C15,19.9371318 15.3357864,20.2729183 15.75,20.2729183 C16.0007649,20.2729183 16.2349384,20.1475924 16.3740377,19.9389434 Z" fill="#000000" />
																					<path d="M4.5,5 L9.5,5 C10.3284271,5 11,5.67157288 11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L4.5,8 C3.67157288,8 3,7.32842712 3,6.5 C3,5.67157288 3.67157288,5 4.5,5 Z M4.5,17 L9.5,17 C10.3284271,17 11,17.6715729 11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L4.5,20 C3.67157288,20 3,19.3284271 3,18.5 C3,17.6715729 3.67157288,17 4.5,17 Z M2.5,11 L6.5,11 C7.32842712,11 8,11.6715729 8,12.5 C8,13.3284271 7.32842712,14 6.5,14 L2.5,14 C1.67157288,14 1,13.3284271 1,12.5 C1,11.6715729 1.67157288,11 2.5,11 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">FRA</span>
																	</a>
																</li>
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/anggaran/real_sispran1')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
																				</g>
																			</svg>
																		</span>
																		<span class="menu-text">Realisasi</span>
																	</a>
																</li>
															</ul>
														</div> -->
													</li>
													<li class="menu-item menu-item-submenu menu-item-rel <?= $submenu == 'sk' ? 'menu-item-here' :  ''; ?>" data-menu-toggle="click" aria-haspopup="true">
														<a href="javascript:;" class="menu-link menu-toggle">
															<span class="menu-text">Remunerasi</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
														<div class="menu-submenu menu-submenu-classic menu-submenu-left">
															<ul class="menu-subnav">
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/sk')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<!--begin::Svg Icon | path:/assets/media/svg/icons/General/Thunder-move.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M16.3740377,19.9389434 L22.2226499,11.1660251 C22.4524142,10.8213786 22.3592838,10.3557266 22.0146373,10.1259623 C21.8914367,10.0438285 21.7466809,10 21.5986122,10 L17,10 L17,4.47708173 C17,4.06286817 16.6642136,3.72708173 16.25,3.72708173 C15.9992351,3.72708173 15.7650616,3.85240758 15.6259623,4.06105658 L9.7773501,12.8339749 C9.54758575,13.1786214 9.64071616,13.6442734 9.98536267,13.8740377 C10.1085633,13.9561715 10.2533191,14 10.4013878,14 L15,14 L15,19.5229183 C15,19.9371318 15.3357864,20.2729183 15.75,20.2729183 C16.0007649,20.2729183 16.2349384,20.1475924 16.3740377,19.9389434 Z" fill="#000000" />
																					<path d="M4.5,5 L9.5,5 C10.3284271,5 11,5.67157288 11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L4.5,8 C3.67157288,8 3,7.32842712 3,6.5 C3,5.67157288 3.67157288,5 4.5,5 Z M4.5,17 L9.5,17 C10.3284271,17 11,17.6715729 11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L4.5,20 C3.67157288,20 3,19.3284271 3,18.5 C3,17.6715729 3.67157288,17 4.5,17 Z M2.5,11 L6.5,11 C7.32842712,11 8,11.6715729 8,12.5 C8,13.3284271 7.32842712,14 6.5,14 L2.5,14 C1.67157288,14 1,13.3284271 1,12.5 C1,11.6715729 1.67157288,11 2.5,11 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
																			<!--end::Svg Icon-->
																		</span>
																		<span class="menu-text">Daftar SK</span>
																	</a>
																</li>
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/sk/pembayaran')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<!--begin::Svg Icon | path:/assets/media/svg/icons/Communication/Adress-book2.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
																				</g>
																			</svg>
																			<!--end::Svg Icon-->
																		</span>
																		<span class="menu-text">Pengajuan Nominatif</span>
																	</a>
																</li>
																<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
																	<a href="<?=base_url('data/sk/nominatif')?>" class="menu-link">
																		<span class="svg-icon menu-icon">
																			<!--begin::Svg Icon | path:/assets/media/svg/icons/Communication/Adress-book2.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
																				</g>
																			</svg>
																			<!--end::Svg Icon-->
																		</span>
																		<span class="menu-text">Remunerasi</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<li class="menu-item menu-item-submenu menu-item-rel <?= $submenu == 'pengadaan' ? 'menu-item-active' :  ''; ?>" data-menu-toggle="click" aria-haspopup="true">
														<a href="<?=base_url('data/pengadaan')?>" class="menu-link">
															<span class="menu-text">Pengadaan</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
													</li>
													<li class="menu-item menu-item-submenu menu-item-rel <?= $submenu == 'kepegawaian' ? 'menu-item-here' :  ''; ?>" data-menu-toggle="click" aria-haspopup="true">
														<a href="<?=base_url('data/kepegawaian')?>" class="menu-link">
															<span class="menu-text">Kepegawaian</span>
															<span class="menu-desc"></span>
															<i class="menu-arrow"></i>
														</a>
													</li>
												</ul>
											</div>
										</div>
										<!--end::Tab Pane-->
									</div>
									<!--end::Tab Content-->
								</div>
								<!--end::Header Menu Wrapper-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Bottom-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Page Heading-->
									<div class="d-flex align-items-baseline flex-wrap mr-5">
										<!--begin::Page Title-->
										<h5 class="text-dark font-weight-bold my-1 mr-5"><?= ucwords($menu); ?></h5>
										<!--end::Page Title-->
										<!--begin::Breadcrumb-->
										<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
											<li class="breadcrumb-item">
												<a href="#" class="text-muted"><?= ucwords($submenu); ?></a>
											</li>
											<?php if (isset($submenu2)): ?>
												<li class="breadcrumb-item">
													<a href="#" class="text-muted"><?= ucwords($submenu2); ?></a>
												</li>
											<?php endif; ?>
											<?php if (isset($submenu3)): ?>
												<li class="breadcrumb-item">
													<a href="#" class="text-muted"><?= ucwords($submenu3); ?></a>
												</li>
											<?php endif; ?>
										</ul>
										<!--end::Breadcrumb-->
									</div>
									<!--end::Page Heading-->
								</div>
								<!--end::Info-->
								<?php if ($menu == 'dashboard' && $submenu == 'sk'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">
										<!--begin::Actions-->
										<?php if (isset($submenu2) && $submenu2 != date('Y')) : ?>
											<a href="<?=base_url('dashboard/sk')?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')?></a>
										<?php else : ?>
											<a href="<?=base_url('dashboard/sk/index?thn='.date('Y', strtotime('last year')))?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')-1?></a>
										<?php endif; ?> &nbsp;&nbsp;


										<!-- <a href="<?=base_url('dashboard/sk?thn=2022')?>" class="btn btn-light-primary font-weight-bolder btn-sm ">2022</a> &nbsp;&nbsp; -->
										<a href="<?=base_url('dashboard/sk')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'semua') ? 'active' : ''; ?>">Semua</a> &nbsp;&nbsp;
										<a href="<?=base_url('dashboard/sk/sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('dashboard/sk/sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
								<?php endif; ?>
								<?php if ($menu == 'dashboard' && $submenu == 'anggaran'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">
										<!--begin::Actions-->
										<?php if (isset($submenu2) && $submenu2 != date('Y')) : ?>
											<a href="<?=base_url('dashboard/anggaran')?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')?></a>
										<?php else : ?>
											<a href="<?=base_url('dashboard/anggaran/index?thn='.date('Y', strtotime('last year')))?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')-1?></a>
										<?php endif; ?> &nbsp;&nbsp;

										<a href="<?=base_url('dashboard/anggaran')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'semua') ? 'active' : ''; ?>">Semua</a> &nbsp;&nbsp;
										<a href="<?=base_url('dashboard/anggaran/sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('dashboard/anggaran/sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
								<?php endif; ?>
								<?php if ($menu == 'dashboard' && $submenu == 'pengadaan'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">
										<!--begin::Actions-->
										<?php if (isset($thn_aktif) && $thn_aktif != date('Y')) : ?>
											<a href="<?=base_url('dashboard/pengadaan')?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')?></a>
										<?php else : ?>
											<a href="<?=base_url('dashboard/pengadaan/index?thn='.date('Y', strtotime('last year')))?>" class="btn btn-light-success font-weight-bolder btn-sm active">Tahun <?=date('Y')-1?></a>
										<?php endif; ?> &nbsp;&nbsp;
										<button id="btn-pengadaan-aktual" class="btn btn-light-primary font-weight-bolder btn-sm active">Aktual</button>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
								<?php endif; ?>
								<!-- data anggaran -->
								<?php if ($menu == 'data' && $submenu == 'anggaran'): ?>
									<!-- rka -->
									<?php if ($submenu2 == 'rka'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">										
										<!--begin::Actions-->										
										<a href="<?=base_url('data/anggaran/rka_sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('data/anggaran/rka_sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
									<?php endif; ?>
									<!-- end rka -->
									<!-- ri -->
									<?php if ($submenu2 == 'ri'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">										
										<!--begin::Actions-->										
										<a href="<?=base_url('data/anggaran/ri_sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('data/anggaran/ri_sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
									<?php endif; ?>
									<!-- end ri -->
									<!-- fra -->
									<?php if ($submenu2 == 'fra'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">										
										<!--begin::Actions-->										
										<a href="<?=base_url('data/anggaran/fra_sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('data/anggaran/fra_sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
									<?php endif; ?>
									<!-- end fra -->
									<!-- realisasi -->
									<?php if ($submenu2 == 'realisasi'): ?>
									<!--begin::Toolbar-->
									<div class="d-flex align-items-center">										
										<!--begin::Actions-->										
										<a href="<?=base_url('data/anggaran/real_sispran1')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 1') ? 'active' : ''; ?>">Sispran 1</a> &nbsp;&nbsp;
										<a href="<?=base_url('data/anggaran/real_sispran2')?>" class="btn btn-light-primary font-weight-bolder btn-sm <?= (isset($submenu3) && $submenu3 == 'sispran 2') ? 'active' : ''; ?>">Sispran 2</a>
										<!--end::Actions-->
									</div>
									<!--end::Toolbar-->
									<?php endif; ?>
									<!-- end realisasi -->
								<?php endif; ?>
								<!-- end data anggaran -->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<?php $this->renderSection('content') ?>
							<!--end::Container-->
							<div class="modal fade" id="info-upload" tabindex="-1" role="dialog" aria-labelledby="infoUploadLabel" aria-hidden="true">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title" id="infoUploadLabel">Informasi Update Data</h5>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <i aria-hidden="true" class="ki ki-close"></i>
							                </button>
							            </div>
							            <div class="modal-body">
							            	<div class="alert alert-warning mb-5 p-5" role="alert">
											    <h4 class="alert-heading"><i class="flaticon-warning" style="color: #FFFFFF;"></i> Perhatian</h4>
											    <p>Dashboard telah melebihi waktu satu bulan tanpa pembaharuan data. Silahkan lakukan pembaharuan data setiap <strong>minggu ke-1 dan ke-3</strong> setiap bulannya.</p>
											    <div class="border-bottom border-white opacity-20 mb-5"></div>
											    <p class="mb-0">Terima kasih.</p>
											</div>
							            </div>
							            <div class="modal-footer">
							                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
							            </div>
							        </div>
							    </div>
							</div>

							<div class="modal fade" id="f-modal-message" tabindex="-1" role="dialog" aria-labelledby="infoFormMessage" aria-hidden="true">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title" id="infoFormMessage">Kirim Pesan</h5>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <i aria-hidden="true" class="ki ki-close"></i>
							                </button>
							            </div>
							            <div class="modal-body">
							            	<form class="form" id="f-message">
							            		<div class="form-group">
							            			<label for="message">Pesan</label>
							            			<textarea class="form-control" rows="3" id="message" name="message"></textarea>
							            		</div>
							            	</form>
							            </div>
							            <div class="modal-footer">
							                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Batal</button>
							                <button type="button" class="btn btn-light-primary font-weight-bold" id="btn-send-msg">Kirim</button>
							            </div>
							        </div>
							    </div>
							</div>

						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted font-weight-bold mr-2">2020©</span>
								<a href="https://www.ftmd.itb.ac.id/" target="_blank" class="text-dark-75 text-hover-primary">Fakultas Teknik Mesin dan Dirgantara</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Nav-->
							<div class="nav nav-dark order-1 order-md-2">
								<a href="#" target="_blank" class="nav-link pr-3 pl-0">About</a>
								<a href="#" target="_blank" class="nav-link px-3">Team</a>
								<a href="#" target="_blank" class="nav-link pl-3 pr-0">Contact</a>
							</div>
							<!--end::Nav-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		<!-- begin::User Panel-->
		<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
			<!--begin::Header-->
			<div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
				<h3 class="font-weight-bold m-0">User Profile
				<!-- <small class="text-muted font-size-sm ml-2">12 messages</small></h3> -->
				<a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
					<i class="ki ki-close icon-xs text-muted"></i>
				</a>
			</div>
			<!--end::Header-->
			<!--begin::Content-->
			<div class="offcanvas-content pr-5 mr-n5">
				<!--begin::Header-->
				<div class="d-flex align-items-center mt-5">
					<div class="symbol symbol-100 mr-5">
						<div class="symbol-label" style="background-image:url(<?=base_url('assets/media/users/300_21.jpg')?>)"></div>
						<i class="symbol-badge bg-success"></i>
					</div>
					<div class="d-flex flex-column">
						<a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary"><?=session()->get('nama')?></a>
						<div class="text-muted mt-1"><?=ucfirst(session()->get('role'))?></div>
						<div class="navi mt-2">
							<a href="#" class="navi-item">
								<span class="navi-link p-0 pb-2">
									<span class="navi-icon mr-1">
										<span class="svg-icon svg-icon-lg svg-icon-primary">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
													<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
									<span class="navi-text text-muted text-hover-primary">info@ftmd.itb.ac.id</span>
								</span>
							</a>
							<a href="<?=base_url('signout')?>" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
						</div>
					</div>
				</div>
				<!--end::Header-->
				<!--begin::Separator-->
				<div class="separator separator-dashed mt-8 mb-5"></div>
				<!--end::Separator-->
			</div>
			<!--end::Content-->
		</div>
		<!-- end::User Panel-->
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="<?=base_url('assets/plugins/global/plugins.bundle.js')?>"></script>
		<script src="<?=base_url('assets/plugins/custom/prismjs/prismjs.bundle.js')?>"></script>
		<script src="<?=base_url('assets/js/scripts.bundle.js')?>"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Vendors(used by this page)-->
		<script src="<?=base_url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')?>"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script type="text/javascript">
			$(window).on('load', function() {
		        // $('#info-upload').modal('show');
		    });

			$('#btn-send-msg').on('click', function() {
				// return console.log('https://api.whatsapp.com/send?phone=08562046826&text=%20' + $('#message').val());
				// var number = '+62856-2046-826';
				// var message = $('#message').val();
				// var url = 'https://api.whatsapp.com/send?phone=' 
				//      + number 
				//      + '&text=' 
				//      + encodeURIComponent(message);

				// console.log(url);

				//   return url;
				 $('#f-modal-message').modal('hide');
			});
		</script>
		<?php $this->renderSection('script') ?>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>