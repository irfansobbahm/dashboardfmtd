<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>
	var HOST_URL = "<?php echo site_url('dashboard/anggaran');?>";
	var HOST_DATA_URL = "<?php echo site_url('data/anggaran');?>";
	var totalRka = <?=$total?>;
</script>
<script src="<?=base_url('assets/js/dashpages/anggaran_tahunan_sispran2_charts.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-lg-4">
			<!--begin::Card-->
			<div class="card card-custom gutter-b" style="min-height:440px;">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Kerma</h3>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<span>Kerma: 
								<a href="<?=base_url('')?>" target="_blank">
									<?=bcdiv(($summary['kerma_alihan']+$summary['kerma_non_alihan']+$summary['realokasi']),1000000000,2)?> M
								</a>
							</span>
							<div class="progress">
								<div class="progress-bar bg-warning bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Kerma Non Alihan: 
								<a href="<?=base_url('data/anggaran?sis=02&sbr=02')?>" target="_blank">
									<?=bcdiv($summary['kerma_non_alihan'],1000000000,2)?> M
								</a>
							</span>
							<div class="progress">
								<div class="progress-bar bg-warning bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Kerma Alihan: 
								<a href="<?=base_url('data/anggaran?sis=02&sbr=01')?>" target="_blank">
									<?=bcdiv($summary['kerma_alihan'],1000000000,2)?> M
								</a>
							</span>
							<div class="progress">
								<div class="progress-bar bg-warning bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Prospektif: 
								<a href="<?=base_url('data/anggaran?sis=02&jns=06')?>" target="_blank">
									<?=bcdiv($summary['prospektif'],1000000000,2)?> M
								</a>
							</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Realokasi Kerma: 
								<a href="<?=base_url('data/anggaran?sis=02&jns=05')?>" target="_blank">
									<?=bcdiv($summary['realokasi'],1000000000,2)?> M
								</a>
							</span>
							<div class="progress">
								<div class="progress-bar bg-warning bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6"></div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<!--begin::Card-->
			<div class="card card-custom gutter-b" style="min-height:350px;">
				<div class="card-header">
					<div class="card-title" style="width:100%">
						<div class="col-md-7"><h3 class="card-label">Penyerapan Per Bulan</h3></div>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<select class="form-control" name="jnspenyerapan" id="sel-penyerapan">
								<option value="all">Kumulatif</option>
								<option value="bulan">Per Bulan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_ri_all" class="d-flex justify-content-center" style="float:left"></div>
					<div id="chart_frabulan" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Alokasi Pegawai</h3>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_alokasi_pegawai"></div>
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-3">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Alokasi Barang</h3>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_alokasi_barang"></div>
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-3">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Alokasi Jasa</h3>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_alokasi_jasa"></div>
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-3">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Alokasi Modal</h3>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_alokasi_modal"></div>
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
