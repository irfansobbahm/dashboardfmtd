<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script src="/assets/js/dashpages/kepegawaian_charts.js"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b" style="min-height: 387px;">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Jabatan Dosen</h3>
					</div>
				</div>
				<div class="card-body">
          <!--begin::Chart-->
  				<div id="chart_jabatan" class="d-flex justify-content-center"></div>
  				<!--end::Chart-->
  			</div>
      </div>
      <!--end::Card-->
		</div>
    <div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Pemenuhan AK Kenaikan Jabatan</h3>
					</div>
				</div>
				<div class="card-body">
          <div id="chart_pemenuhan_ak" class="d-flex justify-content-center"></div>
				</div>
			</div>
      <!--end::Card-->
		</div>
	</div>
	<div class="row">    
		<div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Masa Purnabakti</h3>
					</div>
				</div>
				<div class="card-body">
          <!--begin::Chart-->
  				<div id="chart_purnabakti" class="d-flex justify-content-center"></div>
  				<!--end::Chart-->
  			</div>
      </div>
      <!--end::Card-->
		</div>
    <div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Lama menjabat Jabatan tertentu</h3>
					</div>
				</div>
				<div class="card-body">
          <div id="chart_lama_menjabat" class="d-flex justify-content-center"></div>
				</div>
			</div>
      <!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
