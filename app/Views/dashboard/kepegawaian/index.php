<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>
	var HOST_URL = "<?php echo site_url('dashboard/kepegawaian');?>";
	var HOST_DATA_URL = "<?php echo site_url('data/kepegawaian');?>";
</script>
<script src="<?=base_url('assets/js/dashpages/kepegawaian_charts.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-lg-7">
			<!--begin::Card-->
			<div class="card card-custom gutter-b" style="min-height: 387px;">
				<div class="card-header">
					<div class="card-title" style="width:100%">
						<div class="col-md-6"><h3 class="card-label">Jabatan Fungsional</h3></div>
						<div class="col-md-1"></div>
						<div class="col-md-5">
							<select class="form-control" name="kk" id="sel-kk">
								<option value="all">- Semua KK -</option>
								<option value="irt">Ilmu dan Rekayasa Termal</option>
								<option value="dfp">Dinamika Fluida dan Propulsi</option>
								<option value="mpsr">Mekanika Padatan dan Struktur Ringan</option>
								<option value="itm">Ilmu dan Teknik Material</option>								
								<option value="dk">Dinamika dan Kendali</option>
								<option value="mot">Mekanika dan Operasi Terbang</option>
								<option value="ptp">Perancangan Teknik dan Produksi</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_jabatan" class="d-flex justify-content-center" style="float: left;"></div>
					<div id="chart_jabatan_persen" class="d-flex justify-content-center"></div>
					<!--end::Chart-->
				</div>
				<div class="card-footer">
					<i class="fas fa-user-check"></i> <a href="<?=base_url('data/kepegawaian?stp=aktif')?>" target="_blank">Aktif</a> : <span id="jml-aktif"></span>&nbsp;&nbsp;&nbsp;
					<i class="fas fa-user-tie"></i> <a href="<?=base_url('data/kepegawaian?stp=mdl')?>" target="_blank">Menjabat di luar</a> : <span id="jml-menjabat"></span>&nbsp;&nbsp;&nbsp;
					<i class="fas fa-graduation-cap"></i> <a href="<?=base_url('data/kepegawaian?stp=tubel')?>" target="_blank">Tugas Belajar</a> : <span id="jml-tubel"></span>&nbsp;&nbsp;&nbsp;
					<!-- <i class="fas fa-calendar-check"></i> <a href="<?=base_url('data/kepegawaian?stp=pensiun')?>" target="_blank">Pensiun</a> : <span id="jml-pensiun"></span> -->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-5">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Pemenuhan Syarat Kenaikan Jabatan - Selain GB</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_pemenuhan_ak" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
	<div class="row">    
		<div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title"  style="width:100%">
						<div class="col-md-7"><h3 class="card-label">Masa Purnabakti</h3></div>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<select class="form-control" name="jabatan" id="sel-jabatan-purna">
								<option value="all">- Semua Jabatan -</option>
								<option value="non">Non Jabatan</option>
								<option value="aa">Asisten Ahli</option>
								<option value="l">Lektor</option>
								<option value="lk">Lektor Kepala</option>								
								<option value="gb">Guru Besar</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Chart-->
					<div id="chart_purnabakti" class="d-flex justify-content-center"></div>
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title"  style="width:100%">
						<div class="col-md-7"><h3 class="card-label">Lama menjabat Jabatan tertentu</h3></div>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<select class="form-control" name="jabatan" id="sel-jabatan">
								<option value="all">- Semua Jabatan -</option>
								<option value="non">Non Jabatan</option>
								<option value="aa">Asisten Ahli</option>
								<option value="l">Lektor</option>
								<option value="lk">Lektor Kepala</option>								
								<option value="gb">Guru Besar</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_lama_menjabat" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
