<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>
	var HOST_URL = "<?php echo site_url('dashboard/sk');?>";
	var HOST_DATA_URL = "<?php echo site_url('data/sk/nominatif');?>";
	var thnAktif = "<?php echo $submenu2;?>";
</script>
<script src="<?=base_url('assets/js/dashpages/nominatif_sk_charts.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-lg-4">
			<!--begin::Card-->
			<div class="card card-custom gutter-b" style="min-height:380px;">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">SK</h3>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<!-- <span>Jumlah Total SK: <a href="<?=base_url('data/sk')?>"> 47</a></span> -->
							<span>Jumlah Total SK: <?=$summary['total']?></span>
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Pegawai: <?=bcdiv($summaryBelanja['total_belanja_pegawai'],1000000000,2)?> M</span>
							<div class="progress">
								<div class="progress-bar bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
							<span>Jasa: <?=bcdiv($summaryBelanja['total_belanja_jasa'],1000000000,2)?></span>
							<div class="progress">
								<div class="progress-bar bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
						<span>Dosen: <?=bcdiv($summaryBelanja['total_belanja_dosen'],1000000000,2)?> M</span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						</div>
						<div class="col-md-6" style="padding-top: 20px;">
						<span>Tendik: <?=bcdiv($summaryBelanja['total_belanja_tendik'],1000000000,2)?> M</span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						</div>
						<!-- <div class="col-md-6" style="padding-top: 20px;">
						<span>Mhs: -</span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						</div> -->
						<div class="col-md-6" style="padding-top: 20px;">
						<span>Lainnya: <?=bcdiv($summaryBelanja['total_belanja_lainnya'],1000000000,2)?> M</span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title" style="width:100%">
						<div class="col-md-6"><h3 class="card-label">Status Pembayaran</h3></div>
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<select class="form-control" name="kk" id="sel-bulan">
								<option value="all">- 1 Tahun -</option>
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>								
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_pembayaran" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Jenis Belanja (BKK)</h3>
					</div>
				</div>
				<div class="card-body">
				<!--begin::Chart-->
					<div id="chart_jenis" class="d-flex justify-content-center"></div>
				<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-6">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Kategori Penerima (BKK)</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_penerima" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
