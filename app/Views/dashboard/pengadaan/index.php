<?php $this->extend('layout/template') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
<script>
	var HOST_URL = "<?php echo site_url('dashboard/pengadaan');?>";
	var HOST_DATA_URL = "<?php echo site_url('data/pengadaan');?>";
	var thn_aktif = <?=$thn_aktif?>;
</script>

<script src="<?=base_url('assets/js/dashpages/pengadaan_charts.js')?>"></script>
<?php $this->endSection() ?>

<?php $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-lg-4">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Status Pengadaan Unit</h3>
					</div>
				</div>
				<div class="card-body">
				<!--begin::Chart-->
					<div id="chart_status" class="d-flex justify-content-center"></div>	
					<!--end::Chart-->
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-4">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Status Pengadaan Logistik</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_status_itb" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-4">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Status Deadline Pengadaan Unit</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_deadline" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
		<!-- <div class="col-lg-4">
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Jenis Belanja (Juta)</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_jenis_belanja" class="d-flex justify-content-center"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Jenis Anggaran (Juta)</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_jenis" class="d-flex justify-content-center"></div>
				</div>
			</div>
		</div> -->
	</div>
	<div class="row">    
		<div class="col-lg-8">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title" style="width:100%">
						<div class="col-md-6"><h3 class="card-label">Pengadaan per Bulan</h3></div>
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<select class="form-control" name="diadakan" id="sel-diadakan">
								<option value="all">FTMD dan Logistik</option>
								<option value="FTMD">FTMD</option>
								<option value="Logistik">Logistik</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_pengadaan_bulanan" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
		<div class="col-lg-4" style="display:none" id="area-pengadaan-aktual">
			<!--begin::Card-->
			<div class="card card-custom gutter-b">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Status Aktual Pengadaan Unit</h3>
					</div>
				</div>
				<div class="card-body">
					<div id="chart_aktual" class="d-flex justify-content-center"></div>
				</div>
			</div>
			<!--end::Card-->
		</div>
	</div>
</div>
<?php $this->endSection() ?>
