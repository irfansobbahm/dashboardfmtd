'use strict';
var KTDatatablesDataSourceAjaxClient = function() {

	var initTable1 = function() {
		var table = $('#kt_datatable');

		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: HOST_URL,
				type: 'POST',
				data: {
					pagination: {
						perpage: 50,
					},
				},
			},
			columns: [
				{data: 'no_sk'},
				{data: 'judul_sk'},
				// {data: 'dekan'},
				{data: 'tgl_terbit'},
				{data: 'tgl_berakhir'},
				{data: null, responsivePriority: -1},
			],
			columnDefs: [
				{
					targets: -1,
					width: '70px',
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Ubah">\
								<i class="la la-edit text-primary"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Hapus">\
								<i class="la la-trash text-danger"></i>\
							</a>\
						';
					},
				},
			],
		});
	};

	var initTablePembayaran = function() {
		var table = $('#kt_datatable_pembayaran');

		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: HOST_URL,
				type: 'POST',
				data: {
					pagination: {
						perpage: 50,
					},
				},
			},
			columns: [
				// {data: 'no_sk'},
				// {data: 'judul_sk'},
				{data: 'no_spp'},
				{data: 'keterangan'},
				{data: 'jenis'},
				{data: 'tgl_pengajuan'},
				{data: 'tgl_pembayaran'},
				{data: 'total_nominatif'},
				// {data: 'status'},
				{data: null, responsivePriority: -1},
			],
			columnDefs: [
				{
					targets: 5,
					render: function(data, type, full, meta) {
						return parseInt(data).toLocaleString(); ;
					}
				},
				{
					targets: -1,
					width: '70px',
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Ubah">\
								<i class="la la-edit text-primary"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Hapus">\
								<i class="la la-trash text-danger"></i>\
							</a>\
						';
					},
				},
				// {
				// 	width: '75px',
				// 	targets: -2,
				// 	render: function(data, type, full, meta) {
				// 		var status = {
				// 			1: {'title': 'Baru', 'state': 'danger'},
				// 			2: {'title': 'Diajukan', 'state': 'warning'},
				// 			3: {'title': 'Disetujui Ditkeu', 'state': 'info'},
				// 			4: {'title': 'Ditransfer', 'state': 'success'},
				// 		};
				// 		if (typeof status[data] === 'undefined') {
				// 			return data;
				// 		}
				// 		return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
				// 			'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
				// 	},
				// },
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
			initTablePembayaran();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
});



// 'use strict';
// var KTDatatablesDataSourceAjaxClient = function() {

// 	var initTable1 = function() {
// 		var table = $('#kt_datatable');

// 		// begin first table
// 		table.DataTable({
// 			responsive: true,
// 			//searchDelay: 500,
// 			//processing: true,
// 			// serverSide: true,
// 			ajax: {
// 				url: HOST_URL, 
// 				type: 'POST',
// 				data: {
// 					// parameters for custom backend script demo
// 					pagination: {
// 						perpage: 50,
// 					},
// 					// columnsDef: [
// 					// 	'kegiatan', 'jenis_belanja', 'deskripsi', 'tgl_rencana_ri', 'tgl_rencana_mulai', 'tgl_rencana_selesai', 'total_rab', 
// 					// 	'diadakan', 'jenis_sumber_dana', 'status', 'status_pembayaran'
// 					// ],
// 				},
// 			},
// 			columns: [
// 				{data: 'kegiatan'},
// 				{data: 'jenis_belanja'},
// 				{data: 'deskripsi'},
// 				{data: 'tgl_rencana_ri'},
// 				{data: 'tgl_rencana_mulai'},
// 				{data: 'tgl_rencana_selesai'},
// 				{data: 'total_rab', className: 'text-right'},
// 				{data: 'diadakan'},
// 				{data: 'jenis_sumber_dana'},
// 				{data: 'status'},
// 				{data: 'status_pembayaran'},
// 				{data: null, responsivePriority: -1},
// 			],
// 			columnDefs: [
// 				{
// 					targets: -6,
// 					render: function(data, type, full, meta) {
// 						return parseInt(data).toLocaleString(); ;
// 					}
// 				},
// 				{
// 					targets: -1,
// 					title: 'Actions',
// 					orderable: false,
// 					render: function(data, type, full, meta) {
// 						return '\
// 							<div class="dropdown dropdown-inline">\
// 								<a href="javascript:;" class="btn btn-sm btn-clean" data-toggle="dropdown">\
// 	                                <i class="la la-cog"></i> Actions\
// 	                            </a>\
// 							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
// 									<ul class="nav nav-hoverable flex-column">\
// 							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>\
// 							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-leaf"></i><span class="nav-text">Update Status</span></a></li>\
// 							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-trash"></i><span class="nav-text">Hapus</span></a></li>\
// 									</ul>\
// 							  	</div>\
// 							</div>\
// 						';
// 					},
// 				},
// 				{
// 					width: '75px',
// 					targets: -3,
// 					render: function(data, type, full, meta) {
// 						var status = {
// 							0: {'title': 'Baru', 'class': 'label-light-primary'},
// 							1: {'title': 'Penawaran', 'class': ' label-light-primary'},
// 							2: {'title': 'Pengerjaan', 'class': ' label-light-danger'},
// 							3: {'title': 'Pengiriman', 'class': ' label-light-info'},
// 							4: {'title': 'Serah Terima', 'class': ' label-light-success'},
// 							5: {'title': 'Invoice', 'class': ' label-light-warning'},
// 							6: {'title': 'Pembayaran', 'class': ' label-light-info'},
// 							7: {'title': 'Selesai', 'class': ' label-light-success'},
// 						};
// 						if (typeof status[data] === 'undefined') {
// 							return data;
// 						}
// 						return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
// 					},
// 				},
// 				{
// 					width: '75px',
// 					targets: -2,
// 					render: function(data, type, full, meta) {
// 						var status = {
// 							0: {'title': 'Belum', 'state': 'danger'},
// 							1: {'title': 'Proses', 'state': 'warning'},
// 							2: {'title': 'Keuangan', 'state': 'info'},
// 							3: {'title': 'Selesai', 'state': 'success'},
// 						};
// 						if (typeof status[data] === 'undefined') {
// 							return data;
// 						}
// 						return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
// 					},
// 				},
// 			],
// 		});
// 	};

// 	return {

// 		//main function to initiate the module
// 		init: function() {
// 			initTable1();
// 		},

// 	};

// }();

// jQuery(document).ready(function() {
// 	KTDatatablesDataSourceAjaxClient.init();
// });
