'use strict';
var KTDatatablesDataSourceAjaxClient = function () {

	var initTable1 = function () {
		var table = $('#kt_datatable');
	
		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: HOST_URL,
				type: 'POST',
				data: {
					pagination: {
						perpage: 50,
					},
				},
			},
			columns: [{
					data: 'nama'
				},
				{
					data: 'jabatan_usulan'
				},
				{
					data: 'ak_syarat'
				},
				{
					data: 'ak_pendidikan'
				},
				{
					data: 'ak_pendidikan_syarat'
				},
				{
					data: 'ak_penelitian'
				},
				{
					data: 'ak_penelitian_syarat'
				},
				{
					data: 'ak_pengabdian'
				},
				{
					data: 'ak_pengabdian_syarat'
				},
				{
					data: 'ak_pengembangan'
				},
				{
					data: 'ak_pengembangan_syarat'
				},
				{
					data: 'syarat_ak'
				},
				// {data: 'syarat_umum'},
				{
					data: 'status_angka_kredit'
				},
				// {
				// 	data: 'usia'
				// },
				// {
				// 	data: 'masa_purna'
				// },
				{
					data: null,
					responsivePriority: -1
				},
			],
			columnDefs: [{
				targets: 3,
				render: function (data, type, row) {
						
						if (parseInt(data) < parseInt(row['ak_pendidikan_syarat']))
							return '<span class="font-weight-bold text-danger">' + data + '</span>';
						else
							return data;
					},
				},
				{
					targets: 5,
					render: function (data, type, row) {
						if (parseInt(data) < parseInt(row['ak_penelitian_syarat']))
							return '<span class="font-weight-bold text-danger">' + data + '</span>';
						else
							return data;
					},
				},
				{
					targets: -1,
					width: '70px',
					title: 'Actions',
					orderable: false,
					render: function (data, type, full, meta) {
						return '\
							<a href="/data/kepegawaian/edit/'+data.nip+'" class="btn btn-sm btn-clean btn-icon" title="Ubah">\
								<i class="la la-edit text-primary"></i>\
								</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon rowremove" id="'+data.nip+'" title="Hapus">\
								<i class="la la-trash text-danger"></i>\
							</a>\
						';
					},
				},
				{
					width: '75px',
					targets: -2,
					render: function (data, type, full, meta) {
						var status = {
							0: {
								'title': 'Belum Memenuhi',
								'state': 'danger'
							},
							1: {
								'title': 'Admin KK',
								'state': 'warning'
							},
							2: {
								'title': 'Kepegawaian FTMD',
								'state': 'warning'
							},
							3: {
								'title': 'TPAKK F/S',
								'state': 'primary'
							},
							4: {
								'title': 'Senat F/S',
								'state': 'primary'
							},
							5: {
								'title': 'TPAK ITB',
								'state': 'info'
							},
							6: {
								'title': 'Senat ITB',
								'state': 'info'
							},
							7: {
								'title': 'Kepegawaian Pusat',
								'state': 'info'
							},
							8: {
								'title': 'DIKTI',
								'state': 'success'
							},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
							'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
				{
					targets: -3,
					render: function (data, type, row) {
						if (data == 'Tidak')
							return '<span class="font-weight-bold text-danger">' + data + '</span>';
						else
							return data;
					},
				},
			],
		});

		$('#kt_datatable tbody').on('click', 'a.rowremove', function () {
                
			var id = $(this).attr("id");        	
			// console.log(id)
			if (confirm( "Apakah anda yakin hendak menghapusnya?")) {
				deleteRow(+id);
				table.ajax.reload();
			 }    
		});

		new DataTable('#kt_datatable', {
			fixedColumns: {
				left: 2
			},
			paging: false,
			scrollCollapse: true,
			scrollY: '300px',
			scrollX: true
		});
	};

	return {

		//main function to initiate the module
		init: function () {
			initTable1();
		},

	};

}();



function deleteRow($id) {

	$.ajax( {
		  url     : "/data/kepegawaian/delete",
		  data    : { "id": $id },
		  dataType: "json",
		  type    : "post",			
		  
	}) 		 
}

jQuery(document).ready(function () {
	KTDatatablesDataSourceAjaxClient.init();
});