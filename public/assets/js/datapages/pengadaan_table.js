"use strict";
var KTDatatablesDataSourceAjaxClient = function() {

	var initTable1 = function() {
	
		var table =  $('#kt_datatable').DataTable({
			responsive: true,			
			order: [[0, "desc"]],
			ajax: {
				url: HOST_URL, 
				type: 'POST',
				data: {					
					pagination: {
						perpage: 50,
					},					
				},
			},
			columns: [
			    
			    // {data: 'id_pengadaan', visible:false },
				// {data: 'nama_kegiatan'},
				{data: 'alokasi'},
				{data: 'deskripsi'},
				{data: 'sispran'},
				{data: 'tgl_ri'},
				{data: 'rencana_tgl_mulai'},
				{data: 'rencana_tgl_selesai'},
				{data: 'nominal_rab', className: 'text-right'},
				{data: 'diadakan'},
				{data: 'sumber_dana'},
				{data: 'status'},
				{data: 'status_aktual'},
				{data: 'tgl_pembayaran'},
				{data: null, responsivePriority: -1},
			],
			columnDefs: [
				{
					targets: 6,
					render: function(data, type, full, meta) {
						return parseInt(data).toLocaleString(); ;
					}
				},
				{
					width: '70px',
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
							<a href="/data/pengadaan/edit/'+data.id_pengadaan+'" class="btn btn-sm btn-clean btn-icon" title="Ubah">\
								<i class="la la-edit text-primary"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon rowremove" id="'+data.id_pengadaan+'" title="Hapus">\
								<i class="la la-trash text-danger"></i>\
							</a>\
						';
					},
				},
				{
					width: '75px',
					targets: -4,
					render: function(data, type, full, meta) {
						var status = {
							// 0: {'title': 'Baru', 'class': 'label-light-primary'},
							// 1: {'title': 'Penawaran', 'class': ' label-light-primary'},
							// 2: {'title': 'Penawaran', 'class': ' label-light-primary'},
							// 3: {'title': 'SPK', 'class': ' label-light-danger'},
							// 4: {'title': 'Serah Terima', 'class': ' label-light-success'},
							// 5: {'title': 'Invoice', 'class': ' label-light-warning'},
							// 6: {'title': 'Selesai', 'class': ' label-light-success'},
							0: {'title': 'Pemaketan', 'class': 'label-light-primary'},
							1: {'title': 'Kontrak', 'class': ' label-light-primary'},
							2: {'title': 'Invoice', 'class': ' label-light-warning'},
							3: {'title': 'Selesai', 'class': ' label-light-success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
					},
				},
				{
					width: '75px',
					targets: -2,
					render: function(data, type, full, meta) {
						var status = {
							0: {'title': 'Belum', 'state': 'danger'},
							1: {'title': 'Proses', 'state': 'warning'},
							2: {'title': 'Ditkeu', 'state': 'info'},
							3: {'title': 'Selesai', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
							'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		}); 


	
		$('#kt_datatable tbody').on('click', 'a.rowremove', function () {
                
        	var id = $(this).attr("id");        	
        	
        	if (confirm( "Apakah anda yakin hendak menghapusnya?")) {
        		deleteRow(+id);
        		table.ajax.reload();
 		    }    
    	});


	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};


	function deleteRow($id) {

  		$.ajax( {
  				url     : "/data/pengadaan/delete",
				data    : { "id": $id },
				dataType: "json",
				type    : "post",			
				
  		}) 		 
 	}

	
}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
});

