// Class definition
var KTSelect2 = function() {
  // Private functions
  var forms_select2 = function() {
      // basic
      $('#kt_select2_jenis, #kt_select2_jenis_validate').select2({
          placeholder: '- Pilih Jenis -'
      });

      $('#kt_select2_diadakan, #kt_select2_diadakan_validate').select2({
          placeholder: '- Pilih Jenis -'
      });

      $('#kt_select2_bayar, #kt_select2_bayar_validate').select2({
          placeholder: '- Status Pembayaran -'
      });

      $('#kt_select2_pemohon, #kt_select2_pemohon_validate').select2({
          placeholder: '- Nama Pemohon -'
      });
  }

  // Public functions
  return {
      init: function() {
        forms_select2();
      }
  };
}();

// Class definition

var KTBootstrapDatepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {
        // minimum setup
        $('#kt_datepicker_ri, #kt_datepicker_ri_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_mulai, #kt_datepicker_mulai_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_akhir, #kt_datepicker_akhir_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        //datepicker jenis dokumen

        $('#kt_datepicker_ba, #kt_datepicker_ba_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_spp, #kt_datepicker_spp_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_invoice, #kt_datepicker_invoice_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_kuitansi, #kt_datepicker_kuitansi_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_faktur, #kt_datepicker_faktur_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

         $('#kt_datepicker_npwp, #kt_datepicker_npwp_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_pkp, #kt_datepicker_pkp_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_bank, #kt_datepicker_bank_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_bast, #kt_datepicker_bast_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_jalan, #kt_datepicker_jalan_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_pesanan, #kt_datepicker_pesanan_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_po, #kt_datepicker_po_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_harga, #kt_datepicker_harga_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_kak, #kt_datepicker_kak_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_ro, #kt_datepicker_ro_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_permintaan, #kt_datepicker_permintaan_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_bukti, #kt_datepicker_bukti_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_jasa, #kt_datepicker_jasa_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
  KTSelect2.init();
  KTBootstrapDatepicker.init();
});
