// Class definition
var KTSelect2 = function() {
    // Private functions
    var forms_select2 = function() {
        $('.kt_select2, .kt_select2_validate').select2({
            placeholder: '- Pilih -'
        });
    }

    // Public functions
    return {
        init: function() {
            forms_select2();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTSelect2.init();
});