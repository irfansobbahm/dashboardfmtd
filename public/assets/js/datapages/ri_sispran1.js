"use strict";
var KTDatatablesDataSourceAjaxServer = function() {

	var initTable1 = function() {
	
		//var table = $('#kt_datatable');

		// begin first table
		var table =  $('#kt_datatable').DataTable({
			responsive: true,
			//searchDelay: 5,
			//processing: true,
			//serverSide: true,
			order: [[0, "desc"]],
			ajax: {
				url: HOST_URL, 
				type: 'POST',
				data: {
					// parameters for custom backend script demo
					columnsDef: [
					 //    'id_anggaran',
						// 'id_const',
						// 'kakek_nama',
						'parent_nama',
						'nama',
						// 'tgl_pengajuan',
						'bulan', 
						'thn_anggaran',
						'nominal',
						'keterangan',
					],
				},
			},
			columns: [
			 //    {data: 'id_anggaran'},
				// {data: 'id_const'},
				// {data: 'kakek_nama'},
				{data: 'parent_nama'},
				{data: 'nama'},
				// {data: 'tgl_pengajuan'},
				{data: 'bulan'},
				{data: 'thn_anggaran'},
				{data: 'nominal', className: 'text-right'},
				{data: 'keterangan'},
				{data: null, responsivePriority: -1},
			],
			columnDefs: [
				{
					targets: 0,	
					//visible: false,					
				},
				{
					targets: -5,
					render: function(data, type, full, meta) {
						var bln = {'01': 'Januari', '02': 'Februari', '03': 'Maret', '04': 'April', '05': 'Mei', '06': 'Juni', 
									'07': 'Juli', '08': 'Agustus', '09': 'September', '10': 'Oktober', '11': 'November', '12': 'Desember'};

						return bln[data];
					}
				},
				{
					targets: -3,
					render: function(data, type, full, meta) {
						return parseInt(data).toLocaleString(); ;
					}
				},
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
						<div class="dropdown dropdown-inline"  >\
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" id="'+data.id_anggaran+'">\
	                                <i class="la la-cog"></i>\
	                            </a>\
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
									<ul class="nav nav-hoverable flex-column test">\
							    		<li class="nav-item"><a class="nav-link" href="/data/anggaran/ri_edit_sispran1/'+data.id_anggaran+'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>\
							    		<li class="nav-item"><a class="nav-link hapus" href="" id="'+data.id_anggaran+'"><i class="nav-icon la la-trash"></i><span class="nav-text">Delete</span></a></li>\
									</ul>\
							  	</div>\
							</div>\
							<!--<a href="/data/anggaran/ri_edit_sispran1/'+data.id_anggaran+'" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon rowremove" id="'+data.id_anggaran+'" title="Delete" >\
								<i class="la la-trash"></i>\
							</a>-->\
						';
					},
				},			
			],
		}); //end table



		$('#kt_datatable').on('click', '.dropdown', function () {
                        
            $('a.hapus').on('click',  function () {

                var id = $(this).attr("id");

                if (confirm( "Apakah anda yakin hendak menghapusnya?")) {
        			
        			deleteRow(+id);
 		    	
 		    	}   
                 
            });      	

    	});


	
		/*$('#kt_datatable tbody').on('click', 'a.rowremove', function () {
                
        	var id = $(this).attr("id");        	
        	
        	if (confirm( "Apakah anda yakin hendak menghapusnya?")) {
        		deleteRow(+id);
        		table.ajax.reload();
 		    }    
    	});*/


	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};


	function deleteRow($id) {

  		$.ajax( {
  				url     : "/data/anggaran/ri_delete_sispran1",
				data    : { "id": $id },
				dataType: "json",
				type    : "post",			
				
  		}) 		 
 	}

	
}();


	
jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});


/*source for delete
https://editor.datatables.net/examples/simple/inTableControls.html
https://webdamn.com/datatables-add-edit-delete-with-ajax-php-mysql/
https://datatables.net/examples/api/select_single_row.html
https://www.nuomiphp.com/eplan/en/70263.html
*/