"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

var KTApexChartsDemo = function () {
	// Private functions
	var _ri_all = function () {
		const apexChart = "#chart_ri_all2";
		var options = {
			series: [0.545, 0.365, 0.044],
			chart: {
				height: 300,
				type: 'radialBar',
			},
			plotOptions: {
				radialBar: {
					dataLabels: {
						name: {
							fontSize: '22px',
						},
						value: {
							fontSize: '16px',
						},
						total: {
							show: true,
							label: 'Total RKA',
							formatter: function (w) {
								// By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
								return '9 M'
							}
						}
					},
				}
			},
			labels: ['RI', 'FRA', 'Realisasi'],
			colors: [primary, info, success],
			legend: {
				position: 'bottom',
				show: true,
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _fra_all = function () {
		const apexChart = "#chart_fra_all";
		var options = {
			series: [3, 6],
			chart: {
				//width: 380,
				type: 'pie',
			},
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200
					},
					legend: {
						position: 'top'
					}
				}
			}],
			colors: [primary, info]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _realisasi_all = function () {
		const apexChart = "#chart_realisasi_all";
		var options = {
			series: [4, 5],
			chart: {
				//width: 380,
				type: 'pie',
			},
			// labels: ['RKA', 'Prospektif'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200
					},
					legend: {
						position: 'top'
					}
				}
			}],
			colors: [primary, info]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _frabulan = function () {
		const apexChart = "#chart_frabulan";
		var options = {
			series: [{
				name: 'FRA',
				data: [0.36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Realisasi',
				data: [0.04, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 300
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			yaxis: {
				title: {
					text: 'Rp (milliar)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " milyar"
					}
				}
			},
			colors: [info, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _alokasi_pegawai = function () {
		const apexChart = "#chart_alokasi_pegawai2";
		var options = {
			series: [{
				data: [5.669, 0.013, 0.004, 0]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [warning, primary, info, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true,
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				}
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			yaxis: {
				title: {
					text: 'Rp (milyar)'
				}
			},
			xaxis: {
				categories: [
					'RKA', 'RI', 'FRA', 'Realisasi'
				],
				labels: {
					style: {
						colors: [warning, primary, info, success],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _alokasi_barang = function () {
		const apexChart = "#chart_alokasi_barang";
		var options = {
			series: [{
				data: [2.289, 0.113, 0.062, 0]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [warning, primary, info, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true,
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				}
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			yaxis: {
				title: {
					text: 'Rp (milyar)'
				}
			},
			xaxis: {
				categories: [
					'RKA', 'RI', 'FRA', 'Realisasi'
				],
				labels: {
					style: {
						colors: [warning, primary, info, success],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _alokasi_jasa = function () {
		const apexChart = "#chart_alokasi_jasa";
		var options = {
			series: [{
				data: [9.453, 0.418, 0.297, 0.044]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [warning, primary, info, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true,
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				}
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			yaxis: {
				title: {
					text: 'Rp (milyar)'
				}
			},
			xaxis: {
				categories: [
					'RKA', 'RI', 'FRA', 'Realisasi'
				],
				labels: {
					style: {
						colors: [warning, primary, info, success],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _alokasi_modal = function () {
		const apexChart = "#chart_alokasi_modal";
		var options = {
			series: [{
				data: [9.392, 0, 0, 0]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [warning, primary, info, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true,
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				}
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			yaxis: {
				title: {
					text: 'Rp (milyar)'
				}
			},
			xaxis: {
				categories: [
					'RKA', 'RI', 'FRA', 'Realisasi'
				],
				labels: {
					style: {
						colors: [warning, primary, info, success],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	return {
		// public functions
		init: function () {
			_ri_all();
			_fra_all();
			_realisasi_all();
			_frabulan();
			_alokasi_pegawai();
			_alokasi_barang();
			_alokasi_jasa();
			_alokasi_modal();
		}
	};
}();

// jQuery(document).ready(function () {
// 	KTApexChartsDemo.init();
// });

jQuery(document).ready(function () {
	// KTApexChartsDemo.init();

	// begin penyerapan persen
	var optionsPenyerapanPersen = {
		series: [],
		noData: {
	    text: 'Loading...'
	  }, 
		chart: {
			height: 300,
			type: 'radialBar',
		},
		plotOptions: {
			radialBar: {
				dataLabels: {
					name: {
						fontSize: '22px',
					},
					value: {
						fontSize: '16px',
					},
					total: {
						show: true,
						label: 'Total RKA',
						formatter: function (w) {
							// By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
							return totalRka + ' M';
						}
					}
				},
			}
		},
		labels: ['RI', 'Realisasi'],
		colors: [primary, info, success],
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartPenyerapanPersen = new ApexCharts(document.querySelector("#chart_ri_all"), optionsPenyerapanPersen);
	chartPenyerapanPersen.render();

	var urlPenyerapanPersen = HOST_URL+'/api_penyerapan_persen_s1/00';
	$.getJSON(urlPenyerapanPersen, function(response) {
		chartPenyerapanPersen.updateSeries(response.data);

		// $.each(response.data, function (index, element) {
		// 	var arr = chartRiAll.w.globals.series.slice();
  	//    arr.push(element);			
		// });
	});
	// end penyerapan persen

	// begin penyerapan
	var optionsPenyerapan = {
		series: [{
			name: 'Realisasi',
			data: []
			// data: [0.04, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		}],
		noData: {
	    text: 'Loading...'
	  }, 
		chart: {
			type: 'bar',
			height: 320,
			events: {
        dataPointSelection: function(event, chartContext, opts) {
        	// var jenis = (opts.w.config.series[opts.seriesIndex].name.toLowerCase() == 'fra') ? '03' : '04';
			var jenis = 'REALISASI';

        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
              case 'jan':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=1', '_blank');break;
              case 'feb':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=2', '_blank');break;
              case 'mar':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=3', '_blank');break;
              case 'apr':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=4', '_blank');break;
              case 'mei':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=5', '_blank');break;
              case 'jun':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=6', '_blank');break;
              case 'jul':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=7', '_blank');break;
              case 'ags':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=8', '_blank');break;
              case 'sep':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=9', '_blank');break;
              case 'okt':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=10', '_blank');break;
              case 'nov':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=11', '_blank');break;
              case 'des':
                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&ak=12', '_blank');break;
          }
        }
      }
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			},
		},
		dataLabels: {
			enabled: true,
			offsetY: -20,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val;
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
		},
		yaxis: {
			title: {
				text: 'Rp (milyar)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val + " milyar"
				}
			}
		},
		colors: [info, success]
	};

	var chartPenyerapan = new ApexCharts(document.querySelector('#chart_frabulan'), optionsPenyerapan);
	chartPenyerapan.render();

	var urlPenyerapan = HOST_URL+'/api_penyerapan/01';
	$.getJSON(urlPenyerapan, function(response) {
		chartPenyerapan.updateSeries([
		  {
		    name: 'Realisasi',
		    data: response.data.realisasi
		  },
	  ]);
	});
	// end penyerapan

	// begin alokasi pegawai
	var optionsAlokasiPegawai = {
		series: [{
			name: 'Unit',
			data: []
		}, {
			name: 'Logistik',
			data: []
		}],
		noData: {
			text: 'Loading...'
		},  
		chart: {
			height: 250,
			type: 'bar',
			stacked: true,
			events: {
				dataPointSelection: function(event, chartContext, opts) {
        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	        //   case 'rka':
	        //   		window.open(HOST_DATA_URL+'?jns=RKA&alk=PEGAWAI', '_blank'); break;
	          case 'ri':
	              window.open(HOST_DATA_URL+'?jns=RI&alk=PEGAWAI', '_blank'); break;
	          // case 'fra':
	          //     window.open(HOST_DATA_URL+'?jns=FRA&alk=PEGAWAI', '_blank'); break;
	          case 'realisasi':
	              window.open(HOST_DATA_URL+'?jns=REALISASI&alk=PEGAWAI', '_blank'); break;
          }
        }
			}
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			},
		},
		dataLabels: {
			enabled: false,
			offsetY: -20,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val;
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			// categories: ['RKA', 'RI', 'FRA', 'Realisasi'],
			categories: ['RI', 'Realisasi'],
		},
		yaxis: {
			title: {
				text: 'Rp (milyar)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val + " milyar"
				}
			}
		},
		colors: [warning, success]
	};
	
	var chartAlokasiPegawai = new ApexCharts(document.querySelector("#chart_alokasi_pegawai"), optionsAlokasiPegawai);
	chartAlokasiPegawai.render();

	var urlAlokasiPegawai = HOST_URL+'/api_alokasi_pegawai/01';
	// $.getJSON(urlAlokasiPegawai, function(response) {
		// 	chartAlokasiBarang.updateSeries([{data: response.data}]);
		// });
	
	$.getJSON(urlAlokasiPegawai, function(response) {
		console.log(response.data)
		chartAlokasiPegawai.updateSeries([
			{
				name: 'Unit',
				data: response.data.unit
			},
			{
				name: 'Logistik',
				data: response.data.logistik
			},
	  	]);
	});

	// begin alokasi barang
	var optionsAlokasiBarang = {
		series: [{
			name: 'Unit',
			data: []
		}, {
			name: 'Logistik',
			data: []
		}],
		noData: {
			text: 'Loading...'
		},
		chart: {
			height: 250,
			type: 'bar',
			stacked: true,
			events: {
				dataPointSelection: function(event, chartContext, opts) {
        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	          case 'rka':
	          		window.open(HOST_DATA_URL+'?jns=RKA&alk=BARANG', '_blank'); break;
	          case 'ri':
	             	window.open(HOST_DATA_URL+'?jns=RI&alk=BARANG', '_blank'); break;
	          // case 'fra':
	          //     window.open(HOST_DATA_URL+'?jns=FRA&alk=BARANG', '_blank'); break;
	          case 'realisasi':
	              	window.open(HOST_DATA_URL+'?jns=REALISASI&alk=BARANG', '_blank'); break;
          }
        }
			}
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			},
		},
		dataLabels: {
			enabled: false,
			offsetY: -20,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val;
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			// categories: ['RKA', 'RI', 'FRA', 'Realisasi'],
			categories: ['RI', 'Realisasi'],
		},
		yaxis: {
			title: {
				text: 'Rp (milyar)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val + " milyar"
				}
			}
		},
		colors: [warning, success]
	};

	var chartAlokasiBarang = new ApexCharts(document.querySelector("#chart_alokasi_barang"), optionsAlokasiBarang);
	chartAlokasiBarang.render();

	var urlAlokasiBarang = HOST_URL+'/api_alokasi_barang/01';
	// $.getJSON(urlAlokasiBarang, function(response) {
	// 	chartAlokasiBarang.updateSeries([{data: response.data}]);
	// });

	$.getJSON(urlAlokasiBarang, function(response) {
		// console.log(response.data)
		chartAlokasiBarang.updateSeries([
			{
				name: 'Unit',
				data: response.data.unit
			},
			{
				name: 'Logistik',
				data: response.data.logistik
			},
	  	]);
	});
	// end alokasi barang


	// begin alokasi jasa
	var optionsAlokasiJasa = {
		series: [{
			name: 'Unit',
			data: []
		}, {
			name: 'Logistik',
			data: []
		}],
		noData: {
			text: 'Loading...'
		},
		chart: {
			height: 250,
			type: 'bar',
			stacked: true,
			events: {
				dataPointSelection: function(event, chartContext, opts) {
        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	          case 'rka':
	          		window.open(HOST_DATA_URL+'?jns=RKA&alk=JASA', '_blank'); break;
	          case 'ri':
	              window.open(HOST_DATA_URL+'?jns=RI&alk=JASA', '_blank'); break;
	          // case 'fra':
	          //     window.open(HOST_DATA_URL+'?jns=FRA&alk=JASA', '_blank'); break;
	          case 'realisasi':
	              window.open(HOST_DATA_URL+'?jns=REALISASI&alk=JASA', '_blank'); break;
          }
        }
			}
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			},
		},
		dataLabels: {
			enabled: false,
			offsetY: -20,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val;
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			// categories: ['RKA', 'RI', 'FRA', 'Realisasi'],
			categories: ['RI', 'Realisasi'],
		},
		yaxis: {
			title: {
				text: 'Rp (milyar)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val + " milyar"
				}
			}
		},
		colors: [warning, success]
	};

	var chartAlokasiJasa = new ApexCharts(document.querySelector("#chart_alokasi_jasa"), optionsAlokasiJasa);
	chartAlokasiJasa.render();

	var urlAlokasiJasa = HOST_URL+'/api_alokasi_jasa/01';
	// $.getJSON(urlAlokasiJasa, function(response) {
	// 	chartAlokasiJasa.updateSeries([{data: response.data}]);
	// });
	$.getJSON(urlAlokasiJasa, function(response) {
		// console.log(response.data)
		chartAlokasiJasa.updateSeries([
			{
				name: 'Unit',
				data: response.data.unit
			},
			{
				name: 'Logistik',
				data: response.data.logistik
			},
	  	]);
	});
	// end alokasi jasa

	// begin alokasi modal
	var optionsAlokasiModal = {
		series: [{
			name: 'Unit',
			data: []
		}, {
			name: 'Logistik',
			data: []
		}],
		noData: {
			text: 'Loading...'
		}, 
		chart: {
			type: 'bar',
			stacked: true, 
			height: 250,
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					console.log(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()+'--'+opts.w.config.series[opts.seriesIndex].name.toLowerCase())
					switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
						case 'rka':
								window.open(HOST_DATA_URL+'?jns=RKA&alk=MODAL', '_blank');break;
						case 'ri':
							window.open(HOST_DATA_URL+'?jns=RI&alk=MODAL', '_blank');break;
						// case 'fra':
						// 	window.open(HOST_DATA_URL+'?jns=FRA&alk=MODAL', '_blank');break;
						case 'realisasi':
							window.open(HOST_DATA_URL+'?jns=REALISASI&alk=MODAL', '_blank');break;
					}
				}
			}
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			},
		},
		dataLabels: {
			enabled: false,
			offsetY: -20,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val;
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			// categories: ['RKA', 'RI', 'FRA', 'Realisasi'],
			categories: ['RI', 'Realisasi'],
		},
		yaxis: {
			title: {
				text: 'Rp (milyar)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val + " milyar"
				}
			}
		},
		colors: [warning, success]
	};

	var chartAlokasiModal = new ApexCharts(document.querySelector("#chart_alokasi_modal"), optionsAlokasiModal);
	chartAlokasiModal.render();

	var urlAlokasiModal = HOST_URL+'/api_alokasi_modal/01';
	// $.getJSON(urlAlokasiModal, function(response) {
	// 	chartAlokasiModal.updateSeries([{data: response.data}]);
	// });
	$.getJSON(urlAlokasiModal, function(response) {
		// console.log(response.data)
		chartAlokasiModal.updateSeries([
			{
				name: 'Unit',
				data: response.data.unit
			},
			{
				name: 'Logistik',
				data: response.data.logistik
			},
	  	]);
	});
	// end alokasi modal

	$('#sel-penyerapan').on('change', function() {
		var jns = $(this).val();
		if (jns == 'bulan') {
			var urlPenyerapan = HOST_URL+'/api_penyerapan_perbulan/01';
			$.getJSON(urlPenyerapan, function(response) {
				chartPenyerapan.updateSeries([
				  {
				    name: 'Realisasi',
				    data: response.data.realisasi
				  },
			  ]);
			});
		} else {
			var urlPenyerapan = HOST_URL+'/api_penyerapan/01';
			$.getJSON(urlPenyerapan, function(response) {
				chartPenyerapan.updateSeries([
				  {
				    name: 'Realisasi',
				    data: response.data.realisasi
				  },
			  ]);
			});
		}
	});

});