"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

// var KTApexChartsDemo = function () {
// 	// Private functions
// 	// var _jabatan = function () {
// 	// 	const apexChart = "#chart_jabatan";
// 	// 	var options = {
// 	// 		series: [{
// 	// 			data: [11, 20, 43, 12, 16]
// 	// 		}],
// 	// 		chart: {
// 	// 			height: 250,
// 	// 			type: 'bar',
// 	// 			events: {
// 	// 				click: function (chart, w, e) {
// 	// 					// console.log(chart, w, e)
// 	// 				}
// 	// 			}
// 	// 		},
// 	// 		colors: [success, primary, info, warning, danger],
// 	// 		plotOptions: {
// 	// 			bar: {
// 	// 				columnWidth: '55%',
// 	// 				distributed: true,
// 	// 				dataLabels: {
// 	// 					position: 'top',
// 	// 				},
// 	// 			}
// 	// 		},
// 	// 		dataLabels: {
// 	// 			enabled: true
// 	// 		},
// 	// 		stroke: {
// 	// 			show: true,
// 	// 			width: 2,
// 	// 			colors: ['transparent']
// 	// 		},
// 	// 		legend: {
// 	// 			show: false
// 	// 		},
// 	// 		yaxis: {
// 	// 			title: {
// 	// 				text: 'dosen'
// 	// 			}
// 	// 		},
// 	// 		xaxis: {
// 	// 			categories: [
// 	// 				'GB', 'LK', 'L', 'AA', 'Non'
// 	// 			],
// 	// 			labels: {
// 	// 				style: {
// 	// 					colors: [success, primary, info, warning, danger],
// 	// 					fontSize: '12px'
// 	// 				}
// 	// 			}
// 	// 		}
// 	// 	};

// 	// 	var chart = new ApexCharts(document.querySelector(apexChart), options);
// 	// 	chart.render();
// 	// }

// 	// var _jabatan_persen = function () {
// 	// 	const apexChart = "#chart_jabatan_persen";
// 	// 	var options = {
// 	// 		series: [11, 20, 43, 12, 16],
// 	// 		chart: {
// 	// 			width: 380,
// 	// 			type: 'pie',
// 	// 			events: {
// 	// 				legendClick: function (chart, w, e) {
// 	// 					console.log(chart);
// 	// 				}
// 	// 			}
// 	// 		},
// 	// 		labels: ['GB', 'LK', 'L', 'AA', 'Non'],
// 	// 		responsive: [{
// 	// 			breakpoint: 480,
// 	// 			options: {
// 	// 				chart: {
// 	// 					width: 200,
// 	// 				},
// 	// 				legend: {
// 	// 					position: 'bottom',
// 	// 				}
// 	// 			}
// 	// 		}],
// 	// 		colors: [success, primary, info, warning, danger]
// 	// 	};

// 	// 	var chart = new ApexCharts(document.querySelector(apexChart), options);
// 	// 	chart.render();
// 	// }

// 	var _pemenuhan_ak = function () {
// 		const apexChart = "#chart_pemenuhan_ak";
// 		var options = {
// 			series: [{
// 				data: [91, 0, 0, 0]
// 			}],
// 			chart: {
// 				height: 250,
// 				type: 'bar',
// 				events: {
// 					click: function (chart, w, e) {
// 						// console.log(chart, w, e)
// 					}
// 				}
// 			},
// 			colors: [danger, warning, info, success],
// 			plotOptions: {
// 				bar: {
// 					columnWidth: '55%',
// 					distributed: true,
// 					dataLabels: {
// 						position: 'top', // top, center, bottom
// 					},
// 				}
// 			},
// 			dataLabels: {
// 				enabled: true
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			legend: {
// 				show: false
// 			},
// 			yaxis: {
// 				title: {
// 					text: 'dosen'
// 				}
// 			},
// 			xaxis: {
// 				categories: [
// 					'Belum', 'Memenuhi', 'Diproses FTMD', 'Diproses ITB', 'Diproses Dikti'
// 				],
// 				labels: {
// 					style: {
// 						colors: [danger, warning, primary, info, success],
// 						fontSize: '12px'
// 					}
// 				}
// 			}
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _purnabakti = function () {
// 		const apexChart = "#chart_purnabakti";
// 		var options = {
// 			series: [{
// 				data: [0, 0, 0, 0]
// 			}],
// 			chart: {
// 				height: 250,
// 				type: 'bar',
// 				events: {
// 					click: function (chart, w, e) {
// 						// console.log(chart, w, e)
// 					}
// 				}
// 			},
// 			colors: [danger, warning, info, success],
// 			plotOptions: {
// 				bar: {
// 					columnWidth: '55%',
// 					distributed: true,
// 					dataLabels: {
// 						position: 'top', // top, center, bottom
// 					},
// 				}
// 			},
// 			dataLabels: {
// 				enabled: true
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			legend: {
// 				show: false
// 			},
// 			yaxis: {
// 				title: {
// 					text: 'dosen'
// 				}
// 			},
// 			xaxis: {
// 				categories: [
// 					'< 2th', '2 - 4th', '4 - 6th', '> 6th'
// 				],
// 				labels: {
// 					style: {
// 						colors: [danger, warning, info, success],
// 						fontSize: '12px'
// 					}
// 				}
// 			}
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _lama_menjabat = function () {
// 		const apexChart = "#chart_lama_menjabat";
// 		var options = {
// 			series: [{
// 				data: [0, 0, 0, 0]
// 			}],
// 			chart: {
// 				height: 250,
// 				type: 'bar',
// 				events: {
// 					click: function (chart, w, e) {
// 						// console.log(chart, w, e)
// 					}
// 				}
// 			},
// 			colors: [success, info, warning, danger],
// 			plotOptions: {
// 				bar: {
// 					columnWidth: '55%',
// 					distributed: true,
// 					dataLabels: {
// 						position: 'top', // top, center, bottom
// 					},
// 				}
// 			},
// 			dataLabels: {
// 				enabled: true
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			legend: {
// 				show: false
// 			},
// 			yaxis: {
// 				title: {
// 					text: 'dosen'
// 				}
// 			},
// 			xaxis: {
// 				categories: [
// 					'< 1th', '1 - 3th', '3 - 5th', '> 5th'
// 				],
// 				labels: {
// 					style: {
// 						colors: [success, info, warning, danger],
// 						fontSize: '12px'
// 					}
// 				}
// 			}
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	return {
// 		// public functions
// 		init: function () {
// 			// _jabatan();
// 			// _jabatan_persen();
// 			// _pemenuhan_ak();
// 			// _purnabakti();
// 			// _lama_menjabat();
// 		}
// 	};
// }();

jQuery(document).ready(function () {
	// KTApexChartsDemo.init();

	var optionsFungsional = {
		series: [{
			data: []
		}],
		noData: {
			'title' : 'No data'
		},
		chart: {
			height: 250,
			type: 'bar',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					var kk = $('#sel-kk').val();
        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	          case 'gb':
	          		window.open(HOST_DATA_URL+'?jab=gb&kk='+kk, '_blank');break;
	          case 'lk':
	              window.open(HOST_DATA_URL+'?jab=lk&kk='+kk, '_blank');break;
	          case 'l':
	              window.open(HOST_DATA_URL+'?jab=l&kk='+kk, '_blank');break;
	          case 'aa':
	              window.open(HOST_DATA_URL+'?jab=aa&kk='+kk, '_blank');break;
	          case 'non':
	              window.open(HOST_DATA_URL+'?jab=non&kk='+kk, '_blank');break;
          }
        }
			}
		},
		colors: [success, primary, info, warning, danger],
		plotOptions: {
			bar: {
				columnWidth: '55%',
				distributed: true,
				dataLabels: {
					position: 'top',
				},
			}
		},
		dataLabels: {
			enabled: true
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		legend: {
			show: false
		},
		yaxis: {
			title: {
				text: 'dosen'
			}
		},
		xaxis: {
			categories: [
				'GB', 'LK', 'L', 'AA', 'Non'
			],
			labels: {
				style: {
					colors: [success, primary, info, warning, danger],
					fontSize: '12px'
				}
			}
		}
	};

	var chartFungsional = new ApexCharts(document.querySelector("#chart_jabatan"), optionsFungsional);
	chartFungsional.render();

	var urlFungsional = HOST_URL+'/api_jab_fungsional';
	$.getJSON(urlFungsional, function(response) {
		chartFungsional.updateSeries([{data: response.data}]);
		$('#jml-aktif').html(response.data_status['aktif']);
		$('#jml-menjabat').html(response.data_status['menjabat']);
		$('#jml-tubel').html(response.data_status['tubel']);
		$('#jml-pensiun').html(response.data_status['pensiun']);
	});


	var optionsJabPersen = {
		series: [11, 20, 43, 12, 16],
		noData: {
	    text: 'no data'
	  }, 
		chart: {
			height: 250,
			type: 'pie',
		},
		labels: ['GB', 'LK', 'L', 'AA', 'Non'],
		colors: [success, primary, info, warning, danger], 
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartJabPersen = new ApexCharts(document.querySelector("#chart_jabatan_persen"), optionsJabPersen);
	chartJabPersen.render();

	var urlJabPersen = HOST_URL+'/api_jab_fungsional_persen'
	$.getJSON(urlJabPersen, function(response) {
		chartJabPersen.updateSeries(response.data);
	});


	// begin purnabakti
	var optionsPurna = {
		series: [{
			data: []
		}],
		noData: {
			title: 'no data'
		},
		chart: {
			height: 250,
			type: 'bar',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					var kk = $('#sel-kk').val();
					var jab = $('#sel-jabatan-purna').val()
					if(jab =='all'){
						jab = '';
					}

        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	          case '< 2th':
	          		window.open(HOST_DATA_URL+'?pb=2&kk='+kk+'&jab='+jab, '_blank');break;
	          case '2 - 4th':
	              window.open(HOST_DATA_URL+'?pb=24&kk='+kk+'&jab='+jab, '_blank');break;
	          case '4 - 6th':
	              window.open(HOST_DATA_URL+'?pb=46&kk='+kk+'&jab='+jab, '_blank');break;
	          case '> 6th':
	              window.open(HOST_DATA_URL+'?pb=6&kk='+kk+'&jab='+jab, '_blank');break;
          }
        }
			}
		},
		colors: [danger, warning, info, success],
		plotOptions: {
			bar: {
				columnWidth: '55%',
				distributed: true,
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			}
		},
		dataLabels: {
			enabled: true
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		legend: {
			show: false
		},
		yaxis: {
			title: {
				text: 'dosen'
			}
		},
		xaxis: {
			categories: [
				'< 2th', '2 - 4th', '4 - 6th', '> 6th'
			],
			labels: {
				style: {
					colors: [danger, warning, info, success],
					fontSize: '12px'
				}
			}
		}
	};

	var chartPurna = new ApexCharts(document.querySelector("#chart_purnabakti"), optionsPurna);
	chartPurna.render();

	var urlPurna = HOST_URL+'/api_purnabakti';
	$.getJSON(urlPurna, function(response) {
		chartPurna.updateSeries([{data: response.data}]);
	});
	// end purnabakri


	//begin pemenuhan
	var optionsPemenuhan = {
		series: [{
			data: []
		}],
		noData: {
			title: 'no data'
		},
		chart: {
			height: 250,
			type: 'bar',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					var kk=document.getElementById("sel-kk").value;  

        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
	          case 'belum':
	          		window.open(HOST_DATA_URL+'?st=0&kk='+kk, '_blank');break;
	          case 'memenuhi':
	              	window.open(HOST_DATA_URL+'?st=2&kk='+kk, '_blank');break;
	          case 'tpak ftmd':
	              	window.open(HOST_DATA_URL+'?st=3&kk='+kk, '_blank');break;
	          case 'tpak itb':
	              	window.open(HOST_DATA_URL+'?st=5&kk='+kk, '_blank');break;
			  case 'dikti':
					window.open(HOST_DATA_URL+'?st=8&kk='+kk, '_blank');break;
          }
        }
			}
		},
		colors: [danger, warning, primary, info, success],
		plotOptions: {
			bar: {
				columnWidth: '55%',
				distributed: true,
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			}
		},
		dataLabels: {
			enabled: true
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		legend: {
			show: false
		},
		yaxis: {
			title: {
				text: 'dosen'
			}
		},
		xaxis: {
			categories: [
				'Belum', 'Memenuhi', 'TPAKK FTMD', 'TPAK ITB', 'DIKTI'
			],
			labels: {
				style: {
					colors: [danger, warning, primary, info, success],
					fontSize: '12px'
				}
			}
		}
	};

	var chartPemenuhan = new ApexCharts(document.querySelector("#chart_pemenuhan_ak"), optionsPemenuhan);
	chartPemenuhan.render();

	var urlPemenuhan = HOST_URL+'/api_pemenuhan_ak';
	$.getJSON(urlPemenuhan, function(response) {
		chartPemenuhan.updateSeries([{data: response.data}]);
	});
	//end pemenuhan


	//begin lama
	var optionsLamaMenjabat = {
		series: [{
			data: []
		}],
		chart: {
			height: 250,
			type: 'bar',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					var kk = $('#sel-kk').val();
					var jab = $('#sel-jabatan').val();
					if(jab =='all'){
						jab = '';
					}
					switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
						case '< 2th':
								window.open(HOST_DATA_URL+'?lm=1&kk='+kk+'&jab='+jab, '_blank');break;
								// window.open(HOST_DATA_URL+'?lm=1&kk='+kk+'&jab='+jabatan, '_blank');break;
						case '2 - 6th':
							window.open(HOST_DATA_URL+'?lm=13&kk='+kk+'&jab='+jab, '_blank');break;
							// window.open(HOST_DATA_URL+'?lm=13&kk='+kk+'&jab='+jabatan, '_blank');break;
						case '6 - 10th':
							window.open(HOST_DATA_URL+'?lm=35&kk='+kk+'&jab='+jab, '_blank');break;
							// window.open(HOST_DATA_URL+'?lm=35&kk='+kk+'&jab='+jabatan, '_blank');break;
						case '> 10th':
							window.open(HOST_DATA_URL+'?lm=5&kk='+kk+'&jab='+jab, '_blank');break;
							// window.open(HOST_DATA_URL+'?lm=5&kk='+kk+'&jab='+jabatan, '_blank');break;
					}
				}
			}
		},
		colors: [success, info, warning, danger],
		plotOptions: {
			bar: {
				columnWidth: '55%',
				distributed: true,
				dataLabels: {
					position: 'top', // top, center, bottom
				},
			}
		},
		dataLabels: {
			enabled: true
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		legend: {
			show: false
		},
		yaxis: {
			title: {
				text: 'dosen'
			}
		},
		xaxis: {
			categories: [
				'< 2th', '2 - 6th', '6 - 10th', '> 10th'
			],
			labels: {
				style: {
					colors: [success, info, warning, danger],
					fontSize: '12px'
				}
			}
		}
	};

	var chartLamaMenjabat = new ApexCharts(document.querySelector("#chart_lama_menjabat"), optionsLamaMenjabat);
	chartLamaMenjabat.render();

	var urlLamaMenjabat = HOST_URL+'/api_lama_menjabat';
	$.getJSON(urlLamaMenjabat, function(response) {
		chartLamaMenjabat.updateSeries([{data: response.data}]);
	});
	//end lama


	$('#sel-kk').on('change', function() {
		var kk = $(this).val();
		var jab = $('#sel-jabatan').val();

		var urlFungsional = HOST_URL+'/api_jab_fungsional/'+kk;
		$.getJSON(urlFungsional, function(response) {
			chartFungsional.updateSeries([{data: response.data}]);
			chartJabPersen.updateSeries(response.data);
			$('#jml-aktif').html(response.data_status['aktif']);
			$('#jml-menjabat').html(response.data_status['menjabat']);
			$('#jml-tubel').html(response.data_status['tubel']);
			$('#jml-pensiun').html(response.data_status['pensiun']);
		});

		$.getJSON(urlPemenuhan+'/'+kk, function(response) {
			chartPemenuhan.updateSeries([{data: response.data}]);
		});

		$.getJSON(urlPurna+'/'+kk, function(response) {
			chartPurna.updateSeries([{data: response.data}]);
		});

		$.getJSON(urlLamaMenjabat+'/'+kk+'/'+jab, function(response) {
			chartLamaMenjabat.updateSeries([{data: response.data}]);
		});
	});

	$('#sel-jabatan').on('change', function() {
		var kk = $('#sel-kk').val();
		var jab = $(this).val();
		// $("#sel-jabatan-purna option[value="+jab+"]").attr('selected', 'selected');

		$.getJSON(urlLamaMenjabat+'/'+kk+'/'+jab, function(response) {
			chartLamaMenjabat.updateSeries([{data: response.data}]);
		});

		// $.getJSON(urlPurna+'/'+kk+'/'+jab, function(response) {
		// 	chartPurna.updateSeries([{data: response.data}]);
		// });
	});

	$('#sel-jabatan-purna').on('change', function() {
		var kk = $('#sel-kk').val();
		var jab = $(this).val();
		// $("#sel-jabatan option[value="+jab+"]").attr('selected', 'selected');

		$.getJSON(urlPurna+'/'+kk+'/'+jab, function(response) {
			chartPurna.updateSeries([{data: response.data}]);
		});

		// $.getJSON(urlLamaMenjabat+'/'+kk+'/'+jab, function(response) {
		// 	chartLamaMenjabat.updateSeries([{data: response.data}]);
		// });

	});
});