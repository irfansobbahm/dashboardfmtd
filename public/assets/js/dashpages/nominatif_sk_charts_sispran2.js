"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

var KTApexChartsDemo = function () {

	var _sk = function () {
		const apexChart = "#chart_sk";
		var options = {
			series: [{
				name: 'Jumlah SK Bulan Berjalan',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Jumlah SK Diajukan',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Total SK',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 250
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Jumlah SK'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			colors: [info, warning, danger]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _pembayaran = function () {
		const apexChart = "#chart_pembayaran";
		var options = {
			// series: [{
			// 	name: 'SPP',
			// 	data: [0, 30, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Disetujui Ditkeu',
			// 	data: [0, 0, 42, 31, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'BKK',
			// 	data: [0, 0, 42, 31, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [],
				noData: {
			    text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'SK (dokumen)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			colors: [info]
		};

		var chartPembayaran = new ApexCharts(document.querySelector("#chart_pembayaran"), _pembayaran);
		chartPembayaran.render();
	
		var urlPembayaran = HOST_URL+'/api_status_pembayaran/';
		$.getJSON(urlPembayaran, function(response) {
			console.log(response.data.sk)
			chartPembayaran.updateSeries([
				{
					name: 'SK',
					data: response.data.sk
				},
			]);
		});
	}

	var _jenis = function () {
		const apexChart = "#chart_jenis";
		var options = {
			series: [{
				name: 'Pegawai',
				data: [0, 0, 307.917, 194.117, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Jasa',
				data: [0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var jenis = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'pegawai':
			                  	jenis = '01';
			                  	break;
			                case 'jasa':
			                  	jenis = '03';
			                  	break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?sis=01&jns='+jenis+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _penerima = function () {
		const apexChart = "#chart_penerima";
		var options = {
			series: [{
				name: 'Dosen',
				data: [0, 0, 307.917, 194.117, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Tendik',
				data: [0, 0, 0, 0, 0, 0, 800, 0, 0, 0, 0, 0]
			}, {
				name: 'Mahasiswa',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Lainnya',
				data: [0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var kategori = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'dosen':
			                  	kategori = 'dsn';break;
			                case 'tendik':
			                  	kategori = 'tdk';break;
			                case 'mahasiswa':
			                  	kategori = 'mhs';break;
			                case 'lainnya':
			                  	kategori = 'pl';break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?sis=01&kat='+kategori+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning, success, danger]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var options_update_pembayaran = {
		// const apexChart = "#chart_penerima";
		// var options = {
			// series: [{
			// 	name: 'Dosen',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Tendik',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Mahasiswa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Lainnya',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [{
				name: 'Invoice',
				data: []
			}, {
				name: 'Disetujui Ditkeu',
				data: []
			}, {
				name: 'BKK',
				data: []
			}],
			noData: {
			text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var kategori = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'Invoice':
			                  	kategori = 'dsn';break;
			                case 'Diketahui Ditkeu':
			                  	kategori = 'tdk';break;
			                case 'BKK':
			                  	kategori = 'mhs';break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
						orientation: 'vertical',
					},
				},
			},
			labels: ['SK'],
			dataLabels: {
				enabled: true,
				offsetY: 5,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'SK (dokumen)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return val
					}
				}
			},
			colors: [info]
	};

	// var chart = new ApexCharts(document.querySelector(apexChart), options);
	// chart.render();

	var chartPembayaran = new ApexCharts(document.querySelector("#chart_pembayaran"), options_update_pembayaran);
	chartPembayaran.render();

	var urlPembayaran = 'http://dashboard-ftmd.bah/index.php/dashboard/sk/api_status_pembayaran/';
	// console.log(urlPembayaran)
	$.getJSON(urlPembayaran, function(response) {
		// console.log("ada")
		console.log(response.data)
		chartPembayaran.updateSeries([
			{
				name: 'SK',
				data: response.data.sk_sispran2
			},
		]);
	});

	var optionsPenerima = {
		// const apexChart = "#chart_penerima";
		// var options = {
			// series: [{
			// 	name: 'Dosen',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Tendik',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Mahasiswa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Lainnya',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [],
				noData: {
			    text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var kategori = '' ;
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'dosen':
			                  	kategori = 'dosen';break;
			                case 'tendik':
			                  	kategori = 'tendik';break;
			                case 'mahasiswa':
			                  	kategori = 'mhs';break;
			                case 'lainnya':
			                  	kategori =  'null' ;break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
						orientation: 'vertical',
					},
				},
			},
			labels: ['Dosen', 'Tendik', 'Mahasiswa', 'Lainnya'],
			dataLabels: {
				enabled: true,
				offsetY: 5,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning, success, danger]
	};

	var chartPenerima = new ApexCharts(document.querySelector("#chart_penerima"), optionsPenerima);
	chartPenerima.render();

	var urlPenerima = 'http://dashboard-ftmd.bah/index.php/dashboard/sk/api_penerima/';
	// console.log(urlPenerima)
	$.getJSON(urlPenerima, function(response) {
		// console.log(response.data)
		chartPenerima.updateSeries([
			{
				name: 'Dosen',
				data: response.data.dosen_sispran2
			},
			{
				name: 'Tendik',
				data: response.data.tendik_sispran2
			},
			{
				name: 'Mahasiswa',
				data: response.data.mahasiswa
			},
			{
				name: 'Lainnya',
				data: response.data.luar_sispran2
			},
		]);
	});

	var optionsJenis = {
		// const apexChart = "#chart_jenis";
		// var options = {
			// series: [{
			// 	name: 'Pegawai',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Jasa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [],
				noData: {
			    text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var jenis = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'pegawai':
			                  	jenis = '01';
			                  	break;
			                case 'jasa':
			                  	jenis = '03';
			                  	break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			labels: ['Pegawai', 'Jasa'],
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning]
	};

	var chartJenis = new ApexCharts(document.querySelector("#chart_jenis"), optionsJenis);
	chartJenis.render();

	var urlJenis = 'http://dashboard-ftmd.bah/index.php/dashboard/sk/api_jenis_belanja/';
	$.getJSON(urlJenis, function(response) {
		console.log(response.data)
		chartJenis.updateSeries([
			{
		    name: 'Pegawai',
		    data: response.data.pegawai_sispran2
		  },
		//   {
		//     name: 'Jasa',
		//     data: response.data.jasa_sispran1
		//   },
	  ]);
	});

	return {
		// public functions
		init: function () {
			// _sk();
			// _pembayaran();
			// _jenis();
			// _penerima();
		}
	};
}();

jQuery(document).ready(function () {
	KTApexChartsDemo.init();
});