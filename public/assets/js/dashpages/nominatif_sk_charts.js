"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

var KTApexChartsDemo = function () {

	var _sk = function () {
		const apexChart = "#chart_sk";
		var options = {
			series: [{
				name: 'Jumlah SK Bulan Berjalan',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Jumlah SK Diajukan',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Total SK',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 250
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Jumlah SK'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			colors: [info, warning, danger]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	// var _pembayaran = function () {
	// 	const apexChart = "#chart_pembayaran";
	// 	var options = {
	// 		series: [{
	// 			name: 'SPP',
	// 			data: [0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	// 		}, {
	// 			name: 'Disetujui Ditkeu',
	// 			data: [0, 0, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	// 		}, {
	// 			name: 'BKK',
	// 			data: [0, 0, 0, 90, 0, 0, 0, 0, 0, 0, 0, 0]
	// 		}],
	// 		chart: {
	// 			type: 'bar',
	// 			height: 250
	// 		},
	// 		plotOptions: {
	// 			bar: {
	// 				horizontal: false,
	// 				columnWidth: '65%',
	// 				endingShape: 'flat',
	// 				dataLabels: {
	// 					position: 'top', // top, center, bottom
	// 				},
	// 			},
	// 		},
	// 		dataLabels: {
	// 			enabled: true,
	// 			offsetY: -20,
	// 			style: {
	// 				// fontSize: '12px',
	// 				colors: ["#304758"],
	// 			},
	// 			formatter: function (val) {
	// 				if (val != 0)
	// 					return val;
	// 			},
	// 		},
	// 		stroke: {
	// 			show: true,
	// 			width: 2,
	// 			colors: ['transparent']
	// 		},
	// 		yaxis: {
	// 			title: {
	// 				text: 'SK (dokumen)'
	// 			}
	// 		},
	// 		xaxis: {
	// 			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
	// 		},
	// 		fill: {
	// 			opacity: 1
	// 		},
	// 		colors: [info, warning, success]
	// 	};

	// 	var chart = new ApexCharts(document.querySelector(apexChart), options);
	// 	chart.render();
	// }

	var optionsJenis = {
		// const apexChart = "#chart_jenis";
		// var options = {
			// series: [{
			// 	name: 'Pegawai',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Jasa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [],
				noData: {
			    text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var jenis = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'pegawai':
			                  	jenis = '01';
			                  	break;
			                case 'jasa':
			                  	jenis = '03';
			                  	break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?jns='+jenis+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			labels: ['Pegawai', 'Jasa'],
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning]
	};

	var chartJenis = new ApexCharts(document.querySelector("#chart_jenis"), optionsJenis);
	chartJenis.render();

	var urlJenis = HOST_URL+'/api_jenis_belanja/'+thnAktif;
	console.log(urlJenis)
	$.getJSON(urlJenis, function(response) {
		chartJenis.updateSeries([
			{
		    name: 'Pegawai',
		    data: response.data.pegawai
		  },
		  {
		    name: 'Jasa',
		    data: response.data.jasa
		  },
	  ]);
	});


	var optionsPenerima = {
		// const apexChart = "#chart_penerima";
		// var options = {
			// series: [{
			// 	name: 'Dosen',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Tendik',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Mahasiswa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Lainnya',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [],
				noData: {
			    text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var kategori = '' ;
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'dosen':
			                  	kategori = 'dosen';break;
			                case 'tendik':
			                  	kategori = 'tendik';break;
			                case 'mahasiswa':
			                  	kategori = 'mhs';break;
			                case 'lainnya':
			                  	kategori =  'null' ;break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
						orientation: 'vertical',
					},
				},
			},
			labels: ['Dosen', 'Tendik', 'Mahasiswa', 'Lainnya'],
			dataLabels: {
				enabled: true,
				offsetY: 5,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [info, warning, success, danger]
	};

	var chartPenerima = new ApexCharts(document.querySelector("#chart_penerima"), optionsPenerima);
	chartPenerima.render();

	var urlPenerima = HOST_URL+'/api_penerima/'+thnAktif;
	console.log(urlPenerima)
	$.getJSON(urlPenerima, function(response) {
		chartPenerima.updateSeries([
			{
				name: 'Dosen',
				data: response.data.dosen
			},
			{
				name: 'Tendik',
				data: response.data.tendik
			},
			{
				name: 'Mahasiswa',
				data: response.data.mahasiswa
			},
			{
				name: 'Lainnya',
				data: response.data.luar
			},
		]);
	});

	var options_update_pembayaran = {
		// const apexChart = "#chart_penerima";
		// var options = {
			// series: [{
			// 	name: 'Dosen',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Tendik',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Mahasiswa',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }, {
			// 	name: 'Lainnya',
			// 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// }],
			series: [{
				name: 'Invoice',
				data: []
			}, {
				name: 'Disetujui Ditkeu',
				data: []
			}, {
				name: 'BKK',
				data: []
			}],
			noData: {
			text: 'Loading...'
			  }, 
			chart: {
				type: 'bar',
				height: 250,
				events: {
			        dataPointSelection: function(event, chartContext, opts) {
			        	var kategori = '';
			        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
			        		case 'Invoice':
			                  	kategori = 'dsn';break;
			                case 'Diketahui Ditkeu':
			                  	kategori = 'tdk';break;
			                case 'BKK':
			                  	kategori = 'mhs';break;
			        	}

			        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
			              case 'jan':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=1', '_blank');break;
			              case 'feb':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=2', '_blank');break;
			              case 'mar':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=3', '_blank');break;
			              case 'apr':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=4', '_blank');break;
			              case 'mei':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=5', '_blank');break;
			              case 'jun':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=6', '_blank');break;
			              case 'jul':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=7', '_blank');break;
			              case 'ags':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=8', '_blank');break;
			              case 'sep':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=9', '_blank');break;
			              case 'okt':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=10', '_blank');break;
			              case 'nov':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=11', '_blank');break;
			              case 'des':
			                  window.open(HOST_DATA_URL+'?kat='+kategori+'&bln=12', '_blank');break;
			          	}
		        	}
	      		}
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '65%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
						orientation: 'vertical',
					},
				},
			},
			labels: ['SK'],
			dataLabels: {
				enabled: true,
				offsetY: 5,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				title: {
					text: 'SK (dokumen)'
				}
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return val
					}
				}
			},
			colors: [info]
	};
	var chartPembayaran = new ApexCharts(document.querySelector("#chart_pembayaran"), options_update_pembayaran);
	chartPembayaran.render();

	var urlPembayaran = HOST_URL+'/api_status_pembayaran/';
	$.getJSON(urlPembayaran, function(response) {
		console.log(response.data.sk)
		chartPembayaran.updateSeries([
			{
				name: 'SK',
				data: response.data.sk
			},
		]);
	});


	// }

	return {
		// public functions
		init: function () {
			_sk();
			// _pembayaran();
			// _jenis();
			// _penerima();
		}
	};
}();

jQuery(document).ready(function () {
	// KTApexChartsDemo.init();

	// var data_spp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// var data_ditkeu =[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// var data_bkk = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

	// var options_update_pembayaran = {
	// 	series: [{
	// 		name: 'Invoice',
	// 		data: data_spp
	// 	}, {
	// 		name: 'Disetujui Ditkeu',
	// 		data: data_ditkeu
	// 	}, {
	// 		name: 'BKK',
	// 		data: data_bkk
	// 	}],
	// 	chart: {
	// 		type: 'bar',
	// 		height: 250
	// 	},
	// 	plotOptions: {
	// 		bar: {
	// 			horizontal: false,
	// 			columnWidth: '65%',
	// 			endingShape: 'flat',
	// 			dataLabels: {
	// 				position: 'top', // top, center, bottom
	// 			},
	// 		},
	// 	},
	// 	dataLabels: {
	// 		enabled: true,
	// 		offsetY: -20,
	// 		style: {
	// 			// fontSize: '12px',
	// 			colors: ["#304758"],
	// 		},
	// 		formatter: function (val) {
	// 			if (val != 0)
	// 				return val;
	// 		},
	// 	},
	// 	stroke: {
	// 		show: true,
	// 		width: 2,
	// 		colors: ['transparent']
	// 	},
	// 	yaxis: {
	// 		title: {
	// 			text: 'SK (dokumen)'
	// 		}
	// 	},
	// 	xaxis: {
	// 		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
	// 	},
	// 	fill: {
	// 		opacity: 1
	// 	},
	// 	colors: [info, warning, success]
	// };

	// var chart_pembayaran = new ApexCharts(document.querySelector("#chart_pembayaran"), options_update_pembayaran);
	// chart_pembayaran.render();

	// $('#sel-bulan').on('change', function() {
	// 	var bulan = $(this).val();


	// 	if (bulan == '02') {
	// 		data_spp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_ditkeu = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_bkk = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 	} else if (bulan == '03') {
	// 		data_spp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_ditkeu = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_bkk = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 	} else {
	// 		data_spp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_ditkeu = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 		data_bkk = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// 	}
		
	// 	chart_pembayaran.updateSeries([
	// 		{
	// 			name: 'SPP',
	// 			data: data_spp
	// 		},
	// 		{
	// 			name: 'Disetujui Ditkeu',
	// 			data: data_ditkeu
	// 		},
	// 		{
	// 			name: 'BKK',
	// 			data: data_bkk
	// 		},
	// 	]);
		
	// });

	// begin pengajuan SK
	// var optionsSk = {
	// 	series: [{
	// 		name: 'SK Berjalan',
	// 		data: []
	// 	}, {
	// 		name: 'Diajukan',
	// 		data: []
	// 	}, {
	// 		name: 'Total',
	// 		data: []
	// 	}],
	// 	chart: {
	// 		type: 'bar',
	// 		height: 250
	// 	},
	// 	plotOptions: {
	// 		bar: {
	// 			horizontal: false,
	// 			columnWidth: '65%',
	// 			endingShape: 'flat',
	// 			dataLabels: {
	// 				position: 'top', // top, center, bottom
	// 			},
	// 		},
	// 	},
	// 	dataLabels: {
	// 		enabled: true,
	// 		offsetY: -20,
	// 		style: {
	// 			// fontSize: '12px',
	// 			colors: ["#304758"],
	// 		},
	// 		formatter: function (val) {
	// 			if (val != 0)
	// 				return val;
	// 		},
	// 	},
	// 	stroke: {
	// 		show: true,
	// 		width: 2,
	// 		colors: ['transparent']
	// 	},
	// 	yaxis: {
	// 		title: {
	// 			text: 'Jumlah SK'
	// 		}
	// 	},
	// 	xaxis: {
	// 		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
	// 	},
	// 	fill: {
	// 		opacity: 1
	// 	},
	// 	colors: [info, warning, danger]
	// };

	// var chartSk = new ApexCharts(document.querySelector("#chart_sk"), optionsPenyerapanPersen);
	// chartSk.render();

	// var urlSk = HOST_URL+'/api_sk/02';
	// $.getJSON(urlSk, function(response) {
	// 	chartSk.updateSeries(response.data);
	// });
	// end pengajuan sk

	//chat pembayaran new
	// var optionsBulanan = {
	// 	// series: [{
	// 	// 	name: 'Invoice',
	// 	// 	data: []
	// 	// }, {
	// 	// 	name: 'Disetujui Ditkeu',
	// 	// 	data: []
	// 	// }, {
	// 	// 	name: 'BKK',
	// 	// 	data: []
	// 	// }],
	// 	noData: {
	// 		text: 'Loading . . .'
	// 	}, 
	// 	chart: {
	// 		type: 'bar',
	// 		height: 300,
	// 		events: {
    //     dataPointSelection: function(event, chartContext, opts) {
    //     	// var jenis = '';

    //     	// switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
    //     	// 	case 'invoice':
    //     	// 		jenis = '02';break;
    //     	// 	case 'ditkeu':
    //     	// 		jenis = '03';break;
    //     	// 	case 'bkk':
    //     	// 		jenis = '04';break;
    //     	// }

    //     	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
    //           case 'jan':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=1', '_blank');break;
    //           case 'feb':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=2', '_blank');break;
    //           case 'mar':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=3', '_blank');break;
    //           case 'apr':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=4', '_blank');break;
    //           case 'mei':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=5', '_blank');break;
    //           case 'jun':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=6', '_blank');break;
    //           case 'jul':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=7', '_blank');break;
    //           case 'ags':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=8', '_blank');break;
    //           case 'sep':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=9', '_blank');break;
    //           case 'okt':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=10', '_blank');break;
    //           case 'nov':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=11', '_blank');break;
    //           case 'des':
    //               window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=12', '_blank');break;
    //       }
    //     }
    //   }
	// 	},
	// 	plotOptions: {
	// 		bar: {
	// 			horizontal: false,
	// 			columnWidth: '55%',
	// 			endingShape: 'flat',
	// 			dataLabels: {
	// 				position: 'top', // top, center, bottom
	// 				orientation: 'vertical',
	// 			},
	// 		},
	// 	},
	// 	labels: ['Invoice', 'Diketahui Ditkeu', 'BKK'],
	// 	dataLabels: {
	// 		enabled: true,
	// 		offsetY: 5,
	// 		style: {
	// 			// fontSize: '12px',
	// 			colors: ["#304758"],
	// 		},
	// 		formatter: function (val) {
	// 			if (val != 0)
	// 				return val.toLocaleString('id-ID');
	// 		},
	// 	},
	// 	stroke: {
	// 		show: true,
	// 		width: 2,
	// 		colors: ['transparent']
	// 	},
	// 	xaxis: {
	// 		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
	// 	},
	// 	yaxis: {
	// 		title: {
	// 			text: 'SK (dokumen)'
	// 		}
	// 	},
	// 	fill: {
	// 		opacity: 1
	// 	},
	// 	tooltip: {
	// 		y: {
	// 			formatter: function (val) {
					
	// 			}
	// 		}
	// 	},
	// 	colors: [primary, warning, success]
	// };

	// var chartBulanan = new ApexCharts(document.querySelector("#chart_pembayaran"), optionsBulanan);
	// chartBulanan.render();

	// var urlBulanan = HOST_URL+'/api_status_pembayaran';
	// $.getJSON(urlBulanan, function(response) {
	// 	console.log(response.data)
	// 	chartBulanan.updateSeries([
	// 		// {
	// 		// 	data: response.data
	// 		// },
	// 	]);
	// });

});