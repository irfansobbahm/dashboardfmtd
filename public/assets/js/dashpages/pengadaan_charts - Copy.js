"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';
const grey = '#F0F0AB';

// Class definition
var KTApexChartsDemo = function () {
	// Private functions

	var _status = function () {
		const apexChart = "#chart_status";
		var options = {
			series: [0, 0, 0, 0, 0, 0],
			dataLabels: {
				formatter: function (val, opts) {
					return opts.w.config.series[opts.seriesIndex]
				},
			},
			chart: {
				width: 380,
				type: 'pie',
			},
			labels: ['Baru', 'Penawaran', 'SPK', 'Serah Terima', 'Invoice', 'Selesai'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200,
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [danger, warning, grey, primary, info, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _status_itb = function () {
		const apexChart = "#chart_status_itb";
		var options = {
			series: [0, 0, 0],
			dataLabels: {
				formatter: function (val, opts) {
					return opts.w.config.series[opts.seriesIndex]
				},
			},
			chart: {
				width: 380,
				type: 'pie',
			},
			labels: ['Baru', 'Diproses ITB', 'Selesai'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200,
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [danger, info, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _deadline = function () {
		const apexChart = "#chart_deadline";
		var options = {
			series: [0, 0, 0, 0],
			dataLabels: {
				formatter: function (val, opts) {
					return opts.w.config.series[opts.seriesIndex]
				},
			},
			chart: {
				width: 380,
				type: 'pie',
			},
			labels: ['Lewat Batas', '< 2 hari', '2-7 hari', '> 7 hari'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200,
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [danger, warning, info, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _jenis_belanja = function () {
		const apexChart = "#chart_jenis_belanja";
		var options = {
			series: [{
				data: [0, 0, 0]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [primary, warning, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true
				}
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			xaxis: {
				categories: [
					'Barang Habis', 'Jasa', 'Modal'
				],
				labels: {
					style: {
						colors: [primary, warning, success],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _jenis = function () {
		const apexChart = "#chart_jenis";
		var options = {
			series: [{
				data: [0, 0, 0, 0, 0]
			}],
			chart: {
				height: 250,
				type: 'bar',
				events: {
					click: function (chart, w, e) {
						// console.log(chart, w, e)
					}
				}
			},
			colors: [primary, warning, info, success],
			plotOptions: {
				bar: {
					columnWidth: '55%',
					distributed: true
				}
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			legend: {
				show: false
			},
			xaxis: {
				categories: [
					'KO', 'P3MI', 'KA', 'KNA', 'Inter'
				],
				labels: {
					style: {
						colors: [primary, warning, danger, info, success, grey],
						fontSize: '12px'
					}
				}
			}
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _pengadaan_bulanan = function () {
		const apexChart = "#chart_pengadaan_bulanan";
		var options = {
			series: [{
				name: 'Barang Habis',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Jasa',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}, {
				name: 'Modal',
				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			}],
			chart: {
				type: 'bar',
				height: 300
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'flat',
					dataLabels: {
						position: 'top', // top, center, bottom
					},
				},
			},
			dataLabels: {
				enabled: true,
				offsetY: -20,
				style: {
					// fontSize: '12px',
					colors: ["#304758"],
				},
				formatter: function (val) {
					if (val != 0)
						return val;
				},
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
			},
			yaxis: {
				title: {
					text: 'Rp (juta)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Rp " + val + " juta"
					}
				}
			},
			colors: [primary, warning, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _aktual = function () {
		const apexChart = "#chart_aktual";
		var options = {
			series: [0, 0, 0, 0],
			dataLabels: {
				formatter: function (val, opts) {
					return opts.w.config.series[opts.seriesIndex]
				},
			},
			chart: {
				width: 380,
				type: 'pie',
			},
			labels: ['< 50%', '50 - 75%', '> 75%', 'Selesai'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200,
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [danger, warning, info, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	return {
		// public functions
		init: function () {
			_status();
			_status_itb();
			_deadline();
			_jenis_belanja();
			_jenis();
			_pengadaan_bulanan();
			_aktual();
		}
	};
}();

jQuery(document).ready(function () {
	KTApexChartsDemo.init();

	var showActual = false;
	$('#btn-pengadaan-aktual').on('click', function () {
		if (showActual == false) {
			$('#area-pengadaan-aktual').show();
			showActual = true;
		} else {
			$('#area-pengadaan-aktual').hide();
			showActual = false;
		}
	})
});