"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';
const grey = '#F0F0AB';

// // Class definition
// var KTApexChartsDemo = function () {
// 	// Private functions

// 	var _status = function () {
// 		const apexChart = "#chart_status";
// 		var options = {
// 			series: [0, 0, 0, 0, 0, 1],
// 			dataLabels: {
// 				formatter: function (val, opts) {
// 					return opts.w.config.series[opts.seriesIndex]
// 				},
// 			},
// 			chart: {
// 				width: 380,
// 				type: 'pie',
// 			},
// 			labels: ['Baru', 'Penawaran', 'SPK', 'Serah Terima', 'Invoice', 'Selesai'],
// 			responsive: [{
// 				breakpoint: 480,
// 				options: {
// 					chart: {
// 						width: 200,
// 					},
// 					legend: {
// 						position: 'bottom'
// 					}
// 				}
// 			}],
// 			colors: [danger, warning, grey, primary, info, success]
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _status_itb = function () {
// 		const apexChart = "#chart_status_itb";
// 		var options = {
// 			series: [0, 0, 0],
// 			dataLabels: {
// 				formatter: function (val, opts) {
// 					return opts.w.config.series[opts.seriesIndex]
// 				},
// 			},
// 			chart: {
// 				width: 380,
// 				type: 'pie',
// 			},
// 			labels: ['Baru', 'Diproses ITB', 'Selesai'],
// 			responsive: [{
// 				breakpoint: 480,
// 				options: {
// 					chart: {
// 						width: 200,
// 					},
// 					legend: {
// 						position: 'bottom'
// 					}
// 				}
// 			}],
// 			colors: [danger, info, success]
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _deadline = function () {
// 		const apexChart = "#chart_deadline";
// 		var options = {
// 			series: [0, 0, 0, 0],
// 			dataLabels: {
// 				formatter: function (val, opts) {
// 					return opts.w.config.series[opts.seriesIndex]
// 				},
// 			},
// 			chart: {
// 				width: 380,
// 				type: 'pie',
// 			},
// 			labels: ['Lewat Batas', '< 2 hari', '2-7 hari', '> 7 hari'],
// 			responsive: [{
// 				breakpoint: 480,
// 				options: {
// 					chart: {
// 						width: 200,
// 					},
// 					legend: {
// 						position: 'bottom'
// 					}
// 				}
// 			}],
// 			colors: [danger, warning, info, success]
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _jenis_belanja = function () {
// 		const apexChart = "#chart_jenis_belanja";
// 		var options = {
// 			series: [{
// 				data: [0, 0, 0]
// 			}],
// 			chart: {
// 				height: 250,
// 				type: 'bar',
// 				events: {
// 					click: function (chart, w, e) {
// 						// console.log(chart, w, e)
// 					}
// 				}
// 			},
// 			colors: [primary, warning, success],
// 			plotOptions: {
// 				bar: {
// 					columnWidth: '55%',
// 					distributed: true
// 				}
// 			},
// 			dataLabels: {
// 				enabled: false
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			legend: {
// 				show: false
// 			},
// 			xaxis: {
// 				categories: [
// 					'Barang Habis', 'Jasa', 'Modal'
// 				],
// 				labels: {
// 					style: {
// 						colors: [primary, warning, success],
// 						fontSize: '12px'
// 					}
// 				}
// 			}
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _jenis = function () {
// 		const apexChart = "#chart_jenis";
// 		var options = {
// 			series: [{
// 				data: [0, 0, 0, 0, 0]
// 			}],
// 			chart: {
// 				height: 250,
// 				type: 'bar',
// 				events: {
// 					click: function (chart, w, e) {
// 						// console.log(chart, w, e)
// 					}
// 				}
// 			},
// 			colors: [primary, warning, info, success],
// 			plotOptions: {
// 				bar: {
// 					columnWidth: '55%',
// 					distributed: true
// 				}
// 			},
// 			dataLabels: {
// 				enabled: false
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			legend: {
// 				show: false
// 			},
// 			xaxis: {
// 				categories: [
// 					'KO', 'P3MI', 'KA', 'KNA', 'Inter'
// 				],
// 				labels: {
// 					style: {
// 						colors: [primary, warning, danger, info, success, grey],
// 						fontSize: '12px'
// 					}
// 				}
// 			}
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _pengadaan_bulanan = function () {
// 		const apexChart = "#chart_pengadaan_bulanan";
// 		var options = {
// 			series: [{
// 				name: 'Barang Habis',
// 				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
// 			}, {
// 				name: 'Jasa',
// 				data: [33.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
// 			}, {
// 				name: 'Modal',
// 				data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
// 			}],
// 			chart: {
// 				type: 'bar',
// 				height: 300
// 			},
// 			plotOptions: {
// 				bar: {
// 					horizontal: false,
// 					columnWidth: '55%',
// 					endingShape: 'flat',
// 					dataLabels: {
// 						position: 'top', // top, center, bottom
// 					},
// 				},
// 			},
// 			dataLabels: {
// 				enabled: true,
// 				offsetY: -20,
// 				style: {
// 					// fontSize: '12px',
// 					colors: ["#304758"],
// 				},
// 				formatter: function (val) {
// 					if (val != 0)
// 						return val;
// 				},
// 			},
// 			stroke: {
// 				show: true,
// 				width: 2,
// 				colors: ['transparent']
// 			},
// 			xaxis: {
// 				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
// 			},
// 			yaxis: {
// 				title: {
// 					text: 'Rp (juta)'
// 				}
// 			},
// 			fill: {
// 				opacity: 1
// 			},
// 			tooltip: {
// 				y: {
// 					formatter: function (val) {
// 						return "Rp " + val + " juta"
// 					}
// 				}
// 			},
// 			colors: [primary, warning, success]
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	var _aktual = function () {
// 		const apexChart = "#chart_aktual";
// 		var options = {
// 			series: [0, 0, 0, 1],
// 			dataLabels: {
// 				formatter: function (val, opts) {
// 					return opts.w.config.series[opts.seriesIndex]
// 				},
// 			},
// 			chart: {
// 				width: 380,
// 				type: 'pie',
// 			},
// 			labels: ['< 50%', '50 - 75%', '> 75%', 'Selesai'],
// 			responsive: [{
// 				breakpoint: 480,
// 				options: {
// 					chart: {
// 						width: 200,
// 					},
// 					legend: {
// 						position: 'bottom'
// 					}
// 				}
// 			}],
// 			colors: [danger, warning, info, success]
// 		};

// 		var chart = new ApexCharts(document.querySelector(apexChart), options);
// 		chart.render();
// 	}

// 	return {
// 		// public functions
// 		init: function () {
// 			_status();
// 			_status_itb();
// 			_deadline();
// 			_jenis_belanja();
// 			_jenis();
// 			_pengadaan_bulanan();
// 			_aktual();
// 		}
// 	};
// }();

jQuery(document).ready(function () {
	// KTApexChartsDemo.init();

	// var optionsStatus = {
	// 	// series: [0, 0, 0, 0, 0, 1],
	// 	series: [],
	// 	noData: {
	//     text: 'no data'
	//   }, 
	// 	dataLabels: {
	// 		formatter: function (val, opts) {
	// 			return opts.w.config.series[opts.seriesIndex]
	// 		},
	// 	},
	// 	chart: {
	// 		width: 380,
	// 		type: 'pie',
	// 		events: {
 //        dataPointSelection: function(event, chartContext, opts) {
 //        	console.log(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase());
 //        	// var jenis = (opts.w.config.series[opts.seriesIndex].name.toLowerCase() == 'fra') ? '03' : '04';

 //        	// switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
 //         //      case 'jan':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=1', '_blank');
 //         //      case 'feb':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=2', '_blank');
 //         //      case 'mar':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=3', '_blank');
 //         //      case 'apr':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=4', '_blank');
 //         //      case 'mei':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=5', '_blank');
 //         //      case 'jun':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=6', '_blank');
 //         //      case 'jul':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=7', '_blank');
 //         //      case 'ags':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=8', '_blank');
 //         //      case 'sep':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=9', '_blank');
 //         //      case 'okt':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=10', '_blank');
 //         //      case 'nov':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=11', '_blank');
 //         //      case 'des':
 //         //          window.open(HOST_DATA_URL+'?jns='+jenis+'&ak=12', '_blank');
 //         //  }
 //        }
 //      }
	// 	},
	// 	labels: ['Baru', 'Penawaran', 'SPK', 'Serah Terima', 'Invoice', 'Selesai'],
	// 	responsive: [{
	// 		breakpoint: 480,
	// 		options: {
	// 			chart: {
	// 				width: 200,
	// 			},
	// 			legend: {
	// 				position: 'bottom'
	// 			}
	// 		}
	// 	}],
	// 	colors: [danger, warning, grey, primary, info, success]
	// };

	var optionsStatus = {
		series: [],
		noData: {
	    text: 'no data'
	  }, 
	  dataLabels: {
			formatter: function (val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		chart: {
			height: 350,
			type: 'pie',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					console.log(opts.w.config.labels[opts.dataPointIndex]);
        	switch(opts.w.config.labels[opts.dataPointIndex]) {
	          case 'Pemaketan':
	          		window.open(HOST_DATA_URL+'?st=0&ol=FTMD&th='+thn_aktif, '_blank');break;
	          case 'Kontrak':
	              window.open(HOST_DATA_URL+'?st=1&ol=FTMD&th='+thn_aktif, '_blank');break;
	          case 'Invoice':
	              window.open(HOST_DATA_URL+'?st=2&ol=FTMD&th='+thn_aktif, '_blank');break;
	          case 'Selesai':
	              window.open(HOST_DATA_URL+'?st=3&ol=FTMD&th='+thn_aktif, '_blank');break;

	          // case 'Penawaran':
	          //     window.open(HOST_DATA_URL+'?st=2&ol=FTMD&th='+thn_aktif, '_blank');
	          // case 'SPK':
	          //     window.open(HOST_DATA_URL+'?st=3&ol=FTMD&th='+thn_aktif, '_blank');
	          // case 'BAST':
	          //     window.open(HOST_DATA_URL+'?st=4&ol=FTMD&th='+thn_aktif, '_blank');
	          // case 'Invoice':
	          //     window.open(HOST_DATA_URL+'?st=5&ol=FTMD&th='+thn_aktif, '_blank');
	          // case 'Selesai':
	          //     window.open(HOST_DATA_URL+'?st=6&ol=FTMD&th='+thn_aktif, '_blank');
          }
        }
			}
		},
		// labels: ['Pemaketan Baru', 'Penawaran', 'SPK', 'BAST', 'Invoice', 'Selesai'],
		labels: ['Pemaketan', 'Kontrak', 'Invoice', 'Selesai'],
		// colors: [danger, warning, grey, primary, info, success], 
		colors: [warning, info, primary, success], 
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartStatus = new ApexCharts(document.querySelector("#chart_status"), optionsStatus);
	chartStatus.render();

	var urlStatus = HOST_URL+'/api_status/'+thn_aktif;
	$.getJSON(urlStatus, function(response) {
		chartStatus.updateSeries(response.data);
	});


	var optionsStatusItb = {
		series: [],
		noData: {
	    text: 'no data'
	  }, 
	  dataLabels: {
			formatter: function (val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		chart: {
			height: 350,
			type: 'pie',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					switch(opts.w.config.labels[opts.dataPointIndex]) {
	          case 'Pemaketan':
	          		window.open(HOST_DATA_URL+'?st=0&ol=Logistik&th='+thn_aktif, '_blank');break;
	          // case 'Diproses':
	          //     window.open(HOST_DATA_URL+'?st=1&ol=ITB&th='+thn_aktif, '_blank');
	          case 'Selesai':
	              window.open(HOST_DATA_URL+'?st=3&ol=Logistik&th='+thn_aktif, '_blank');break;
          }
        }
			}
		},
		// labels: ['Pemaketan Baru', 'Diproses ITB', 'BAST'],
		// colors: [danger, info, success], 
		labels: ['Pemaketan', 'Selesai'],
		colors: [warning, success], 
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartStatusItb = new ApexCharts(document.querySelector("#chart_status_itb"), optionsStatusItb);
	chartStatusItb.render();

	var urlStatusItb = HOST_URL+'/api_status_itb/'+thn_aktif;
	$.getJSON(urlStatusItb, function(response) {
		chartStatusItb.updateSeries(response.data);
	});

	var optionsDeadline = {
		series: [],
		noData: {
	    text: 'no data'
	  }, 
	  dataLabels: {
			formatter: function (val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		chart: {
			height: 350,
			type: 'pie',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					switch(opts.w.config.labels[opts.dataPointIndex]) {
	          case '> 7 hari':
	              window.open(HOST_DATA_URL+'?de=1&th='+thn_aktif, '_blank');break;
	          case '3-7 hari':
	              window.open(HOST_DATA_URL+'?de=2&th='+thn_aktif, '_blank');break;
	          case '< 3 hari':
	              window.open(HOST_DATA_URL+'?de=3&th='+thn_aktif, '_blank');break;
	          case 'Lewat Batas':
	          		window.open(HOST_DATA_URL+'?de=4&th='+thn_aktif, '_blank');break;
	          	          
	          
          }
        }
			}
		},
		labels: ['> 7 hari', '3-7 hari', '< 3 hari', 'Lewat Batas'],
		colors: [success, info, warning, danger], 
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartDeadline = new ApexCharts(document.querySelector("#chart_deadline"), optionsDeadline);
	chartDeadline.render();

	var urlDeadline = HOST_URL+'/api_deadline/'+thn_aktif;
	$.getJSON(urlDeadline, function(response) {
		chartDeadline.updateSeries(response.data);
	});


	var optionsAktual = {
		series: [],
		noData: {
	    text: 'no data'
	  }, 
	  dataLabels: {
			formatter: function (val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		chart: {
			height: 350,
			type: 'pie',
			events: {
				dataPointSelection: function(event, chartContext, opts) {
					switch(opts.w.config.labels[opts.dataPointIndex]) {
	          case 'Selesai':
	          		window.open(HOST_DATA_URL+'?ol=FTMD&ak=4&th='+thn_aktif, '_blank');break;
	          case '> 75%':
	              window.open(HOST_DATA_URL+'?ol=FTMD&ak=3&th='+thn_aktif, '_blank');break;
	          case '50 - 75%':
	              window.open(HOST_DATA_URL+'?ol=FTMD&ak=2&th='+thn_aktif, '_blank');break;
	          case '< 50%':
	              window.open(HOST_DATA_URL+'?ol=FTMD&ak=1&th='+thn_aktif, '_blank');break;
          }
        }
			}
		},
		labels: ['< 50%', '50 - 75%', '> 75%', 'Selesai'],
		colors: [danger, warning, info, success], 
		legend: {
			position: 'bottom',
			show: true,
		}
	};

	var chartAktual = new ApexCharts(document.querySelector("#chart_aktual"), optionsAktual);
	chartAktual.render();

	var urlAktual = HOST_URL+'/api_aktual/'+thn_aktif;
	$.getJSON(urlAktual, function(response) {
		chartAktual.updateSeries(response.data);
	});


	var optionsBulanan = {
		series: [{
			name: 'Barang',
			data: []
		}, {
			name: 'Jasa',
			data: []
		}, {
			name: 'Modal',
			data: []
		}],
		noData: {
			text: 'no data'
		}, 
		chart: {
			type: 'bar',
			height: 300,
			events: {
        dataPointSelection: function(event, chartContext, opts) {
        	var jenis = '';

        	switch(opts.w.config.series[opts.seriesIndex].name.toLowerCase()) {
        		case 'barang':
        			jenis = '02';break;
        		case 'jasa':
        			jenis = '03';break;
        		case 'modal':
        			jenis = '04';break;
        	}

        	switch(opts.w.config.xaxis.categories[opts.dataPointIndex].toLowerCase()) {
              case 'jan':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=1', '_blank');break;
              case 'feb':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=2', '_blank');break;
              case 'mar':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=3', '_blank');break;
              case 'apr':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=4', '_blank');break;
              case 'mei':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=5', '_blank');break;
              case 'jun':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=6', '_blank');break;
              case 'jul':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=7', '_blank');break;
              case 'ags':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=8', '_blank');break;
              case 'sep':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=9', '_blank');break;
              case 'okt':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=10', '_blank');break;
              case 'nov':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=11', '_blank');break;
              case 'des':
                  window.open(HOST_DATA_URL+'?jns='+jenis+'&th='+thn_aktif+'&bl=12', '_blank');break;
          }
        }
      }
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'flat',
				dataLabels: {
					position: 'top', // top, center, bottom
					orientation: 'vertical',
				},
			},
		},
		dataLabels: {
			enabled: true,
			offsetY: 5,
			style: {
				// fontSize: '12px',
				colors: ["#304758"],
			},
			formatter: function (val) {
				if (val != 0)
					return val.toLocaleString('id-ID');
			},
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
		},
		yaxis: {
			title: {
				text: 'Rp (juta)'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function (val) {
					return "Rp " + val.toLocaleString('id-ID') + " juta"
				}
			}
		},
		colors: [primary, warning, success]
	};

	var chartBulanan = new ApexCharts(document.querySelector("#chart_pengadaan_bulanan"), optionsBulanan);
	chartBulanan.render();

	var urlBulanan = HOST_URL+'/api_bulanan/'+thn_aktif+'/all';
	$.getJSON(urlBulanan, function(response) {
		chartBulanan.updateSeries([
			{
				name: 'Barang',
				data: response.data.barang
			},
			{
				name: 'Jasa',
				data: response.data.jasa
			},
			{
				name: 'Modal',
				data: response.data.modal
			},
		]);
	});

	var showActual = false;
	$('#btn-pengadaan-aktual').on('click', function () {
		if (showActual == false) {
			$('#area-pengadaan-aktual').show();
			showActual = true;
		} else {
			$('#area-pengadaan-aktual').hide();
			showActual = false;
		}
	});

	$('#sel-diadakan').on('change', function() {
		var diadakan = $(this).val();	

		var urlBulanan = HOST_URL+'/api_bulanan/'+thn_aktif+'/'+diadakan;
		$.getJSON(urlBulanan, function(response) {
			chartBulanan.updateSeries([
				{
					name: 'Barang',
					data: response.data.barang
				},
				{
					name: 'Jasa',
					data: response.data.jasa
				},
				{
					name: 'Modal',
					data: response.data.modal
				},
			]);
		});
		
	});
});